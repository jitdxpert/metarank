<?php

use Facebook\GraphUser;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;

require_once(dirname(__FILE__) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	if (isset($_GET['code'])) {
		if (isset($_GET["state"]) && strrpos($_GET["state"], "linkedIn_", -strlen($_GET["state"])) !== FALSE) {
			require_once(BASE_PATH . "/../phpinc/lib/li/linkedin.php");
			$accessToken = $linkedinSession->processRedirect();
			if ($accessToken == null) {
				//TODO: Handle this failure
				$_SESSION["googleAnalyticsAction"] = "LinkedIn Login Failure";
			} else {
				// Logged in by LinkedIn
				$_SESSION["linkedInOAuthToken"] = $accessToken;
				$accessTokenExpiration = new DateTime();
				$accessTokenExpiration->setTimestamp(time() + $linkedinSession->getAccessTokenExpiration());
				
				$information = $linkedinSession->get("/people/~:(email-address,last-modified-timestamp,id,first-name,last-name,maiden-name,formatted-name,headline,location:(name),industry,current-share,num-connections,num-connections-capped,summary,specialties,positions,picture-url,site-standard-profile-request,api-standard-profile-request:(headers),public-profile-url,phone-numbers,bound-account-types,im-accounts,main-address,twitter-accounts,primary-twitter-account,connections,group-memberships,proposal-comments,associations,honors,interests,publications,patents,languages,skills,certifications,educations,courses,volunteer,three-current-positions,three-past-positions,num-recommenders,recommendations-received,mfeed-rss-url,following,job-bookmarks,suggestions,date-of-birth,member-url-resources,related-profile-views,network)");
				
				$_SESSION["linkedInUniqueID"] = $information["id"];
				
				$email = $metaRankDatabase->getLinkedInUserEmail($_SESSION["linkedInUniqueID"]);
				if ($email == null) {
					// We don't have an account for this LinkedIn user yet, so create one
					$_SESSION["linkedInOAuthTokenExpiration"] = time() + $linkedinSession->getAccessTokenExpiration();
					$_SESSION["linkedInRegistrationInformation"] = $information;
					
					$service = "LinkedIn";
					$registrationInformation = array(
						"first_name" => $information["firstName"],
						"last_name" => $information["lastName"],
						"date_of_birth" => (isset($information["dateOfBirth"]) ? $information["dateOfBirth"]["year"] . "-" . str_pad($information["dateOfBirth"]["month"], 2, "0", STR_PAD_LEFT) . "-" . str_pad($information["dateOfBirth"]["day"], 2, "0", STR_PAD_LEFT) : ''),
						"gender" => "",
						"phone" => "",
						"email" => $information["emailAddress"]
					);
					
					$pageTitle = "MetaRank - Register with LinkedIn";
					if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
						require_once(BASE_PATH . "/../phpinc/header.php");
					}
					
					include (BASE_PATH . "/../phpinc/register.php");
					
					if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
						require_once(BASE_PATH . "/../phpinc/footer.php");
					}
					
					exit();
				} else {
					$userLocked = $metaRankDatabase->checkUserIsLocked($email);
					if ($userLocked["locked"]) {
						if ($userLocked["modification_reason"] == "") {
							$message = "This username is locked. If you have used the Forgot Password link, complete the instructions in the email we sent to reset your password and unlock your account.";
						} else {
							$message = "This username is locked. Reason: " . $userLocked["modification_reason"];
						}
						
						include(BASE_PATH . "/../phpinc/errors/generic.php");
						
						exit();
					} else {
						$metaRankDatabase->logSuccessfulLogin($email);

						session_regenerate_id(true);

						$metaRankDatabase->loadSessionUserInformation($email);

						$metaRankDatabase->updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"));
						
						if (isset($_SESSION["locationPostalCode"])) {
							$metaRankDatabase->updateUserLocation($_SESSION["userID"], $_SESSION["locationCountry"], $_SESSION["locationAdministrativeArea"], $_SESSION["locationLocality"], $_SESSION["locationPostalCode"], $_SESSION["locationLatitude"], $_SESSION["locationLongitude"]);
							$metaRankDatabase->scoreMetaRankBonus($_SESSION["userID"]);
						}
						unset($_SESSION["locationCountry"]);
						unset($_SESSION["locationAdministrativeArea"]);
						unset($_SESSION["locationLocality"]);
						unset($_SESSION["locationPostalCode"]);
						unset($_SESSION["locationLatitude"]);
						unset($_SESSION["locationLongitude"]);
						
						$_SESSION["isLoggedIn"] = true;
						
						$metaRankDatabase->updateLinkedInAccessToken($_SESSION["userID"], $_SESSION["linkedInOAuthToken"], $accessTokenExpiration);
						$_SESSION["linkedInOAuthTokenExpiration"] = time() + $linkedinSession->getAccessTokenExpiration();
						
						$metaRankDatabase->scoreLinkedIn($_SESSION["userID"], $information);
						$metaRankDatabase->updateMetaScore($_SESSION["userID"]);
								
						$metaRankDatabase->rankGlobalLinkedIn($_SESSION["userID"]);
						$metaRankDatabase->computeGlobalLinkedInParticipants();
						
						$metaRankDatabase->rankGlobal($_SESSION["userID"]);
						$metaRankDatabase->computeGlobalParticipants();
						
						$metaRankDatabase->commitChanges();
						
						$_SESSION["googleAnalyticsAction"] = "LinkedIn Login";
						
						header('Location: ' . $_SESSION["redirectURL"], true, 303);
						exit();
					}
				}
			}
		} 
		else if (!isset($_SESSION["facebookAccessToken"])) {
			require_once(BASE_PATH . "/../phpinc/lib/fb/facebook.php");
			
			$fbLoginHelper = new FacebookRedirectLoginHelper(curHostURL() . "/metarank/htdocs/");
			try {
				$facebookSession = $fbLoginHelper->getSessionFromRedirect(curHostURL() . "/metarank/htdocs/");
			} catch(FacebookRequestException $ex) {
				// When Facebook returns an error
			} catch(\Exception $ex) {
				// When validation fails or other local issues
			}
			
			if (isset($facebookSession) && $facebookSession) {
				// Logged in by Facebook
				// Get the GraphUser object for the current user:
				try {
					$fbUserProfile = (new FacebookRequest($facebookSession, 'GET', '/me'))->execute()->getGraphObject(GraphUser::className());
					$_SESSION["facebookUniqueID"] = $fbUserProfile->getId();
				} catch (FacebookRequestException $e) {
					// The Graph API returned an error
				} catch (\Exception $e) {
					// Some other error occurred
				}

				$accessToken = $facebookSession->getAccessToken()->extend(FACEBOOK_APP_ID, FACEBOOK_SECRET);
				$_SESSION["facebookAccessToken"] = $accessToken->__toString();
				
				
				$email = $metaRankDatabase->getFacebookUserEmail($_SESSION["facebookUniqueID"]);
				
				if ($email == null) {
					// We don't have an account for this Facebook user yet, so create one
					$_SESSION["facebookAccessTokenExpiration"] = $accessToken->getExpiresAt()->getTimestamp();
					
					$email = $fbUserProfile->getProperty("email");
					$gender = $fbUserProfile->getProperty("gender");
					$birthday = $fbUserProfile->getBirthday();
					$service = "Facebook";
					$registrationInformation = array(
						"first_name" => $fbUserProfile->getFirstName(),
						"last_name" => $fbUserProfile->getLastName(),
						"date_of_birth" => ($birthday !== null ? $birthday->format("Y-m-d") : ""),
						"gender" => ($gender == "male" ? "M" : ($gender == "female" ? "F" : "")),
						"phone" => "",
						"email" => ($email != null ? $email : "")
					);

					
					$pageTitle = "MetaRank - Register with Facebook";
					if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
						require_once(BASE_PATH . "/../phpinc/header.php");
					}
					
					include (BASE_PATH . "/../phpinc/register.php");
					
					if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
						require_once(BASE_PATH . "/../phpinc/footer.php");
					}
					
					exit();
				} else {
					$userLocked = $metaRankDatabase->checkUserIsLocked($email);
					if ($userLocked["locked"]) {
						if ($userLocked["modification_reason"] == "") {
							$message = "This username is locked. If you have used the Forgot Password link, complete the instructions in the email we sent to reset your password and unlock your account.";
						} else {
							$message = "This username is locked. Reason: " . $userLocked["modification_reason"];
						}
						
						include(BASE_PATH . "/../phpinc/errors/generic.php");
						
						exit();
					} else {
						$metaRankDatabase->logSuccessfulLogin($email);

						session_regenerate_id(true);

						$metaRankDatabase->loadSessionUserInformation($email);

						$metaRankDatabase->updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"));
						
						if (isset($_SESSION["locationPostalCode"])) {
							$metaRankDatabase->updateUserLocation($_SESSION["userID"], $_SESSION["locationCountry"], $_SESSION["locationAdministrativeArea"], $_SESSION["locationLocality"], $_SESSION["locationPostalCode"], $_SESSION["locationLatitude"], $_SESSION["locationLongitude"]);
							$metaRankDatabase->scoreMetaRankBonus($_SESSION["userID"]);
						}
						unset($_SESSION["locationCountry"]);
						unset($_SESSION["locationAdministrativeArea"]);
						unset($_SESSION["locationLocality"]);
						unset($_SESSION["locationPostalCode"]);
						unset($_SESSION["locationLatitude"]);
						unset($_SESSION["locationLongitude"]);
						
						$_SESSION["isLoggedIn"] = true;
						
						$metaRankDatabase->updateFacebookAccessToken($_SESSION["userID"], $_SESSION["facebookAccessToken"], $accessToken->getExpiresAt());
						$_SESSION["facebookAccessTokenExpiration"] = $accessToken->getExpiresAt()->getTimestamp();
						
						$metaRankDatabase->scoreFacebook($_SESSION["userID"], $_SESSION["facebookAccessToken"]);
						$metaRankDatabase->updateMetaScore($_SESSION["userID"]);
						
						$metaRankDatabase->rankGlobalFacebook($_SESSION["userID"]);
						$metaRankDatabase->computeGlobalFacebookParticipants();
						
						$metaRankDatabase->rankGlobal($_SESSION["userID"]);
						$metaRankDatabase->computeGlobalParticipants();
						
						$metaRankDatabase->commitChanges();
						
						$_SESSION["googleAnalyticsAction"] = "Facebook Login";
						
						header('Location: ' . $_SESSION["redirectURL"], true, 303);
						exit();
					}
				}
			} else {
				//TODO: Facebook error, no session or invalid login credentials/declined permissions
				$_SESSION["googleAnalyticsAction"] = "Facebook Login Failure";
			}
		}
	} else if (isset($_GET["oauth_verifier"]) && isset($_GET["oauth_token"]) && $_GET["oauth_token"] == $_SESSION["twitterOAuthToken"]) {
		require_once(BASE_PATH . "/../phpinc/lib/tw/twitter.php");
		
		$twitterSession = new TwitterOAuth(TWITTER_APP_ID, TWITTER_SECRET, $_SESSION["twitterOAuthToken"], $_SESSION["twitterOAuthTokenSecret"]);
		$accessToken = $twitterSession->getAccessToken($_GET["oauth_verifier"]);
		
		$_SESSION["twitterOAuthToken"] = $accessToken["oauth_token"];
		$_SESSION["twitterOAuthTokenSecret"] = $accessToken["oauth_token_secret"];
		
		// Use real access token from now on
		$twitterSession = new TwitterOAuth(TWITTER_APP_ID, TWITTER_SECRET, $_SESSION["twitterOAuthToken"], $_SESSION["twitterOAuthTokenSecret"]);
		$information = $twitterSession->get("account/verify_credentials");
		
		$_SESSION["twitterUniqueID"] = $information->id;
		$_SESSION["twitterProfilePictureURL"] = $information->profile_image_url_https;
		
		$email = $metaRankDatabase->getTwitterUserEmail($_SESSION["twitterUniqueID"]);
		if ($email == null) {
			// We don't have an account for this Twitter user yet, so create one
			$_SESSION["twitterProfilePictureURL"] = $information->profile_image_url_https;
			
			$first_name = $information->name;
			$last_name = "";
			if (substr_count($information->name, " ") > 0) {
				$first_name = substr($information->name, 0, strpos($information->name, " "));
				$last_name = substr($information->name, strrpos($information->name, " ") + 1);
			}
			
			$service = "Twitter";
			$registrationInformation = array(
				"first_name" => $first_name,
				"last_name" => $last_name,
				"date_of_birth" => "",
				"gender" => "",
				"phone" => "",
				"email" => ""
			);
			
			$pageTitle = "MetaRank - Register with Twitter";
			if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
				require_once(BASE_PATH . "/../phpinc/header.php");
			}
			
			include (BASE_PATH . "/../phpinc/register.php");
			
			if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
				require_once(BASE_PATH . "/../phpinc/footer.php");
			}
			
			exit();
		} else {
			$userLocked = $metaRankDatabase->checkUserIsLocked($email);
			if ($userLocked["locked"]) {
				if ($userLocked["modification_reason"] == "") {
					$message = "This username is locked. If you have used the Forgot Password link, complete the instructions in the email we sent to reset your password and unlock your account.";
				} else {
					$message = "This username is locked. Reason: " . $userLocked["modification_reason"];
				}
				
				include(BASE_PATH . "/../phpinc/errors/generic.php");
				
				exit();
			} else {
				$metaRankDatabase->logSuccessfulLogin($email);

				session_regenerate_id(true);

				$metaRankDatabase->loadSessionUserInformation($email);

				$metaRankDatabase->updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"));
				
				if (isset($_SESSION["locationPostalCode"])) {
					$metaRankDatabase->updateUserLocation($_SESSION["userID"], $_SESSION["locationCountry"], $_SESSION["locationAdministrativeArea"], $_SESSION["locationLocality"], $_SESSION["locationPostalCode"], $_SESSION["locationLatitude"], $_SESSION["locationLongitude"]);
					$metaRankDatabase->scoreMetaRankBonus($_SESSION["userID"]);
				}
				unset($_SESSION["locationCountry"]);
				unset($_SESSION["locationAdministrativeArea"]);
				unset($_SESSION["locationLocality"]);
				unset($_SESSION["locationPostalCode"]);
				unset($_SESSION["locationLatitude"]);
				unset($_SESSION["locationLongitude"]);
				
				$_SESSION["isLoggedIn"] = true;
				
				$metaRankDatabase->updateTwitterOAuthToken($_SESSION["userID"], $_SESSION["twitterOAuthToken"], $_SESSION["twitterOAuthTokenSecret"]);
				
				$metaRankDatabase->scoreTwitter($_SESSION["userID"], $_SESSION["twitterOAuthToken"], $_SESSION["twitterOAuthTokenSecret"]);
				$metaRankDatabase->updateMetaScore($_SESSION["userID"]);
				
				$metaRankDatabase->rankGlobalTwitter($_SESSION["userID"]);
				$metaRankDatabase->computeGlobalTwitterParticipants();
				
				$metaRankDatabase->rankGlobal($_SESSION["userID"]);
				$metaRankDatabase->computeGlobalParticipants();
				
				$metaRankDatabase->commitChanges();
				
				$_SESSION["googleAnalyticsAction"] = "Twitter Login";
				
				header('Location: ' . $_SESSION["redirectURL"], true, 303);
				exit();
			}
		}
	}
	
	$pageTitle = "MetaRank";
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
		require_once(dirname(__FILE__) . "/../phpinc/header.php");
	} 
	
	
	// EDITED BY SURAJIT ON 22/09/2015 
	require_once(BASE_PATH . "/../phpinc/db.php");
	require_once(BASE_PATH . "/../phpinc/lib/fb/facebook.php");
	require_once(BASE_PATH . "/../phpinc/lib/li/linkedin.php");
	require_once(BASE_PATH . "/../phpinc/lib/tw/twitter.php");
	
	//use Facebook\FacebookRedirectLoginHelper;
	
	$_SESSION["facebookUniqueID"] = null;
	$_SESSION["facebookAccessToken"] = null;
	$_SESSION["facebookAccessTokenExpiration"] = null;
	$fbscope = array("user_birthday","user_location","email","read_stream","user_about_me","user_education_history","user_work_history","user_website","user_friends");
	//$fbscope = array("user_birthday","user_location","email","user_about_me","user_education_history","user_work_history","user_website","user_friends");
	//$fbscope = array('scope'=>'manage_pages,read_insights,user_photos,email,publish_actions,user_birthday,user_about_me,user_website,user_friends,user_work_history,user_location,user_education_history');
	//$fbLoginHelper = new FacebookRedirectLoginHelper(curHostURL() . "/");
	$fbLoginHelper = new FacebookRedirectLoginHelper(curHostURL() . "/metarank/htdocs/");
	
	$_SESSION["linkedInUniqueID"] = null;
	$_SESSION["linkedInOAuthToken"] = null;
	$_SESSION["linkedInOAuthTokenExpiration"] = null;
	//$liscope = array(LinkedIn::SCOPE_BASIC_PROFILE, LinkedIn::SCOPE_FULL_PROFILE, LinkedIn::SCOPE_EMAIL_ADDRESS, LinkedIn::SCOPE_NETWORK, LinkedIn::SCOPE_CONTACT_INFO);
	$liscope = array(LinkedIn::SCOPE_BASIC_PROFILE,  LinkedIn::SCOPE_EMAIL_ADDRESS);
	$_SESSION["twitterUniqueID"] = null;
	
	$twitterSession = new TwitterOAuth(TWITTER_APP_ID, TWITTER_SECRET);
	$temporaryCredentials = $twitterSession->getRequestToken(curHostURL() . "/metarank/htdocs/");
	$_SESSION["twitterOAuthToken"] = $temporaryCredentials["oauth_token"];
	$_SESSION["twitterOAuthTokenSecret"] = $temporaryCredentials["oauth_token_secret"];
		
	// EDITED BY SURAJIT ON 22/09/2015 	
	
	?>
	
	<section role="main" class="ui-content">
		<div id="index_page" data-title="<?php echo $pageTitle; ?>">			
			<div class="content-wrapper">
				<div id="get_started" class="left">
					<p class="black-text"><span class="meta-logotext">meta</span><span class="rank-logotext">rank</span> aggregates your rankings into a single hierarchy. Ranks are collected from various sources. Knowing your place in a hierarchy and the place of others is wise.</p>
				</div>
				<br />
                
                <div class="new_login">
                	<div>
                        <p>
                        	<a id="facebook_login" onclick="ga('send','event','Login','click','Facebook')" href="<?php echo $fbLoginHelper->getLoginUrl($fbscope);?>" class="btn azm-social azm-btn azm-border-bottom azm-facebook">
                            	<i class="fa fa-facebook"></i> Sign in with Facebook
                            </a>
                        </p>
                        <p>
                        	<a id="linkedin_login" onclick="ga('send','event','Login','click','LinkedIn')" href="<?php echo $linkedinSession->getLoginUrl($liscope);?>" class="btn azm-social azm-btn azm-border-bottom azm-linkedin">
                            	<i class="fa fa-linkedin"></i> Sign in with LinkedIn
                            </a>
                        </p>
                        <p>
                        	<a id="twitter_login" onclick="ga('send','event','Login','click','Twitter')" href="<?php echo $twitterSession->getAuthorizeURL($temporaryCredentials);?>" class="btn azm-social azm-btn azm-border-bottom azm-twitter">
                            	<i class="fa fa-twitter"></i> Sign in with Twitter
                            </a>
                        </p>
                        <p>
                            <a id="login_button" href="<?php echo BASE_URL; ?>/login/" class="btn azm-social azm-btn azm-border-bottom azm-email-2" onclick="ga('send','event','Button','click','Login')" data-ajax="false">
                                <i class="fa fa-envelope"></i> Sign in with Email
                            </a>
                        </p><br /><br />
                    </div>
                </div>
			</div>
		</div><!-- /content -->
	</section><!-- /page -->
    
<?php
} else {
	if (isset($_GET['code'])) {
		if (isset($_GET["state"]) && strrpos($_GET["state"], "linkedIn_", -strlen($_GET["state"])) !== FALSE) {
			require_once(BASE_PATH . "/../phpinc/lib/li/linkedin.php");
			$accessToken = $linkedinSession->processRedirect();
			if ($accessToken == null) {
				//TODO: Handle this failure
				if ($_SESSION["linkedInOAuthToken"] == null) {
					$_SESSION["googleAnalyticsAction"] = "LinkedIn Connection Failure";
				} else {
					$_SESSION["googleAnalyticsAction"] = "LinkedIn Reauthorization Failure";
				}
			} else {
				// Logged in by LinkedIn
				$_SESSION["linkedInOAuthToken"] = $accessToken;
				$accessTokenExpiration = new DateTime();
				$accessTokenExpiration->setTimestamp(time() + $linkedinSession->getAccessTokenExpiration());
				
				$information = $linkedinSession->get("/people/~:(email-address,last-modified-timestamp,id,first-name,last-name,maiden-name,formatted-name,headline,location:(name),industry,current-share,num-connections,num-connections-capped,summary,specialties,positions,picture-url,site-standard-profile-request,api-standard-profile-request:(headers),public-profile-url,phone-numbers,bound-account-types,im-accounts,main-address,twitter-accounts,primary-twitter-account,connections,group-memberships,proposal-comments,associations,honors,interests,publications,patents,languages,skills,certifications,educations,courses,volunteer,three-current-positions,three-past-positions,num-recommenders,recommendations-received,mfeed-rss-url,following,job-bookmarks,suggestions,date-of-birth,member-url-resources,related-profile-views,network)");
				
				$_SESSION["linkedInUniqueID"] = $information["id"];
				
				$email = $metaRankDatabase->getLinkedInUserEmail($_SESSION["linkedInUniqueID"]);
				if ($email == null) {
					// We don't have an account for this LinkedIn user yet, so create one
					$email = $information["emailAddress"];
					
					if (!$metaRankDatabase->createLinkedInUser($_SESSION["userID"], $_SESSION["linkedInUniqueID"], $_SESSION["linkedInOAuthToken"], $accessTokenExpiration)) {
						//TODO: Handle this error
						$metaRankDatabase->rollbackChanges();
					} else {
						$_SESSION["linkedInOAuthTokenExpiration"] = time() + $linkedinSession->getAccessTokenExpiration();
						
						$metaRankDatabase->scoreLinkedIn($_SESSION["userID"], $information);
						$metaRankDatabase->updateMetaScore($_SESSION["userID"]);
						
						$metaRankDatabase->rankGlobalLinkedIn($_SESSION["userID"]);
						$metaRankDatabase->computeGlobalLinkedInParticipants();
						
						$metaRankDatabase->rankGlobal($_SESSION["userID"]);
						$metaRankDatabase->computeGlobalParticipants();

						$metaRankDatabase->commitChanges();
						
						$_SESSION["googleAnalyticsAction"] = "LinkedIn Connected";
						
						header('Location: ' . BASE_URL . '/linkedin/', true, 303);
						exit();
					}
				} else {
					$metaRankDatabase->updateLinkedInAccessToken($_SESSION["userID"], $_SESSION["linkedInOAuthToken"], $accessTokenExpiration);
					$_SESSION["linkedInOAuthTokenExpiration"] = time() + $linkedinSession->getAccessTokenExpiration();
					
					$metaRankDatabase->scoreLinkedIn($_SESSION["userID"], $information);
					$metaRankDatabase->updateMetaScore($_SESSION["userID"]);
					
					$metaRankDatabase->rankGlobalLinkedIn($_SESSION["userID"]);
					$metaRankDatabase->computeGlobalLinkedInParticipants();
					
					$metaRankDatabase->rankGlobal($_SESSION["userID"]);
					$metaRankDatabase->computeGlobalParticipants();
					
					$metaRankDatabase->commitChanges();
					
					$_SESSION["googleAnalyticsAction"] = "LinkedIn Reauthorized";
					
					header('Location: ' . BASE_URL . '/linkedin/', true, 303);
					exit();
				}
			}
		} else if (!isset($_SESSION["facebookAccessToken"])) {
			require_once(BASE_PATH . "/../phpinc/lib/fb/facebook.php");
			
			$fbLoginHelper = new FacebookRedirectLoginHelper(curHostURL() . "/metarank/htdocs/");
			try {
				$facebookSession = $fbLoginHelper->getSessionFromRedirect(curHostURL() . "/metarank/htdocs/");
			} catch(FacebookRequestException $ex) {
				// When Facebook returns an error
			} catch(\Exception $ex) {
				// When validation fails or other local issues
			}
			
			if (isset($facebookSession) && $facebookSession) {
				// Logged in by Facebook
				// Get the GraphUser object for the current user:
				try {
					$fbUserProfile = (new FacebookRequest($facebookSession, 'GET', '/me'))->execute()->getGraphObject(GraphUser::className());
					$_SESSION["facebookUniqueID"] = $fbUserProfile->getId();
				} catch (FacebookRequestException $e) {
					// The Graph API returned an error
				} catch (\Exception $e) {
					// Some other error occurred
				}
				
				$accessToken = $facebookSession->getAccessToken()->extend(FACEBOOK_APP_ID, FACEBOOK_SECRET);
				$_SESSION["facebookAccessToken"] = $accessToken->__toString();
				
				$email = $metaRankDatabase->getFacebookUserEmail($_SESSION["facebookUniqueID"]);
				if ($email == null) {
					// We don't have an account for this Facebook user yet, so create one
					$email = $fbUserProfile->getProperty("email");
					if (!$metaRankDatabase->createFacebookUser($_SESSION["userID"],$_SESSION["facebookUniqueID"],$_SESSION["facebookAccessToken"],$accessToken->getExpiresAt())){
						// TODO: Handle this error
						$metaRankDatabase->rollbackChanges();
					} else {
						$_SESSION["facebookAccessTokenExpiration"] = $accessToken->getExpiresAt()->getTimestamp();
						
						$metaRankDatabase->scoreFacebook($_SESSION["userID"], $_SESSION["facebookAccessToken"]);
						$metaRankDatabase->updateMetaScore($_SESSION["userID"]);
						
						$metaRankDatabase->rankGlobalFacebook($_SESSION["userID"]);
						$metaRankDatabase->computeGlobalFacebookParticipants();
						
						$metaRankDatabase->rankGlobal($_SESSION["userID"]);
						$metaRankDatabase->computeGlobalParticipants();

						$metaRankDatabase->commitChanges();
						
						$_SESSION["googleAnalyticsAction"] = "Facebook Connected";
						
						header('Location: ' . BASE_URL . '/facebook/', true, 303);
						exit();
					}
				} else {
					$metaRankDatabase->updateFacebookAccessToken($_SESSION["userID"], $_SESSION["facebookAccessToken"], $accessToken->getExpiresAt());
					$_SESSION["facebookAccessTokenExpiration"] = $accessToken->getExpiresAt()->getTimestamp();
					
					$metaRankDatabase->scoreFacebook($_SESSION["userID"], $_SESSION["facebookAccessToken"]);
					$metaRankDatabase->updateMetaScore($_SESSION["userID"]);
					
					$metaRankDatabase->rankGlobalFacebook($_SESSION["userID"]);
					$metaRankDatabase->computeGlobalFacebookParticipants();
					
					$metaRankDatabase->rankGlobal($_SESSION["userID"]);
					$metaRankDatabase->computeGlobalParticipants();
					
					$metaRankDatabase->commitChanges();
					
					$_SESSION["googleAnalyticsAction"] = "Facebook Reauthorized";
					
					header('Location: ' . BASE_URL . '/facebook/', true, 303);
					exit();
				}
			} else {
				//TODO: Facebook error, no session or invalid login credentials/declined permissions
				if ($_SESSION["facebookUniqueID"] == null) {
					$_SESSION["googleAnalyticsAction"] = "Facebook Connection Failure";
				} else {
					$_SESSION["googleAnalyticsAction"] = "Facebook Reauthorization Failure";
				}
			}
		}
	} else if (isset($_GET["oauth_verifier"]) && isset($_GET["oauth_token"]) && $_GET["oauth_token"] == $_SESSION["twitterOAuthToken"]) {
		require_once(BASE_PATH . "/../phpinc/lib/tw/twitter.php");
		
		$twitterSession = new TwitterOAuth(TWITTER_APP_ID, TWITTER_SECRET, $_SESSION["twitterOAuthToken"], $_SESSION["twitterOAuthTokenSecret"]);
		$accessToken = $twitterSession->getAccessToken($_GET["oauth_verifier"]);
		
		$_SESSION["twitterOAuthToken"] = $accessToken["oauth_token"];
		$_SESSION["twitterOAuthTokenSecret"] = $accessToken["oauth_token_secret"];
		
		// Use real access token from now on
		$twitterSession = new TwitterOAuth(TWITTER_APP_ID, TWITTER_SECRET, $_SESSION["twitterOAuthToken"], $_SESSION["twitterOAuthTokenSecret"]);
		$information = $twitterSession->get("account/verify_credentials");
		
		$_SESSION["twitterUniqueID"] = $information->id;
		$_SESSION["twitterProfilePictureURL"] = $information->profile_image_url_https;
		
		$email = $metaRankDatabase->getTwitterUserEmail($_SESSION["twitterUniqueID"]);
		if ($email == null) {
			// We don't have an account for this Twitter user yet, so create one
			if (!$metaRankDatabase->createTwitterUser($_SESSION["userID"], $_SESSION["twitterUniqueID"], $_SESSION["twitterOAuthToken"], $_SESSION["twitterOAuthTokenSecret"], $information->profile_image_url_https)) {
				// TODO: Handle this error
				$metaRankDatabase->rollbackChanges();
			} else {
				$metaRankDatabase->scoreTwitter($_SESSION["userID"], $_SESSION["twitterOAuthToken"], $_SESSION["twitterOAuthTokenSecret"]);
				$metaRankDatabase->updateMetaScore($_SESSION["userID"]);
				
				$metaRankDatabase->rankGlobalTwitter($_SESSION["userID"]);
				$metaRankDatabase->computeGlobalTwitterParticipants();
				
				$metaRankDatabase->rankGlobal($_SESSION["userID"]);
				$metaRankDatabase->computeGlobalParticipants();

				$metaRankDatabase->commitChanges();
				
				$_SESSION["googleAnalyticsAction"] = "Twitter Connected";
				
				header('Location: ' . BASE_URL . '/twitter/', true, 303);
				exit();
			}
		} else {
			$metaRankDatabase->updateTwitterOAuthToken($_SESSION["userID"], $_SESSION["twitterOAuthToken"], $_SESSION["twitterOAuthTokenSecret"]);
			
			$metaRankDatabase->scoreTwitter($_SESSION["userID"], $_SESSION["twitterOAuthToken"], $_SESSION["twitterOAuthTokenSecret"]);
			$metaRankDatabase->updateMetaScore($_SESSION["userID"]);
			
			$metaRankDatabase->rankGlobalTwitter($_SESSION["userID"]);
			$metaRankDatabase->computeGlobalTwitterParticipants();
			
			$metaRankDatabase->rankGlobal($_SESSION["userID"]);
			$metaRankDatabase->computeGlobalParticipants();
			
			$metaRankDatabase->commitChanges();
			
			$_SESSION["googleAnalyticsAction"] = "Twitter Reauthorized";
			
			header('Location: ' . BASE_URL . '/twitter/', true, 303);
			exit();
		}
	}
	
	include(BASE_PATH . "/../phpinc/reauthorization.php");
	
	$pageTitle = "MetaRank";
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
		require_once(BASE_PATH . "/../phpinc/header.php");
	}
	
	$service = "";
	$reportCardDetails = $metaRankDatabase->getMetaRankReportCardDetails($_SESSION["userID"]);
	$postalCodeRankResults = $metaRankDatabase->getMetaRankPostalCodeRank($_SESSION["userID"]);
	$localityRankResults = $metaRankDatabase->getMetaRankLocalityRank($_SESSION["userID"]);
	$administrativeAreaRankResults = $metaRankDatabase->getMetaRankAdministrativeAreaRank($_SESSION["userID"]);
	$countryRankResults = $metaRankDatabase->getMetaRankCountryRank($_SESSION["userID"]);
	$globalRankResults = $metaRankDatabase->getMetaRankGlobalRank($_SESSION["userID"]);
	if ($globalRankResults != null) {
		$tmp = $metaRankDatabase->getMetaRankGlobalParticipants();
		if ($tmp != null) {
			$globalRankResults += $tmp;
		} else {
			$globalRankResults = null;
		}
	}
	$plot1Data = $metaRankDatabase->getMetaRankRankHistory($_SESSION["userID"]);
	$plot2Data = $metaRankDatabase->getMetaRankParticipantsHistory();
	$plot3Data = $metaRankDatabase->getMetaRankScoreHistory($_SESSION["userID"]);
	
	$leaderboard = $metaRankDatabase->getMetaRankLeaderboard($_SESSION["userID"]);
	$AllScore    = $metaRankDatabase->getFacebookScore();
	
	$facebookRankResults = $metaRankDatabase->getFacebookGlobalRank($_SESSION["userID"]);
	$linkedinRankResults = $metaRankDatabase->getLinkedInGlobalRank($_SESSION["userID"]);
	$twitterRankResults = $metaRankDatabase->getTwitterGlobalRank($_SESSION["userID"]); ?>
    
	<section role="main" class="ui-content">
		<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
		<div id="home_page" data-title="<?php echo $pageTitle; ?>">
			<div class="content-wrapper">
				<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
				<div id="content_column">
					<?php
					include(BASE_PATH . "/../phpinc/subheader.php");
					include(BASE_PATH . "/../phpinc/ranks.php");
					include(BASE_PATH . "/../phpinc/leaderboard.php");
					?>
					<div class="content-box">
						<p class="left larger-message">Details</p>
						<table class="details">
							<tr>
								<td>Bonus</td>
								<td>Score:</td>
								<td><?php echo round($reportCardDetails["bonus_score"], 0); ?></td>
							</tr>
							<?php if ($facebookRankResults != null) { ?>
                                <tr>
                                    <td><a href="<?php echo BASE_URL; ?>/facebook/" data-ajax="false">Facebook</a></td>
                                    <td>
                                        <p>Rank:</p>
                                        <p>Score:</p>
                                    </td>
                                    <td>
                                        <p><?php echo $facebookRankResults["rank"]; ?><span class="superscript"><?php echo getOrdinal($facebookRankResults["rank"]); ?></span></p>
                                        <p><?php echo round($reportCardDetails["facebook_score"], 0); ?></p>
                                    </td>
                                </tr>
							<?php } 
							if ($linkedinRankResults != null) { ?>
                                <tr>
                                    <td><a href="<?php echo BASE_URL; ?>/linkedin/" data-ajax="false">LinkedIn</a></td>
                                    <td>
                                        <p>Rank:</p>
                                        <p>Score:</p>
                                    </td>
                                    <td>
                                        <p><?php echo $linkedinRankResults["rank"]; ?><span class="superscript"><?php echo getOrdinal($linkedinRankResults["rank"]); ?></span></p>
                                        <p><?php echo round($reportCardDetails["linkedin_score"], 0); ?></p>
                                    </td>
                                </tr>
							<?php }
							if ($twitterRankResults != null) { ?>
                                <tr>
                                    <td><a href="<?php echo BASE_URL; ?>/twitter/" data-ajax="false">Twitter</a></td>
                                    <td>
                                        <p>Rank:</p>
                                        <p>Score:</p>
                                    </td>
                                    <td>
                                        <p><?php echo $twitterRankResults["rank"]; ?><span class="superscript"><?php echo getOrdinal($twitterRankResults["rank"]); ?></span></p>
                                        <p><?php echo round($reportCardDetails["twitter_score"], 0); ?></p>
                                    </td>
                                </tr>
							<?php } ?>
							<tr>
								<td></td>
								<td>Metascore:</td>
								<td><?php echo round($reportCardDetails["score"], 0); ?></td>
							</tr>
						</table>
					</div>
					<div class="content-box">
						<p class="left">Note: The MetaRank Report Card is the aggregation of all other report cards, points, ranks and scores.</p>
					</div>
					<div style="clear:both;"></div>
				</div>
			</div>
		</div><!-- /content -->
	</section><!-- /page -->
    
<?php
}
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>
