<?php
require_once(dirname(dirname(dirname(__FILE__))) . "/../phpinc/session.php");

if (isset($_GET["disconnect"])) {
	if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
		$_SESSION['redirectURL'] = BASE_URL . '/twitter/options/?disconnect';
		header('Location: ' . BASE_URL . '/login/', true, 303);
		exit();
	} else {
		if (!$metaRankDatabase->disconnectTwitterFromUser($_SESSION["userID"])) {
			$metaRankDatabase->rollbackChanges();
		} else {
			$metaRankDatabase->commitChanges();
			
			$_SESSION["twitterUniqueID"] = null;
			$_SESSION["twitterOAuthToken"] = null;
			$_SESSION["twitterOAuthTokenSecret"] = null;
		}
	}
} else {
	if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
		$_SESSION['redirectURL'] = BASE_URL . '/twitter/options/';
		header('Location: ' . BASE_URL . '/login/', true, 303);
		exit();
	}
}

include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - Twitter";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
} ?>

<section data-role="main" class="ui-content">
	<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
    <div id="twitter_options_page" data-title="<?php echo $pageTitle; ?>">
        <div class="content-wrapper">
			<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
            <div id="content_column">
				<?php
				$service = "Twitter";
				$subTitle = "Twitter Options";
				include(BASE_PATH . "/../phpinc/subheader.php");
				?>
                <div class="content-box">
                    <p class="left larger-message">Twitter Options</p>
					<?php if (!isset($_SESSION["twitterUniqueID"]) || $_SESSION["twitterUniqueID"] == "") {
						require_once(BASE_PATH . "/../phpinc/db.php");
						require_once(BASE_PATH . "/../phpinc/lib/tw/twitter.php");
						$_SESSION["twitterUniqueID"] = "";
						$twitterSession = new TwitterOAuth(TWITTER_APP_ID, TWITTER_SECRET);
						$temporaryCredentials = $twitterSession->getRequestToken(curHostURL() . "/metarank/htdocs/");
						$_SESSION["twitterOAuthToken"] = $temporaryCredentials["oauth_token"];
						$_SESSION["twitterOAuthTokenSecret"] = $temporaryCredentials["oauth_token_secret"]; ?>
                        <p>
                        	<a href="<?php echo $twitterSession->getAuthorizeURL($temporaryCredentials, FALSE); ?>" data-ajax="false">
                            	<button type="button">Connect to Twitter</button>
                            </a>
                        </p>
					<?php } else { ?>
	                    <p>
                        	<a href="<?php echo BASE_URL; ?>/twitter/options?disconnect" data-ajax="false">
                            	<button type="button">Disconnect from Twitter</button>
                            </a>
                        </p>
					<?php } ?>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>