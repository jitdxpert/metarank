<?php
require_once(dirname(dirname(__FILE__)) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/twitter/';
	header('Location: ' . BASE_URL . '/login/', true, 303);
	exit();
}

if (!isset($_SESSION["twitterUniqueID"]) || $_SESSION["twitterUniqueID"] == "") {
	header('Location: ' . BASE_URL . '/twitter/options/', true, 303);
	exit();
}

include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - Twitter";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}

$service = "Twitter";
$reportCardDetails = $metaRankDatabase->getTwitterReportCardDetails($_SESSION["userID"]);
$postalCodeRankResults = $metaRankDatabase->getTwitterPostalCodeRank($_SESSION["userID"]);
$localityRankResults = $metaRankDatabase->getTwitterLocalityRank($_SESSION["userID"]);
$administrativeAreaRankResults = $metaRankDatabase->getTwitterAdministrativeAreaRank($_SESSION["userID"]);
$countryRankResults = $metaRankDatabase->getTwitterCountryRank($_SESSION["userID"]);
$globalRankResults = $metaRankDatabase->getTwitterGlobalRank($_SESSION["userID"]);
if ($globalRankResults != null) {
	$tmp = $metaRankDatabase->getTwitterGlobalParticipants();
	if ($tmp != null) {
		$globalRankResults += $tmp;
	} else {
		$globalRankResults = null;
	}
}
$plot1Data = $metaRankDatabase->getTwitterRankHistory($_SESSION["userID"]);
$plot2Data = $metaRankDatabase->getTwitterParticipantsHistory();
$plot3Data = $metaRankDatabase->getTwitterScoreHistory($_SESSION["userID"]);

$leaderboard = $metaRankDatabase->getTwitterLeaderboard($_SESSION["userID"]);
$AllScore    = $metaRankDatabase->getFacebookScore(); ?>

<section data-role="main" class="ui-content">
	<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
    <div id="twitter_page" data-title="<?php echo $pageTitle; ?>">
        <div class="content-wrapper">
        	<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
            <div id="content_column">
				<?php
				include(BASE_PATH . "/../phpinc/subheader.php");
				include(BASE_PATH . "/../phpinc/ranks.php");
				include(BASE_PATH . "/../phpinc/leaderboard.php");
				?>
                <div class="content-box">
                    <p class="left larger-message">Details</p>
                    <table class="service-details">
                        <tbody>
                            <tr>
                                <td>Original Tweets:</td>
                                <td><?php echo $reportCardDetails["original_tweets"]; ?></td>
                            </tr>
                            <tr>
                                <td>Replies/Retweets:</td>
                                <td><?php echo $reportCardDetails["other_tweets"]; ?></td>
                            </tr>
                            <tr>
                                <td>Retweeted Total:</td>
                                <td><?php echo $reportCardDetails["retweeted_total"]; ?></td>
                            </tr>
                            <tr>
                                <td>Favorited Total:</td>
                                <td><?php echo $reportCardDetails["favorited_total"]; ?></td>
                            </tr>
                            <tr>
                                <td>Favourites:</td>
                                <td><?php echo $reportCardDetails["favorites"]; ?></td>
                            </tr>
                            <tr>
                                <td>Statuses:</td>
                                <td><?php echo $reportCardDetails["statuses"]; ?></td>
                            </tr>
                            <tr>
                                <td>Friends:</td>
                                <td><?php echo $reportCardDetails["friends"]; ?></td>
                            </tr>
                            <tr>
                                <td>Followers:</td>
                                <td><?php echo $reportCardDetails["followers"]; ?></td>
                            </tr>
                            <tr>
                                <td>Listed:</td>
                                <td><?php echo $reportCardDetails["listed"]; ?></td>
                            </tr>
                            <tr>
                                <td>Verified:</td>
                                <td><?php echo ($reportCardDetails["verified"] == 1 ? "yes" : "no"); ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <p class="left">
                    	Twitter updated: 
						<script id="modified_time" type="text/javascript">
						$("#modified_time").replaceWith(new Date("<?php echo $reportCardDetails["modified_time"]; ?>").toLocaleString());
                        </script>.
                    </p>
                    <p class="left">Note: Twitter Report Card is based on activity for the life of the Twitter account.</p>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>