// INFO: We don't want jqm to keep the first page indefinitely, we need to break the cache, even if we are doing ajax
var BASE_URL = $('#BASE_URL').val();

$(document).on("click", ".ui-link", function () {
	$.mobile.loading("show");
});

$(document).on("pagecontainerchange", function (evt, ui) {
	
	if ($.mobile.firstPage && ui.prevPage && ($.mobile.firstPage == ui.prevPage) && ui.toPage && ($.mobile.firstPage[0] != ui.toPage[0])) {
		$.mobile.firstPage.remove();
	}
	
	$("#mobile_menu").on("panelopen", function( event, ui ) {
		$.mobile.loading("hide");
	});
	
	$('.ui-content').css('min-height', $(window).height() - ($('header').height() + $('footer').height() + 73));
	
	// Update location
	if (window.location.hash == "#_=_") window.location.hash = "";
	var loadUrl = BASE_URL + "/ajax/update_location.php";

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var city = "";
			var state = "";
			var zipcode = "";
			var country = "";

			geocoder = new google.maps.Geocoder();
			geocoder.geocode({ 'location': new google.maps.LatLng(position.coords.latitude, position.coords.longitude) }, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[0]) {
						results[0].address_components.forEach(function(address_component) {
							address_component.types.every(function(address_component_type) {
								if (address_component_type == "sublocality" || (city == "" && address_component_type == "locality")) {
									city = address_component.short_name;
									return false;
								} else if (address_component_type == "administrative_area_level_1") {
									state = address_component.short_name;
									return false;
								} else if (address_component_type == "postal_code") {
									zipcode = address_component.short_name;
									return false;
								} else if (address_component_type == "country") {
									country = address_component.short_name;
									return false;
								}
								return true;
							});
						});

						$.post(
							loadUrl,
							{lat: position.coords.latitude, long: position.coords.longitude, city: city, country: country, region: state, postal_code: zipcode},
							function(responseText){
								//$("#result").html(responseText);
								console.log("Update Location: " + responseText);
							},
							"json"
						);
					}
				} else {
					//todo: fix this eventuality... if geocoding fails from google
				}
			});
		}, function(error) {
			//todo: fix this eventuality...
			$.post(
				loadUrl,
				{disabled: "true"},
				function(responseText){
					//$("#result").html(responseText);
					console.log("Update Location: " + responseText);
				},
				"json"
			);
		});
	}
	else
	{
		//todo: fix this eventuality...
		$.post(
			loadUrl,
			{},
			function(responseText){
				//$("#result").html(responseText);
				console.log("Update Location: " + responseText);
			},
			"json"
		);
	}
});

function linkedInShareCallback(url) {
	ga('send', 'social', 'linkedin', 'share', url);
}

twttr.ready(function(twttr) {
	twttr.events.bind('tweet', function (event) {
		if (!event) return;
		ga('send', 'social', 'twitter', 'tweet', 'http://www.metarank.com/');
	});
});

var plot1 = null;
var plot2 = null;
var plot3 = null;

$(window).bind('orientationchange', function(event) {
	if (plot1 != null) {
        plot1.data("jqplot").replot();
    }
	
	if (plot2 != null) {
        plot2.data("jqplot").replot();
    }
	
	if (plot3 != null) {
        plot3.data("jqplot").replot();
    }
});

$(window).on("resize", function() {
	if (plot1 != null) {
        plot1.data("jqplot").replot();
    }
	
	if (plot2 != null) {
        plot2.data("jqplot").replot();
    }
	
	if (plot3 != null) {
        plot3.data("jqplot").replot();
    }
});

function getDayName(appDate, seperator){
	var dayNameArr = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thrusday", "Friday", "Saturday");
	var dateArr = appDate.split(seperator); // split the date into array using the date seperator
	var month = eval(dateArr[1]); 
	var day = eval(dateArr[2]);
	var year = eval(dateArr[0]);
	// Calculate the total number of days, with taking care of leapyear 
	var totalDays = day + (2*month) + parseInt(3*(month+1)/5) + year + parseInt(year/4) - parseInt(year/100) + parseInt(year/400) + 2;
	// Mod of the total number of days with 7 gives us the day number
	var dayNo = (totalDays%7);
	// if the resultant mod of 7 is 0 then its Saturday so assign the dayNo to 7
	if(dayNo == 0){
		dayNo = 7;
	}
	return dayNameArr[dayNo-1]; // return the repective Day Name from the array
}

$(document).bind('pagecontainershow', function(evt, ui) {
	// INFO: Google Analytics
	ga('send', 'pageview', $.mobile.path.parseUrl(window.location.href).pathname);

	// INFO: Fix Facebook like buttons
	try{
		FB.XFBML.parse();
	} catch(err){}
	
	// INFO: Fix LinkedIn share buttons
	try {
		IN.parse(document.body);
	} catch(err){}
	

	// INFO: Fix Twitter tweet buttons
	try {
		twttr.widgets.load(ui.toPage[0]);
	} catch(err){}
	
	var dayNameIndex = new Array();
	dayNameIndex["Sunday"] = 0;
	dayNameIndex["Monday"] = 1;
	dayNameIndex["Tuesday"] = 2;
	dayNameIndex["Wednesday"] = 3;
	dayNameIndex["Thrusday"] = 4;
	dayNameIndex["Friday"] = 5;
	dayNameIndex["Saturday"] = 6;
	
	// INFO: Render plots
	plotData1 = $("#plotData1", ui.toPage).val();
	if (typeof plotData1 != "undefined") {
		tmp = JSON.parse(plotData1);
		if (tmp[0][0] != null) {
			var startDate = tmp[0][0].split(' ');
			var dayName = getDayName(startDate[0], "/");
			var dayIndex = parseInt(dayNameIndex[dayName]);
			var ektavar = 0 - dayNameIndex[dayName];
			
			var ymd = startDate[0].split('/');
			var d = new Date(ymd[0], ymd[1], ymd[2]);
			d.setDate(d.getDate() - Math.abs(ektavar));
			var sunday = d.getFullYear() + '/' + d.getMonth() + '/' + d.getDate();
			
			min = (parseInt(tmp[0][1]) - 25 >= 0 ? parseInt(tmp[0][1]) - 25 : 0);
			max = parseInt(tmp[0][1]) + 25;
			
			for (var i = 1; i < tmp.length; i++) {
				if ((parseInt(tmp[i][1]) - 25) < min && (parseInt(tmp[i][1]) - 25) >= 0) {
					min = parseInt(tmp[i][1]) - 25;
				}
				
				if ((parseInt(tmp[i][1]) + 25) > max) {
					max = parseInt(tmp[i][1]) + 25;
				}
			}
			
			if ( tmp.length <= 7 ) {
				var inter = '1 day';
			} else if ( tmp.length > 7 && tmp.length <= 30 ) {
				var inter = '1 week';
			} else if ( tmp.length > 30 && tmp.length <= 365 ) {
				var inter = '1 month';
			} else if ( tmp.length > 365 ) {
				var inter = '1 year';
			}
		} else {
			min = 2;
			max = 0;
		}
		console.log(min, max);
		
		
		
		// In the case where we initially loaded a report card, and then navigate to another report card, specifying a context is not enough for jqPlot to properly apply to the toPage rather than the previous page. So, remove the previous page's chart first.
		if (plot1 != null) {
			plot1.remove();
		}
		
		plot1 = $('#chart1', ui.toPage);
		plot1.jqplot([tmp], {
			animate: true,
			title:'',
			grid: {
				background: '#FFFFFF',
				drawBorder: true,
				shadow: false,
				borderWidth: 0.75,
				borderColor: '#FF9900',
				gridLineColor: '#EAEAEA',
			},
			seriesDefaults: {
				lineWidth: 5,
				color: '#FF9900',
				markerOptions: {
					size: 4,
					color: '#FFFFFF',
				},
				rendererOptions: {
				  	smooth: true
			  	}
			},
			axes:{
				xaxis:{
					renderer:$.jqplot.DateAxisRenderer,
					min: sunday,
					tickOptions:{
						formatString:'%b %d'
					},
					tickInterval: '1 week',
					label:''
				},
				yaxis:{
					label:'',
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					min: min,      // minimum numerical value of the axis.  Determined automatically.
					max: max
				}
			},
			highlighter: {
				show: true,
				sizeAdjust: 7.5
			},
			cursor: {
				show: false
			}
		});
	} else {
		plot1 = null;
	}
	
	plotData2 = $("#plotData2", ui.toPage).val();
	if (typeof plotData2 != "undefined") {
		tmp = JSON.parse(plotData2);
		if (tmp[0][0] != null) {
			var startDate = tmp[0][0].split(' ');
			var dayName = getDayName(startDate[0], "/");
			var dayIndex = parseInt(dayNameIndex[dayName]);
			var ektavar = 0 - dayNameIndex[dayName];
			
			var ymd = startDate[0].split('/');
			var d = new Date(ymd[0], ymd[1], ymd[2]);
			d.setDate(d.getDate() - Math.abs(ektavar));
			var sunday = d.getFullYear() + '/' + d.getMonth() + '/' + d.getDate();
			
			min = (parseInt(tmp[0][1]) - 25 >= 0 ? parseInt(tmp[0][1]) - 25 : 0);
			max = parseInt(tmp[0][1]) + 25;
			
			for (var i = 1; i < tmp.length; i++) {
				if ((parseInt(tmp[i][1]) - 25) < min && (parseInt(tmp[i][1]) - 25) >= 0) {
					min = parseInt(tmp[i][1]) - 25;
				}
				
				if ((parseInt(tmp[i][1]) + 5) > max) {
					max = parseInt(tmp[i][1]) + 5;
				}
			}
			
			if ( tmp.length <= 7 ) {
				var inter = '1 day';
			} else if ( tmp.length > 7 && tmp.length <= 30 ) {
				var inter = '1 week';
			} else if ( tmp.length > 30 && tmp.length <= 365 ) {
				var inter = '1 month';
			} else if ( tmp.length > 365 ) {
				var inter = '1 year';
			}
		} else {
			min = 0;
			max = 2;
		}
		console.log(min, max);
		
		// In the case where we initially loaded a report card, and then navigate to another report card, specifying a context is not enough for jqPlot to properly apply to the toPage rather than the previous page. So, remove the previous page's chart first.
		if (plot2 != null) {
			plot2.remove();
		}
		
		plot2 = $('#chart2', ui.toPage);
		plot2.jqplot([tmp], {
			animate: true,
			title:'',
			grid: {
				background: '#FFFFFF',
				drawBorder: true,
				shadow: false,
				borderWidth: 0.75,
				borderColor: '#33CC00',
				gridLineColor: '#EAEAEA',
			},
			seriesDefaults: {
				lineWidth: 5,
				color: '#33CC00',
				markerOptions: {
					size: 4,
					color: '#FFFFFF',
				},
				rendererOptions: {
				  	smooth: true
			  	}
			},
			axes:{
				xaxis:{
					renderer:$.jqplot.DateAxisRenderer,
					min: sunday,
					tickOptions:{
						formatString:'%b %d'
					},
					tickInterval: '1 week',
					label:''
				},
				yaxis:{
					label:'',
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					max: max,      // minimum numerical value of the axis.  Determined automatically.
					min: min
				}
			},
			highlighter: {
				show: true,
				sizeAdjust: 7.5
			},
			cursor: {
				show: false
			}
		});
	} else {
		plot2 = null;
	}
	
	plotData3 = $("#plotData3", ui.toPage).val();
	if (typeof plotData3 != "undefined") {
		tmp = JSON.parse(plotData3);
		if (tmp[0][0] != null) {
			var startDate = tmp[0][0].split(' ');
			var dayName = getDayName(startDate[0], "/");
			var dayIndex = parseInt(dayNameIndex[dayName]);
			var ektavar = 0 - dayNameIndex[dayName];
			
			var ymd = startDate[0].split('/');
			var d = new Date(ymd[0], ymd[1], ymd[2]);
			d.setDate(d.getDate() - Math.abs(ektavar));
			var sunday = d.getFullYear() + '/' + d.getMonth() + '/' + d.getDate();
			
			min = (parseInt(tmp[0][1]) - 25 >= 0 ? parseInt(tmp[0][1]) - 25 : 0);
			max = parseInt(tmp[0][1]) + 25;
			
			for (var i = 1; i < tmp.length; i++) {
				if ((parseInt(tmp[i][1]) - 25) < min && (parseInt(tmp[i][1]) - 25) >= 0) {
					min = parseInt(tmp[i][1]) - 25;
				}
				
				if ((parseInt(tmp[i][1]) + 25) > max) {
					max = parseInt(tmp[i][1]) + 25;
				}
			}
			
			if ( tmp.length <= 7 ) {
				var inter = '1 day';
			} else if ( tmp.length > 7 && tmp.length <= 30 ) {
				var inter = '1 week';
			} else if ( tmp.length > 30 && tmp.length <= 365 ) {
				var inter = '1 month';
			} else if ( tmp.length > 365 ) {
				var inter = '1 year';
			}
		} else {
			min = 0;
			max = 2;
		}
		console.log(min, max);
		
		
		// In the case where we initially loaded a report card, and then navigate to another report card, specifying a context is not enough for jqPlot to properly apply to the toPage rather than the previous page. So, remove the previous page's chart first.
		if (plot3 != null) {
			plot3.remove();
		}
		
		plot3 = $('#chart3', ui.toPage);
		plot3.jqplot([tmp], {
			animate: true,
			title:'',
			grid: {
				background: '#FFFFFF',
				drawBorder: true,
				shadow: false,
				borderWidth: 0.75,
				borderColor: '#509FE9',
				gridLineColor: '#EAEAEA',
			},
			seriesDefaults: {
				lineWidth: 5,
				color: '#509FE9',
				markerOptions: {
					size: 4,
					color: '#FFFFFF',
				},
				rendererOptions: {
				  	smooth: true
			  	}
			},
			axes:{
				xaxis:{
					renderer:$.jqplot.DateAxisRenderer,
					min: sunday,
					tickOptions:{
						formatString:'%b %d'
					},
					tickInterval: '1 week',
					label:''
				},
				yaxis:{
					label:'',
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					max: max,      // minimum numerical value of the axis.  Determined automatically.
					min: min
				}
			},
			highlighter: {
				show: true,
				sizeAdjust: 7.5
			},
			cursor: {
				show: false
			}
		});
	} else {
		plot3 = null;
	}
	
	// Animate those content boxes!
	var duration = 300;

	$('.content-box', ui.toPage).each(function(i) {
		$(this).delay( i*(duration/2) ).animate({"margin-top": "10px", opacity: 1}, duration);
	});
});

// Settings Page Events
$(document).on("change", "#settings_page input[name='privacy_flags']", updatePrivacyFlags);
$(document).on("change", "#settings_page input[name='email_flags']", updateEmailFlags);

function postToFeed(user, message) {
	FB.ui({
		method: 'feed',
		link: 'https://dev.metarank.com/',
		caption: 'The Human Hierarchy Project',
		description: user + " ranked " + message + "!\nWhat will you rank?"
	}, function(response){
		if (response && !response.error_code) {
			ga('send', 'social', 'facebook', 'post', 'https://dev.metarank.com/');
		}
	});
}

/* Update Privacy Flags Related */
function updatePrivacyFlags(evt) {
	var privacyFlags = 0;
	
	privacyFlagInputs = $("input[name='privacy_flags']:checked");
	for (var i = 0; i < privacyFlagInputs.length; i++) {
		privacyFlags |= privacyFlagInputs[i].value;
	}
	$.ajax({
		url: BASE_URL + '/ajax/update_privacy_flags.php',
		data: {"privacy_flags": privacyFlags},
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					/*switch (data.commands[i].action) {
						case "command...":
							break;
					}*/
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}

function addFacebookFriends(evt) {
	$.ajax({
		url: BASE_URL + '/ajax/import_facebook_friends.php',
		type: "GET",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
					window.location.href=BASE_URL + '/account/friends/';
				}, 3000);
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
					window.location.href=BASE_URL + '/account/friends/';
				}, 3000);
				
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				window.location.href=BASE_URL + '/account/friends/';
			}, 3000);
		}
	});
	//evt.preventDefault();
	return false;
}

function cancelFriendRequest(button) {
	var parentRow = $(button).closest("tr");
	$.ajax({
		url: BASE_URL + '/ajax/delete_sent_friend_request.php',
		data: {"friend": $(button).data("friend")},
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				
				if (parentRow.siblings().length == 0) {
					$(parentRow).parent().append("<tr><td colspan='3' style='text-align: center;'>No pending requests.</td></tr>");
				}
				
				$(parentRow).stop(true, true).fadeOut({queue: false}).children('td, th').animate({ padding: 0 }).wrapInner('<div class="animation-fix" />').children().slideUp(function() {$(this).closest('tr').remove();});
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	return false;
}

function declineFriendRequest(button) {
	var parentRow = $(button).closest("tr");
	$.ajax({
		url: BASE_URL + '/ajax/delete_pending_friend_request.php',
		data: {"friend": $(button).data("friend")},
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				
				if (parentRow.siblings().length == 0) {
					$(parentRow).parent().append("<tr><td colspan='4' style='text-align: center;'>No received requests.</td></tr>");
				}
				
				$(parentRow).stop(true, true).fadeOut({queue: false}).children('td, th').animate({ padding: 0 }).wrapInner('<div class="animation-fix" />').children().slideUp(function() {$(this).closest('tr').remove();});
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	return false;
}

function acceptFriendRequest(button) {
	var parentRow = $(button).closest("tr");
	$.ajax({
		url: BASE_URL + '/ajax/accept_friend_request.php',
		data: {"friend": $(button).data("friend")},
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				
				if (parentRow.siblings().length == 0) {
					$(parentRow).parent().append("<tr><td colspan='4' style='text-align: center;'>No received requests.</td></tr>");
				}
				
				friends = $("#friends");
				if (friends.length == 1) {
					friends.empty();
				}
				
				newRow = document.createElement("tr");
				
				column1 = document.createElement("td");
				
				oldImage = $("img", parentRow);
				column1.appendChild(oldImage[0]);
				newRow.appendChild(column1);
				
				column2 = document.createElement("td");
				column2.innerText = oldImage.attr("alt");
				newRow.appendChild(column2);
				
				column3 = document.createElement("td");
				deleteButton = document.createElement("input");
				deleteButton.setAttribute("type", "button");
				deleteButton.setAttribute("data-friend", $("input[type='button']", parentRow).data("friend"));
				deleteButton.setAttribute("value", "Delete");
				deleteButton.setAttribute("onclick", "deleteFriend(this)");
				
				column3.appendChild(deleteButton);
				newRow.appendChild(column3);
				
				friends.append(newRow);
				$(deleteButton).button();
				$(newRow).children('td, th').animate({padding: 0}).wrapInner('<div class="animation-fix" />').children().animate({"padding-top": "4px", opacity: 1}, 300);
				
				$(parentRow).stop(true, true).fadeOut({queue: false}).children('td, th').animate({ padding: 0 }).wrapInner('<div class="animation-fix" />').children().slideUp(function() {$(this).closest('tr').remove();});
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	return false;
}

function addFriend(button) {
	var parentRow = $(button).closest("tr");
	$.ajax({
		url: BASE_URL + '/ajax/send_friend_request.php',
		data: {"friend": $(button).data("friend")},
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				pendingSentFriendRequests = $("#pending_sent_friend_requests");
				if (pendingSentFriendRequests.length == 1) {
					pendingSentFriendRequests.empty();
				}
				
				newRow = document.createElement("tr");
				
				column1 = document.createElement("td");
				
				oldImage = $("img", parentRow);
				column1.appendChild(oldImage[0]);
				newRow.appendChild(column1);
				
				column2 = document.createElement("td");
				column2.innerText = oldImage.attr("alt");
				newRow.appendChild(column2);
				
				column3 = document.createElement("td");
				cancelButton = document.createElement("input");
				cancelButton.setAttribute("type", "button");
				cancelButton.setAttribute("data-friend", $("input[type='button']", parentRow).data("friend"));
				cancelButton.setAttribute("value", "Cancel");
				cancelButton.setAttribute("onclick", "cancelFriendRequest(this)");
				
				column3.appendChild(cancelButton);
				newRow.appendChild(column3);
				
				pendingSentFriendRequests.append(newRow);
				$(cancelButton).button();
				$(newRow).children('td, th').animate({padding: 0}).wrapInner('<div class="animation-fix" />').children().animate({"padding-top": "4px", opacity: 1}, 300);
				
				$(parentRow).stop(true, true).fadeOut({queue: false}).children('td, th').animate({ padding: 0 }).wrapInner('<div class="animation-fix" />').children().slideUp(function() {$(this).closest('tr').remove();});
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	return false;
}

function deleteFriend(button) {
	var parentRow = $(button).closest("tr");
	$.ajax({
		url: BASE_URL + '/ajax/delete_friend.php',
		data: {"friend": $(button).data("friend")},
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				
				if (parentRow.siblings().length == 0) {
					$(parentRow).parent().append("<tr><td colspan='3' style='text-align: center;'>You have no friends. Try searching for some!</td></tr>");
				}
				
				$(parentRow).stop(true, true).fadeOut({queue: false}).children('td, th').animate({ padding: 0 }).wrapInner('<div class="animation-fix" />').children().slideUp(function() {$(this).closest('tr').remove();});
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	return false;
}

function emailSearch() {
	$.ajax({
		url: BASE_URL + '/ajax/email_search.php',
		data: $("#email_search").serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				
				// First empty previous search results
				searchResults = $("#email_search_results");
				searchResults.empty();
				
				newRow = document.createElement("tr");
				if (data.commands.length == 0) {
					column = document.createElement("td");
					column.setAttribute("colspan", "3");
					column.innerText = "No users found with that email address.";
					newRow.appendChild(column);
					searchResults.append(newRow);
					$(newRow).stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				} else {
					var duration = 300;
					
					for (var i = 0; i < data.commands.length; i++) {
						switch (data.commands[i].action) {
							case "addEmailSearchResult":
								column1 = document.createElement("td");
								profileImage = document.createElement("img");
								profileImage.setAttribute("src", data.commands[i].params[3]);
								profileImage.setAttribute("alt", data.commands[i].params[1] + " " + data.commands[i].params[2]);
								
								column1.appendChild(profileImage);
								newRow.appendChild(column1);
								
								column2 = document.createElement("td");
								column2.innerText = data.commands[i].params[1] + " " + data.commands[i].params[2];
								newRow.appendChild(column2);
								
								column3 = document.createElement("td");
								addButton = document.createElement("input");
								addButton.setAttribute("type", "button");
								addButton.setAttribute("data-friend", data.commands[i].params[0]);
								addButton.setAttribute("value", "Add");
								addButton.setAttribute("onclick", "addFriend(this)");
								
								
								column3.appendChild(addButton);
								newRow.appendChild(column3);
								
								searchResults.append(newRow);
								$(addButton).button();
								$(newRow).delay(i * (duration%2)).children('td, th').animate({padding: 0}).wrapInner('<div class="animation-fix" />').children().animate({"padding-top": "4px", opacity: 1}, duration);
								break;
						}
					}
				}
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	return false;
}

function updateEmailFlags(evt) {
	var emailFlags = 0;
	
	emailFlagInputs = $("input[name='email_flags']:checked");
	for (var i = 0; i < emailFlagInputs.length; i++) {
		emailFlags |= emailFlagInputs[i].value;
	}
	$.ajax({
		url: BASE_URL + '/ajax/update_email_flags.php',
		data: {"email_flags": emailFlags},
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					/*switch (data.commands[i].action) {
						case "command...":
							break;
					}*/
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}
function updateProfilePicture(options) {
	$.ajax({
		url: BASE_URL + '/ajax/update_profile_picture.php',
		data: {"profile_picture": options.option[0].value},
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					switch (data.commands[i].action) {
						case "replaceImage":
							$(".user-image").attr("src", data.commands[i].params[0]);
							break;
					}
				}
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					/*switch (data.commands[i].action) {
						case "command...":
							break;
					}*/
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
}

function formatLocalDate(date) {
	tzo = -date.getTimezoneOffset(),
	dif = tzo >= 0 ? '+' : '-',
	pad = function(num) {
		norm = Math.abs(Math.floor(num));
		return (norm < 10 ? '0' : '') + norm;
	};
	return date.getFullYear() 
		+ '-' + pad(date.getMonth()+1)
		+ '-' + pad(date.getDate())
		+ 'T00:00:00';/*
		+ 'T' + pad(date.getHours())
		+ ':' + pad(date.getMinutes()));*/
}

function updateAddressCountry(evt) {
	var addressFieldNumber = this.id.slice(-4);
	var curFieldSet = $("fieldset[data-addressfield-number=" + addressFieldNumber + "]", this.form);
	var data = curFieldSet.serialize();
	data = data.replace(/%5B%5D=/g, "=");
	data += "&addressFieldNumber=" + addressFieldNumber + "&" + $(this.form.removeFields).serialize();
	$.ajax({
		url: BASE_URL + '/ajax/update_address_country.php',
		data: data,
		type: "POST",
		success: function(theForm, addressHTML, textStatus, jqXHR) {
			curFieldSet.replaceWith(addressHTML);
			$("fieldset[data-addressfield-number=" + addressFieldNumber + "]", theForm).trigger("create");
		}.bind(window, this.form),
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Country selection error: " + errorThrown);
		}
	});
}

function updateScheduledPerks(evt) {
	$.ajax({
		url: BASE_URL + '/ajax/update_scheduled_perks.php',
		data: $(this).serialize(),
		type: "POST",
		success: function(scheduledPerksHTML, textStatus, jqXHR) {
			$("#scheduled_perks").empty().append(scheduledPerksHTML);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Business selection error: " + errorThrown);
		}
	});
}

function updateActivePerks(evt) {
	$.ajax({
		url: BASE_URL + '/ajax/update_active_perks.php',
		data: $(this).serialize(),
		type: "POST",
		success: function(activePerksHTML, textStatus, jqXHR) {
			$("#active_perks").empty().append(activePerksHTML);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Business selection error: " + errorThrown);
		}
	});
}

function updateCompletedPerks(evt) {
	$.ajax({
		url: BASE_URL + '/ajax/update_completed_perks.php',
		data: $(this).serialize(),
		type: "POST",
		success: function(completedPerksHTML, textStatus, jqXHR) {
			$("#completed_perks").empty().append(completedPerksHTML);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Business selection error: " + errorThrown);
		}
	});
}

function updateScheduledPromotions(evt) {
	$.ajax({
		url: BASE_URL + '/ajax/update_scheduled_promotions.php',
		data: $(this).serialize(),
		type: "POST",
		success: function(scheduledPromotionsHTML, textStatus, jqXHR) {
			$("#scheduled_promotions").empty().append(scheduledPromotionsHTML);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Business selection error: " + errorThrown);
		}
	});
}

function updateActivePromotions(evt) {
	$.ajax({
		url: BASE_URL + '/ajax/update_active_promotions.php',
		data: $(this).serialize(),
		type: "POST",
		success: function(activePromotionsHTML, textStatus, jqXHR) {
			$("#active_promotions").empty().append(activePromotionsHTML);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Business selection error: " + errorThrown);
		}
	});
}

function updateCompletedPromotions(evt) {
	$.ajax({
		url: BASE_URL + '/ajax/update_completed_promotions.php',
		data: $(this).serialize(),
		type: "POST",
		success: function(completedPromotionsHTML, textStatus, jqXHR) {
			$("#completed_promotions").empty().append(completedPromotionsHTML);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Business selection error: " + errorThrown);
		}
	});
}

$(document).ready(function(evt) {
	$( "[data-role='header'], [data-role='footer']" ).toolbar();
});

var RecaptchaOptions = {theme:'clean'};
function showRecaptcha(element) {
	Recaptcha.create("6LekgwgTAAAAAGA7JT_0NrNUJvS23iAr-CTj1jje", element, RecaptchaOptions);
}
function reloadRecaptcha() {
	Recaptcha.reload();
}
function removeRecaptcha() {
	Recaptcha.destroy();
}


/* Update Business Related */
function updateBusiness(evt) {
	var geocodeAddresses = new Array();
				
	var addressFieldsets = $("#edit_business_page fieldset");
	addressFieldsets.each(function(index) {
		var address = "";
		fields = $("input[type=text]:enabled, select:enabled", this);
		for (var i = 0; i < fields.length; i++) {
			curField = $(fields[i]);
			if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
				address += ", ";
			} else {
				address += " ";
			}
			address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
		}
		
		if (fields.length != 0) {
			curLatitude = $("input[name='latitude[]']", this);
			curLongitude = $("input[name='longitude[]']", this);
			
			runGeocode = false;
			
			if (curLatitude.length == 0) {
				latitudeElem = document.createElement("input");
				latitudeElem.setAttribute("type", "hidden");
				latitudeElem.setAttribute("name", "latitude[]");
				this.appendChild(latitudeElem);
				curLatitude = $(latitudeElem);
				runGeocode = true;
			}
			
			if (curLongitude.length == 0) {
				longitudeElem = document.createElement("input");
				longitudeElem.setAttribute("type", "hidden");
				longitudeElem.setAttribute("name", "longitude[]");
				this.appendChild(longitudeElem);
				curLongitude = $(longitudeElem);
				runGeocode = true;
			}
			
			if (!runGeocode) {
				if (curLatitude.val() == "" || curLongitude.val() == "") {
					runGeocode = true;
				}
			}
			
			if (runGeocode) {
				geocodeAddresses.push({address: address, fieldSet: this});
			}
		}
		
		if (index == addressFieldsets.length - 1) {
			if (geocodeAddresses.length > 0) {
				var runningGeocodes = geocodeAddresses.length;
				for (var i = 0; i < geocodeAddresses.length; i++) {
					geocodeInformation = geocodeAddresses[i];
					
					geocoder = new google.maps.Geocoder();
					geocoder.geocode({address: geocodeInformation.address}, function(address, fieldSet, results, status) {
						curLatitude = $("input[name='latitude[]']", fieldSet);
						curLongitude = $("input[name='longitude[]']", fieldSet);
						
						if (status == google.maps.GeocoderStatus.OK) {
							if (results[0].geometry.location_type.toUpperCase() == "ROOFTOP") {
								curLatitude.val(results[0].geometry.location.lat());
								curLongitude.val(results[0].geometry.location.lng());
							} else {
								curLatitude.val("");
								curLongitude.val("");
								console.log('Geocode for "' + address + '" was not successful for the following reason: Location type was not specific.');
							}
						} else {
							curLatitude.val("");
							curLongitude.val("");
							console.log('Geocode for "' + address + '" was not successful for the following reason: ' + status);
						}
						
						runningGeocodes--;
						
						if (runningGeocodes == 0) {
							theForm = $(fieldSet).closest("form");
							latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
							for (var j = 0; j < latLngs.length; j++) {
								if (latLngs[j].value == "") {
									var address = "";
									fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
									for (var k = 0; k < fields.length; k++) {
										curField = $(fields[k]);
										if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
											address += ", ";
										} else {
											address += " ";
										}
										address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
									}
									var statusMessage = $("#status_message");
									statusMessage.removeClass("info");
									statusMessage.addClass("error");
									statusMessage.text("Invalid address: " + address);
									statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
									setTimeout(function() {
										statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
									}, 3000);
									return false;
								}
							}
							$.ajax({
								url: theForm[0].action,
								data: theForm.serialize(),
								type: "POST",
								success: function(data, textStatus, jqXHR) {
									if (data.success) {
										var statusMessage = $("#status_message");
										statusMessage.removeClass("error");
										statusMessage.addClass("info");
										statusMessage.text(data.message);
										statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
										setTimeout(function() {
											statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
										}, 3000);
									} else {
										var statusMessage = $("#status_message");
										statusMessage.removeClass("info");
										statusMessage.addClass("error");
										statusMessage.text(data.message);
										statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
										setTimeout(function() {
											statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
										}, 3000);
										for (var i = 0; i < data.commands.length; i++) {
											/*switch (data.commands[i].action) {
												case "command...":
													break;
											}*/
										}
									}
								},
								error: function(jqXHR, textStatus, errorThrown) {
									var statusMessage = $("#status_message");
									statusMessage.removeClass("info");
									statusMessage.addClass("error");
									statusMessage.text(errorThrown);
									statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
									setTimeout(function() {
										statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
									}, 3000);
								}
							});
						}
					}.bind(window, geocodeInformation.address, geocodeInformation.fieldSet));
				}
			} else {
				theForm = $(this).closest("form");
				latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
				for (var i = 0; i < latLngs.length; i++) {
					if (latLngs[i].value == "") {
						var address = "";
						fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
						for (var i = 0; i < fields.length; i++) {
							curField = $(fields[i]);
							if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
								address += ", ";
							} else {
								address += " ";
							}
							address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
						}
						var statusMessage = $("#status_message");
						statusMessage.removeClass("info");
						statusMessage.addClass("error");
						statusMessage.text("Invalid address: " + address);
						statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
						setTimeout(function() {
							statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						}, 3000);
						return false;
					}
				}
				$.ajax({
					url: theForm[0].action,
					data: theForm.serialize(),
					type: "POST",
					success: function(data, textStatus, jqXHR) {
						if (data.success) {
							var statusMessage = $("#status_message");
							statusMessage.removeClass("error");
							statusMessage.addClass("info");
							statusMessage.text(data.message);
							statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							setTimeout(function() {
								statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
							}, 3000);
						} else {
							var statusMessage = $("#status_message");
							statusMessage.removeClass("info");
							statusMessage.addClass("error");
							statusMessage.text(data.message);
							statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							setTimeout(function() {
								statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
							}, 3000);
							for (var i = 0; i < data.commands.length; i++) {
								/*switch (data.commands[i].action) {
									case "command...":
										break;
								}*/
							}
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						var statusMessage = $("#status_message");
						statusMessage.removeClass("info");
						statusMessage.addClass("error");
						statusMessage.text(errorThrown);
						statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
						setTimeout(function() {
							statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						}, 3000);
					}
				});
				
			}
		}
	});
	
	evt.preventDefault();
	return false;
}

/* Add Business Related */
function addBusiness(evt) {
	var geocodeAddresses = new Array();
				
	var addressFieldsets = $("#add_business_page fieldset");
	addressFieldsets.each(function(index) {
		var address = "";
		fields = $("input[type=text]:enabled, select:enabled", this);
		for (var i = 0; i < fields.length; i++) {
			curField = $(fields[i]);
			if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
				address += ", ";
			} else {
				address += " ";
			}
			address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
		}
		
		if (fields.length != 0) {
			curLatitude = $("input[name='latitude[]']", this);
			curLongitude = $("input[name='longitude[]']", this);
			
			runGeocode = false;
			
			if (curLatitude.length == 0) {
				latitudeElem = document.createElement("input");
				latitudeElem.setAttribute("type", "hidden");
				latitudeElem.setAttribute("name", "latitude[]");
				this.appendChild(latitudeElem);
				curLatitude = $(latitudeElem);
				runGeocode = true;
			}
			
			if (curLongitude.length == 0) {
				longitudeElem = document.createElement("input");
				longitudeElem.setAttribute("type", "hidden");
				longitudeElem.setAttribute("name", "longitude[]");
				this.appendChild(longitudeElem);
				curLongitude = $(longitudeElem);
				runGeocode = true;
			}
			
			if (!runGeocode) {
				if (curLatitude.val() == "" || curLongitude.val() == "") {
					runGeocode = true;
				}
			}
			
			if (runGeocode) {
				geocodeAddresses.push({address: address, fieldSet: this});
			}
		}
		
		if (index == addressFieldsets.length - 1) {
			if (geocodeAddresses.length > 0) {
				var runningGeocodes = geocodeAddresses.length;
				for (var i = 0; i < geocodeAddresses.length; i++) {
					geocodeInformation = geocodeAddresses[i];
					
					geocoder = new google.maps.Geocoder();
					geocoder.geocode({address: geocodeInformation.address}, function(address, fieldSet, results, status) {
						curLatitude = $("input[name='latitude[]']", fieldSet);
						curLongitude = $("input[name='longitude[]']", fieldSet);
						
						if (status == google.maps.GeocoderStatus.OK) {
							if (results[0].geometry.location_type.toUpperCase() == "ROOFTOP") {
								curLatitude.val(results[0].geometry.location.lat());
								curLongitude.val(results[0].geometry.location.lng());
							} else {
								curLatitude.val("");
								curLongitude.val("");
								console.log('Geocode for "' + address + '" was not successful for the following reason: Location type was not specific.');
							}
						} else {
							curLatitude.val("");
							curLongitude.val("");
							console.log('Geocode for "' + address + '" was not successful for the following reason: ' + status);
						}
						
						runningGeocodes--;
						
						if (runningGeocodes == 0) {
							theForm = $(fieldSet).closest("form");
							latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
							for (var j = 0; j < latLngs.length; j++) {
								if (latLngs[j].value == "") {
									var address = "";
									fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
									for (var k = 0; k < fields.length; k++) {
										curField = $(fields[k]);
										if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
											address += ", ";
										} else {
											address += " ";
										}
										address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
									}
									var statusMessage = $("#status_message");
									statusMessage.removeClass("info");
									statusMessage.addClass("error");
									statusMessage.text("Invalid address: " + address);
									statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
									setTimeout(function() {
										statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
									}, 3000);
									return false;
								}
							}
							$.ajax({
								url: theForm[0].action,
								data: theForm.serialize(),
								type: "POST",
								success: function(data, textStatus, jqXHR) {
									if (data.success) {
										var statusMessage = $("#status_message");
										statusMessage.removeClass("error");
										statusMessage.addClass("info");
										statusMessage.text(data.message);
										statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
										setTimeout(function() {
											statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
										}, 3000);
									} else {
										var statusMessage = $("#status_message");
										statusMessage.removeClass("info");
										statusMessage.addClass("error");
										statusMessage.text(data.message);
										statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
										setTimeout(function() {
											statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
										}, 3000);
										for (var i = 0; i < data.commands.length; i++) {
											/*switch (data.commands[i].action) {
												case "command...":
													break;
											}*/
										}
									}
								},
								error: function(jqXHR, textStatus, errorThrown) {
									var statusMessage = $("#status_message");
									statusMessage.removeClass("info");
									statusMessage.addClass("error");
									statusMessage.text(errorThrown);
									statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
									setTimeout(function() {
										statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
									}, 3000);
								}
							});
						}
					}.bind(window, geocodeInformation.address, geocodeInformation.fieldSet));
				}
			} else {
				theForm = $(this).closest("form");
				latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
				for (var i = 0; i < latLngs.length; i++) {
					if (latLngs[i].value == "") {
						var address = "";
						fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
						for (var i = 0; i < fields.length; i++) {
							curField = $(fields[i]);
							if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
								address += ", ";
							} else {
								address += " ";
							}
							address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
						}
						var statusMessage = $("#status_message");
						statusMessage.removeClass("info");
						statusMessage.addClass("error");
						statusMessage.text("Invalid address: " + address);
						statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
						setTimeout(function() {
							statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						}, 3000);
						return false;
					}
				}
				$.ajax({
					url: theForm[0].action,
					data: theForm.serialize(),
					type: "POST",
					success: function(data, textStatus, jqXHR) {
						if (data.success) {
							var statusMessage = $("#status_message");
							statusMessage.removeClass("error");
							statusMessage.addClass("info");
							statusMessage.text(data.message);
							statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							setTimeout(function() {
								statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
							}, 3000);
						} else {
							var statusMessage = $("#status_message");
							statusMessage.removeClass("info");
							statusMessage.addClass("error");
							statusMessage.text(data.message);
							statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							setTimeout(function() {
								statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
							}, 3000);
							for (var i = 0; i < data.commands.length; i++) {
								/*switch (data.commands[i].action) {
									case "command...":
										break;
								}*/
							}
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						var statusMessage = $("#status_message");
						statusMessage.removeClass("info");
						statusMessage.addClass("error");
						statusMessage.text(errorThrown);
						statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
						setTimeout(function() {
							statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						}, 3000);
					}
				});
				
			}
		}
	});
	
	evt.preventDefault();
	return false;
}

/* Add User Related */
function addUser(evt) {
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					/*switch (data.commands[i].action) {
						case "command...":
							break;
					}*/
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}

/* Update User Related */
function updateUser(evt) {
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					/*switch (data.commands[i].action) {
						case "command...":
							break;
					}*/
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}

/* Change Credentials Related */
function changeCredentials(evt) {
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					switch (data.commands[i].action) {
						case "removeCaptcha":
							removeRecaptcha();
							break;
					}
				}
				$("#change_credentials_page input, #change_credentials_page select").val("");
				$("#change_credentials_page select").selectmenu("refresh");
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					switch (data.commands[i].action) {
						case "loadCaptcha":
							var elevationCaptcha = $("#elevation_captcha")[0];
							if (elevationCaptcha.innerHTML == "") {
								showRecaptcha(elevationCaptcha);
							} else {
								reloadRecaptcha();
							}
							break;
						case "removeCaptcha":
							removeRecaptcha();
							break;
					}
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}

/* Profile Update Related */
function updateProfile(evt) {
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					/*switch (data.commands[i].action) {
						case "command...":
							break;
					}*/
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}

/* Forgot Password Related */
function resetPassword(evt) {
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				 if (data.message != '') {
					var statusMessage = $("#status_message");
					statusMessage.removeClass("error");
					statusMessage.addClass("info");
					statusMessage.text(data.message);
					statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
					if (data.commands.length == 0 || data.commands[0].action != "removeFields") {
						setTimeout(function() {
							statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						}, 3000);
					}
				}
				for (var i = 0; i < data.commands.length; i++) {
					switch (data.commands[i].action) {
						case "showPhase2":
							$("#password_reset_phase2 p").text(data.commands[i].params[0]);
							$("#password_reset_phase2 input").textinput("enable");
							$("#password_reset_phase1 input").textinput("disable");
							$("#password_reset_phase2 input").val("");
							$("#password_reset_phase1").stop(true, true).fadeOut({queue: false}).slideUp();
							$("#password_reset_phase2").stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							break;
						case "removeFields":
							$("#password_reset_phase2").stop(true, true).fadeOut({queue: false}).slideUp();
							$("#navigation_box input[type=submit]").parent().fadeOut();
							$("#navigation_box .ui-btn:first-of-type").css('margin-right', 'auto');
							break;
					}
				}
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					switch (data.commands[i].action) {
						case "showPhase1":
							$("#password_reset_phase2 p").text("");
							$("#password_reset_phase2 input").textinput("disable");
							$("#password_reset_phase1 input").textinput("enable");
							$("#password_reset_phase1 input").val("");
							$("#password_reset_phase2").stop(true, true).fadeOut({queue: false}).slideUp();
							$("#password_reset_phase1").stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							break;
					}
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}

function resetPasswordCompletion(evt) {
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				 if (data.message != '') {
					var statusMessage = $("#status_message");
					statusMessage.removeClass("error");
					statusMessage.addClass("info");
					statusMessage.text(data.message);
					statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
					if (data.commands.length == 0 || data.commands[0].action != "removeFields") {
						setTimeout(function() {
							statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						}, 3000);
					}
				}
				for (var i = 0; i < data.commands.length; i++) {
					switch (data.commands[i].action) {
						case "showPhase2":
							$("#password_reset_completion_phase2 input").textinput("enable");
							$("#password_reset_completion_phase1 input").textinput("disable");
							$("#password_reset_completion_phase1").stop(true, true).fadeOut({queue: false}).slideUp();
							$("#password_reset_completion_phase2").stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							break;
						case "removeFields":
							$("#password_reset_completion_phase2").stop(true, true).fadeOut({queue: false}).slideUp();
							$("#navigation_box input[type=submit]").parent().fadeOut();
							$("#navigation_box .ui-btn:first-of-type").css('margin-right', 'auto');
							break;
					}
				}
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					switch (data.commands[i].action) {
						case "showPhase1":
							$("#password_reset_completion_phase2 input").textinput("disable");
							$("#password_reset_completion_phase1 input").textinput("enable");
							$("#password_reset_completion_phase2").stop(true, true).fadeOut({queue: false}).slideUp();
							$("#password_reset_completion_phase1").stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							break;
					}
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}

/* Registration Related */
function register(evt) {
	evt.preventDefault();
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if(data.success) {
				ga('send', 'event', 'Process', 'Register', (typeof evt.target.elements["service"] != "undefined" ? evt.target.elements["service"].value : "email"), 0);
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				//$(document.body).pagecontainer("change", BASE_URL + '/');
				window.location.href=BASE_URL + '/';
			} else {
				ga('send', 'event', 'Process', 'Register', (typeof evt.target.elements["service"] != "undefined" ? evt.target.elements["service"].value : "email"), 1);
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for(var i=0;i<data.commands.length;i++) {
					/*switch (data.commands[i].action) {
						case "command...":
							break;
					}*/
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			ga('send', 'event', 'Process', 'Register', (typeof evt.target.elements["service"] != "undefined" ? evt.target.elements["service"].value : "email"), 2);
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});

	return false;
}

/* Login Related */
function login(evt) {
	$("#navigation_box input[type=submit]").parent().fadeOut();
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				ga('send', 'event', 'Process', 'Login', 'email', 0);
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
					var loginRedirectURL = $("#login_redirect_url");
					if (loginRedirectURL.val() == BASE_URL + "/account/promotions/new/" || loginRedirectURL.val() == BASE_URL + "/account/perks/new/") {
						window.location.href = loginRedirectURL.val();						
					} else {
						window.location.href = loginRedirectURL.val();	
						//$(document.body).pagecontainer("change", loginRedirectURL.val());
					}
				}, 3000);
			} else {
				ga('send', 'event', 'Process', 'Login', 'email', 1);
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					switch (data.commands[i].action) {
						case "loadCaptcha":
							var loginCaptcha = $("#login_captcha")[0];
							if (loginCaptcha.innerHTML == "") {
								showRecaptcha(loginCaptcha);
							} else {
								reloadRecaptcha();
							}
							break;
					}
				}
				$("#navigation_box input[type=submit]").parent().fadeIn();
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			ga('send', 'event', 'Process', 'Login', 'email', 2);
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	
	evt.preventDefault();
	return false;
}

/* New Promotion Related */
function getLocalTimezone(date) {
	tzo = -date.getTimezoneOffset(),
	dif = tzo >= 0 ? '+' : '-',
	pad = function(num) {
		norm = Math.abs(Math.floor(num));
		return (norm < 10 ? '0' : '') + norm;
	};
	return dif + pad(tzo / 60) + ':' + pad(tzo % 60);
}

function handleMSForm(e) {
	var next = "";
	
	var pageID = $(e.target).closest("div[data-role=page]").attr("id");
	
	// Validate...
	// If we got this far, we are either valid according to HTML5, or the browser does not support HTML5 form validation...
	if (typeof e.target.checkValidity !== "function") {
		var cancel = false;
		var message;
		if (pageID == "new_promotion_step2") {
			if ($("#promotion_name").val() == "") {
				cancel = true;
				message = "Promotion name is required.";
			} else if ($("#promotion_description").val() == "") {
				cancel = true;
				message = "Promotion description is required.";
			} else if ($("#promotion_value").val() == "") {
				cancel = true;
				message = "Promotion value is required.";
			} else if (parseFloat($("#promotion_value").val()).toFixed(2) == "") {
				cancel = true;
				message = "Promotion value must be a number to two decimal places.";
			} else if ($("#promotion_max_recipients").val() == "") {
				cancel = true;
				message = "Promotion maximum recipients is required.";
			} else if (parseInt($("#promotion_max_recipients").val()) == "") {
				cancel = true;
				message = "Promotion maximum recipients must be a positive number.";
			}
		} else if (pageID == "new_promotion_step3") {
			if ($("#eligibility_distance").val == "") {
				cancel = true;
				message = "Eligibility distance is required.";
			} else if (parseInt($("#eligibility_distance").val()) == "") {
				cancel = true;
				message = "Eligibility distance must be a positive number or zero.";
			} else if ($("#eligibility_distance").val() != "0") {
				latLngs = $("#" + pageID + " input[name='latitude[]'], #" + pageID + " input[name='longitude[]']");
				for (var i = 0; i < latLngs.length; i++) {
					if (latLngs[i].value == "") {
						var address = "";
						fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
						for (var i = 0; i < fields.length; i++) {
							curField = $(fields[i]);
							if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
								address += ", ";
							} else {
								address += " ";
							}
							address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
						}
						cancel = true;
						message = "Invalid Address: " + address;
					}
				}
			}
			
			if (cancel) {
				e.preventDefault();
				alert(message);
				return false;
			}
			
			if ($("#eligibility_distance").val() != "0") {
				reviewEligibilityFromAddresses.val("");
				
				var geocodeAddresses = new Array();
				
				var addressFieldsets = $("#new_promotion_step3 fieldset");
				addressFieldsets.each(function(index) {
					var address = "";
					fields = $("input[type=text]:enabled, select:enabled", this);
					for (var i = 0; i < fields.length; i++) {
						curField = $(fields[i]);
						if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
							address += ", ";
						} else {
							address += " ";
						}
						address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
					}
					
					if (fields.length != 0) {
						reviewEligibilityFromAddresses.val(reviewEligibilityFromAddresses.val() + address + "\n");
						
						curLatitude = $("input[name='latitude[]']", this);
						curLongitude = $("input[name='longitude[]']", this);
						
						runGeocode = false;
						
						if (curLatitude.length == 0) {
							latitudeElem = document.createElement("input");
							latitudeElem.setAttribute("type", "hidden");
							latitudeElem.setAttribute("name", "latitude[]");
							this.appendChild(latitudeElem);
							curLatitude = $(latitudeElem);
							runGeocode = true;
						}
						
						if (curLongitude.length == 0) {
							longitudeElem = document.createElement("input");
							longitudeElem.setAttribute("type", "hidden");
							longitudeElem.setAttribute("name", "longitude[]");
							this.appendChild(longitudeElem);
							curLongitude = $(longitudeElem);
							runGeocode = true;
						}
						
						if (!runGeocode) {
							if (curLatitude.val() == "" || curLongitude.val() == "") {
								runGeocode = true;
							}
						}
						
						if (runGeocode) {
							geocodeAddresses.push({address: address, fieldSet: this});
						}
					}
					
					if (index == addressFieldsets.length - 1) {
						if (geocodeAddresses.length > 0) {
							var runningGeocodes = geocodeAddresses.length;
							for (var i = 0; i < geocodeAddresses.length; i++) {
								geocodeInformation = geocodeAddresses[i];
								
								geocoder = new google.maps.Geocoder();
								geocoder.geocode({address: geocodeInformation.address}, function(address, fieldSet, results, status) {
									curLatitude = $("input[name='latitude[]']", fieldSet);
									curLongitude = $("input[name='longitude[]']", fieldSet);
									
									if (status == google.maps.GeocoderStatus.OK) {
										if (results[0].geometry.location_type.toUpperCase() == "ROOFTOP") {
											curLatitude.val(results[0].geometry.location.lat());
											curLongitude.val(results[0].geometry.location.lng());
										} else {
											curLatitude.val("");
											curLongitude.val("");
											console.log('Geocode for "' + address + '" was not successful for the following reason: Location type was not specific.');
										}
									} else {
										curLatitude.val("");
										curLongitude.val("");
										console.log('Geocode for "' + address + '" was not successful for the following reason: ' + status);
									}
									
									runningGeocodes--;
									
									if (runningGeocodes == 0) {
										theForm = $(fieldSet).closest("form");
										latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
										for (var j = 0; j < latLngs.length; j++) {
											if (latLngs[j].value == "") {
												var address = "";
												fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
												for (var k = 0; k < fields.length; k++) {
													curField = $(fields[k]);
													if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
														address += ", ";
													} else {
														address += " ";
													}
													address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
												}
												alert("Invalid Address: " + address);
												return false;
											}
										}
										
										$(document.body).pagecontainer("change", "#new_promotion_step4", {transition: "flip"});
									}
								}.bind(window, geocodeInformation.address, geocodeInformation.fieldSet));
							}
						} else {
							theForm = $(this).closest("form");
							latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
							for (var i = 0; i < latLngs.length; i++) {
								if (latLngs[i].value == "") {
									var address = "";
									fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
									for (var i = 0; i < fields.length; i++) {
										curField = $(fields[i]);
										if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
											address += ", ";
										} else {
											address += " ";
										}
										address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
									}
									alert("Invalid Address: " + address);
									return false;
								}
							}
							
							$(document.body).pagecontainer("change", "#new_promotion_step4", {transition: "flip"});
						}
					}
				});
						
				e.preventDefault();
				return false;
			}
		} else if (pageID == "new_promotion_step4") {
			if ($("#checkin_option option:selected").val() != "") {
				if ($("#checkin_location").val() == "") {
					cancel = true;
					message = "Checkin Location is required.";
				} else if ($("#checkin_hashtag").val() == "") {
					cancel = true;
					message = "Checkin Hashtag is required.";
				} else if ($("#checkin_hashtag").val().indexOf(" ") != -1) {
					cancel = true;
					message = "Checkin Hashtag may not contain spaces.";
				}
			} else if ($("#post_option option:selected").val() != "") {
				if ($("#post_link").val() == "") {
					cancel = true;
					message = "Post Link is required.";
				}
			} else if ($("#like_option option:selected").val() != "") {
				if ($("#like_link").val() == "") {
					cancel = true;
					message = "Like Link is required.";
				}
			} else {
				cancel = true;
				message = "At least one option must be selected.";
			}
		} else if (pageID == "new_promotion_step5") {
			if (!$("#agrees_to_terms")[0].checked) {
				cancel = true;
				message = "You must indicate your agreement to the above terms.";
			} else if ($("#signatory").val() == "") {
				cancel = true;
				message = "Your e-signature is required.";
			}
		} else if (pageID == "new_perk_step2") {
			if ($("#perk_name").val() == "") {
				cancel = true;
				message = "Perk name is required.";
			} else if ($("#perk_description").val() == "") {
				cancel = true;
				message = "Perk description is required.";
			} else if ($("#perk_value").val() == "") {
				cancel = true;
				message = "Perk value is required.";
			} else if (parseFloat($("#perk_value").val()).toFixed(2) == "") {
				cancel = true;
				message = "Perk value must be a number to two decimal places.";
			}
			
		} else if (pageID == "new_perk_step3") {
			if ($("#eligibility_distance").val == "") {
				cancel = true;
				message = "Eligibility distance is required.";
			} else if (parseInt($("#eligibility_distance").val()) == "") {
				cancel = true;
				message = "Eligibility distance must be a positive number or zero.";
			} else if ($("#eligibility_distance").val() != "0") {
				latLngs = $("#" + pageID + " input[name='latitude[]'], #" + pageID + " input[name='longitude[]']");
				for (var i = 0; i < latLngs.length; i++) {
					if (latLngs[i].value == "") {
						var address = "";
						fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
						for (var i = 0; i < fields.length; i++) {
							curField = $(fields[i]);
							if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
								address += ", ";
							} else {
								address += " ";
							}
							address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
						}
						cancel = true;
						message = "Invalid Address: " + address;
					}
				}
			}
			
			if (cancel) {
				e.preventDefault();
				alert(message);
				return false;
			}
			
			// First run geocodes
			if ($("#eligibility_distance").val() != "0") {
				reviewEligibilityFromAddresses.val("");
				
				var geocodeAddresses = new Array();
				
				var addressFieldsets = $("#new_perk_step3 fieldset");
				addressFieldsets.each(function(index) {
					var address = "";
					fields = $("input[type=text]:enabled, select:enabled", this);
					for (var i = 0; i < fields.length; i++) {
						curField = $(fields[i]);
						if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
							address += ", ";
						} else {
							address += " ";
						}
						address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
					}
					
					if (fields.length != 0) {
						reviewEligibilityFromAddresses.val(reviewEligibilityFromAddresses.val() + address + "\n");
						
						curLatitude = $("input[name='latitude[]']", this);
						curLongitude = $("input[name='longitude[]']", this);
						
						runGeocode = false;
						
						if (curLatitude.length == 0) {
							latitudeElem = document.createElement("input");
							latitudeElem.setAttribute("type", "hidden");
							latitudeElem.setAttribute("name", "latitude[]");
							this.appendChild(latitudeElem);
							curLatitude = $(latitudeElem);
							runGeocode = true;
						}
						
						if (curLongitude.length == 0) {
							longitudeElem = document.createElement("input");
							longitudeElem.setAttribute("type", "hidden");
							longitudeElem.setAttribute("name", "longitude[]");
							this.appendChild(longitudeElem);
							curLongitude = $(longitudeElem);
							runGeocode = true;
						}
						
						if (!runGeocode) {
							if (curLatitude.val() == "" || curLongitude.val() == "") {
								runGeocode = true;
							}
						}
						
						if (runGeocode) {
							geocodeAddresses.push({address: address, fieldSet: this});
						}
					}
					
					if (index == addressFieldsets.length - 1) {
						if (geocodeAddresses.length > 0) {
							var runningGeocodes = geocodeAddresses.length;
							for (var i = 0; i < geocodeAddresses.length; i++) {
								geocodeInformation = geocodeAddresses[i];
								
								geocoder = new google.maps.Geocoder();
								geocoder.geocode({address: geocodeInformation.address}, function(address, fieldSet, results, status) {
									curLatitude = $("input[name='latitude[]']", fieldSet);
									curLongitude = $("input[name='longitude[]']", fieldSet);
									
									if (status == google.maps.GeocoderStatus.OK) {
										if (results[0].geometry.location_type.toUpperCase() == "ROOFTOP") {
											curLatitude.val(results[0].geometry.location.lat());
											curLongitude.val(results[0].geometry.location.lng());
										} else {
											curLatitude.val("");
											curLongitude.val("");
											console.log('Geocode for "' + address + '" was not successful for the following reason: Location type was not specific.');
										}
									} else {
										curLatitude.val("");
										curLongitude.val("");
										console.log('Geocode for "' + address + '" was not successful for the following reason: ' + status);
									}
									
									runningGeocodes--;
									
									if (runningGeocodes == 0) {
										theForm = $(fieldSet).closest("form");
										latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
										for (var j = 0; j < latLngs.length; j++) {
											if (latLngs[j].value == "") {
												var address = "";
												fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
												for (var k = 0; k < fields.length; k++) {
													curField = $(fields[k]);
													if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
														address += ", ";
													} else {
														address += " ";
													}
													address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
												}
												alert("Invalid Address: " + address);
												return false;
											}
										}

										$(document.body).pagecontainer("change", "#new_perk_step4", {transition: "flip"});
									}
								}.bind(window, geocodeInformation.address, geocodeInformation.fieldSet));
							}
						} else {
							theForm = $(this).closest("form");
							latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
							for (var i = 0; i < latLngs.length; i++) {
								if (latLngs[i].value == "") {
									var address = "";
									fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
									for (var i = 0; i < fields.length; i++) {
										curField = $(fields[i]);
										if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
											address += ", ";
										} else {
											address += " ";
										}
										address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
									}
									alert("Invalid Address: " + address);
									return false;
								}
							}
							
							$(document.body).pagecontainer("change", "#new_perk_step4", {transition: "flip"});
						}
					}
				});
						
				e.preventDefault();
				return false;
			}
		} else if (pageID == "new_perk_step4") {
			if (!$("#agrees_to_terms")[0].checked) {
				cancel = true;
				message = "You must indicate your agreement to the above terms.";
			} else if ($("#signatory").val() == "") {
				cancel = true;
				message = "Your e-signature is required.";
			}
		}
		
		if (cancel) {
			e.preventDefault();
			alert(message);
			return false;
		}
	} else {
		if (pageID == "new_promotion_step3") {
			if ($("#eligibility_distance").val() != "0") {
				reviewEligibilityFromAddresses.val("");
				
				var geocodeAddresses = new Array();
				
				var addressFieldsets = $("#new_promotion_step3 fieldset");
				addressFieldsets.each(function(index) {
					var address = "";
					fields = $("input[type=text]:enabled, select:enabled", this);
					for (var i = 0; i < fields.length; i++) {
						curField = $(fields[i]);
						if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
							address += ", ";
						} else {
							address += " ";
						}
						address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
					}
					
					if (fields.length != 0) {
						reviewEligibilityFromAddresses.val(reviewEligibilityFromAddresses.val() + address + "\n");
						
						curLatitude = $("input[name='latitude[]']", this);
						curLongitude = $("input[name='longitude[]']", this);
						
						runGeocode = false;
						
						if (curLatitude.length == 0) {
							latitudeElem = document.createElement("input");
							latitudeElem.setAttribute("type", "hidden");
							latitudeElem.setAttribute("name", "latitude[]");
							this.appendChild(latitudeElem);
							curLatitude = $(latitudeElem);
							runGeocode = true;
						}
						
						if (curLongitude.length == 0) {
							longitudeElem = document.createElement("input");
							longitudeElem.setAttribute("type", "hidden");
							longitudeElem.setAttribute("name", "longitude[]");
							this.appendChild(longitudeElem);
							curLongitude = $(longitudeElem);
							runGeocode = true;
						}
						
						if (!runGeocode) {
							if (curLatitude.val() == "" || curLongitude.val() == "") {
								runGeocode = true;
							}
						}
						
						if (runGeocode) {
							geocodeAddresses.push({address: address, fieldSet: this});
						}
					}
					
					if (index == addressFieldsets.length - 1) {
						if (geocodeAddresses.length > 0) {
							var runningGeocodes = geocodeAddresses.length;
							for (var i = 0; i < geocodeAddresses.length; i++) {
								geocodeInformation = geocodeAddresses[i];
								
								geocoder = new google.maps.Geocoder();
								geocoder.geocode({address: geocodeInformation.address}, function(address, fieldSet, results, status) {
									curLatitude = $("input[name='latitude[]']", fieldSet);
									curLongitude = $("input[name='longitude[]']", fieldSet);
									
									if (status == google.maps.GeocoderStatus.OK) {
										if (results[0].geometry.location_type.toUpperCase() == "ROOFTOP") {
											curLatitude.val(results[0].geometry.location.lat());
											curLongitude.val(results[0].geometry.location.lng());
										} else {
											curLatitude.val("");
											curLongitude.val("");
											console.log('Geocode for "' + address + '" was not successful for the following reason: Location type was not specific.');
										}
									} else {
										curLatitude.val("");
										curLongitude.val("");
										console.log('Geocode for "' + address + '" was not successful for the following reason: ' + status);
									}
									
									runningGeocodes--;
									
									if (runningGeocodes == 0) {
										theForm = $(fieldSet).closest("form");
										latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
										for (var j = 0; j < latLngs.length; j++) {
											if (latLngs[j].value == "") {
												var address = "";
												fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
												for (var k = 0; k < fields.length; k++) {
													curField = $(fields[k]);
													if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
														address += ", ";
													} else {
														address += " ";
													}
													address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
												}
												alert("Invalid Address: " + address);
												return false;
											}
										}
										
										$(document.body).pagecontainer("change", "#new_promotion_step4", {transition: "flip"});
									}
								}.bind(window, geocodeInformation.address, geocodeInformation.fieldSet));
							}
						} else {
							theForm = $(this).closest("form");
							latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
							for (var i = 0; i < latLngs.length; i++) {
								if (latLngs[i].value == "") {
									var address = "";
									fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
									for (var i = 0; i < fields.length; i++) {
										curField = $(fields[i]);
										if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
											address += ", ";
										} else {
											address += " ";
										}
										address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
									}
									alert("Invalid Address: " + address);
									return false;
								}
							}
							
							$(document.body).pagecontainer("change", "#new_promotion_step4", {transition: "flip"});
						}
					}
				});
						
				e.preventDefault();
				return false;
			}
		} else if (pageID == "new_promotion_step4") {
			// We have HTML5 validation, lets use it for the complex step4 navigation instead of the above!... Someday...
			if ($("#checkin_option option:selected").val() == "" && $("#post_option option:selected").val() == "" && $("#like_option option:selected").val() == "") {
				alert("At least one option must be selected.");
				e.preventDefault();
				return false;
			}
		} else if (pageID == "new_perk_step3") {
			if ($("#eligibility_distance").val() != "0") {
				reviewEligibilityFromAddresses.val("");
				
				var geocodeAddresses = new Array();
				
				var addressFieldsets = $("#new_perk_step3 fieldset");
				addressFieldsets.each(function(index) {
					var address = "";
					fields = $("input[type=text]:enabled, select:enabled", this);
					for (var i = 0; i < fields.length; i++) {
						curField = $(fields[i]);
						if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
							address += ", ";
						} else {
							address += " ";
						}
						address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
					}
					
					if (fields.length != 0) {
						reviewEligibilityFromAddresses.val(reviewEligibilityFromAddresses.val() + address + "\n");
						
						curLatitude = $("input[name='latitude[]']", this);
						curLongitude = $("input[name='longitude[]']", this);
						
						runGeocode = false;
						
						if (curLatitude.length == 0) {
							latitudeElem = document.createElement("input");
							latitudeElem.setAttribute("type", "hidden");
							latitudeElem.setAttribute("name", "latitude[]");
							this.appendChild(latitudeElem);
							curLatitude = $(latitudeElem);
							runGeocode = true;
						}
						
						if (curLongitude.length == 0) {
							longitudeElem = document.createElement("input");
							longitudeElem.setAttribute("type", "hidden");
							longitudeElem.setAttribute("name", "longitude[]");
							this.appendChild(longitudeElem);
							curLongitude = $(longitudeElem);
							runGeocode = true;
						}
						
						if (!runGeocode) {
							if (curLatitude.val() == "" || curLongitude.val() == "") {
								runGeocode = true;
							}
						}
						
						if (runGeocode) {
							geocodeAddresses.push({address: address, fieldSet: this});
						}
					}
					
					if (index == addressFieldsets.length - 1) {
						if (geocodeAddresses.length > 0) {
							var runningGeocodes = geocodeAddresses.length;
							for (var i = 0; i < geocodeAddresses.length; i++) {
								geocodeInformation = geocodeAddresses[i];
								
								geocoder = new google.maps.Geocoder();
								geocoder.geocode({address: geocodeInformation.address}, function(address, fieldSet, results, status) {
									curLatitude = $("input[name='latitude[]']", fieldSet);
									curLongitude = $("input[name='longitude[]']", fieldSet);
									
									if (status == google.maps.GeocoderStatus.OK) {
										if (results[0].geometry.location_type.toUpperCase() == "ROOFTOP") {
											curLatitude.val(results[0].geometry.location.lat());
											curLongitude.val(results[0].geometry.location.lng());
										} else {
											curLatitude.val("");
											curLongitude.val("");
											console.log('Geocode for "' + address + '" was not successful for the following reason: Location type was not specific.');
										}
									} else {
										curLatitude.val("");
										curLongitude.val("");
										console.log('Geocode for "' + address + '" was not successful for the following reason: ' + status);
									}
									
									runningGeocodes--;
									
									if (runningGeocodes == 0) {
										theForm = $(fieldSet).closest("form");
										latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
										for (var j = 0; j < latLngs.length; j++) {
											if (latLngs[j].value == "") {
												var address = "";
												fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
												for (var k = 0; k < fields.length; k++) {
													curField = $(fields[k]);
													if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
														address += ", ";
													} else {
														address += " ";
													}
													address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
												}
												alert("Invalid Address: " + address);
												return false;
											}
										}

										$(document.body).pagecontainer("change", "#new_perk_step4", {transition: "flip"});
									}
								}.bind(window, geocodeInformation.address, geocodeInformation.fieldSet));
							}
						} else {
							theForm = $(this).closest("form");
							latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
							for (var i = 0; i < latLngs.length; i++) {
								if (latLngs[i].value == "") {
									var address = "";
									fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
									for (var i = 0; i < fields.length; i++) {
										curField = $(fields[i]);
										if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
											address += ", ";
										} else {
											address += " ";
										}
										address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
									}
									alert("Invalid Address: " + address);
									return false;
								}
							}
							
							$(document.body).pagecontainer("change", "#new_perk_step4", {transition: "flip"});
						}
					}
				});
						
				e.preventDefault();
				return false;
			}
		}
	}
	
	next = this.nextStep.value;

	//now - we need to go the next page...
	//if next step isn't a full url, we assume internal link
	//logic will be, if something.something, do a post
	if (next.indexOf(".") == -1) {
		var nextPage = "#" + next;
		$(document.body).pagecontainer("change", nextPage, {transition: "flip"});
	} else {
		//gather the fields
		var formData = {};
		
		var forms = $("form");
		for (var i = 0; i < forms.length; i++) {
			theForm = forms[i];
			var data = $(theForm).serializeArray();

			//store them - assumes unique names
			for (var j=0; j < data.length; j++) {
				//If nextStep, it's our metadata, don't store it in formdata
				if (data[j].name == "nextStep") {
					continue;
				}
				
				if (data[j].name.indexOf("[]") == data[j].name.length - 2) {
					if (!formData.hasOwnProperty(data[j].name)) {
						formData[data[j].name] = new Array();
					}
					formData[data[j].name].push(data[j].value);
				} else {
					formData[data[j].name] = data[j].value;
				}
			}
		}
		
		var encodedData = "";
		for (var key in formData) {
			if (formData.hasOwnProperty(key)) {
				if (typeof formData[key] == "string") {
					encodedData += key + "=" + formData[key] + "&";
				} else {
					for (var i = 0; i < formData[key].length; i++) {
						encodedData += key + "=" + formData[key][i] + "&";
					}
				}
			}
		}
		
		if (encodedData != "") {
			encodedData = encodedData.substring(0, encodedData.length - 1);
		}
		
		$("#navigation_box").stop(true, true).fadeOut({queue: false}).slideUp();
		
		$.ajax({
			url: next,
			type: "POST",
			data: encodedData,
			success: function(data, textStatus, jqXHR) {
				if (data.success) {
					var statusMessage = $("#status_message");
					statusMessage.removeClass("error");
					statusMessage.addClass("info");
					statusMessage.text(data.message);
					statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
					setTimeout(function() {
						statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						window.location.href = '/account/';
					}, 3000);
				} else {
					var statusMessage = $("#status_message");
					statusMessage.removeClass("info");
					statusMessage.addClass("error");
					statusMessage.text(data.message);
					statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
					$("#navigation_box").stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
					setTimeout(function() {
						statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
					}, 3000);
					for (var i = 0; i < data.commands.length; i++) {
						/*switch (data.commands[i].action) {
							case "command...":
								break;
						}*/
					}
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(errorThrown);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				$("#navigation_box").stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			}
		});
	}
	
	e.preventDefault();
	return false;
}