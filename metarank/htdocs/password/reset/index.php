<?php
// Try to hide from interlopers...
if (!isset($_GET["uuid"]) || strlen($_GET["uuid"]) != 32 || !ctype_xdigit($_GET["uuid"])) {
	header("HTTP/1.1 404 Not Found");
	exit();
}

require_once(dirname(dirname(dirname(__FILE__))) . "/../phpinc/defines.php");
require_once(BASE_PATH . "/../phpinc/session.php");

$pageTitle = "MetaRank - Password Reset Completion";
require_once(BASE_PATH . "/../phpinc/header.php"); ?>

<section data-role="main" class="ui-content">
	<?php include(BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
    <div id="password_reset_completion_page" data-title="<?php echo $pageTitle; ?>">
        <div class="content-wrapper">
            <p class="orange-text larger-message">Password Reset Completion</p>
            <br />
            <br />
            <div id="status_message" style="display:none;"></div>
            <form action="<?php echo BASE_URL; ?>/ajax/reset_password.php" method="POST">
                <div id="password_reset_completion_phase1">
                    <label for="uuid">Password Reset Code:</label>
                    <input type="text" id="uuid" name="uuid" value="<?php echo $_GET["uuid"]; ?>" required />
                </div>
                <div id="password_reset_completion_phase2" style="display:none;">
                    <label for="password">New Password:</label>
                    <input type="password" id="password" name="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" onchange="
                        this.setCustomValidity(this.validity.patternMismatch ? 'Password must be at least 6 characters long and contain at least one digit, one uppercase letter, one lowercase letter' : '');
                        if(this.checkValidity()) document.getElementById('password_confirm').pattern = this.value.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
                    " disabled />
                    <label for="password_confirm">Confirm New Password:</label>
                    <input type="password" id="password_confirm" name="password_confirm" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" onchange="
                        this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above.' : '');
                    " disabled />
                </div>
                <p id="navigation_box">
                    <button type="submit">Submit</button>
                    <button type="button">
                    	<a style="color:#fff;display:block;font-weight:400;text-decoration:none;" href="<?php echo BASE_URL; ?>/login/" data-ajax="false">Back to Login</a>
                    </button> 
                </p>
            </form>
            <script type="text/javascript">
            $("#password_reset_completion_page form").on("submit", resetPasswordCompletion);
            </script>
        </div>
    </div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>