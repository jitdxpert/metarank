<?php
require_once(dirname(dirname(__FILE__)) . "/../phpinc/session.php");
include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - Contact Us";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
} ?>

<section data-role="main" class="ui-content">
	<?php include(BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
    <div id="contact_page" data-title="<?php echo $pageTitle; ?>">
    	<div class="content-wrapper">
        	<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
            <div id="content_column">
				<?php if (isset($_SESSION["isLoggedIn"]) && $_SESSION["isLoggedIn"]) {
					$service = "";
					$subTitle = "Contact Us";
					include(BASE_PATH . "/../phpinc/subheader.php");
				} ?>
                <div class="content-box left">
                	<p class="larger-message">Contact Us</p>
                    <a href="mailto:contact@metarank.com" data-ajax="false">
                    	<img height="44" width="42" alt="Email" src="<?php echo BASE_URL; ?>/img/icon_email.png">
                    </a>
                    <a href="http://facebook.com/MetaRank" target="_blank" data-ajax="false">
                    	<img style="padding:0 5px 0 15px" height="44" width="44" alt="Facebook" src="<?php echo BASE_URL; ?>/img/icon_facebook.png">
                    </a>
                    <a href="http://twitter.com/MetaRank" target="_blank" data-ajax="false">
                    	<img style="padding:0 5px 0 15px" height="44" width="44" alt="Twitter" src="<?php echo BASE_URL; ?>/img/icon_twitter.png">
                    </a><br>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>