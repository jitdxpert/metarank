<?php
require_once(dirname(dirname(__FILE__)) . "/../phpinc/session.php");
include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - FAQ";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
} ?>

<section data-role="main" class="ui-content">
	<?php include(BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
    <div id="faq_page" data-title="<?php echo $pageTitle; ?>">
    	<div class="content-wrapper">
        	<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
            <div id="content_column">
				<?php if (isset($_SESSION["isLoggedIn"]) && $_SESSION["isLoggedIn"]) {
					$service = "";
					$subTitle = "FAQ";
					include(BASE_PATH . "/../phpinc/subheader.php");
				} ?>
                <div class="content-box left">
                	<p class="larger-message">FAQ</p>
                    <p class="larger-message">Can others see my rank?</p>
                    <p>No one other than yourself can see your rank, unless you choose to share this information in your privacy settings. This information is invisible to others by default.</p>
                    <p class="larger-message">Are you keeping, storing or sharing my Facebook, Twitter, or other social network data?</p>
                    <p>No. MetaRank does connect to these services and process data, applying a ranking algorithm and then saving the generated value. The data itself is never saved to any database.</p>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>