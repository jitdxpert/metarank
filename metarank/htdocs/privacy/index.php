<?php
require_once(dirname(dirname(__FILE__)) . "/../phpinc/session.php");
include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - Privacy Policy";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
} ?>

<section data-role="main" class="ui-content">
	<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
	<div id="privacy_page" data-title="<?php echo $pageTitle; ?>">
		<div class="content-wrapper">
        	<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
			<div id="content_column">
				<?php if (isset($_SESSION["isLoggedIn"]) && $_SESSION["isLoggedIn"]) {
					$service = "";
					$subTitle = "Privacy Policy";
					include(BASE_PATH . "/../phpinc/subheader.php");
				} ?>
                <div class="content-box left">
					<p class="larger-message">Privacy Policy</p>
					<p>The MetaRank service is brought to you by MetaRank and is being made available to you at absolutely no cost. The Privacy Policy governs our policies and practices with respect to the use of MetaRank and the personal information we collect in connection with our website and our mobile applications. As such, by using this site, you consent to our privacy policy. If you have any questions about MetaRank’s privacy policy, please email MetaRank at privacy@metarank.com.</p>
					<p>At MetaRank, (“MetaRank”, “we”, “us”, and “our”), we take privacy very seriously. We are committed to protecting and respecting your privacy. Please review this privacy policy to understand how we collect, use and protect personal information you provide through our website, http://www.metarank.com, and how we use that information to offer you our products and services.</p>
					<p>Our privacy policy may be updated from time to time, and we will notify you of any material changes by posting the new privacy policy on our site at http://www.metarank.com/privacy/.</p>
					<p class="larger-message">Information We Collect and /or Retrieve</p>
					<p>MetaRank collects information from you and third parties (ie: Facebook, Twitter), but we do not store this information in our data base. Your personal data is collected and processed by MetaRank’s patented algorithms. The algorithms provide a score and only the score is stored in our data base.</p>
					<p>Certain areas of our site do not require you to register and thus are available to you without you providing any personal information. In order to take full advantage of the product or service functionality which we offer through our site, we may ask for the following types of information:</p>
					<ul>
						<li>Information you provide – When you register for our service, our registration process requires you to submit information pertinent to you as a person. Additionally, we may require you to submit personal information such as your name, email address and zip code.<li>
						<li>Profile information about you retrieved from third parties – In order to benefit from the full functionality of the service, you must allow MetaRank access to your personal and social profile data such as your Facebook profile or Twitter profile. In such instances, you’re authorizing MetaRank access to your profile data using OAuth. Simply put, OAuth allows you to authorize MetaRank to retrieve data on your behalf, in order for MetaRank to keep your score current and updated, without giving us your credentials to these third party sites.</li>
						<li>Logs – When you visit our site, our servers automatically record information that your browser sends. These logs may include, but is not limited to, information such as your web request, Internet Protocol address, browser type, the page you were visiting prior to our site, the date and time of your request, and one or more cookies.</li>
						<li>Cookies – We utilize cookies to provide a personalized product experience and to track usage. We may use “session ID cookies” to enable certain features of the site, to better understand how you interact with our site and to estimate audience size and monitor aggregate usage patterns. You can configure your browser to accept, reject, or prompt you to accept cookies before you visit websites. However, some MetaRank features and services may not function properly if cookies are disabled.</li>
					</ul>
					<p>Access to your personal information is absolutely restricted and used in accordance with specific internal procedures and safeguards governing access in order to operate, develop or improve our service.</p>
					<p class="larger-message">Information Usage </p>
					<p>We use the information you provide and the information we collect about you for the following purposes:</p>
					<ol>
						<li>To provide our services including display of customized content such as to generate searches against various online data sources in order to build your MetaRank profile and generate your unique MetaRank position or ranking.</li>
						<li>We may use your email address to send you information and updates pertaining to your account. You may also receive occasional company news, updates, related product or service information, etc. If at any time you would like to unsubscribe from receiving future emails, you may do so by changing your contact preferences by logging into account settings.</li>
						<li>To protect, maintain, and improve our services through research and analysis as well as to ensure our technology infrastructure is performing optimally.</li>
						<li>To protect MetaRank interests as well as that of its user base.</li>
						<li>Anonymously and in aggregate fashion, allow you the ability to compare your MetaRank ranking with others in your geographic location.</li>
						<li>We may sell, transfer, or otherwise share all of MetaRank assets, including your personal information, in connection with a merger, acquisition, reorganization, or sale of assets.</li>
					</ol>
					<p class="larger-message">Do we disclose any information to outside parties?</p>
					<p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others’ rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>
					<p class="larger-message">How do we protect your information?</p>
					<p>MetaRank is committed to protecting your personal information. We employ numerous measures, including SSL encryption, database encryption, and strict adherence to ‘need to know’ principle designed to protect your information from unauthorized access.</p>
					<p class="larger-message">Third Party Sites and Links</p>
					<p>This privacy policy applies to MetaRank services only. We do not exercise control over the content that we retrieve for you as part of our products or services, or links from within our various services. If you click on these links, these other sites may place their own cookies or other files on your computer, collect data or solicit personal information from you. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites.</p>
					<p class="larger-message">Deactivating and Deleting Your Account</p>
					<p>Deactivating your account puts your account on hold. It is the same as you telling us that you might want to reactivate your account at some point in the future.</p>
					<p>When you delete your account, it is permanently deleted from MetaRank. You should only delete your account if you are sure you never want to reactivate it.</p>
					<p class="larger-message">Children’s Online Privacy Protection Act Compliance</p>
					<p>We take safety issues very seriously and are in compliance with the requirements of COPPA (Children’s Online Privacy Protection Act) and we do not knowingly collect any information from anyone under 13 years of age. Our site, products and services are all directed to people who are 13 years old or older. If we learn that we have collected personal information of a child under 13 years of age, we will take steps to delete this information.</p>
					<p class="larger-message">Terms of Use</p>
					<p>Please review our Terms of Use section establishing the use, disclaimers, and limitations of liability governing the use of our site at http://www.metarank.com/tou/.</p>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
	</div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>