<?php
require_once(dirname(dirname(__FILE__)) . "/../phpinc/session.php");

if (isset($_SESSION['isLoggedIn']) && $_SESSION['isLoggedIn']) {
	header('Location: ' . BASE_URL . '/', true, 303);
	exit();
}

$pageTitle = 'MetaRank - Login';
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}

require_once(BASE_PATH . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/lib/fb/facebook.php");
require_once(BASE_PATH . "/../phpinc/lib/li/linkedin.php");
require_once(BASE_PATH . "/../phpinc/lib/tw/twitter.php");

use Facebook\FacebookRedirectLoginHelper;

$_SESSION["facebookUniqueID"] = null;
$_SESSION["facebookAccessToken"] = null;
$_SESSION["facebookAccessTokenExpiration"] = null;
$fbscope = array("user_birthday","user_location","email","read_stream","user_about_me","user_education_history","user_work_history","user_website","user_friends");
//$fbscope = array("user_birthday","user_location","email","user_about_me","user_education_history","user_work_history","user_website","user_friends");
//$fbscope = array('scope'=>'manage_pages,read_insights,user_photos,email,publish_actions,user_birthday,user_about_me,user_website,user_friends,user_work_history,user_location,user_education_history');
//$fbLoginHelper = new FacebookRedirectLoginHelper(curHostURL() . "/");
$fbLoginHelper = new FacebookRedirectLoginHelper(curHostURL() . "/metarank/htdocs/");

$_SESSION["linkedInUniqueID"] = null;
$_SESSION["linkedInOAuthToken"] = null;
$_SESSION["linkedInOAuthTokenExpiration"] = null;
//$liscope = array(LinkedIn::SCOPE_BASIC_PROFILE, LinkedIn::SCOPE_FULL_PROFILE, LinkedIn::SCOPE_EMAIL_ADDRESS, LinkedIn::SCOPE_NETWORK, LinkedIn::SCOPE_CONTACT_INFO);
$liscope = array(LinkedIn::SCOPE_BASIC_PROFILE,  LinkedIn::SCOPE_EMAIL_ADDRESS);
$_SESSION["twitterUniqueID"] = null;

$twitterSession = new TwitterOAuth(TWITTER_APP_ID, TWITTER_SECRET);
$temporaryCredentials = $twitterSession->getRequestToken(curHostURL() . "/metarank/htdocs/");
$_SESSION["twitterOAuthToken"] = $temporaryCredentials["oauth_token"];
$_SESSION["twitterOAuthTokenSecret"] = $temporaryCredentials["oauth_token_secret"];
?>
	<section data-role="main" class="ui-content">
		<?php include(BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
		<div id="login_page" data-title="<?php echo $pageTitle; ?>">
			<div class="content-wrapper">
				<br>
                <div class="signup-sec">
                	<span>Not a member? </span>
                    <a id="signup_button" href="<?php echo BASE_URL; ?>/register/" onclick="ga('send','event','Button','click','Register')" data-ajax="false">
                        <button>Sign Up</button>
                    </a>
                </div><br />
				<div id="status_message" style="display:none;"></div>
				<form action="<?php echo BASE_URL; ?>/ajax/login.php" method="POST" id="login_form">
					<input type="hidden" id="login_redirect_url" value="<?php echo BASE_URL . '/'; ?>">
					<label for="email">Email:</label>
					<input type="email" id="email" name="email" required />
					<label for="password">Password:</label>
					<input type="password" id="password" name="password" required />
					<div id="forgot_password">
						<a href="<?php echo BASE_URL; ?>/password/" data-ajax="false">Forgot password?</a>
					</div>
					<div id="login_captcha">
						<?php if ($metaRankDatabase->getLoginFailureCountByRemoteIP() > MAX_LOGIN_FAILURES) { ?>
						<script type="text/javascript">
						showRecaptcha($("#login_captcha")[0]);
						</script>
						<?php } ?>
					</div>
					<p id="navigation_box">
						<button style="float:right;" type="submit" onclick="ga('send','event','Login','click','Email')">Login with Email</button>
                        <a href="<?php echo BASE_URL; ?>/" data-ajax="false">
                        	<button style="background-color:#777777;float:left;color:#fff;display:block;font-weight:400;text-decoration:none;" type="button">Back</button>
                        </a>
					</p>
				</form>
				<script type="text/javascript">
				$("#login_form").on("submit", login);
				</script>
			</div>
		</div>
	</section>
    
<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>