<?php
require_once(dirname(__FILE__) . "/../../phpinc/db.php");
require_once(dirname(__FILE__) . "/../../phpinc/addressfield.php");
require_once(dirname(__FILE__) . "/../../phpinc/session.php");

if (isset($_SESSION['isLoggedIn']) && $_SESSION['isLoggedIn']) {
	header('Location: ' . '/', true, 303);
	exit();
}

$pageTitle = "MetaRank - Account Registration";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(dirname(__FILE__) . "/../../phpinc/header.php");
}

$registrationInformation = array("first_name" => "", "last_name" => "", "date_of_birth" => "", "gender" => "", "phone" => "", "email" => "");

include (dirname(__FILE__) . "/../../phpinc/register.php");

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(dirname(__FILE__) . "/../../phpinc/footer.php");
}
?>