<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["profile_picture"]) || $_POST["profile_picture"] == ""
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		if ($_POST["profile_picture"] != "facebook" && $_POST["profile_picture"] != "linkedin" && $_POST["profile_picture"] != "twitter") {
			$message = "Invalid request: Invalid profile picture specified.";
		} else {
			// Begin processing
			require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
			require_once(BASE_PATH . "/../phpinc/session.php");
			require_once(BASE_PATH . "/../phpinc/db.php");
			
			if (!$metaRankDatabase->updateUserProfilePicture($_SESSION["userID"], $_POST["profile_picture"])) {
				$metaRankDatabase->rollbackChanges();
				$message = "Error updating user profile picture. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
			} else {
				$metaRankDatabase->commitChanges();
				$_SESSION["userProfilePicture"] = $_POST["profile_picture"];
				
				if ($_SESSION["userProfilePicture"] == "facebook") {
					$pictureURL = "https://graph.facebook.com/" . $_SESSION["facebookUniqueID"] . "/picture";
				} else if ($_SESSION["userProfilePicture"] == "linkedin") {
					require_once(BASE_PATH . '/../phpinc/lib/li/linkedin.php');
					$linkedinSession->setAccessToken($_SESSION["linkedInOAuthToken"]);

					try {
						$information = $linkedinSession->get("/people/~:(picture-url)");
						
						$pictureURL = $information["pictureUrl"];
					} catch (\RuntimeException $re) {
						//TODO: Handle this error
					}
				} else if ($_SESSION["userProfilePicture"] == "twitter") {
					$pictureURL = $_SESSION["twitterProfilePictureURL"];
				} else {
					// MetaRank picture
					$pictureURL = BASE_URL . "/img/anonymous.png";
				}
				
				$success = true;
				$message = "Profile successfully updated.";
				$commands[] = '{"action":"replaceImage","params":["' . $pictureURL . '"]}';
			}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>