<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["email"]) || $_POST["email"] == ""
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
			$message = "Invalid email address.";
		} else {
			// Begin processing
			require_once(dirname(dirname(__FILE__)) . "/../phpinc/session.php");
			
			$results = $metaRankDatabase->findFriendsByEmail($_POST["email"], $_SESSION["userID"]);
			
			if ($results === null) {
				$message = "Unable to search at this time.";
			} else {
				$success = true;
				$message = "Search successful.";
				
				foreach($results as $result) {
					if ($result["profile_picture"] == "facebook") {
						$profilePictureURL = "https://graph.facebook.com/" . $result["facebook_id"] . "/picture";
					} else if ($result["profile_picture"] == "linkedin") {
						require_once(BASE_PATH . '/../phpinc/lib/li/linkedin.php');
						$linkedinSession->setAccessToken($result["linkedin_oauth_token"]);

						try {
							$information = $linkedinSession->get("/people/~:(picture-url)");

							$profilePictureURL = $information["pictureUrl"];
						} catch (\RuntimeException $re) {
						//TODO: Handle this error
							$profilePictureURL = BASE_URL . "/img/anonymous.png";
						}
					} else if ($result["profile_picture"] == "twitter") {
						$profilePictureURL = $result["twitter_profile_picture_url"];
					} else {
						$profilePictureURL = BASE_URL . "/img/anonymous.png";
					}
					
					$commands[] = '{"action":"addEmailSearchResult","params":["' . $result["id"] . '","' . $result["first_name"] . '","' . $result["last_name"] . '","' . $profilePictureURL . '"]}';
				}
			}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>