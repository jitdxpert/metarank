<?php
	$success  = false;
	$message  = "";
	$commands = array();

	// First check for required fields
	include(dirname(dirname(__FILE__)) . "/../phpinc/session.php");
	if(!isset($_SESSION["facebookAccessToken"]) || $_SESSION["facebookAccessToken"] == null) {
		$message = "Invalid request: Not connected to Facebook.";		
	} else {
		// Begin processing		
		if(!$metaRankDatabase->addFriendsFromFacebook($_SESSION["userID"], $_SESSION["facebookAccessToken"])) {
			$message = "Unable to add friends from Facebook at this time.";
		} else {
			$success = true;
			$message = "Facebook friends added successful.";
			$metaRankDatabase->commitChanges();
		}
	}	
	header('Content-Type: application/json');	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>