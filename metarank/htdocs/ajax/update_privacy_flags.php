<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["privacy_flags"]) || !is_numeric($_POST["privacy_flags"])
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		// Begin processing
		require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
		require_once(BASE_PATH . "/../phpinc/session.php");
		require_once(BASE_PATH . "/../phpinc/db.php");
		
		if (!$metaRankDatabase->updateUserPrivacyFlags($_SESSION["userID"], $_POST["privacy_flags"])) {
			$metaRankDatabase->rollbackChanges();
			$message = "Error updating privacy settings. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
		} else {
			$metaRankDatabase->commitChanges();
			$_SESSION["userPrivacyFlags"] = $_POST["privacy_flags"];
			
			$success = true;
			$message = "Privacy settings successfully updated.";
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>