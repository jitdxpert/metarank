<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["first_name"]) || $_POST["first_name"] == "" ||
		!isset($_POST["last_name"]) || $_POST["last_name"] == "" ||
		!isset($_POST["date_of_birth"]) || $_POST["date_of_birth"] == "" ||
		!isset($_POST["gender"]) || $_POST["gender"] == "" ||
		!isset($_POST["phone"]) || $_POST["phone"] == ""
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		$date_of_birth = DateTime::createFromFormat("Y-m-d", $_POST["date_of_birth"]);
		if (!$date_of_birth || $date_of_birth->format("Y-m-d") != $_POST["date_of_birth"]) {
			$message = "Invalid birthday.";
		} else {
			// Begin processing
			require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
			require_once(BASE_PATH . "/../phpinc/session.php");
			require_once(BASE_PATH . "/../phpinc/db.php");
			
			if (!$metaRankDatabase->updateUserProfile($_SESSION["userID"], $_POST["first_name"], $_POST["last_name"], $_POST["date_of_birth"], $_POST["gender"], $_POST["phone"])) {
				$metaRankDatabase->rollbackChanges();
				$message = "Error updating user profile. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
			} else {
				$metaRankDatabase->commitChanges();
				$_SESSION["userFirstName"] = $_POST["first_name"];
				$_SESSION["userLastName"] = $_POST["last_name"];
				$_SESSION["userDateOfBirth"] = $_POST["date_of_birth"];
				$_SESSION["userGender"] = $_POST["gender"];
				$_SESSION["userPhone"] = $_POST["phone"];
				
				$success = true;
				$message = "Profile successfully updated.";
			}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>