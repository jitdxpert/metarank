<?php
	require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
	require_once(BASE_PATH . "/../phpinc/session.php");
	require_once(BASE_PATH . "/../phpinc/db.php");
	
	use GeoIp2\Database\Reader;
	
	$success = false;
	$message = "";
	$commands = array();
	
	if (isset($_POST["postal_code"])) {
		// Rely on HTML5 location services
		if (isset($_SESSION["isLoggedIn"]) && $_SESSION["isLoggedIn"]) {
			if (!$metaRankDatabase->updateUserLocation($_SESSION["userID"], $_POST["country"], $_POST["region"], $_POST["city"], $_POST["postal_code"], $_POST["lat"], $_POST["long"])) {
				$metaRankDatabase->rollbackChanges();
				$message = "Location update failed.";
			} else {
				$success = true;
				
				$metaRankDatabase->scoreMetaRankBonus($_SESSION["userID"]);
				
				$metaRankDatabase->commitChanges();
			}
		} else {
			$success = true;
			$_SESSION["locationCountry"] = $_POST["country"];
			$_SESSION["locationAdministrativeArea"] = $_POST["region"];
			$_SESSION["locationLocality"] = $_POST["city"];
			$_SESSION["locationPostalCode"] = $_POST["postal_code"];
			$_SESSION["locationLatitude"] = $_POST["lat"];
			$_SESSION["locationLongitude"] = $_POST["long"];
		}
	} else {
		// If HTML5 location services are not available or were declined, fall back to GeoIP lookup
		require_once(BASE_PATH . "/../phpinc/lib/geoip2.phar");

		$reader = new Reader(BASE_PATH . '/../phpinc/lib/GeoLite2-City.mmdb');
		try {
			$record = $reader->city($_SERVER["REMOTE_ADDR"]);
			
			if (isset($_SESSION["isLoggedIn"]) && $_SESSION["isLoggedIn"]) {
				if (!$metaRankDatabase->updateUserLocation($_SESSION["userID"], $record->country->isoCode, $record->mostSpecificSubdivision->isoCode, $record->city->name, $record->postal->code, $record->location->latitude, $record->location->longitude)) {
					$metaRankDatabase->rollbackChanges();
					$message = "Location update failed.";
				} else {
					$success = true;
					
					$metaRankDatabase->scoreMetaRankBonus($_SESSION["userID"]);
					
					$metaRankDatabase->commitChanges();
				}
			} else {
				$success = true;
				$_SESSION["locationCountry"] = $record->country->isoCode;
				$_SESSION["locationAdministrativeArea"] = $record->mostSpecificSubdivision->isoCode;
				$_SESSION["locationLocality"] = $record->city->name;
				$_SESSION["locationPostalCode"] = $record->postal->code;
				$_SESSION["locationLatitude"] = $record->location->latitude;
				$_SESSION["locationLongitude"] = $record->location->longitude;
			}
		} catch (Exception $e) {
			//TODO: Insert a value for "unknown location"
			$message = "Unknown location!";
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>