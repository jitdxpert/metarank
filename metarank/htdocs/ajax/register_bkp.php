<?php
	$success = false;
	$message = "";
	$commands = array();
	// First check for required fields
	if (
		!isset($_POST["first_name"]) || $_POST["first_name"] == "" ||
		!isset($_POST["last_name"]) || $_POST["last_name"] == "" ||
		!isset($_POST["date_of_birth"]) || $_POST["date_of_birth"] == "" ||
		!isset($_POST["gender"]) || $_POST["gender"] == "" ||
		!isset($_POST["phone"]) || $_POST["phone"] == "" ||
		!isset($_POST["email"]) || $_POST["email"] == "" ||
		!isset($_POST["password"]) || $_POST["password"] == "" ||
		!isset($_POST["password_confirm"]) || $_POST["password_confirm"] == "" ||
		!isset($_POST["security_question"]) || strlen($_POST["security_question"]) != 32 || !ctype_xdigit($_POST["security_question"]) ||
		!isset($_POST["security_answer"]) || $_POST["security_answer"] == ""
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
			$message = "Invalid email address.";
		} else if ($_POST["password"] != $_POST["password_confirm"]) {
			$message = "Password mismatch.";
		} else {
			$date_of_birth = DateTime::createFromFormat("Y-m-d", $_POST["date_of_birth"]);
			if (!$date_of_birth || $date_of_birth->format("Y-m-d") != $_POST["date_of_birth"]) {
				$message = "Invalid birthday.";
			} else {
				// Begin processing
				require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
				require_once(BASE_PATH . "/../phpinc/session.php");
				require_once(BASE_PATH . "/../phpinc/db.php");
				
				$userID = $metaRankDatabase->createUser($_POST["first_name"], $_POST["last_name"], $_POST["date_of_birth"], $_POST["gender"], $_POST["phone"], $_POST["email"], $_POST["password"], $_POST["security_question"], $_POST["security_answer"]);
				if ($userID == null) {
					$message = "Email address already in use.";
					$metaRankDatabase->rollbackChanges();
				} else {
					if (isset($_POST["service"])) {
						if ($_POST["service"] == "LinkedIn") {
							if (!$metaRankDatabase->updateUserProfilePicture($userID, "linkedin")) {
								$message = "Could not set profile picture.";
								$metaRankDatabase->rollbackChanges();
							} else {
								$oAuthTokenExpiration = new DateTime();
								$oAuthTokenExpiration->setTimestamp($_SESSION["linkedInOAuthTokenExpiration"]);
								if (!$metaRankDatabase->createLinkedInUser($userID, $_SESSION["linkedInUniqueID"], $_SESSION["linkedInOAuthToken"], $oAuthTokenExpiration)) {
									$message = "Could not link to LinkedIn account.";
									$metaRankDatabase->rollbackChanges();
								} else {
									$success = true;
									$message = "Registration successful.";
									
									// Also log them in!
									$information = $_SESSION["linkedInRegistrationInformation"];
									
									$metaRankDatabase->logSuccessfulLogin($_POST["email"]);

									session_regenerate_id(true);

									$metaRankDatabase->loadSessionUserInformation($_POST["email"]);

									$metaRankDatabase->updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"));
									
									$metaRankDatabase->scoreLinkedIn($_SESSION["userID"], $information);
									$metaRankDatabase->updateMetaScore($_SESSION["userID"]);
									
									$metaRankDatabase->rankGlobalLinkedIn($_SESSION["userID"]);
									$metaRankDatabase->computeGlobalLinkedInParticipants();
									
									$metaRankDatabase->rankGlobal($_SESSION["userID"]);
									$metaRankDatabase->computeGlobalParticipants();

									$metaRankDatabase->commitChanges();

									$_SESSION["isLoggedIn"] = true;

									sendEmailVerification($_POST["email"], $userID);
								}
							}
						} else if ($_POST["service"] == "Facebook") {
							if (!$metaRankDatabase->updateUserProfilePicture($userID, "facebook")) {
								$message = "Could not set profile picture.";
								$metaRankDatabase->rollbackChanges();
							} else {
								$accessTokenExpiration = new DateTime();
								$accessTokenExpiration->setTimestamp($_SESSION["facebookAccessTokenExpiration"]);
								if (!$metaRankDatabase->createFacebookUser($userID, $_SESSION["facebookUniqueID"], $_SESSION["facebookAccessToken"], $accessTokenExpiration)) {
									$message = "Could not link to Facebook account.";
									$metaRankDatabase->rollbackChanges();
								} else {
									$success = true;
									$message = "Registration successful.";
									
									// Also log them in!
									$metaRankDatabase->logSuccessfulLogin($_POST["email"]);

									session_regenerate_id(true);

									$metaRankDatabase->loadSessionUserInformation($_POST["email"]);

									$metaRankDatabase->updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"));
									
									$metaRankDatabase->scoreFacebook($_SESSION["userID"], $_SESSION["facebookAccessToken"]);
									$metaRankDatabase->updateMetaScore($_SESSION["userID"]);
									
									$metaRankDatabase->rankGlobalFacebook($_SESSION["userID"]);
									$metaRankDatabase->computeGlobalFacebookParticipants();
									
									$metaRankDatabase->rankGlobal($_SESSION["userID"]);
									$metaRankDatabase->computeGlobalParticipants();

									$metaRankDatabase->commitChanges();

									$_SESSION["isLoggedIn"] = true;

									sendEmailVerification($_POST["email"], $userID);
								}
							}
						} else if ($_POST["service"] == "Twitter") {
							if (!$metaRankDatabase->updateUserProfilePicture($userID, "twitter")) {
								$message = "Could not set profile picture.";
								$metaRankDatabase->rollbackChanges();
							} else {
								if (!$metaRankDatabase->createTwitterUser($userID, $_SESSION["twitterUniqueID"], $_SESSION["twitterOAuthToken"], $_SESSION["twitterOAuthTokenSecreet"], $_SESSION["twitterProfilePictureURL"])) {
									$message = "Could not link to Twitter account.";
									$metaRankDatabase->rollbackChanges();
								} else {
									$success = true;
									$message = "Registration successful.";
									
									// Also log them in!
									$metaRankDatabase->logSuccessfulLogin($_POST["email"]);

									session_regenerate_id(true);

									$metaRankDatabase->loadSessionUserInformation($_POST["email"]);

									$metaRankDatabase->updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"));
									
									$metaRankDatabase->scoreTwitter($_SESSION["userID"], $_SESSION["twitterOAuthToken"], $_SESSION["twitterOAuthTokenSecret"]);
									$metaRankDatabase->updateMetaScore($_SESSION["userID"]);
									
									$metaRankDatabase->rankGlobalTwitter($_SESSION["userID"]);
									$metaRankDatabase->computeGlobalTwitterParticipants();
									
									$metaRankDatabase->rankGlobal($_SESSION["userID"]);
									$metaRankDatabase->computeGlobalParticipants();

									$metaRankDatabase->commitChanges();

									$_SESSION["isLoggedIn"] = true;
									
									sendEmailVerification($_POST["email"], $userID);
								}
							}
						} else {
							$success = true;
							$message = "Registration successful.";
							
							// Also log them in!
							$metaRankDatabase->logSuccessfulLogin($_POST["email"]);

							session_regenerate_id(true);

							$metaRankDatabase->loadSessionUserInformation($_POST["email"]);
							
							$metaRankDatabase->updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"));
							
							$metaRankDatabase->commitChanges();
							
							$_SESSION["isLoggedIn"] = true;
							
							// Send userID in email
							sendEmailVerification($_POST["email"], $userID);
						}
					} else {
						$success = true;
						$message = "Registration successful.";
						
						// Also log them in!
						$metaRankDatabase->logSuccessfulLogin($_POST["email"]);

						session_regenerate_id(true);

						$metaRankDatabase->loadSessionUserInformation($_POST["email"]);
						
						$metaRankDatabase->updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"));
						
						$metaRankDatabase->commitChanges();
						
						$_SESSION["isLoggedIn"] = true;
						
						// Send userID in email
						sendEmailVerification($_POST["email"], $userID);
					}
				}
			}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>