<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (!isset($_POST["email"]) || !isset($_POST["password"])) {
		$message = "Invalid request: Missing fields.";
	} else {
		// Begin processing
		require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
		require_once(BASE_PATH . "/../phpinc/session.php");
		require_once(BASE_PATH . "/../phpinc/db.php");
		
		$userLocked = $metaRankDatabase->checkUserIsLocked($_POST["email"]);
		if ($userLocked["locked"]) {
			if ($userLocked["modification_reason"] == "") {
				$message = "This username is locked. If you have used the Forgot Password link, complete the instructions in the email we sent to reset your password and unlock your account.";
			} else {
				$message = "This username is locked. Reason: " . $userLocked["modification_reason"];
			}
		} else {
			$requireCaptcha = false;
			$loginIPFailures = $metaRankDatabase->getLoginFailureCountByRemoteIP();
			$loginUsernameFailures = $metaRankDatabase->getLoginFailureCountByUsername($_POST["email"]);
			
			if ($loginIPFailures > MAX_LOGIN_FAILURES || $loginUsernameFailures > MAX_LOGIN_FAILURES) {
				$requireCaptcha = true;
			}
			
			if ($requireCaptcha) {
				if (!isset($_POST["recaptcha_challenge_field"]) || !isset($_POST["recaptcha_challenge_field"])) {
					$message = "Invalid request: Missing fields.";
					$commands[] = '{"action":"loadCaptcha"}';
				} else {
					// Check captcha first
					require_once(BASE_PATH . "/../phpinc/recaptchalib.php");
					
					$resp = recaptcha_check_answer (RECAPTCHA_PRIVATE_KEY, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

					if (!$resp->is_valid) {
						$message = "The captcha wasn't entered correctly. Go back and try it again.";
						$commands[] = '{"action":"loadCaptcha"}';
						
						$metaRankDatabase->logFailedLogin($_POST["email"]);
					} else {
						if ($metaRankDatabase->checkUserPassword($_POST["email"], $_POST["password"])) {
							$success = true;
							$message = "Authenticated.";
							$commands[] = '{"action":"login"}';
							
							$metaRankDatabase->logSuccessfulLogin($_POST["email"]);
							
							session_regenerate_id(true);
							
							$metaRankDatabase->loadSessionUserInformation($_POST["email"]);
							
							$metaRankDatabase->updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"));
							
							if (isset($_SESSION["locationPostalCode"])) {
								$metaRankDatabase->updateUserLocation($_SESSION["userID"], $_SESSION["locationCountry"], $_SESSION["locationAdministrativeArea"], $_SESSION["locationLocality"], $_SESSION["locationPostalCode"], $_SESSION["locationLatitude"], $_SESSION["locationLongitude"]);
								$metaRankDatabase->scoreMetaRankBonus($_SESSION["userID"]);
							}
							unset($_SESSION["locationCountry"]);
							unset($_SESSION["locationAdministrativeArea"]);
							unset($_SESSION["locationLocality"]);
							unset($_SESSION["locationPostalCode"]);
							unset($_SESSION["locationLatitude"]);
							unset($_SESSION["locationLongitude"]);
							
							$_SESSION["isLoggedIn"] = true;
						} else {
							$message = "Authentication failed.";
							$commands[] = '{"action":"loadCaptcha"}';
							
							$metaRankDatabase->logFailedLogin($_POST["email"]);
						}
					}
					
					$metaRankDatabase->commitChanges();
				}
			} else {
				if ($metaRankDatabase->checkUserPassword($_POST["email"], $_POST["password"])) {
					$success = true;
					$message = "Authenticated.";
					
					$metaRankDatabase->logSuccessfulLogin($_POST["email"]);
					
					session_regenerate_id(true);
					
					$metaRankDatabase->loadSessionUserInformation($_POST["email"]);
					
					$metaRankDatabase->updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"));
					
					if (isset($_SESSION["locationPostalCode"])) {
						$metaRankDatabase->updateUserLocation($_SESSION["userID"], $_SESSION["locationCountry"], $_SESSION["locationAdministrativeArea"], $_SESSION["locationLocality"], $_SESSION["locationPostalCode"], $_SESSION["locationLatitude"], $_SESSION["locationLongitude"]);
						$metaRankDatabase->scoreMetaRankBonus($_SESSION["userID"]);
					}
					unset($_SESSION["locationCountry"]);
					unset($_SESSION["locationAdministrativeArea"]);
					unset($_SESSION["locationLocality"]);
					unset($_SESSION["locationPostalCode"]);
					unset($_SESSION["locationLatitude"]);
					unset($_SESSION["locationLongitude"]);
							
					$_SESSION["isLoggedIn"] = true;
				} else {
					$message = "Authentication failed.";
					if ($loginIPFailures == MAX_LOGIN_FAILURES || $loginUsernameFailures == MAX_LOGIN_FAILURES) {
						$commands[] = '{"action":"loadCaptcha"}';
					}
					
					$metaRankDatabase->logFailedLogin($_POST["email"]);
				}
				
				$metaRankDatabase->commitChanges();
			}
		}
	}
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>