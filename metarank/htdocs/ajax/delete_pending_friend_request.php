<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["friend"]) || strlen($_POST["friend"]) != 32 || !ctype_xdigit($_POST["friend"])
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		// Begin processing
		require_once(dirname(dirname(__FILE__)) . "/../phpinc/session.php");
		
		if (!$metaRankDatabase->deleteFriendRequest($_POST["friend"], $_SESSION["userID"])) {
			$message = "Unable to delete friend request at this time.";
		} else {
			$success = true;
			$message = "Friend request successfully deleted.";
			
			$metaRankDatabase->commitChanges();
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>