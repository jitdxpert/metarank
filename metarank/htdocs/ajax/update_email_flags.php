<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["email_flags"]) || !is_numeric($_POST["email_flags"])
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		// Begin processing
		require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
		require_once(BASE_PATH . "/../phpinc/session.php");
		require_once(BASE_PATH . "/../phpinc/db.php");
		
		if (!$metaRankDatabase->updateUserEmailFlags($_SESSION["userID"], $_POST["email_flags"])) {
			$metaRankDatabase->rollbackChanges();
			$message = "Error updating email settings. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
		} else {
			$metaRankDatabase->commitChanges();
			$_SESSION["userEmailFlags"] = $_POST["email_flags"];
			
			$success = true;
			$message = "Email settings successfully updated.";
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>