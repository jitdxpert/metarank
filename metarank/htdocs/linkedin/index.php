<?php
require_once(dirname(dirname(__FILE__)) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/linkedin/';
	header('Location: ' . BASE_URL . '/login/', true, 303);
	exit();
}

if (!isset($_SESSION["linkedInUniqueID"]) || $_SESSION["linkedInUniqueID"] == "") {
	header('Location: ' . BASE_URL . '/linkedin/options/', true, 303);
	exit();
}

include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - LinkedIn";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}

$service = "LinkedIn";
$reportCardDetails = $metaRankDatabase->getLinkedInReportCardDetails($_SESSION["userID"]);
$postalCodeRankResults = $metaRankDatabase->getLinkedInPostalCodeRank($_SESSION["userID"]);
$localityRankResults = $metaRankDatabase->getLinkedInLocalityRank($_SESSION["userID"]);
$administrativeAreaRankResults = $metaRankDatabase->getLinkedInAdministrativeAreaRank($_SESSION["userID"]);
$countryRankResults = $metaRankDatabase->getLinkedInCountryRank($_SESSION["userID"]);
$globalRankResults = $metaRankDatabase->getLinkedInGlobalRank($_SESSION["userID"]);
if ($globalRankResults != null) {
	$tmp = $metaRankDatabase->getLinkedInGlobalParticipants();
	if ($tmp != null) {
		$globalRankResults += $tmp;
	} else {
		$globalRankResults = null;
	}
}
$plot1Data = $metaRankDatabase->getLinkedInRankHistory($_SESSION["userID"]);
$plot2Data = $metaRankDatabase->getLinkedInParticipantsHistory();
$plot3Data = $metaRankDatabase->getLinkedInScoreHistory($_SESSION["userID"]);

$leaderboard = $metaRankDatabase->getLinkedInLeaderboard($_SESSION["userID"]);
$AllScore    = $metaRankDatabase->getFacebookScore(); ?>

<section data-role="main" class="ui-content">
	<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
    <div id="linkedin_page" data-title="<?php echo $pageTitle; ?>">
        <div class="content-wrapper">
        	<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
            <div id="content_column">
				<?php
				include(BASE_PATH . "/../phpinc/subheader.php");
				include(BASE_PATH . "/../phpinc/ranks.php");
				include(BASE_PATH . "/../phpinc/leaderboard.php");
				?>
                <div class="content-box">
                    <p class="left larger-message">Details</p>
                    <table class="service-details">
                        <tbody>
                            <tr>
                                <td>Recommendations:</td>
                                <td><?php echo $reportCardDetails["recommendations"]; ?></td>
                            </tr>
                            <tr>
                                <td>Connections:</td>
                                <td><?php echo $reportCardDetails["connections"]; ?></td>
                            </tr>
                            <tr>
                                <td>Foreign Connections:</td>
                                <td><?php echo $reportCardDetails["foreign_connections"]; ?></td>
                            </tr>
                            <tr>
                                <td>Positions:</td>
                                <td><?php echo $reportCardDetails["positions"]; ?></td>
                            </tr>
                            <tr>
                                <td>Current Positions:</td>
                                <td><?php echo $reportCardDetails["current_positions"]; ?></td>
                            </tr>
                            <tr>
                                <td>Educations:</td>
                                <td><?php echo $reportCardDetails["educations"]; ?></td>
                            </tr>
                    	</tbody>
                    </table>
                    <p class="left">
                    	LinkedIn updated: 
						<script id="modified_time" type="text/javascript">
						$("#modified_time").replaceWith(new Date("<?php echo $reportCardDetails["modified_time"]; ?>").toLocaleString());
                        </script>.
                    </p>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>