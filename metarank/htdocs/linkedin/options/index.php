<?php 
require_once(dirname(dirname(dirname(__FILE__))) . "/../phpinc/session.php");

if (isset($_GET["disconnect"])) {
	if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
		$_SESSION['redirectURL'] = BASE_URL . '/linkedin/options/?disconnect';
		header('Location: ' . BASE_URL . '/login/', true, 303);
		exit();
	} else {
		if (!$metaRankDatabase->disconnectLinkedInFromUser($_SESSION["userID"])) {
			$metaRankDatabase->rollbackChanges();
		} else {
			$metaRankDatabase->commitChanges();
			
			$_SESSION["linkedInUniqueID"] = null;
			$_SESSION["linkedInOAuthToken"] = null;
			$_SESSION["linkedInOAuthTokenExpiration"] = null;
		}
	}
} else {
	if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
		$_SESSION['redirectURL'] = BASE_URL . '/linkedin/options/';
		header('Location: ' . BASE_URL . '/login/', true, 303);
		exit();
	}
}

include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - LinkedIn Options";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
} ?>

<section data-role="main" class="ui-content">
	<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
    <div id="linkedin_options_page" data-title="<?php echo $pageTitle; ?>">
        <div class="content-wrapper">
			<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
            <div id="content_column">
				<?php
				$service = "LinkedIn";
				$subTitle = "LinkedIn Options";
				include(BASE_PATH . "/../phpinc/subheader.php"); 
				?>
                <div class="content-box">
                    <p class="left larger-message">LinkedIn Options</p>
					<?php if (!isset($_SESSION["linkedInUniqueID"]) || $_SESSION["linkedInUniqueID"] == "") {
						require_once(BASE_PATH . "/../phpinc/db.php");
						require_once(BASE_PATH . "/../phpinc/lib/li/linkedin.php");
						
						$_SESSION["linkedInUniqueID"] = "";
						//$liscope = array(LinkedIn::SCOPE_BASIC_PROFILE, LinkedIn::SCOPE_FULL_PROFILE, LinkedIn::SCOPE_EMAIL_ADDRESS, LinkedIn::SCOPE_NETWORK, LinkedIn::SCOPE_CONTACT_INFO);
						$liscope = array(LinkedIn::SCOPE_BASIC_PROFILE, LinkedIn::SCOPE_EMAIL_ADDRESS);						
						?>
                    	<p>
                        	<a href="<?php echo $linkedinSession->getLoginUrl($liscope); ?>" data-ajax="false">
                        		<button type="button">Connect to LinkedIn</button>
                        	</a>
                        </p>
					<?php } else { ?>
	                    <p>
                        	<a href="<?php echo BASE_URL; ?>/linkedin/options?disconnect" data-ajax="false">
                            	<button type="button">Disconnect from LinkedIn</button>
                            </a>
                        </p>
					<?php } ?>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>