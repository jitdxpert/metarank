<?php
require_once(dirname(dirname(__FILE__)) . "/../phpinc/session.php");
include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - Terms of Use";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
} ?>
	
<section data-role="main" class="ui-content">
	<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
	<div id="tou_page" data-title="<?php echo $pageTitle; ?>">
        <div class="content-wrapper">
        	<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
            <div id="content_column">
				<?php if (isset($_SESSION["isLoggedIn"]) && $_SESSION["isLoggedIn"]) {
					$service = "";
					$subTitle = "Terms of Use";
					include(BASE_PATH . "/../phpinc/subheader.php");
				} ?>
                <div class="content-box left">
                    <p class="larger-message">Terms of Use</p>
                    <p>Welcome to MetaRank. We are excited that you’ve decided to sign up and check out our site and service. The MetaRank site, http://www.metarank.com, and the underlying service are brought to you by MetaRank. PLEASE READ THESE TERMS OF USE CAREFULLY BEFORE USING THIS SITE. USE OF THIS SITE CONSTITUTES ACCEPTANCE OF THESE TERMS.</p>
                    <p>We reserve the right to update our Terms of Use from time to time without notice. However, the most current version will be posted at http://www.metarank.com/tou/.</p>
                    <p>The MetaRank service is available to you at absolutely no cost and may be de-activated by you at any time.</p>
                    <p class="larger-message">Permissible Use of our Services</p>
                    <p>When you register for our services, you certify that you are at least 13 years old and that you are registering as yourself. You agree to provide accurate, current and complete information about you as part of our registration process and maintain and update that information as needed. You also agree to have registered or signed-up ONLY once with any of our service offerings and to maintain your access through OAuth providers (e.g. Facebook, LinkedIn, or Twitter) in a secure manner and bear fully the responsibility for all use of your account and actions associated with it. Failure to adhere to these guidelines will result in account deletion and removal of all information associated with it.</p>
                    <p>MetaRank services are for personal use only. You may not use MetaRank services to access information that relates to third parties (anyone other than you personally). This restriction does not include data that the services make available about you and your social network associates that is derived from public open source searches or from your social network data you grant us access to.</p>
                    <p>You may not use the service for any activity that is governed by the Fair Credit Reporting Act in the United States or any other similar or equivalent legislation in countries or territories outside of the United States.</p>
                    <p>By signing up for our service and authorizing us to pull content from third parties, you automatically grant, represent and warrant that you have the right to the data contained therein and grant the Company to use, copy, reformat, translate, excerpt (in whole or in part) and distribute such content for any purpose as directed by you. You may remove such content from our site at any time. We do not assert any ownership over content we pulled on you.</p>
                    <p class="larger-message">Intellectual Property</p>
                    <p>All of the “Content” on our site, and any service we provide, is protected by U.S. and International copyright laws and is the property of the Company, its users, or providers of the content under license. By “Content”, we mean any information on our site including, but not limited to, text, graphics, graphical elements, as well as color combinations, button shapes, site layout and look and feel of the web pages. Your use of our services or site gives you no rights in or to our copyrights, trademarks, trade secrets or any other intellectual property rights displayed on our site.</p>
                    <p>You must display all copyrights, trademarks, service marks and other proprietary notices contained in the original Content on any copy you make of the Content. You may not sell or modify the Content or reproduce, display, distribute or otherwise use, publicly or privately, the Content for any commercial or other purpose not specifically authorized by us. The use of the Content on any other site, in a link from any other site or in a networked computer environment for any purpose is prohibited.</p>
                    <p class="larger-message">User Conduct</p>
                    <p>You agree to use MetaRank and its systems for lawful purposes only. You may not use or allow others to use your account to:</p>
                    <ol>
                        <li>Impersonate any person, or falsely state or otherwise misrepresent your affiliation with a person or entity.</li>
                        <li>Forge headers or manipulate identifiers or other data in order to disguise the origin of any content transmitted through our site or to manipulate your presence on our site.</li>
                        <li>Use any automated means, including without limitation, agents, robots, scripts, or spiders to access, monitor, copy or harvest data from any part of our site.</li>
                        <li>Take any action that imposes an unreasonably or disproportionately large load on our infrastructure or disrupts the functioning of our systems or services.</li>
                        <li>Take any action that damages or disrupts the functioning of our systems or services.</li>
                        <li>Sign up for more than one User account, sign up for a User account on behalf of an individual other than yourself.</li>
                        <li>Solicit personal information from anyone under 18 years of age, or solicit passwords or personally identifying information for commercial or unlawful purposes.</li>
                        <li>Attempt to probe, scan or test the vulnerability of our systems or authentication measures.</li>
                        <li>Access or attempt to access data or systems not intended and/or authorized for such user.</li>
                        <li>Disclose or share your password with third parties.</li>
                        <li>Attempt to decipher, recompile, disassemble or reverse engineer any of the software comprising of or in any way a part of the site.</li>
                    </ol>
                    <p>We respect other people’s rights and expect you to do the same.</p>
                    <p class="larger-message">Privacy Policy</p>
                    <p>Please review MetaRank’s privacy policy to understand how we collect, protect and disclose personal information you provide and how we use that information to offer you our products and services.</p>
                    <p class="larger-message">Linking Policy</p>
                    <p>You are granted a limited, non-exclusive right to create a hypertext link to this site provided that such link 1) does not portray MetaRank and/or its affiliates or any of their respective products and services in a false, misleading, derogatory or otherwise defamatory manner; and 2) does not imply any sponsorship, approval, endorsement or affiliation of your site or services by MetaRank. This limited linking right may be revoked at any time.</p>
                    <p>MetaRank makes no representations, warranties or endorsements with respect to any non-MetaRank site which may be accessed from this Site. When you access a non-MetaRank site, please understand that MetaRank has no control over the content or information at that site. It is your responsibility to protect your system from such items as viruses, worms, Trojan horses and other destructive items.</p>
                    
                    <p class="larger-message">Miscellaneous</p>
                    <p>DISCLAIMERS:</p>
                    <p>THIS SITE, AS WELL AS THE SERVICES PROVIDED BY THE COMPANY, ARE PROVIDED “AS IS”. THE COMPANY AND OUR PARTNERS DISCLAIM ANY AND ALL REPRESENTATIONS AND WARRANTIES, EITHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT. The COMPANY CANNOT GUARANTEE AND DOES NOT PROMISE ANY SPECIFIC RESULTS FROM USE OF THE SITE AND/OR THE SERVICE AND/OR ANY PLATFORM APPLICATIONS. NEITHER GLOBAL TOTEM NOR ANY OF OUR PARTNERS MAKE ANY WARRANTY THAT THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIAL THAT YOU OBTAIN FROM THE USE OF OUR SERVICES WILL MEET YOUR EXPECTATIONS. ANY ERRORS THAT WE SHOULD MAKE WILL IMMEDIATELY BE CORRETED. YOU EXPRESSLY AGREE THAT YOU WILL ASSUME THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF OUR SERVICES AND THE ACCURACY OR COMPLETENESS OF THEIR CONTENT.</p>
                    <p>METARANK RETAINS THE RIGHT TO REVISE THESE RULES AT ANY TIME.</p>
                    <p>INDEMNIFICATION – You agree to indemnify, defend, and hold harmless MetaRank, its officers, directors, employees, and any third party information providers to our site from and against all damages, liabilities, costs, charges and expenses, including reasonable attorney’s fees, that we may incur as a result of your breach of the Terms of Use or any claims brought relating to information submitted by you, actions taken by you or in any way relating to your conduct with any other user of MetaRank services.</p>
                    <p>GOVERNING LAW AND JURISDICTION – The Terms and Conditions are governed by the laws of the State of California and any disputes relating to these Terms of Use or use of our site or services will be resolved in the federal or state courts in the State of California. You agree that any legal action or proceeding between MetaRank and you for any purpose concerning this site or the parties’ obligations hereunder shall be brought exclusively in the jurisdiction and venue of the federal and state courts located in Los Angeles County, California.</p>
                    <p>INTERNATIONAL USE – Recognizing the global nature of the Internet, you agree to comply with all local rules including, without limitation, rules about the Internet, data, email or privacy. Specifically, you agree to comply with all applicable laws regarding the transmission of technical data exported from the United States or the country in which you reside. The Company makes no claims that the Content may be lawfully viewed or downloaded outside of the United States. Access to the Content may not be legal by certain persons or in certain countries. If you access the site from outside of the United States, you do so at your own risk and are responsible for compliance with the laws of your jurisdiction.</p>
                    <p>These Terms of Use, including those that are incorporated by reference, constitute the entire and only agreement between you and MetaRank and govern your use of our site. Please visit the Terms of Use page periodically as we may revise them at any time by updating them on our site.</p>
                    <p>Please report any violations of the Terms of Use to us by e-mail.</p>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>