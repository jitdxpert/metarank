<?php
require_once(dirname(dirname(dirname(__FILE__))) . "/../phpinc/session.php");

use Facebook\FacebookRedirectLoginHelper;

if (isset($_GET["disconnect"])) {
	if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
		$_SESSION['redirectURL'] = BASE_URL . '/facebook/options/?disconnect';
		header('Location: ' . BASE_URL . '/login/', true, 303);
		exit();
	} else {
		if (!$metaRankDatabase->disconnectFacebookFromUser($_SESSION["userID"])) {
			$metaRankDatabase->rollbackChanges();
		} else {
			$metaRankDatabase->commitChanges();
			
			$_SESSION["facebookUniqueID"] = null;
			$_SESSION["facebookAccessToken"] = null;
			$_SESSION["facebookAccessTokenExpiration"] = null;
		}
	}
} else {
	if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
		$_SESSION['redirectURL'] = BASE_URL . '/facebook/options/';
		header('Location: ' . BASE_URL . '/login/', true, 303);
		exit();
	}
}

include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - Facebook";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
} ?>

<section data-role="main" class="ui-content">
	<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
    <div id="facebook_options_page" data-title="<?php echo $pageTitle; ?>">
        <div class="content-wrapper">
        	<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
            <div id="content_column">
				<?php
				$service = "Facebook";
				$subTitle = "Facebook Options";
				include(BASE_PATH . "/../phpinc/subheader.php");
				?>
                <div class="content-box">
                    <p class="left larger-message">Facebook Options</p>
					<?php if (!isset($_SESSION["facebookUniqueID"]) || $_SESSION["facebookUniqueID"] == "") {
						require_once(BASE_PATH . "/../phpinc/db.php");
						require_once(BASE_PATH . "/../phpinc/lib/fb/facebook.php");
						
						$_SESSION["facebookUniqueID"] = "";
						$fbscope = array("user_birthday","user_location","email","read_stream","user_about_me","user_education_history","user_work_history","user_website","user_friends");
						//$fbscope = array("user_birthday","user_location","email","user_about_me","user_education_history","user_work_history","user_website","user_friends");
						//$fbscope = array('scope'=>'manage_pages,read_insights,user_photos,email,publish_actions,user_birthday,user_about_me,user_website,user_friends,user_work_history,user_location,user_education_history');
						//$fbLoginHelper = new FacebookRedirectLoginHelper(curHostURL() . "/");
						$fbLoginHelper = new FacebookRedirectLoginHelper(curHostURL() . "/metarank/htdocs/"); ?>
                        <p>
                        	<a href="<?php echo $fbLoginHelper->getLoginUrl($fbscope); ?>" data-ajax="false">
                            	<button type="button">Connect to Facebook</button>
                            </a>
                        </p>
					<?php } else { ?>
	                    <p>
                        	<a href="<?php echo BASE_URL; ?>/facebook/options?disconnect" data-ajax="false">
                            	<button type="button">Disconnect from Facebook</button>
                            </a>
                        </p>
					<?php } ?>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>