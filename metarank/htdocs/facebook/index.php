<?php
require_once(dirname(dirname(__FILE__)) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/facebook/';
	header('Location: ' . BASE_URL . '/login/', true, 303);
	exit();
}

if (!isset($_SESSION["facebookUniqueID"]) || $_SESSION["facebookUniqueID"] == null) {
	header('Location: ' . BASE_URL . '/facebook/options/', true, 303);
	exit();
}

include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - Facebook";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}

$service = "Facebook";
$reportCardDetails = $metaRankDatabase->getFacebookReportCardDetails($_SESSION["userID"]);
$postalCodeRankResults = $metaRankDatabase->getFacebookPostalCodeRank($_SESSION["userID"]);
$localityRankResults = $metaRankDatabase->getFacebookLocalityRank($_SESSION["userID"]);
$administrativeAreaRankResults = $metaRankDatabase->getFacebookAdministrativeAreaRank($_SESSION["userID"]);
$countryRankResults = $metaRankDatabase->getFacebookCountryRank($_SESSION["userID"]);
$globalRankResults = $metaRankDatabase->getFacebookGlobalRank($_SESSION["userID"]);
if ($globalRankResults != null) {
	$tmp = $metaRankDatabase->getFacebookGlobalParticipants();
	if ($tmp != null) {
		$globalRankResults += $tmp;
	} else {
		$globalRankResults = null;
	}
}
$plot1Data = $metaRankDatabase->getFacebookRankHistory($_SESSION["userID"]);
$plot2Data = $metaRankDatabase->getFacebookParticipantsHistory();
$plot3Data = $metaRankDatabase->getFacebookScoreHistory($_SESSION["userID"]);

$leaderboard = $metaRankDatabase->getFacebookLeaderboard($_SESSION["userID"]);
$AllScore    = $metaRankDatabase->getFacebookScore();
?>

<section data-role="main" class="ui-content">
	<?php include(BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
    <div id="facebook_page" data-title="<?php echo $pageTitle; ?>">
        <div class="content-wrapper">
			<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
            <div id="content_column">
				<?php
				include(BASE_PATH . "/../phpinc/subheader.php");
				include(BASE_PATH . "/../phpinc/ranks.php");
				include(BASE_PATH . "/../phpinc/leaderboard.php");
				?>
                <div class="content-box">
                    <p class="left larger-message">Details</p>
                    <table class="service-details">
                        <tbody>
                            <tr>
                                <td>Checkins:</td>
                                <td><?php echo $reportCardDetails["checkins"]; ?></td>
                            </tr>
                            <tr>
                                <td>Checkin Likes:</td>
                                <td><?php echo $reportCardDetails["checkin_likes"]; ?></td>
                            </tr>
                            <tr>
                                <td>Status Posts:</td>
                                <td><?php echo $reportCardDetails["status_posts"]; ?></td>
                            </tr>
                            <tr>
                                <td>Links Posted:</td>
                                <td><?php echo $reportCardDetails["links_posted"]; ?></td>
                            </tr>
                            <tr>
                                <td>Photos Posted:</td>
                                <td><?php echo $reportCardDetails["photos_posted"]; ?></td>
                            </tr>
                            <tr>
                                <td>Likes of your Photos:</td>
                                <td><?php echo $reportCardDetails["liked_photos"]; ?></td>
                            </tr>
                            <tr>
                                <td>Videos Posted:</td>
                                <td><?php echo $reportCardDetails["videos_posted"]; ?></td>
                            </tr>
                            <tr>
                                <td>Likes of your Videos:</td>
                                <td><?php echo $reportCardDetails["liked_videos"]; ?></td>
                            </tr>
                            <tr>
                                <td>Comments You Posted:</td>
                                <td><?php echo $reportCardDetails["comments_posted"]; ?></td>
                            </tr>
                            <tr>
                                <td>Likes of your Comments:</td>
                                <td><?php echo $reportCardDetails["liked_comments"]; ?></td>
                            </tr>
                            <tr>
                                <td>Tagged Videos:</td>
                                <td><?php echo $reportCardDetails["tagged_videos"]; ?></td>
                            </tr>
                            <tr>
                                <td>Tagged Checkins:</td>
                                <td><?php echo $reportCardDetails["tagged_checkins"]; ?></td>
                            </tr>
                            <tr>
                                <td>Tagged Photos:</td>
                                <td><?php echo $reportCardDetails["tagged_photos"]; ?></td>
                            </tr>
                            <tr>
                                <td>Wall Posts Received:</td>
                                <td><?php echo $reportCardDetails["wall_posts_received"]; ?></td>
                            </tr>
                            <tr>
                                <td>Comments Received:</td>
                                <td><?php echo $reportCardDetails["comments_received"]; ?></td>
                            </tr>
                            <tr class="bold">
                                <td>Unique Individuals:</td>
                                <td><?php echo $reportCardDetails["unique_users"]; ?></td>
                            </tr>
                    	</tbody>
                    </table>
                    <p class="left">
                    	Facebook Updated: 
						<script id="modified_time" type="text/javascript">
						$("#modified_time").replaceWith(new Date("<?php echo $reportCardDetails["modified_time"]; ?>").toLocaleString());
                        </script>.
                    </p>
                    <p class="left">Note: The Facebook Report Card is based on activity from the previous 180 days. Facebook rank is updated daily.</p>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>