<?php
require_once(dirname(dirname(dirname(__FILE__))) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/friends/';
	header('Location: ' . BASE_URL . '/login/', true, 303);
	exit();
}

include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - Friends";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}

$pendingSentFriendRequests = $metaRankDatabase->getPendingSentFriendRequests($_SESSION["userID"]);
$pendingReceivedFriendRequests = $metaRankDatabase->getPendingReceivedFriendRequests($_SESSION["userID"]);
$friends = $metaRankDatabase->getFriends($_SESSION["userID"]);

$leaderboard = $metaRankDatabase->getFriendsLeaderboard($_SESSION["userID"]); ?>

<section data-role="main" class="ui-content">
	<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
    <div id="friends_page" data-title="<?php echo $pageTitle; ?>">
        <div class="content-wrapper">
			<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
            <div id="content_column">
				<?php
				$service = "";
				$subTitle = "Friends";
				include(BASE_PATH . "/../phpinc/subheader.php");
				?>
                <div class="content-box">
                    <p class="left larger-message">Send Friend Request</p>
                    <p class="left">Let your friends know about MetaRank:</p>
                    <div class="left" style="margin-bottom: 1em;">
                        <div class="fb-send" data-href="http://www.metarank.com/" data-colorscheme="light" style="z-index:9001;"></div>
                        <script type="IN/Share" data-url="http://www.metarank.com/" data-onsuccess="linkedInShareCallback"></script>
                        <a href="https://twitter.com/share" class="twitter-share-button" data-text="Check out this site!" data-url="http://www.metarank.com" data-related="MetaRank" data-count="none">Tweet</a>
                    </div>
                    <p class="left">Add your Facebook friends already on MetaRank:</p>
                    <button type="submit" onclick="addFacebookFriends()">Add Friends from Facebook</button>
                    <p class="left">Search by Email: </p>
                    <input type="email" id="email_search" name="email" required />
                    <button type="button" onclick="emailSearch()">Search</button>
                    <table id="email_search_results"></table>
                </div>
                <div class="content-box">
                    <p class="left larger-message">Pending Friend Requests</p>
                    <p class="left">Sent:</p>
                    <table id="pending_sent_friend_requests">
						<?php if ($pendingSentFriendRequests === null || count($pendingSentFriendRequests) == 0) { ?>
                            <tr>
                                <td colspan="3" style="text-align: center;">No pending requests.</td>
                            </tr>
						<?php } else {
							foreach ($pendingSentFriendRequests as $pendingRequest) {
								if ($pendingRequest["profile_picture"] == "facebook") {
							        $profilePictureURL = "https://graph.facebook.com/" . $pendingRequest["facebook_id"] . "/picture";
								} else if ($pendingRequest["profile_picture"] == "linkedin") {
									require_once(BASE_PATH . '/../phpinc/lib/li/linkedin.php');
									$linkedinSession->setAccessToken($pendingRequest["linkedin_oauth_token"]);
									try {
										$information = $linkedinSession->get("/people/~:(picture-url)");
										$profilePictureURL = $information["pictureUrl"];
									} catch (\RuntimeException $re) {
										//TODO: Handle this error
							            $profilePictureURL = BASE_URL . "/img/anonymous.png";
									}
								} else if ($pendingRequest["profile_picture"] == "twitter") {
									$profilePictureURL = $pendingRequest["twitter_profile_picture_url"];
								} else {
									$profilePictureURL = BASE_URL . "/img/anonymous.png";
								} ?>
                                <tr>
                                    <td><img src="<?php echo $profilePictureURL; ?>" alt="<?php echo $pendingRequest["first_name"] . " " . $pendingRequest["last_name"]; ?>"/></td>
                                    <td><?php echo $pendingRequest["first_name"] . " " . $pendingRequest["last_name"]; ?></td>
                                    <td><button type="button" data-friend="<?php echo $pendingRequest["id"]; ?>" onclick="cancelFriendRequest(this)">Cancel</button></td>
                                </tr>
							<?php 
							}
						} ?>
                    </table>
                    <br />
                    <p class="left">Received:</p>
                    <table id="pending_received_friend_requests">
						<?php if ($pendingReceivedFriendRequests === null || count($pendingReceivedFriendRequests) == 0) { ?>
                            <tr>
                                <td colspan="4" style="text-align: center;">No received requests.</td>
                            </tr>
						<?php } else {
							foreach ($pendingReceivedFriendRequests as $pendingRequest) {
								if ($pendingRequest["profile_picture"] == "facebook") {
									$profilePictureURL = "https://graph.facebook.com/" . $pendingRequest["facebook_id"] . "/picture";
								} else if ($pendingRequest["profile_picture"] == "linkedin") {
									require_once(BASE_PATH . '/../phpinc/lib/li/linkedin.php');
									$linkedinSession->setAccessToken($pendingRequest["linkedin_oauth_token"]);
									try {
										$information = $linkedinSession->get("/people/~:(picture-url)");
										$profilePictureURL = $information["pictureUrl"];
									} catch (\RuntimeException $re) {
										//TODO: Handle this error
										$profilePictureURL = BASE_URL . "/img/anonymous.png";
									}
								} else if ($pendingRequest["profile_picture"] == "twitter") {
									$profilePictureURL = $pendingRequest["twitter_profile_picture_url"];
								} else {
									$profilePictureURL = "/img/anonymous.png";
								} ?>
                                <tr>
                                    <td><img src="<?php echo $profilePictureURL;?>" alt="<?php echo $pendingRequest["first_name"]." ".$pendingRequest["last_name"]; ?>"/></td>
                                    <td><?php echo $pendingRequest["first_name"] . " " . $pendingRequest["last_name"]; ?></td>
                                    <td><button type="button" data-friend="<?php echo $pendingRequest["id"]; ?>" onclick="acceptFriendRequest(this)">Accept</button></td>
                                    <td><button type="button" data-friend="<?php echo $pendingRequest["id"]; ?>" onclick="declineFriendRequest(this)">Decline</button></td>
                                </tr>
							<?php 
							}
						} ?>
                    </table>
                </div>
                <div class="content-box">
                    <p class="left larger-message">Friends</p>
                    <table id="friends">
						<?php if ($friends === null || count($friends) == 0) { ?>
                            <tr>
                                <td colspan="3" style="text-align: center;">You have no friends. Try searching for some!</td>
                            </tr>
						<?php } else {
							foreach ($friends as $friend) {
								if ($friend["profile_picture"] == "facebook") {
									$profilePictureURL = "https://graph.facebook.com/" . $friend["facebook_id"] . "/picture";
								} else if ($friend["profile_picture"] == "linkedin") {
									require_once(BASE_PATH . '/../phpinc/lib/li/linkedin.php');
									$linkedinSession->setAccessToken($friend["linkedin_oauth_token"]);
									try {
										$information = $linkedinSession->get("/people/~:(picture-url)");
										$profilePictureURL = $information["pictureUrl"];
									} catch (\RuntimeException $re) {
										//TODO: Handle this error
										$profilePictureURL = BASE_URL . "/img/anonymous.png";
									}
								} else if ($friend["profile_picture"] == "twitter") {
									$profilePictureURL = $friend["twitter_profile_picture_url"];
								} else {
									$profilePictureURL = BASE_URL . "/img/anonymous.png";
								} ?>
                                <tr>
                                    <td><img src="<?php echo $profilePictureURL; ?>" alt="<?php echo $friend["first_name"] . " " . $friend["last_name"]; ?>" /></td>
                                    <td><?php echo $friend["first_name"] . " " . $friend["last_name"]; ?></td>
                                    <td><button type="button" data-friend="<?php echo $friend["id"]; ?>" onclick="deleteFriend(this)">Delete</button></td>
                                </tr>
							<?php
							}
						} ?>
                    </table>
                </div>
				<?php include(BASE_PATH . "/../phpinc/friend_leaderboard.php"); ?>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>