<?php
if (!isset($_GET["uuid"]) || strlen($_GET["uuid"]) != 32 || !ctype_xdigit($_GET["uuid"])) {
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

require_once(dirname(dirname(dirname(__FILE__))) . "/../phpinc/session.php");

$pageTitle = "MetaRank - Verify Email";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
} ?>

<section data-role="main" class="ui-content">
	<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
    <div id="verify_email_page" data-title="<?php echo $pageTitle; ?>">
        <div class="content-wrapper">
            <div class="green-header">
                <p>Email Verification</p>
            </div>
            <div class="grey-box">
				<?php
				if ($metaRankDatabase->verifyEmail($_GET["uuid"])) {
					$metaRankDatabase->commitChanges(); ?>
                    <p>Email successfully verified!</p>
					<?php if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
						$_SESSION['redirectURL'] = BASE_URL . '/account/'; ?>
                        
                        <p>Redirecting to login page...</p>
                        <script type="text/javascript">
						setTimeout(function() {
							$(document.body).pagecontainer('change', '<?php echo BASE_URL; ?>/login/');
						}, 3000);
						</script>
					<?php } else { ?>
                        <p>Redirecting to MetaRank Report Card...</p>
                        <script type="text/javascript">
						setTimeout(function() {
							$(document.body).pagecontainer('change', '<?php echo BASE_URL; ?>/');
						}, 3000);
                        </script>
					<?php 
					}
				} else { ?>
         	       <p>Unable to verify email address. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.</p>
				<?php } ?>
            </div>
            <div id="navigation_box">
                <a href="<?php echo BASE_URL; ?>/" data-ajax="false">
                    <button>Home</button>
                </a>
            </div>
        </div>
    </div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>