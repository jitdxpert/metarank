<?php
require_once(dirname(dirname(dirname(__FILE__))) . "/../phpinc/session.php");
include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - Benefits";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
} ?>

<section data-role="main" class="ui-content">
	<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
    <div id="about_page" data-title="<?php echo $pageTitle; ?>">
    	<div class="content-wrapper">
        	<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
            <div id="content_column">
				<?php if (isset($_SESSION["isLoggedIn"]) && $_SESSION["isLoggedIn"]) {
					$service = "";
					$subTitle = "Benefits";
					include(BASE_PATH . "/../phpinc/subheader.php");
				} ?>
                <div class="content-box left">
                	<p class="larger-message">Benefits</p>
                    <p>Coming soon.</p>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>