<?php
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/settings/credentials/';
	header('Location: ' . BASE_URL . '/login/', true, 303);
	exit();
}

include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - Manage Credentials";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
} ?>

<section data-role="main" class="ui-content">
	<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
	<div id="change_credentials_page" data-title="<?php echo $pageTitle; ?>">
		<div class="content-wrapper">
			<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
			<div id="content_column">
				<?php
                $service = "";
                $subTitle = "Manage Credentials";
                include(BASE_PATH . "/../phpinc/subheader.php");
                ?>
				<div class="content-box">
					<form action="<?php echo BASE_URL; ?>/ajax/change_credentials.php" method="POST">
						<div id="fields">
							<label for="password">Current Password:</label>
							<input type="password" id="password" name="password" required />
							<p>Leave new password fields blank to retain current password.</p>
							<label for="new_password">New Password:</label>
							<input type="password" id="new_password" name="new_password" pattern="<?php echo PASSWORD_HTML5_REGULAR_EXPRESSION; ?>" onchange="
								this.setCustomValidity(this.validity.patternMismatch ? 'Password must be at least 6 characters long and contain at least one digit, one uppercase letter, one lowercase letter' : '');
								if(this.checkValidity()) document.getElementById('new_password_confirm').pattern = this.value.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
							"/>
							<label for="new_password_confirm">Confirm New Password:</label>
							<input type="password" id="new_password_confirm" name="new_password_confirm" pattern="<?php echo PASSWORD_HTML5_REGULAR_EXPRESSION; ?>" onchange="
								this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above.' : '');
							" />
                            
							<?php /*?>
                            <label for="security_question">Security Question:</label>
							<select id="security_question" name="security_question">
								<option value="">Select a question if you wish to change it.</option>
								<?php
								$securityQuestions = $metaRankDatabase->getSecurityQuestions();
								foreach ($securityQuestions as $row) { ?>
									<option value="<?php echo $row["id"]; ?>"><?php echo $row["question"]; ?></option>
								<?php } ?>
							</select>
							<label for="security_answer">Answer:</label>
							<input type="text" id="security_answer" name="security_answer" />
                            <?php */?>
                            
							<div id="status_message" style="display:none;"></div>
							<button type="submit">Change Information</button>
							<p><button type="button" onclick="window.history.back()">Back</button></p>
						</div>
						<div id="elevation_captcha">
							<?php if ($metaRankDatabase->getElevationFailureCount($_SESSION["userID"]) > MAX_ELEVATION_FAILURES) { ?>
								<script type="text/javascript">
                                showRecaptcha($("#elevation_captcha")[0]);
                                </script>
							<?php } ?>
                        </div>
					</form>
				</div>
				<div style="clear:both;"></div>
				<script type="text/javascript">
				$("#change_credentials_page form").on("submit", changeCredentials);
				</script>
			</div>
		</div>
	</div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>