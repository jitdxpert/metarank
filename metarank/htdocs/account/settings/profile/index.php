<?php
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/settings/profile/';
	header('Location: ' . BASE_URL . '/login/', true, 303);
	exit();
}

include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - Manage Profile";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
} ?>

<section data-role="main" class="ui-content">
	<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
	<div id="edit_profile_page" data-title="<?php echo $pageTitle; ?>">
		<div class="content-wrapper">
        	<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
			<div id="content_column">
				<?php
				$service = "";
				$subTitle = "Manage Credentials";
				include(BASE_PATH . "/../phpinc/subheader.php");
				?>
				<div class="content-box">
					<form action="<?php echo BASE_URL; ?>/ajax/update_profile.php" method="POST">
						<div id="fields">
							<label for="first_name">First Name:</label>
							<input type="text" id="first_name" name="first_name" value="<?php echo $_SESSION["userFirstName"]; ?>" required />
							<label for="last_name">Last Name:</label>
							<input type="text" id="last_name" name="last_name" value="<?php echo $_SESSION["userLastName"]; ?>" required />
							<label for="date_of_birth">Date of Birth:</label>
							<input type="date" id="date_of_birth" placeholder="YYYY-mm-dd" name="date_of_birth"  value="<?php echo $_SESSION["userDateOfBirth"]; ?>" />
							<label>Gender:</label>
							<label><input type="radio" name="gender" value="F"<?php echo ($_SESSION["userGender"] == "F" ? " checked" : ""); ?>> Female</label>
							<label><input type="radio" name="gender" value="M"<?php echo ($_SESSION["userGender"] == "M" ? " checked" : ""); ?>> Male</label>
							</label>
							<label for="phone">Phone:</label>
							<input type="tel" id="phone" name="phone" value="<?php echo $_SESSION["userPhone"]; ?>" required />
							<label for="email">Email:</label>
							<input type="email" id="email" name="email" value="<?php echo $_SESSION["userEmail"]; ?>" disabled />
							<p>To change your email, please contact technical support.</p>
							<div id="status_message" style="display:none;"></div>
							<button type="submit">Update</button>
							<button type="button" onclick="window.history.back()">Back</button>
						</div>
					</form>
				</div>
				<div style="clear:both;"></div>
				<script type="text/javascript">
				$("#edit_profile_page form").on("submit", updateProfile);
				</script>
			</div>
		</div>
	</div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>