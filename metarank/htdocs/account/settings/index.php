<?php
require_once(dirname(dirname(dirname(__FILE__))) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/settings/';
	header('Location: ' . BASE_URL . '/login/', true, 303);
	exit();
}

include(BASE_PATH . "/../phpinc/reauthorization.php");

$pageTitle = "MetaRank - Settings";
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
} ?>

<section data-role="main" class="ui-content">
	<?php include (BASE_PATH . "/../phpinc/google_analytics_action.php"); ?>
	<div id="settings_page" data-title="<?php echo $pageTitle; ?>">
		<div class="content-wrapper">
			<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
			<div id="content_column">
				<?php
                $service = "";
                $subTitle = "Account Settings";
                include(BASE_PATH . "/../phpinc/subheader.php");
                ?>
				<div class="content-box">
					<p class="left larger-message">General Settings</p>
					<div id="status_message"></div>
					<p class="settings-label">First Name: <span id="first_name"><?php echo $_SESSION["userFirstName"]; ?></span></p>
					<p class="settings-label">Last Name: <span id="first_name"><?php echo $_SESSION["userLastName"]; ?></span></p>
					<p class="settings-label">Date of Birth: <span id="date_of_birth_value"></span></p>
					<script type="text/javascript">$("#date_of_birth_value")[0].innerText = new Date("<?php echo $_SESSION["userDateOfBirth"]; ?>").toLocaleDateString();</script>
					<p class="settings-label">Gender: <span id="gender"><?php echo ($_SESSION["userGender"] == "F" ? "Female" : ($_SESSION["userGender"] == "M" ? "Male" : "unspecified")); ?></span></p>
					<p class="settings-label">E-mail: <span id="email"><?php echo $_SESSION["userEmail"]; ?></span></p>
					<p class="settings-label">Phone: <span id="phone"><?php echo $_SESSION["userPhone"]; ?></span></p>
					<p><a href="<?php echo BASE_URL; ?>/account/settings/profile/" data-ajax="false"><button>Update Profile</button></a></p>
					<p><a href="<?php echo BASE_URL; ?>/account/settings/credentials/" data-ajax="false"><button>Change Password</button></a></p>
					<br>
					<p class="settings-label">Privacy:</p>
					<label for="private_scores">Keep my scores private</label>
					<input type="checkbox" id="private_scores" name="privacy_flags" value="1"<?php echo (($_SESSION["userPrivacyFlags"] & 1) == 1 ? " checked" : ""); ?>>
					<br>
					<p class="settings-label">E-mail Notifications:</p>
					<label for="notification_account_changes">Changes to my account</label>
					<input type="checkbox" id="notification_account_changes" name="email_flags" value="1"<?php echo (($_SESSION["userEmailFlags"] & 1) == 1 ? " checked" : ""); ?>>
					<label for="notification_rank_changes">Changes to my rank</label>
					<input type="checkbox" id="notification_rank_changes" name="email_flags" value="2"<?php echo (($_SESSION["userEmailFlags"] & 2) == 2 ? " checked" : ""); ?>>
					<label for="notification_news">News &amp; updates about MetaRank</label>
					<input type="checkbox" id="notification_news" name="email_flags" value="4"<?php echo (($_SESSION["userEmailFlags"] & 4) == 4 ? " checked" : ""); ?>>
				</div>
				<div class="content-box">
					<p class="left larger-message">Profile Picture</p>
					<select id="profile_picture" data-role="none">
						<?php if ($_SESSION["facebookUniqueID"] != null) { ?>
							<option value="facebook" data-img-src="https://graph.facebook.com/<?php echo $_SESSION["facebookUniqueID"]; ?>/picture"<?php echo ($_SESSION["userProfilePicture"] == "facebook" ? " selected" : ""); ?>>Facebook</option>
						<?php }
						if ($_SESSION["linkedInUniqueID"] != null) {
							require_once(BASE_PATH . '/../phpinc/lib/li/linkedin.php');
							$linkedinSession->setAccessToken($_SESSION["linkedInOAuthToken"]);
							try {
								$information = $linkedinSession->get("/people/~:(picture-url)");
								
								$pictureURL = $information["pictureUrl"];
							} catch (\RuntimeException $re) {
								//TODO: Handle this error
							} ?>
                            <option value="linkedin" data-img-src="<?php echo $pictureURL; ?>"<?php echo ($_SESSION["userProfilePicture"] == "linkedin" ? " selected" : ""); ?>>LinkedIn</option>
						<?php 
						}
						if ($_SESSION["twitterUniqueID"] != null) {
							$pictureURL = $_SESSION["twitterProfilePictureURL"]; ?>
                            <option value="twitter" data-img-src="<?php echo $pictureURL; ?>"<?php echo ($_SESSION["userProfilePicture"] == "twitter" ? " selected" : ""); ?>>Twitter</option>
						<?php } ?>
					</select>
					<script type="text/javascript">
					$("#profile_picture").imagepicker({selected: updateProfilePicture, show_label: true});
					</script>
				</div>
				<div class="content-box">
					<p class="left larger-message">Linked Services</p>
					<?php $noServices = true;
					if ($_SESSION["facebookUniqueID"] != null) {
						$noServices = false; ?>
						<p><a href="<?php echo BASE_URL; ?>/facebook/options/" data-ajax="false"><button>Facebook Options</button></a></p>
					<?php
					}
					if ($_SESSION["linkedInUniqueID"] != null) {
						$noServices = false; ?>
						<p><a href="<?php echo BASE_URL; ?>/linkedin/options/" data-ajax="false"><button>LinkedIn Options</button></a></p>
					<?php
					}
					if ($_SESSION["twitterUniqueID"] != null) {
						$noServices = false; ?>
						<p><a href="<?php echo BASE_URL; ?>/twitter/options/" data-ajax="false"><button>Twitter Options</button></a></p>
					<?php
					}
					if ($noServices) { ?>
						<p>No services have been linked.</p>
					<?php
					} ?>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
	</div>
</section>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>