<div data-role="panel" data-position="right" data-display="reveal" data-theme="a" id="mobile_menu">
    <?php if (!isset($_SESSION["isLoggedIn"]) || !$_SESSION["isLoggedIn"]) { ?>
    	<h3>METARANK</h3>
        <ul>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/" onclick="ga('send','event','MobileMenu','click','Home')" data-ajax="false">Home</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/register/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/register/" onclick="ga('send','event','MobileMenu','click','Register')" data-ajax="false">Sign Up</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/login/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/login/" onclick="ga('send','event','MobileMenu','click','Login')" data-ajax="false">Login</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/contact/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/contact/" onclick="ga('send','event','MobileMenu','click','Contact')" data-ajax="false">Contact</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/about/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/about/" onclick="ga('send','event','MobileMenu','click','About')" data-ajax="false">About</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/faq/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/faq/" onclick="ga('send','event','MobileMenu','click','FAQ')" data-ajax="false">FAQ</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/tou/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/tou/" onclick="ga('send','event','MobileMenu','click','Terms')" data-ajax="false">Terms of Use</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/privacy/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/privacy/" onclick="ga('send','event','MobileMenu','click','Privacy')" data-ajax="false">Privacy Policy</a>
            </li>
        </ul>
    <?php } else { ?>
        <h3>RANKINGS</h3>
        <ul>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/" onclick="ga('send','event','MobileMenu','click','MetaRank')" data-ajax="false">MetaRank</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/facebook/"||$_SERVER["REQUEST_URI"] == "/metarank/htdocs/facebook/options/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/facebook/" onclick="ga('send','event','MobileMenu','click','Facebook')" data-ajax="false">Facebook</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/linkedin/"||$_SERVER["REQUEST_URI"] == "/metarank/htdocs/linkedin/options/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/linkedin/" onclick="ga('send','event','MobileMenu','click','LinkedIn')" data-ajax="false">LinkedIn</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/twitter/"||$_SERVER["REQUEST_URI"] == "/metarank/htdocs/twitter/options/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/twitter/" onclick="ga('send','event','MobileMenu','click','Twitter')" data-ajax="false">Twitter</a>
            </li>
        </ul>
        
        <h3>MY ACCOUNT</h3>
        <ul>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/account/friends/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/account/friends/" onclick="ga('send','event','Menu','click','Friends')" data-ajax="false">Friends</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/account/benefits/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/account/benefits/" onclick="ga('send','event','Menu','click','Benefits')" data-ajax="false">Benefits</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/account/settings/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/account/settings/" onclick="ga('send','event','Menu','click','Settings')" data-ajax="false">Settings</a>
            </li>        
            <li>
                <a href="http://viral.dev.metarank.com/" onclick="ga('send','event','Menu','click','Viral')" data-ajax="false">Viral</a>
            </li>
        </ul>

        <h3>COMPANY</h3>
        <ul>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/contact/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/contact/" onclick="ga('send','event','Menu','click','Contact')" data-ajax="false">Contact</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/about/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/about/" onclick="ga('send','event','Menu','click','About')" data-ajax="false">About</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/faq/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/faq/" onclick="ga('send','event','Menu','click','FAQ')" data-ajax="false">FAQ</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/tou/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/tou/" onclick="ga('send','event','Menu','click','Terms')" data-ajax="false">Terms of Use</a>
            </li>
            <li<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/privacy/" ? ' class="active"' : ''); ?>>
                <a href="<?php echo BASE_URL; ?>/privacy/" onclick="ga('send','event','Menu','click','Privacy')" data-ajax="false">Privacy Policy</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL; ?>/logout/" onclick="ga('send','event','Menu','click','Logout')" data-ajax="false">Log Out </a>
            </li>
        </ul>
    <?php } ?>
</div>