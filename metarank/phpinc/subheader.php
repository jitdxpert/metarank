<?php
if ($_SESSION["userProfilePicture"] == "facebook") {
	$pictureURL = "https://graph.facebook.com/" . $_SESSION["facebookUniqueID"] . "/picture";
} else if ($_SESSION["userProfilePicture"] == "linkedin") {
	require_once(dirname(__FILE__) . '/lib/li/linkedin.php');
	$linkedinSession->setAccessToken($_SESSION["linkedInOAuthToken"]);
	try {
		$information = $linkedinSession->get("/people/~:(picture-url)");
		$pictureURL = $information["pictureUrl"];
	} catch (\RuntimeException $re) {
		//TODO: Handle this error
		$pictureURL = BASE_URL . "/img/anonymous.png";
	}
} else if ($_SESSION["userProfilePicture"] == "twitter") {
	//TODO: Handle this error
	$pictureURL = $_SESSION["twitterProfilePictureURL"];
} else {
	// MetaRank picture
	$pictureURL = BASE_URL . "/img/anonymous.png";
} ?>

<div class="valign-center-wrap">
	<div class="valign-center top-box">
    	<img class="user-image" src="<?php echo $pictureURL; ?>">
        <p class="user-name"><?php echo $_SESSION["userFirstName"] . " " . $_SESSION["userLastName"]; ?></p>
    </div>
    <div class="valign-center top-box">
    	<img class="service-image" src="<?php echo BASE_URL; ?>/img/logo<?php echo ($service != "" ? "_" . $service : ""); ?>.png">
    </div>
    <div class="valign-center top-box">
		<?php if (isset($subTitle)) { ?>
        	<p class="meta-score-label"><?php echo $subTitle; ?></p>
		<?php } else { ?>
        	<p class="meta-score-label"><?php echo ($service == "" ? "Metascore" : "Score"); ?></p>
            <p class="meta-score"><?php echo round($reportCardDetails["score"], 0); ?></p>
		<?php } ?>
    </div>
</div>