		<?php include(BASE_PATH . "/../phpinc/mobile-menu.php"); ?>
        
        <footer data-role="footer" data-theme="a">
            <div class="content-wrapper">
                <span>
                    <a href="<?php echo BASE_URL; ?>/contact/" onclick="ga('send','event','Footer','click','Contact')" data-ajax="false">Contact</a> &#8226;
                    <a href="<?php echo BASE_URL; ?>/about/" onclick="ga('send','event','Footer','click','About')" data-ajax="false">About</a> &#8226;
                    <a href="<?php echo BASE_URL; ?>/faq/" onclick="ga('send','event','Footer','click','FAQ')" data-ajax="false">FAQ</a> &#8226;
                    <a href="<?php echo BASE_URL; ?>/tou/" onclick="ga('send','event','Footer','click','Terms')" data-ajax="false">Terms</a> &#8226;
                    <a href="<?php echo BASE_URL; ?>/privacy/" onclick="ga('send','event','Footer','click','Privacy')" data-ajax="false">Privacy</a>
                </span>
                <span>MetaRank &copy; 2014</span>
            </div>
        </footer><!-- /footer -->
   	</section>
</body>
</html>