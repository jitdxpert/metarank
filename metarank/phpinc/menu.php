<aside id="menu">
	<?php if (!isset($_SESSION["isLoggedIn"]) || !$_SESSION["isLoggedIn"]) { ?>
    	<h3>METARANK</h3>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/" ? ' class="active"' : ''); ?>>
        	<a href="<?php echo BASE_URL; ?>/" onclick="ga('send','event','Menu','click','Home')" data-ajax="false">Home</a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/register/" ? ' class="active"' : ''); ?>>
        	<a href="<?php echo BASE_URL; ?>/register/" onclick="ga('send','event','Menu','click','Register')" data-ajax="false">Sign Up</a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/login/" ? ' class="active"' : ''); ?>>
        	<a href="<?php echo BASE_URL; ?>/login/" onclick="ga('send','event','Menu','click','Login')" data-ajax="false">Login</a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/contact/" ? ' class="active"' : ''); ?>>
            <a href="<?php echo BASE_URL; ?>/contact/" onclick="ga('send','event','Menu','click','Contact')" data-ajax="false">Contact</a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/about/" ? ' class="active"' : ''); ?>>
            <a href="<?php echo BASE_URL; ?>/about/" onclick="ga('send','event','Menu','click','About')" data-ajax="false">About</a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/faq/" ? ' class="active"' : ''); ?>>
            <a href="<?php echo BASE_URL; ?>/faq/" onclick="ga('send','event','Menu','click','FAQ')" data-ajax="false">FAQ</a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/tou/" ? ' class="active"' : ''); ?>>
            <a href="<?php echo BASE_URL; ?>/tou/" onclick="ga('send','event','Menu','click','Terms')" data-ajax="false">Terms of Use</a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/privacy/" ? ' class="active"' : ''); ?>>
            <a href="<?php echo BASE_URL; ?>/privacy/" onclick="ga('send','event','Menu','click','Privacy')" data-ajax="false">Privacy Policy</a>
        </div>
	<?php } else { ?>
        <h3>RANKINGS</h3>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/" ? ' class="active"' : ''); ?>>
        	<a href="<?php echo BASE_URL; ?>/" onclick="ga('send','event','Menu','click','MetaRank')" data-ajax="false">MetaRank </a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/facebook/"||$_SERVER["REQUEST_URI"] == "/metarank/htdocs/facebook/options/" ? ' class="active"' : ''); ?>>
	        <a href="<?php echo BASE_URL; ?>/facebook/" onclick="ga('send','event','Menu','click','Facebook')" data-ajax="false">Facebook </a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/linkedin/"||$_SERVER["REQUEST_URI"] == "/metarank/htdocs/linkedin/options/" ? ' class="active"' : ''); ?>>
	        <a href="<?php echo BASE_URL; ?>/linkedin/" onclick="ga('send','event','Menu','click','LinkedIn')" data-ajax="false">LinkedIn </a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/twitter/"||$_SERVER["REQUEST_URI"] == "/metarank/htdocs/twitter/options/" ? ' class="active"' : ''); ?>>
        	<a href="<?php echo BASE_URL; ?>/twitter/" onclick="ga('send','event','Menu','click','Twitter')" data-ajax="false">Twitter </a>
        </div>
        
       	<h3>MY ACCOUNT</h3>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/account/friends/" ? ' class="active"' : ''); ?>>
	        <a href="<?php echo BASE_URL; ?>/account/friends/" onclick="ga('send','event','Menu','click','Friends')" data-ajax="false">Friends</a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/account/benefits/" ? ' class="active"' : ''); ?>>
	        <a href="<?php echo BASE_URL; ?>/account/benefits/" onclick="ga('send','event','Menu','click','Benefits')" data-ajax="false">Benefits</a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/account/settings/" ? ' class="active"' : ''); ?>>
        	<a href="<?php echo BASE_URL; ?>/account/settings/" onclick="ga('send','event','Menu','click','Settings')" data-ajax="false">Settings</a>
        </div>        
        <div>
	        <a href="http://viral.dev.metarank.com/" onclick="ga('send','event','Menu','click','Viral')" data-ajax="false">Viral</a>
        </div>
        
        <h3>COMPANY</h3>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/contact/" ? ' class="active"' : ''); ?>>
            <a href="<?php echo BASE_URL; ?>/contact/" onclick="ga('send','event','Menu','click','Contact')" data-ajax="false">Contact</a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/about/" ? ' class="active"' : ''); ?>>
            <a href="<?php echo BASE_URL; ?>/about/" onclick="ga('send','event','Menu','click','About')" data-ajax="false">About</a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/faq/" ? ' class="active"' : ''); ?>>
            <a href="<?php echo BASE_URL; ?>/faq/" onclick="ga('send','event','Menu','click','FAQ')" data-ajax="false">FAQ</a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/tou/" ? ' class="active"' : ''); ?>>
            <a href="<?php echo BASE_URL; ?>/tou/" onclick="ga('send','event','Menu','click','Terms')" data-ajax="false">Terms of Use</a>
        </div>
        <div<?php echo ($_SERVER["REQUEST_URI"] == "/metarank/htdocs/privacy/" ? ' class="active"' : ''); ?>>
            <a href="<?php echo BASE_URL; ?>/privacy/" onclick="ga('send','event','Menu','click','Privacy')" data-ajax="false">Privacy Policy</a>
        </div>
        <div>
        	<a href="<?php echo BASE_URL; ?>/logout/" onclick="ga('send','event','Menu','click','Logout')" data-ajax="false">Log Out </a>
        </div>
	<?php } ?>
</aside>