<?php
	require_once(dirname(__FILE__) . "/session.php");
	require_once(BASE_PATH . "/../phpinc/lib/fb/facebook.php");
	require_once(BASE_PATH . "/../phpinc/lib/li/linkedin.php");
	
	use Facebook\FacebookRedirectLoginHelper;
	
	if (isset($_SESSION["isLoggedIn"]) && $_SESSION["isLoggedIn"]) {
		if ($_SESSION["facebookAccessTokenExpiration"] != null && time() >= $_SESSION["facebookAccessTokenExpiration"]) {
			$serviceName = "Facebook";
			$disconnectURL = BASE_URL . "/facebook/options/?disconnect";
			
			$fbscope = array("user_birthday","user_location","email","read_stream","user_about_me","user_education_history","user_work_history","user_website","user_friends");
			//$fbscope = array("user_birthday","user_location","email","user_about_me","user_education_history","user_work_history","user_website","user_friends");
			//$fbscope = array('scope'=>'manage_pages,read_insights,user_photos,email,publish_actions,user_birthday,user_about_me,user_website,user_friends,user_work_history,user_location,user_education_history');
			$fbLoginHelper = new FacebookRedirectLoginHelper(curHostURL() . "/");
			$reauthorizationURL = $fbLoginHelper->getLoginUrl($fbscope);
		} else if ($_SESSION["linkedInOAuthTokenExpiration"] != null && time() >= $_SESSION["linkedInOAuthTokenExpiration"]) {
			$serviceName = "LinkedIn";
			$disconnectURL = BASE_URL . "/linkedin/options/?disconnect";
			
			$liscope = array(LinkedIn::SCOPE_BASIC_PROFILE, LinkedIn::SCOPE_FULL_PROFILE, LinkedIn::SCOPE_EMAIL_ADDRESS, LinkedIn::SCOPE_NETWORK, LinkedIn::SCOPE_CONTACT_INFO);
			$liscope = array(LinkedIn::SCOPE_BASIC_PROFILE, LinkedIn::SCOPE_EMAIL_ADDRESS);
			$reauthorizationURL = $linkedinSession->getLoginUrl($liscope);
		}
		
		if (isset($reauthorizationURL)) {
			$pageTitle = "MetaRank - Reauthorization Required for " . $serviceName;
			if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
				require_once(BASE_PATH . "/../phpinc/header.php");
			}
?>
		<section data-role="page" id="reauthorization_page" data-title="<?php echo $pageTitle; ?>">
			<div role="main" class="ui-content">
				<div class="content-wrapper">
<?php include(BASE_PATH . "/../phpinc/menu.php"); ?>
					<div id="content_column"><?php
			$service = "";
			$subTitle = $serviceName . " Reauthorization";
			include(BASE_PATH . "/../phpinc/subheader.php");
?>
						<div class="content-box left">
							<p class="larger-message"><?php echo $serviceName; ?> Reauthorization</p>
							<p>In order for our service to properly evaluate your data on <?php echo $serviceName; ?>, your reauthorization of our application is required at this time.</p>
							<p>This can occur either because the expiration time since your last authorization has been reached, or you have chosen to de-authorize our application.</p>
							<p>If you wish to disconnect from the above service rather than reauthorize, note that your previous score data for the service will be permanently deleted.</p>
							<p><a href="<?php echo $reauthorizationURL; ?>"><button>Reauthorize on <?php echo $serviceName; ?></button></a> <a href="<?php echo $disconnectURL; ?>"><button>Disconnect from <?php echo $serviceName; ?></button></a></p>
						</div>
						<div style="clear:both;"></div>
					</div>
				</div>
			</div>
		</section><?php
			
			if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
				require_once(BASE_PATH . "/../phpinc/footer.php");
			}
			
			exit();
		}
	}
?>