<?php
	if ($postalCodeRankResults !== null) {
?>
						<div class="content-box">
							<p class="left larger-message"><?php echo $postalCodeRankResults["postal_code"]; ?></p>
							<p class="rank-result-label"><?php echo ($service == "" ? "Metarank:" : "Rank:"); ?><span class="rank-result"><?php echo $postalCodeRankResults["rank"]; ?><span class="superscript"><?php echo getOrdinal($postalCodeRankResults["rank"]); ?></span></span></p>
							<p class="participants-number-label">Participants:<span class="participants-number"><?php echo $postalCodeRankResults["participants"]; ?></span></p>
						</div><?php
	}
	
	if ($localityRankResults !== null) {
?>
						<div class="content-box">
							<p class="left larger-message"><?php echo $localityRankResults["locality"]; ?></p>
							<p class="rank-result-label"><?php echo ($service == "" ? "Metarank:" : "Rank:"); ?><span class="rank-result"><?php echo $localityRankResults["rank"]; ?><span class="superscript"><?php echo getOrdinal($localityRankResults["rank"]); ?></span></span></p>
							<p class="participants-number-label">Participants:<span class="participants-number"><?php echo $localityRankResults["participants"]; ?></span></p>
						</div><?php
	}
	
	if ($administrativeAreaRankResults !== null) {
?>
						<div class="content-box">
							<p class="left larger-message"><?php echo $administrativeAreaRankResults["administrative_area"]; ?></p>
							<p class="rank-result-label"><?php echo ($service == "" ? "Metarank:" : "Rank:"); ?><span class="rank-result"><?php echo $administrativeAreaRankResults["rank"]; ?><span class="superscript"><?php echo getOrdinal($administrativeAreaRankResults["rank"]); ?></span></span></p>
							<p class="participants-number-label">Participants:<span class="participants-number"><?php echo $administrativeAreaRankResults["participants"]; ?></span></p>
						</div><?php
	}
	
	if ($countryRankResults !== null) {
?>
						<div class="content-box">
							<p class="left larger-message"><?php echo $countryRankResults["country"]; ?></p>
							<p class="rank-result-label"><?php echo ($service == "" ? "Metarank:" : "Rank:"); ?><span class="rank-result"><?php echo $countryRankResults["rank"]; ?><span class="superscript"><?php echo getOrdinal($countryRankResults["rank"]); ?></span></span></p>
							<p class="participants-number-label">Participants:<span class="participants-number"><?php echo $countryRankResults["participants"]; ?></span></p>
						</div><?php
	}
	
	if ($globalRankResults !== null) {
?>
						<div class="content-box">
							<p class="left larger-message">Global</p>
							<div class="top-right">
								<a href='javascript:postToFeed("<?php echo $_SESSION["userFirstName"] . " " . $_SESSION["userLastName"]; ?>", "<?php echo $globalRankResults["rank"] . getOrdinal($globalRankResults["rank"]) . ($service != "" ? " on " . $service : ""); ?>");'><img class="facebook-share" src="<?php echo BASE_URL; ?>/img/share-on-fb.gif"></a>
								<script type="IN/Share" data-url="http://www.metarank.com/" data-onsuccess="linkedInShareCallback"></script>
								<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.metarank.com" data-text="I'm ranked <?php echo $globalRankResults["rank"] . getOrdinal($globalRankResults["rank"]) . ($service != "" ? " on " . $service : ""); ?>! What will you rank?" data-via="metarank" data-count="none">Tweet</a>
							</div>
							<p class="rank-result-label"><?php echo ($service == "" ? "Metarank:" : "Rank:"); ?><span class="rank-result"><?php echo $globalRankResults["rank"]; ?><span class="superscript"><?php echo getOrdinal($globalRankResults["rank"]); ?></span></span></p>
							<div style="margin-top:-12px;" class="infochart" align="center"><div id="chart1" class="mechart2 jqplot-target" style="position: relative;"></div></div>
							<p class="participants-number-label">Participants:<span class="participants-number"><?php echo $globalRankResults["participants"]; ?></span></p>
							<div style="margin-top:-9px;" class="infochart" align="center"> <div id="chart2" class="mechart2 jqplot-target" style="margin-top: -4px; position: relative;"></div></div>
							<p class="score-number-label"><?php echo ($service == "" ? "Metascore:" : "Score:"); ?><span class="score-number"><?php echo round($reportCardDetails["score"], 0); ?></span></p>
							<p class="score-number-label">Worst Score:<span class="score-number"><?php echo round(min($AllScore), 0); ?></span></p>
							<p class="score-number-label">Best Score:<span class="score-number"><?php echo round(max($AllScore), 0); ?></span></p>
							<div style="margin-top:-9px;" class="infochart" align="center"> <div id="chart3" class="mechart2 jqplot-target" style="margin-top: -4px; position: relative;"></div></div>
							<input type="hidden" id="plotData1" value='[<?php if (count($plot1Data) > 0) {echo '["' . date('Y/m/d', strtotime($plot1Data[0]["modified_time"])) . '","' . round($plot1Data[0]["rank"]) . '"]'; for ($i = 1; $i < count($plot1Data); $i++) {echo ',["' . date('Y/m/d', strtotime($plot1Data[$i]["modified_time"])) . '","' . round($plot1Data[$i]["rank"]) . '"]';}} else {echo "[null]";} ?>]'>
							<input type="hidden" id="plotData2" value='[<?php if (count($plot2Data) > 0) {echo '["' . date('Y/m/d', strtotime($plot2Data[0]["modified_time"])) . '","' . round($plot2Data[0]["participants"]) . '"]'; for ($i = 1; $i < count($plot2Data); $i++) {echo ',["' . date('Y/m/d', strtotime($plot2Data[$i]["modified_time"])) . '","' . round($plot2Data[$i]["participants"]) . '"]';}} else {echo "[null]";} ?>]'>
							<input type="hidden" id="plotData3" value='[<?php if (count($plot3Data) > 0) {echo '["' . date('Y/m/d', strtotime($plot3Data[0]["modified_time"])) . '","' . round($plot3Data[0]["score"]) . '"]'; for ($i = 1; $i < count($plot3Data); $i++) {echo ',["' . date('Y/m/d', strtotime($plot3Data[$i]["modified_time"])) . '","' . round($plot3Data[$i]["score"]) . '"]';}} else {echo "[null]";} ?>]'>
                            
                            <?php /*?><input type="hidden" id="plotData1" value='[<?php if (count($plot1Data) > 0) {echo '["' . date('Y/m/d H:i:s', strtotime($plot1Data[0]["modified_time"])) . '","' . $plot1Data[0]["rank"] . '"]'; for ($i = 1; $i < count($plot1Data); $i++) {echo ',["' . date('Y/m/d H:i:s', strtotime($plot1Data[$i]["modified_time"])) . '","' . $plot1Data[$i]["rank"] . '"]';}} else {echo "[null]";} ?>]'>
							<input type="hidden" id="plotData2" value='[<?php if (count($plot2Data) > 0) {echo '["' . date('Y/m/d H:i:s', strtotime($plot2Data[0]["modified_time"])) . '","' . $plot2Data[0]["participants"] . '"]'; for ($i = 1; $i < count($plot2Data); $i++) {echo ',["' . date('Y/m/d H:i:s', strtotime($plot2Data[$i]["modified_time"])) . '","' . $plot2Data[$i]["participants"] . '"]';}} else {echo "[null]";} ?>]'>
							<input type="hidden" id="plotData3" value='[<?php if (count($plot3Data) > 0) {echo '["' . date('Y/m/d H:i:s', strtotime($plot3Data[0]["modified_time"])) . '","' . $plot3Data[0]["score"] . '"]'; for ($i = 1; $i < count($plot3Data); $i++) {echo ',["' . date('Y/m/d H:i:s', strtotime($plot3Data[$i]["modified_time"])) . '","' . $plot3Data[$i]["score"] . '"]';}} else {echo "[null]";} ?>]'><?php */?>
						</div><?php
	}
?>