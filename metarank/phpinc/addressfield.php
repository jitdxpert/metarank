<?php
require_once(dirname(__FILE__) . "/db.php");

function check_plain($text) {
  return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
}

function element_sort($a, $b) {
  $a_weight = (is_array($a) && isset($a['#weight'])) ? $a['#weight'] : 0;
  $b_weight = (is_array($b) && isset($b['#weight'])) ? $b['#weight'] : 0;
  if ($a_weight == $b_weight) {
    return 0;
  }
  return ($a_weight < $b_weight) ? -1 : 1;
}

function element_children(&$elements, $sort = FALSE) {
  // Do not attempt to sort elements which have already been sorted.
  $sort = isset($elements['#sorted']) ? !$elements['#sorted'] : $sort;

  // Filter out properties from the element, leaving only children.
  $children = array();
  $sortable = FALSE;
  foreach ($elements as $key => $value) {
    if ($key === '' || $key[0] !== '#') {
      $children[$key] = $value;
      if (is_array($value) && isset($value['#weight'])) {
        $sortable = TRUE;
      }
    }
  }
  // Sort the children if necessary.
  if ($sort && $sortable) {
    uasort($children, 'element_sort');
    // Put the sorted children back into $elements in the correct order, to
    // preserve sorting if the same element is passed through
    // element_children() twice.
    foreach ($children as $key => $child) {
      unset($elements[$key]);
      $elements[$key] = $child;
    }
    $elements['#sorted'] = TRUE;
  }

  return array_keys($children);
}

function country_get_list() {
$countries = $metaRankDatabase->getCountries();
$result = array("byID" => array(), "byCode" => array());
foreach ($countries as $row) {
	$result["byID"][$row["id"]] = array("code" => $row["code"], "name" => $row["name"]);
	$result["byCode"][$row["code"]] = array("id" => $row["id"], "name" => $row["name"]);
}
return $result;
}

/**
 * Returns an array of default values for the addressfield form elements.
 */
function addressfield_default_values($available_countries = NULL) {
  if (!isset($available_countries)) {
    //$available_countries = country_get_list();
  }

  //TODO: Make this a user option, or use geolocation to determine current country of user
  $default_country = '2A318410235F40EE91B1E3529C446000'; // USA

  return array(
    'country' => $default_country,
    'name_line' => '',
    'first_name' => '',
    'last_name' => '',
    'organization_name' => '',
    'administrative_area' => '',
    'sub_administrative_area' => '',
    'locality' => '',
    'dependent_locality' => '',
    'postal_code' => '',
    'thoroughfare' => '',
    'premise' => '',
    'sub_premise' => '',
    'data' => ''
  );
}

function render_address(array $address, array $removeFields) {
  $element = addressfield_field_formatter_view($address, $removeFields);

  return _render_address($element);
}

function _render_address($element) {
  if (isset($element["#access"]) && $element["#access"] === FALSE) {
    return '';
  }
  
  $html = '';
  
  if (isset($element["#prefix"])) {
    $html .= $element["#prefix"];
  }
  
  if (isset($element["#tag"])) {
    $html .= "<" . $element["#tag"];
  } else {
    $html .= "<div";
  }
  
  if (isset($element["#attributes"])) {
    foreach ($element["#attributes"] as $attribute_name => $attribute_value) {
      if (is_array($attribute_value)) {
        $html .= " " . $attribute_name . "='";
        foreach($attribute_value as $attribute_sub_value) {
          $html .= $attribute_sub_value . " ";
        }
        $html .= "'";
      } else {
        $html .= " " . $attribute_name . "='" . $attribute_value . "'";
      }
    }
  }
  if (isset($element["#title"])) {
    $html .= " title='" . $element["#title"] . "'";
  }
  $html .= ">" . (isset($element["#children"]) ? $element["#children"] : '');
  
  foreach (element_children($element) as $key) {
    $child = &$element[$key];
	$html .= _render_address($child);
  }
  
  if (isset($element["#tag"])) {
	$html .= "</" . $element["#tag"] . ">";
  } else {
    $html .= "</div>";
  }
  if (isset($element["#suffix"])) {
    $html .= $element["#suffix"];
  }
  return $html;
}

function render_address_form(array $address, array $removeFields, $addressFieldNumber) {
  $element = addressfield_field_widget_form($address, $removeFields);

  return _render_address_form($element, $addressFieldNumber);
}

function _render_address_form($element, $addressFieldNumber) {
  $html = '';
  if (isset($element["#prefix"])) {
    $html .= $element["#prefix"];
  }
  if (isset($element["#type"])) {
    if ($element["#type"] == 'fieldset') {
      $html .= "<fieldset data-addressfield-number='" . $addressFieldNumber . "'";
    } else if ($element["#type"] == 'addressfield_container') {
      $html .= "<div";
    } else if ($element["#type"] == 'select') {
	  if (isset($element["#title"])) {
	    $html .= "<label for='" . $element["#id"] . "_" . $addressFieldNumber . "'>" . $element["#title"] . "</label>";
	  }
      $html .= "<select";
    } else if ($element["#type"] == 'textfield') {
	  if (isset($element["#title"])) {
	    $html .= "<label for='" . $element["#id"] . "_" . $addressFieldNumber . "'>" . $element["#title"] . "</label>";
	  }
	  $html .= "<input type='text'";
	  if (isset($element["#default_value"])) {
	    $html .= " value='" . htmlspecialchars($element["#default_value"], ENT_QUOTES, 'UTF-8', false) . "'";
	  }
    } else if ($element["#type"] == 'hidden') {
	  $html .= "<input type='hidden' value='' name='" . $element["#name"] . "' />";
    } else {
	  $html .= "<div";
	}
  } else if (isset($element["#tag"])) {
    $html .= "<" . $element["#tag"];
  }
  if (isset($element["#attributes"])) {
    foreach ($element["#attributes"] as $attribute_name => $attribute_value) {
      if (is_array($attribute_value)) {
        $html .= " " . $attribute_name . "='";
        foreach($attribute_value as $attribute_sub_value) {
          $html .= $attribute_sub_value . " ";
        }
        $html .= "'";
      } else {
        $html .= " " . $attribute_name . "='" . $attribute_value . "'";
      }
    }
  }
  if (isset($element["#size"])) {
    $html .= " size='" . $element["#size"] . "'";
  }
  if (isset($element["#title"])) {
    $html .= " title='" . $element["#title"] . "'";
  }
  if (isset($element["#required"])) {
    $html .= " required";
  }
  if (isset($element["#name"]) && (!isset($element["#type"]) || $element["#type"] != 'hidden')) {
    $html .= " id='" . $element['#id'] . '_' . $addressFieldNumber . "' name='" . $element["#name"] . "'";
  }
  if (isset($element["#type"])) {
    if ($element["#type"] == 'textfield') {
  
      $html .= "/>";
    } else if ($element["#type"] != 'hidden') {
      $html .= ">";
    }
    if ($element["#type"] == 'select') {
      if (isset($element["#empty_value"])) {
	    $html .= "<option value=''> - None - </option>";
	  }
	  if (isset($element["#options"]["byID"])) {
	    foreach($element["#options"]["byID"] as $option_value => $option_data) {
          $html .= "<option value='" . htmlspecialchars($option_value, ENT_QUOTES, 'UTF-8', false) . "'" . ($element["#default_value"] == $option_value ? " selected" : "") . " data-code='" . $option_data["code"] . "'>" . htmlspecialchars($option_data["name"], ENT_QUOTES, 'UTF-8', false) . "</option>";
	    }
	  } else {
	    foreach($element["#options"] as $option_value => $option_text) {
          $html .= "<option value='" . htmlspecialchars($option_value, ENT_QUOTES, 'UTF-8', false) . "'" . ($element["#default_value"] == $option_value ? " selected" : "") . ">" . htmlspecialchars($option_text, ENT_QUOTES, 'UTF-8', false) . "</option>";
	    }
      }
    }
  } else {
    $html .= ">";
  }
  
  foreach (element_children($element) as $key) {
    $child = &$element[$key];
	$html .= _render_address_form($child, $addressFieldNumber);
  }
  
  if (isset($element["#type"])) {
    if ($element["#type"] == 'fieldset') {
	  $html .= "</fieldset>";
    } else if ($element["#type"] == 'addressfield_container') {
	  $html .= "</div>";
    } else if ($element["#type"] == 'select') {
	  $html .= "</select>";
    } else if ($element["#type"] != 'textfield' && $element["#type"] != 'hidden') {
	  $html .= "</div>";
	}
  } else if (isset($element["#tag"])) {
	$html .= "</" . $element["#tag"] . ">";
  }
  if (isset($element["#suffix"])) {
    $html .= $element["#suffix"];
  }
  return $html;
}

/**
 * Implements hook_field_widget_form()
 */
function addressfield_field_widget_form(array $address, array $removeFields) {
  $element = array();
  // Merge in default values to provide a value for every expected array key.
  $address += addressfield_default_values();

  $element['#type'] = 'fieldset';


  // Generate the address form.
  $context = array(
    'mode' => 'form',
	'removeFields' => $removeFields
  );
  $element += addressfield_generate($address, $context);

  return $element;
}

/**
 * Implements hook_field_formatter_view().
 */
function addressfield_field_formatter_view(array $address, array $removeFields) {
  $element = array();
  
  // Generate the address format.
  $context = array(
	'mode' => 'render',
	'removeFields' => $removeFields
  );
  $element += addressfield_generate($address, $context);

  return $element;
}

/**
 * Addresses forms and display formats are collaboratively generated by one or
 * more format handler plugins. An address with a name and a company, for example,
 * will be generated by three handlers:
 *   - 'address' that will generate the country, locality, street blocks
 *   - 'organization' that will add the organization block to the address
 *   - 'name-full' that will add a first name and last name block to the address
 *
 * A format handler is a CTools plugin of type 'addressfield' / 'format'. Each
 * handler is passed the format in turn, and can add to or modify the format.
 *
 * The format itself is a renderable array stub. This stub will be transformed
 * into either a Form API array suitable for use as part of a form or into a
 * renderable array suitable for use with drupal_render(). The following
 * modifications are done:
 *   - when rendering as a form, every element which name (its key in the array)
 *     is a valid addressfield column (see addressfield_field_schema()), will
 *     be transformed into a form element, either using a type explicitly
 *     defined in '#widget_type' or using 'select' if '#options' is set or
 *     'textfield' if it is not. In addition, the '#default_value' of every
 *     field will be populated from the address being edited.
 *   - when rendering as a formatter, every element which name (its key in the array)
 *     is a valid addressfield column (see addressfield_field_schema()), will
 *     be transformed into a renderable element, either using a type explicitly
 *     defined in '#render_type' or else using 'addressfield_container'. When
 *     the type is 'addressfield_container' the element will be rendered as
 *     an HTML element set by '#tag' (default: span).
 */

/**
 * Generate a format for a given address.
 *
 * @param $address
 *   The address format being generated.
 * @param $handlers
 *   The format handlers to use to generate the format.
 * @param $context
 *   An associative array of context information pertaining to how the address
 *   format should be generated. If no mode is given, it will initialize to the
 *   default value. The remaining context keys should only be present when the
 *   address format is being generated for a field:
 *   - mode: either 'form' or 'render'; defaults to 'render'.
 *   - field: the field info array.
 *   - instance: the field instance array.
 *   - langcode: the langcode of the language the field is being rendered in.
 *   - delta: the delta value of the given address.
 *
 * @return
 *   A renderable array suitable for use as part of a form (if 'mode' is 'form')
 *   or for formatted address output when passed to drupal_render().
 */
function addressfield_generate($address, array $context = array()) {
  // If no mode is given in the context array, default it to 'render'.
  if (empty($context['mode'])) {
    $context['mode'] = 'render';
  }

  $format = array();
  addressfield_format_address_generate($format, $address, $context);

  // Store the address in the format, for processing.
  $format['#address'] = $address;

  // Post-process the format stub, depending on the rendering mode.
  if ($context['mode'] == 'form') {
    $format['#addressfield'] = TRUE;
    $format['#required'] = FALSE;
	addressfield_process_format_form($format);
  }
  elseif ($context['mode'] == 'render') {
    addressfield_render_address($format);
  }

  return $format;
}

/**
 * Generate a full-fledged form from a format snippet, as returned by addressfield_formats().
 */
function addressfield_process_format_form(&$format) {
  _addressfield_process_format_form($format, $format['#address'], $format['#required']);
}

function _addressfield_process_format_form(&$format, $address, $required) {
  foreach (element_children($format) as $key) {
    $child = &$format[$key];

    // Automatically convert any element in the format array to an appropriate
    // form element that matches one of the address component names.
    if (in_array($key, array('name_line', 'first_name', 'last_name', 'organization_name', 'country', 'administrative_area', 'sub_administrative_area', 'locality', 'dependent_locality', 'postal_code', 'thoroughfare', 'premise', 'sub_premise'))) {
      // If the element didn't specify a #widget_type and has options, turn it
      // into a select list and unset its #size value, which is typically used
      // to provide the width of a textfield.
      if (!isset($child["#type"]) || $child["#type"] != 'hidden') {
	    if (isset($child['#options'])) {
          $child['#type'] = 'select';
          unset($child['#size']);
        } else {
          // Otherwise go ahead and make it a textfield.
          $child['#type'] = 'textfield';
        }
      }
	  $child['#name'] = $key . '[]';
	  $child['#id'] = $key;

      if (!$required) {
        unset($child['#required']);
      }

      if (isset($address[$key])) {
        $child['#default_value'] = $address[$key];
      }
    }

    // Recurse through the element's children if it has any.
    _addressfield_process_format_form($child, $address, $required);
  }
}

/**
 * Render an address in a given format.
 */
function addressfield_render_address(&$format) {
  _addressfield_render_address($format, $format['#address']);
}

function _addressfield_render_address(&$format, $address) {
  foreach (element_children($format) as $key) {
    $child = &$format[$key];

    // Automatically expand elements that match one of the fields of the address
    // structure.
    if (in_array($key, array('name_line', 'first_name', 'last_name', 'organization_name', 'country', 'administrative_area', 'sub_administrative_area', 'locality', 'dependent_locality', 'postal_code', 'thoroughfare', 'premise', 'sub_premise'), TRUE)) {
      $child['#type'] = 'addressfield_container';
      if (!isset($child['#tag'])) {
        $child['#tag'] = 'span';
      }

      // If the element instructs us to render the option value instead of the
      // raw address element value and its #options array has a matching key,
      // swap it out for the option value now.
      $child['#children'] = '';

      if (!empty($child['#render_option_value']) && (isset($child['#options'][$address[$key]]) || (isset($child['#options']['byID']) && isset($child['#options']['byID'][$address[$key]])))) {
		if (isset($child['#options'][$address[$key]])) {
			$child['#children'] = check_plain($child['#options'][$address[$key]]);
		} else {
			// Support countries (eventually other fields by ID)
			$child['#children'] = check_plain($child['#options']['byID'][$address[$key]]["name"]);
		}
      }
      elseif (isset($address[$key]) && (isset($child['#options']['byID']) && isset($child['#options']['byID'][$address[$key]]))) {
		// Support countries (eventually other fields by ID)
		$child['#children'] = check_plain($child['#options']['byID'][$address[$key]]["code"]);
	  } elseif (isset($address[$key])) {
        $child['#children'] = check_plain($address[$key]);
      }

      // Skip empty elements.
      if ((string) $child['#children'] === '') {
        $child['#access'] = FALSE;
      }

      // Add #field_prefix and #field_suffix to the prefixes and suffixes.
      if (isset($child['#field_prefix'])) {
        $child['#prefix'] = (isset($child['#prefix']) ? $child['#prefix'] : '') . $child['#field_prefix'];
      }
      if (isset($child['#field_suffix'])) {
        $child['#suffix'] = (isset($child['#suffix']) ? $child['#suffix'] : '') . $child['#field_suffix'];
      }
    }

    // Recurse through the child.
    _addressfield_render_address($child, $address);
  }
}

function addressfield_format_address_generate(&$format, $address, $context = array()) {
  // We start with a reasonable default: a simple block format suitable
  // for international shipping. We extend it with country-specific heuristics
  // below.
  
  $countries = country_get_list();
  
  // The name block
  $format['name_block'] = array(
    '#type' => 'addressfield_container',
    '#attributes' => array(
      'class' => array('name-block'),
    ),
    '#weight' => 0,
  );
  $format['name_block']['first_name'] = array(
    '#title' => 'First Name',
    '#tag' => 'span',
    '#attributes' => array(
      'class' => array('first_name')
    ),
    '#size' => 30,
    // The #required will be automatically set to FALSE when processing.
    '#required' => TRUE,
  );
  $format['name_block']['last_name'] = array(
    '#title' => 'Last Name',
    '#tag' => 'span',
    '#attributes' => array(
      'class' => array('last_name')
    ),
    '#size' => 30,
    // The #required will be automatically set to FALSE when processing.
    '#required' => TRUE,
  );
  $format['name_block']['organization_name'] = array(
    '#title' => 'Company',
    '#tag' => 'div',
    '#attributes' => array(
      'class' => array('organization_name')
    ),
    '#size' => 30,
    // The #required will be automatically set to FALSE when processing.
    '#required' => FALSE,
  );

  // The street block.
  $format['street_block'] = array(
    '#type' => 'addressfield_container',
    '#attributes' => array(
      'class' => array('street-block'),
    ),
    '#weight' => 10,
  );
  $format['street_block']['thoroughfare'] = array(
    '#title' => 'Address 1',
    '#tag' => 'div',
    '#attributes' => array(
      'class' => array('thoroughfare')
    ),
    '#size' => 45,
    // The #required will be automatically set to FALSE when processing.
    '#required' => TRUE,
  );
  $format['street_block']['premise'] = array(
    '#title' => 'Address 2',
    '#tag' => 'div',
    '#attributes' => array(
      'class' => array('premise')
    ),
    '#size' => 30,
  );
  $format['locality_block'] = array(
    '#type' => 'addressfield_container',
    '#attributes' => array(
      'class' => array('addressfield-container-inline', 'locality-block', 'country-' . $countries['byID'][$address['country']]["code"]),
    ),
    '#weight' => 50,
  );
   $format['locality_block']['postal_code'] = array(
    '#title' => 'Postal code',
    '#size' => 10,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('postal-code')
    ),
  );
  $format['locality_block']['locality'] = array(
    '#title' => 'City',
    '#size' => 30,
    '#required' => TRUE,
    '#prefix' => ' ',
    '#attributes' => array(
      'class' => array('locality')
    ),
  );
  $format['country'] = array(
    '#title' => 'Country',
    '#options' => $countries,
    '#render_option_value' => TRUE,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('country')
    ),
    '#weight' => 100,
  );

  // Those countries do not seem to have a relevant postal code.
  static $countries_no_postal_code = array('AF', 'AG', 'AL', 'AO', 'BB', 'BI', 'BJ', 'BO', 'BS', 'BW', 'BZ', 'CF', 'CG', 'CM', 'CO', 'DJ', 'DM', 'EG', 'ER', 'FJ', 'GD', 'GH', 'GM', 'GQ', 'GY', 'HK', 'IE', 'KI', 'KM', 'KP', 'KY', 'LC', 'LY', 'ML', 'MR', 'NA', 'NR', 'RW', 'SB', 'SC', 'SL', 'SR', 'ST', 'TD', 'TG', 'TL', 'TO', 'TT', 'TV', 'TZ', 'UG', 'VC', 'VU', 'WS', 'ZW');
  if (in_array($format['country']['#options']['byID'][$address['country']]["code"], $countries_no_postal_code)) {
    $format['locality_block']['postal_code']['#type'] = 'hidden';

    // Remove the prefix from the first widget of the block.
    $element_children = element_children($format['locality_block']);
    $first_child = reset($element_children);
    unset($format['locality_block'][$first_child]['#prefix']);
  }

  // Those countries generally use the administrative area in postal addresses.
  static $countries_administrative_area = array('AR', 'AU', 'BR', 'BS', 'BY', 'BZ', 'CA', 'CN', 'DO', 'EG', 'ES', 'FJ', 'FM', 'GB', 'HN', 'ID', 'IE', 'IN', 'IT', 'JO', 'JP', 'KI', 'KN', 'KR', 'KW', 'KY', 'KZ', 'MX', 'MY', 'MZ', 'NG', 'NI', 'NR', 'NZ', 'OM', 'PA', 'PF', 'PG', 'PH', 'PR', 'PW', 'RU', 'SM', 'SO', 'SR', 'SV', 'TH', 'TW', 'UA', 'US', 'UY', 'VE', 'VI', 'VN', 'YU', 'ZA');
  if (in_array($format['country']['#options']['byID'][$address['country']]["code"], $countries_administrative_area)) {
    $format['locality_block']['administrative_area'] = array(
      '#title' => 'State',
      '#size' => 10,
      '#required' => TRUE,
      '#prefix' => ' ',
      '#attributes' => array(
        'class' => array('state')
      ),
    );
  }

  // A few countries have a well-known list of administrative divisions.
  if ($format['country']['#options']['byID'][$address['country']]["code"] == 'US') {
    $format['locality_block']['administrative_area']['#options'] = array(
      ''   => '--',
      'AL' => 'Alabama',
      'AK' => 'Alaska',
      'AZ' => 'Arizona',
      'AR' => 'Arkansas',
      'CA' => 'California',
      'CO' => 'Colorado',
      'CT' => 'Connecticut',
      'DE' => 'Delaware',
      'DC' => 'District Of Columbia',
      'FL' => 'Florida',
      'GA' => 'Georgia',
      'HI' => 'Hawaii',
      'ID' => 'Idaho',
      'IL' => 'Illinois',
      'IN' => 'Indiana',
      'IA' => 'Iowa',
      'KS' => 'Kansas',
      'KY' => 'Kentucky',
      'LA' => 'Louisiana',
      'ME' => 'Maine',
      'MD' => 'Maryland',
      'MA' => 'Massachusetts',
      'MI' => 'Michigan',
      'MN' => 'Minnesota',
      'MS' => 'Mississippi',
      'MO' => 'Missouri',
      'MT' => 'Montana',
      'NE' => 'Nebraska',
      'NV' => 'Nevada',
      'NH' => 'New Hampshire',
      'NJ' => 'New Jersey',
      'NM' => 'New Mexico',
      'NY' => 'New York',
      'NC' => 'North Carolina',
      'ND' => 'North Dakota',
      'OH' => 'Ohio',
      'OK' => 'Oklahoma',
      'OR' => 'Oregon',
      'PA' => 'Pennsylvania',
      'RI' => 'Rhode Island',
      'SC' => 'South Carolina',
      'SD' => 'South Dakota',
      'TN' => 'Tennessee',
      'TX' => 'Texas',
      'UT' => 'Utah',
      'VT' => 'Vermont',
      'VA' => 'Virginia',
      'WA' => 'Washington',
      'WV' => 'West Virginia',
      'WI' => 'Wisconsin',
      'WY' => 'Wyoming',
      ' ' => '--',
      'AA' => 'Armed Forces (Americas)',
      'AE' => 'Armed Forces (Europe, Canada, Middle East, Africa)',
      'AP' => 'Armed Forces (Pacific)',
      'AS' => 'American Samoa',
      'FM' => 'Federated States of Micronesia',
      'GU' => 'Guam',
      'MH' => 'Marshall Islands',
      'MP' => 'Northern Mariana Islands',
      'PW' => 'Palau',
      'PR' => 'Puerto Rico',
      'VI' => 'Virgin Islands',
    );
    $format['locality_block']['postal_code']['#title'] = 'ZIP Code';

    if ($context['mode'] == 'render') {
      $format['locality_block']['locality']['#suffix'] = ',';
    }
	
	$format['street_block']['thoroughfare']["#attributes"] += array("placeholder" => "Full Street Address (e.g. 1600 Pennsylvania Ave)");
	$format['street_block']['premise']["#attributes"] += array("placeholder" => "STE, APT, etc.");
  }
  else if ($format['country']['#options']['byID'][$address['country']]["code"] == 'IT') {
    $format['locality_block']['administrative_area']['#options'] = array(
      '' => '--',
      'AG' => 'Agrigento',
      'AL' => 'Alessandria',
      'AN' => 'Ancona',
      'AO' => "Valle d'Aosta/Vallée d'Aoste",
      'AP' => 'Ascoli Piceno',
      'AQ' => "L'Aquila",
      'AR' => 'Arezzo',
      'AT' => 'Asti',
      'AV' => 'Avellino',
      'BA' => 'Bari',
      'BG' => 'Bergamo',
      'BI' => 'Biella',
      'BL' => 'Belluno',
      'BN' => 'Benevento',
      'BO' => 'Bologna',
      'BR' => 'Brindisi',
      'BS' => 'Brescia',
      'BT' => 'Barletta-Andria-Trani',
      'BZ' => 'Bolzano/Bozen',
      'CA' => 'Cagliari',
      'CB' => 'Campobasso',
      'CE' => 'Caserta',
      'CH' => 'Chieti',
      'CI' => 'Carbonia-Iglesias',
      'CL' => 'Caltanissetta',
      'CN' => 'Cuneo',
      'CO' => 'Como',
      'CR' => 'Cremona',
      'CS' => 'Cosenza',
      'CT' => 'Catania',
      'CZ' => 'Catanzaro',
      'EN' => 'Enna',
      'FC' => 'Forlì-Cesena',
      'FE' => 'Ferrara',
      'FG' => 'Foggia',
      'FI' => 'Firenze',
      'FM' => 'Fermo',
      'FR' => 'Frosinone',
      'GE' => 'Genova',
      'GO' => 'Gorizia',
      'GR' => 'Grosseto',
      'IM' => 'Imperia',
      'IS' => 'Isernia',
      'KR' => 'Crotone',
      'LC' => 'Lecco',
      'LE' => 'Lecce',
      'LI' => 'Livorno',
      'LO' => 'Lodi',
      'LT' => 'Latina',
      'LU' => 'Lucca',
      'MB' => 'Monza e Brianza',
      'MC' => 'Macerata',
      'ME' => 'Messina',
      'MI' => 'Milano',
      'MN' => 'Mantova',
      'MO' => 'Modena',
      'MS' => 'Massa-Carrara',
      'MT' => 'Matera',
      'NA' => 'Napoli',
      'NO' => 'Novara',
      'NU' => 'Nuoro',
      'OG' => 'Ogliastra',
      'OR' => 'Oristano',
      'OT' => 'Olbia-Tempio',
      'PA' => 'Palermo',
      'PC' => 'Piacenza',
      'PD' => 'Padova',
      'PE' => 'Pescara',
      'PG' => 'Perugia',
      'PI' => 'Pisa',
      'PN' => 'Pordenone',
      'PO' => 'Prato',
      'PR' => 'Parma',
      'PT' => 'Pistoia',
      'PU' => 'Pesaro e Urbino',
      'PV' => 'Pavia',
      'PZ' => 'Potenza',
      'RA' => 'Ravenna',
      'RC' => 'Reggio di Calabria',
      'RE' => "Reggio nell'Emilia",
      'RG' => 'Ragusa',
      'RI' => 'Rieti',
      'RM' => 'Roma',
      'RN' => 'Rimini',
      'RO' => 'Rovigo',
      'SA' => 'Salerno',
      'SI' => 'Siena',
      'SO' => 'Sondrio',
      'SP' => 'La Spezia',
      'SR' => 'Siracusa',
      'SS' => 'Sassari',
      'SV' => 'Savona',
      'TA' => 'Taranto',
      'TE' => 'Teramo',
      'TN' => 'Trento',
      'TO' => 'Torino',
      'TP' => 'Trapani',
      'TR' => 'Terni',
      'TS' => 'Trieste',
      'TV' => 'Treviso',
      'UD' => 'Udine',
      'VA' => 'Varese',
      'VB' => 'Verbano-Cusio-Ossola',
      'VC' => 'Vercelli',
      'VE' => 'Venezia',
      'VI' => 'Vicenza',
      'VR' => 'Verona',
      'VS' => 'Medio Campidano',
      'VT' => 'Viterbo',
      'VV' => 'Vibo Valentia',
    );
    $format['locality_block']['administrative_area']['#title'] = 'Province';
  }
  elseif ($format['country']['#options']['byID'][$address['country']]["code"] == 'BR') {
    $format['locality_block']['dependent_locality'] = array(
      '#title' => 'Neighborhood',
      '#tag' => 'div',
      '#attributes' => array('class' => array('dependent-locality')),
      '#size' => 25,
      '#render_option_value' => FALSE,
    );
    $format['locality_block']['administrative_area']['#render_option_value'] = TRUE;
    $format['locality_block']['administrative_area']['#options'] = array(
      '' => '--',
      'AC' => 'Acre',
      'AL' => 'Alagoas',
      'AM' => 'Amazonas',
      'AP' => 'Amapá',
      'BA' => 'Bahia',
      'CE' => 'Ceará',
      'DF' => 'Distrito Federal',
      'ES' => 'Espírito Santo',
      'GO' => 'Goiás',
      'MA' => 'Maranhão',
      'MG' => 'Minas Gerais',
      'MS' => 'Mato Grosso do Sul',
      'MT' => 'Mato Grosso',
      'PA' => 'Pará',
      'PB' => 'Paraíba',
      'PE' => 'Pernambuco',
      'PI' => 'Piauí',
      'PR' => 'Paraná',
      'RJ' => 'Rio de Janeiro',
      'RN' => 'Rio Grande do Norte',
      'RO' => 'Rondônia',
      'RR' => 'Roraima',
      'RS' => 'Rio Grande do Sul',
      'SC' => 'Santa Catarina',
      'SE' => 'Sergipe',
      'SP' => 'São Paulo',
      'TO' => 'Tocantins',
    );
    // Change some titles to make translation easier.
    $format['street_block']['#attributes'] = array(
      'class' => array('addressfield-container-inline'),
    );
    $format['street_block']['thoroughfare'] = array(
      '#title' => 'Thoroughfare',
      '#tag'   => NULL,
      '#attributes' => array('class' => array('thoroughfare')),
      '#size' => 30,
      // The #required will be automatically set to FALSE when processing.
      '#required' => TRUE,
    );
    $format['street_block']['premise'] = array(
      '#title' => 'Complement',
      '#tag' => NULL,
      '#attributes' => array('class' => array('premise')),
      '#size' => 20,
      '#prefix' => ', ',
    );
    $format['locality_block']['locality']['#suffix'] = ' - ';
    // Hide suffixes and prefixes while in form.
    if ($context['mode'] == 'form') {
      $format['street_block']['premise']['#prefix'] = NULL;
      $format['street_block']['premise']['#suffix'] = NULL;
      $format['locality_block']['locality']['#suffix'] = NULL;
    }
    // Render an extra field for 'Neighborhood'.
    $format['locality_block']['dependent_locality']['#render_option_value'] = TRUE;
    // Change some weights to conform local standards
    // Neighborhood.
    $format['locality_block']['dependent_locality']['#weight'] = 0;
    // City.
    $format['locality_block']['locality']['#weight'] = 5;
    // State.
    $format['locality_block']['administrative_area']['#weight'] = 10;
    // Postal Code.
    $format['locality_block']['postal_code']['#weight'] = 16;
    $format['locality_block']['postal_code']['#tag'] = 'div';
  }
  else if ($format['country']['#options']['byID'][$address['country']]["code"] == 'CA') {
    $format['locality_block']['administrative_area']['#options'] = array(
      '' => '--',
      'AB' => 'Alberta',
      'BC' => 'British Columbia',
      'MB' => 'Manitoba',
      'NB' => 'New Brunswick',
      'NL' => 'Newfoundland and Labrador',
      'NT' => 'Northwest Territories',
      'NS' => 'Nova Scotia',
      'NU' => 'Nunavut',
      'ON' => 'Ontario',
      'PE' => 'Prince Edward Island',
      'QC' => 'Quebec',
      'SK' => 'Saskatchewan',
      'YT' => 'Yukon Territory',
    );
    $format['locality_block']['administrative_area']['#title'] = 'Province';

    if ($context['mode'] == 'render') {
      $format['locality_block']['locality']['#suffix'] = ',';
    }
  }
  else if ($format['country']['#options']['byID'][$address['country']]["code"] == 'AU') {
    $format['locality_block']['administrative_area']['#options'] = array(
      '' => '--',
      'ACT' => 'Australian Capital Territory',
      'NSW' => 'New South Wales',
      'NT' => 'Northern Territory',
      'QLD' => 'Queensland',
      'SA' => 'South Australia',
      'TAS' => 'Tasmania',
      'VIC' => 'Victoria',
      'WA' => 'Western Australia',
    );
  }
  else if ($format['country']['#options']['byID'][$address['country']]["code"] == 'NZ') {
    $format['locality_block']['locality']['#title'] = ('Town/City');
    $format['locality_block']['postal_code']['#title'] = 'Postcode';
    $format['locality_block']['administrative_area']['#render_option_value'] = TRUE;
    $format['locality_block']['administrative_area']['#title'] = 'Region';
    $format['locality_block']['administrative_area']['#required'] = FALSE;
    $format['locality_block']['administrative_area']['#options'] = array(
      ''   => '--',
      'AUK' => 'Auckland',
      'BOP' => 'Bay of Plenty',
      'CAN' => 'Canterbury',
      'HKB' => "Hawke's Bay",
      'MWT' => 'Manawatu-Wanganui',
      'NTL' => 'Northland',
      'OTA' => 'Otago',
      'STL' => 'Southland',
      'TKI' => 'Taranaki',
      'WKO' => 'Waikato',
      'WGN' => 'Wellington',
      'WTC' => 'West Coast',
      'GIS' => 'Gisborne District',
      'MBH' => 'Marlborough District',
      'NSN' => 'Nelson',
      'TAS' => 'Tasman District',
      'CIT' => 'Chatham Islands Territory',
    );
  }

  // Those countries tend to put the postal code after the locality.
  static $countries_postal_code_after_locality = array('AU', 'BD', 'BF', 'BH', 'BM', 'BN', 'BT', 'CA', 'FM', 'GB', 'ID', 'IN', 'JM', 'JO', 'KH', 'LB', 'LS', 'LV', 'MM', 'MN', 'MV', 'MW', 'NG', 'NP', 'NZ', 'PE', 'PK', 'PR', 'PW', 'SA', 'SG', 'SO', 'TH', 'US', 'VI', 'VG', 'VN');
  if (in_array($format['country']['#options']['byID'][$address['country']]["code"], $countries_postal_code_after_locality)) {
    // Take the widget out of the array.
    $postal_code_widget = $format['locality_block']['postal_code'];
    $postal_code_widget['#prefix'] = ' ';
    unset($format['locality_block']['postal_code']);

    // Add it back.
    $format['locality_block']['postal_code'] = $postal_code_widget;

    // Remove the prefix from the first widget of the block.
    $element_children = element_children($format['locality_block']);
    $first_child = reset($element_children);
    unset($format['locality_block'][$first_child]['#prefix']);
  }

  // GB-specific tweaks
  if ($format['country']['#options']['byID'][$address['country']]["code"] == 'GB') {
    // Locality
    $format['locality_block']['locality'] = array_merge(
      $format['locality_block']['locality'],
      array(
        '#title' => 'Town/City',
        '#weight' => 40,
        '#prefix' => '',
        '#tag' => 'div',
      )
    );

    // Administrative
    $format['locality_block']['administrative_area'] = array_merge(
      $format['locality_block']['administrative_area'],
      array(
        '#title' => 'County',
        '#required' => FALSE,
        '#weight' => 50,
        '#size' => 30,
        '#prefix' => '',
        '#tag' => 'div',
      )
    );

    // Postal code
    $format['locality_block']['postal_code'] = array_merge(
      $format['locality_block']['postal_code'],
      array(
        '#title' => 'Postcode',
        '#weight' => 60,
        '#prefix' => '',
        '#tag' => 'div',
      )
    );
  }

  if ($context['mode'] == 'form') {
    // Form mode, move the country selector to the top of the form.
    $format['country']['#weight'] = -10;

    if (isset($context['field'])) {
      $format['country']['#options'] = country_get_list();
    }

    if (isset($context['delta']) && $context['delta'] > 0) {
      // On subsequent elements of a field, we make the country field non
      // required and add a ' - None - ' option to it, so as to allow the
      // user to remove the address by clearing the country field.
      $format['country']['#required'] = FALSE;
      $format['country']['#empty_value'] = '';
    }
  }
  if (!isset($format['locality_block']['administrative_area'])) {
	$format['administrative_area']['#type'] = 'hidden';
  }
  if (!isset($format['locality_block']['dependent_locality'])) {
	$format['dependent_locality']['#type'] = 'hidden';
  }
  
  foreach ($context['removeFields'] as $fieldName) {
    unset($format['name_block'][$fieldName]);
	unset($format['street_block'][$fieldName]);
	unset($format['locality_block'][$fieldName]);
	unset($format[$fieldName]);
  }
}
?>