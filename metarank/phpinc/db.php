<?php
	require_once(dirname(__FILE__) . "/defines.php");
	

	class MetaRankDatabase {
		protected $_conn;
		
		public function __construct() {
			$this->_conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);

			// Check connection
			if (mysqli_connect_errno()) {
				if (isset($success) && isset($message) && isset($commands)) {
					$success = false;
					$message = "We are currently experiencing technical difficulties, and are unable to complete your request. Please try again later.";
					
					header('Content-Type: application/json');
				
					echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
				} else {
					include(BASE_PATH . "/../phpinc/errors/technical_difficulties.php");
				}
				die("Failed to connect to MySQL: " . mysqli_connect_error());
			}

			mysqli_autocommit($this->_conn, false);

			// We want UTC timezone
			mysqli_query($this->_conn, "SET time_zone = '+0:00'");
		}
		
		public function commitChanges() {
			return mysqli_commit($this->_conn);
		}

		public function rollbackChanges() {
			return mysqli_rollback($this->_conn);
		}
		
		public function deleteFriend($friendA, $friendB) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "DELETE FROM rank_friends WHERE (initiator = UNHEX(?) AND responder = UNHEX(?)) OR (initiator = UNHEX(?) AND responder = UNHEX(?))");
			mysqli_stmt_bind_param($stmt, "ssss", $friendA, $friendB, $friendB, $friendA);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}
		
		public function deleteFriendRequest($initiator, $responder) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "DELETE FROM rank_friends WHERE initiator = UNHEX(?) AND responder = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "ss", $initiator, $responder);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}
		
		public function acceptFriendRequest($initiator, $responder) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_friends SET accepted = 'Y' WHERE initiator = UNHEX(?) AND responder = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "ss", $initiator, $responder);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}
		
		public function sendFriendRequest($initiator, $responder) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_friends SET initiator = UNHEX(?), responder = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "ss", $initiator, $responder);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}
		
		public function findFriendsByEmail($email, $searcherUserID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(rank_users.id) AS id, rank_users.first_name, rank_users.last_name, rank_user_settings.profile_picture, rank_users_facebook.unique_id AS facebook_id, rank_users_linkedin.oauth_token AS linkedin_oauth_token, rank_users_twitter.profile_picture_url AS twitter_profile_picture_url FROM rank_users LEFT JOIN rank_user_settings ON rank_users.id =  rank_user_settings.user_id LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_friends AS initiator_test ON rank_users.id = initiator_test.initiator AND initiator_test.responder = UNHEX(?) LEFT JOIN rank_friends AS responder_test ON rank_users.id = responder_test.responder and responder_test.initiator = UNHEX(?) WHERE rank_users.email = ? AND rank_users.id != UNHEX(?) AND initiator_test.initiator IS NULL AND responder_test.responder IS NULL");
			mysqli_stmt_bind_param($stmt, "ssss", $searcherUserID, $searcherUserID, $email, $searcherUserID);
			if (mysqli_stmt_execute($stmt)) {
				$result = array();
				
				$recordSet = mysqli_stmt_get_result($stmt);
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}
		
		public function getPendingSentFriendRequests($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(rank_users.id) AS id, rank_users.first_name, rank_users.last_name, rank_user_settings.profile_picture, rank_users_facebook.unique_id AS facebook_id, rank_users_linkedin.oauth_token AS linkedin_oauth_token, rank_users_twitter.profile_picture_url AS twitter_profile_picture_url FROM rank_users LEFT JOIN rank_user_settings ON rank_users.id =  rank_user_settings.user_id LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_friends ON rank_users.id = rank_friends.responder WHERE rank_friends.initiator = UNHEX(?) AND rank_friends.accepted IS NULL");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = array();
				
				$recordSet = mysqli_stmt_get_result($stmt);
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}
		
		public function getPendingReceivedFriendRequests($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(rank_users.id) AS id, rank_users.first_name, rank_users.last_name, rank_user_settings.profile_picture, rank_users_facebook.unique_id AS facebook_id, rank_users_linkedin.oauth_token AS linkedin_oauth_token, rank_users_twitter.profile_picture_url AS twitter_profile_picture_url FROM rank_users LEFT JOIN rank_user_settings ON rank_users.id =  rank_user_settings.user_id LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_friends ON rank_users.id = rank_friends.initiator WHERE rank_friends.responder = UNHEX(?) AND rank_friends.accepted IS NULL");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = array();
				
				$recordSet = mysqli_stmt_get_result($stmt);
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}
		
		public function getFriends($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(rank_users.id) AS id, rank_users.first_name, rank_users.last_name, rank_user_settings.profile_picture, rank_users_facebook.unique_id AS facebook_id, rank_users_linkedin.oauth_token AS linkedin_oauth_token, rank_users_twitter.profile_picture_url AS twitter_profile_picture_url FROM rank_users LEFT JOIN rank_user_settings ON rank_users.id =  rank_user_settings.user_id LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_friends AS initiator_test ON rank_users.id = initiator_test.initiator LEFT JOIN rank_friends AS responder_test ON rank_users.id = responder_test.responder WHERE (initiator_test.responder = UNHEX(?) AND initiator_test.accepted = 'Y') OR (responder_test.initiator = UNHEX(?) AND responder_test.accepted = 'Y')");
			mysqli_stmt_bind_param($stmt, "ss", $userID, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = array();
				
				$recordSet = mysqli_stmt_get_result($stmt);
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}
		
		public function addFriendsFromFacebook($userID, $facebookAccessToken) {
			$result = false;
			
			$limit = 5000;
			$offset = 0;
			$friend_ids = array();
			$graph_url = "https://graph.facebook.com/me/friends?limit=".$limit."&offset=".$offset."&access_token=".$facebookAccessToken;
			do {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $graph_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$response = curl_exec($ch);
				curl_close($ch);
				$friends = json_decode($response);
				$nr = count(@$friends->data);
				if (isset($feed->error->message)) {
					// TODO: Handle errors here
					break;
				} else {
					for ($i=0; $i<$nr; $i++) {
						$friend_ids[] = mysqli_real_escape_string($this->_conn, $friends->data[$i]->id);
					}
					$graph_url = @$friends->paging->next;
				}
			} while ($nr >= $limit);
			$sql = "SELECT HEX(rank_users_facebook.user_id) AS id FROM rank_users_facebook LEFT JOIN rank_friends AS initiator_test ON rank_users_facebook.user_id = initiator_test.initiator AND initiator_test.responder = UNHEX(?) LEFT JOIN rank_friends AS responder_test ON rank_users_facebook.user_id = responder_test.responder AND responder_test.initiator = UNHEX(?) WHERE initiator_test.initiator IS NULL AND responder_test.responder IS NULL AND rank_users_facebook.unique_id IN ";
			$i = 0;
			$sql .= "('".$friend_ids[$i];
			for ($i = 1; $i < count($friend_ids); $i++) {
				$sql .= "', '".$friend_ids[$i];
			}
			$sql .= "');";
			$stmt = mysqli_prepare($this->_conn, $sql);
			mysqli_stmt_bind_param($stmt, "ss", $userID, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				$stmt2 = mysqli_prepare($this->_conn, "INSERT INTO rank_friends SET initiator = UNHEX(?), responder = UNHEX(?), accepted = 'Y'");
				while ($row = mysqli_fetch_array($recordSet)) {
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $row["id"]);
					mysqli_stmt_execute($stmt2);
				}
				mysqli_stmt_close($stmt2);
			}
			mysqli_stmt_close($stmt);
			$result = TRUE;
			
			return $result;
		}

		public function getAllUserScoringDetails() {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(rank_users.id) AS id, rank_users_facebook.access_token AS facebook_access_token, rank_users_linkedin.oauth_token AS linkedin_oauth_token, rank_users_twitter.oauth_token AS twitter_oauth_token, rank_users_twitter.oauth_token_secret AS twitter_oauth_token_secret FROM rank_users LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id ORDER BY rank_users.creation_time ASC");
			if (!mysqli_error($this->_conn)) {
				$result = array();
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			mysqli_free_result($recordSet);
			
			return $result;
		}
		
		public function getFriendsLeaderboard($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, 
				"(SELECT 'higher' AS table_name, higher_score.rank, HEX(higher_users.id) AS id, CONCAT(higher_users.first_name, ' ', higher_users.last_name) AS name, higher_users.profile_picture, higher_users.facebook_id, higher_users.linkedin_oauth_token, higher_users.twitter_profile_picture_url, higher_users.score FROM (SELECT rank_users.id, rank_users.first_name, rank_users.last_name, rank_user_settings.profile_picture, rank_users_facebook.unique_id AS facebook_id, rank_users_linkedin.oauth_token AS linkedin_oauth_token, rank_users_twitter.profile_picture_url AS twitter_profile_picture_url, rank_users.meta_score AS score FROM rank_users LEFT JOIN rank_user_settings ON rank_users.id = rank_user_settings.user_id LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id INNER JOIN rank_friends ON (rank_users.id = rank_friends.initiator AND rank_friends.responder = UNHEX(?)) OR (rank_users.id = rank_friends.responder AND rank_friends.initiator = UNHEX(?)) LEFT JOIN rank_users AS target_user ON target_user.id = UNHEX(?) WHERE rank_users.meta_score > target_user.meta_score ORDER BY rank_users.meta_score ASC LIMIT 10) AS higher_users INNER JOIN (SELECT COUNT(*) AS rank FROM rank_users INNER JOIN rank_friends ON (rank_users.id = rank_friends.initiator AND rank_friends.responder = UNHEX(?)) OR (rank_users.id = rank_friends.responder AND rank_friends.initiator = UNHEX(?)) LEFT JOIN rank_users AS target_user ON target_user.id = UNHEX(?) WHERE rank_users.meta_score > target_user.meta_score) AS higher_score)" .
				" UNION ALL " .
				"(SELECT 'equal' AS table_name, @r := @r + 1 AS rank, HEX(equal_users.id) AS id, CONCAT(equal_users.first_name, ' ', equal_users.last_name) AS name, equal_users.profile_picture, equal_users.facebook_id, equal_users.linkedin_oauth_token, equal_users.twitter_profile_picture_url, equal_users.score FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_users.first_name, rank_users.last_name, rank_user_settings.profile_picture, rank_users_facebook.unique_id AS facebook_id, rank_users_linkedin.oauth_token AS linkedin_oauth_token, rank_users_twitter.profile_picture_url AS twitter_profile_picture_url, rank_users.meta_score AS score FROM rank_users LEFT JOIN rank_user_settings ON rank_users.id = rank_user_settings.user_id LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_friends ON (rank_users.id = rank_friends.initiator AND rank_friends.responder = UNHEX(?)) OR (rank_users.id = rank_friends.responder AND rank_friends.initiator = UNHEX(?)) LEFT JOIN rank_users AS target_user ON target_user.id = UNHEX(?) WHERE rank_users.meta_score = target_user.meta_score AND (rank_friends.initiator IS NOT NULL OR rank_users.id = UNHEX(?)) ORDER BY rank_users.creation_time ASC) AS equal_users)" .
				" UNION ALL " .
				"(SELECT 'lower' AS table_name, @i := @i + 1 AS rank, HEX(lower_users.id) AS id, CONCAT(lower_users.first_name, ' ', lower_users.last_name) AS name, lower_users.profile_picture, lower_users.facebook_id, lower_users.linkedin_oauth_token, lower_users.twitter_profile_picture_url, lower_users.score FROM (SELECT @i := 0) AS initialize, (SELECT rank_users.id, rank_users.first_name, rank_users.last_name, rank_user_settings.profile_picture, rank_users_facebook.unique_id AS facebook_id, rank_users_linkedin.oauth_token AS linkedin_oauth_token, rank_users_twitter.profile_picture_url AS twitter_profile_picture_url, rank_users.meta_score AS score FROM rank_users LEFT JOIN rank_user_settings ON rank_users.id = rank_user_settings.user_id LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id INNER JOIN rank_friends ON (rank_users.id = rank_friends.initiator AND rank_friends.responder = UNHEX(?)) OR (rank_users.id = rank_friends.responder AND rank_friends.initiator = UNHEX(?)) LEFT JOIN rank_users AS target_user ON target_user.id = UNHEX(?) WHERE rank_users.meta_score < target_user.meta_score ORDER BY rank_users.meta_score DESC LIMIT 10) AS lower_users)"
			);
			mysqli_stmt_bind_param($stmt, "sssssssssssss", $userID, $userID, $userID, $userID, $userID, $userID, $userID, $userID, $userID, $userID, $userID, $userID, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank_above = 0;
				$num_equal = 0;
				$obtained = 0;
				
				$higher = array();
				$equal = array();
				$lower = array();
				
				$rank_above = 0;
				
				while ($row = mysqli_fetch_array($recordSet)) {
					if ($row["table_name"] == "higher") {
						$higher[] = $row;
						$rank_above = $row["rank"];
					} else if ($row["table_name"] == "equal") {
						$equal[] = $row;
					} else {
						$lower[] = $row;
					}
				}
				
				// Correct rank numbers in $higher
				for ($i = 0; $i < count($higher); $i++) {
					$higher[$i]["rank"] = $rank_above--;
				}
				$rank_above += count($higher);
				
				// Reverse higher since we obtained it in reverse order
				$higher = array_reverse($higher);
				
				$num_equal = count($equal);
				
				// Correct rank numbers in $equal
				for ($i = 0; $i < $num_equal; $i++) {
					$equal[$i]["rank"] += $rank_above;
				}
				
				// Correct rank numbers in $lower
				for ($i = 0; $i < count($lower); $i++) {
					$lower[$i]["rank"] += $rank_above + $num_equal;
				}
				
				$dataTable = array_merge($higher, $equal, $lower);
				
				if (count($dataTable) > 0) {
					// Find target user
					$targetUserIndex = 0;
					for ($i = 0; $i < count($dataTable); $i++) {
						if ($dataTable[$i]["id"] == $userID) {
							$targetUserIndex = $i;
							break;
						}
					}
					
					$harvestedIndices = array($targetUserIndex);
					$offset = -1;
					$iteration = 0;
					
					// Alternate adding from above and below target user until 11 results are returned
					while (count($harvestedIndices) < 11 && count($harvestedIndices) != count($dataTable)) {
						$curIndex = $targetUserIndex + $offset;
						if ($curIndex >= 0 && $curIndex < count($dataTable)) {
							$harvestedIndices[] = $curIndex;
						}
						
						if ($iteration % 2 == 0) {
							$offset = abs($offset);
						} else {
							$offset = -($offset + 1);
						}
						$iteration++;
					}
					
					// Sort the $harvestedIndices and return those rows from $dataTable
					asort($harvestedIndices);
					
					$result = array();
					foreach ($harvestedIndices as $index) {
						$result[] = $dataTable[$index];
					}
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getMetaRankLeaderboard($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, 
				"(SELECT 'higher' AS table_name, higher_score.rank, HEX(higher_users.id) AS id, higher_users.score, higher_users.gender, higher_users.locality, higher_users.administrative_area, higher_users.country FROM (SELECT rank_users.id, rank_users.meta_score AS score, rank_users.gender, rank_user_locations.locality, rank_user_locations.administrative_area, countries.code AS country FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON countries.id = rank_user_locations.country_id LEFT JOIN rank_users AS target_user ON target_user.id = UNHEX(?) WHERE rank_users.meta_score > target_user.meta_score ORDER BY rank_users.meta_score ASC LIMIT 10) AS higher_users INNER JOIN (SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_users AS target_user ON target_user.id = UNHEX(?) WHERE rank_users.meta_score > target_user.meta_score) AS higher_score)" .
				" UNION ALL " .
				"(SELECT 'equal' AS table_name, @r := @r + 1 AS rank, HEX(equal_users.id) AS id, equal_users.score, equal_users.gender, equal_users.locality, equal_users.administrative_area, equal_users.country FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_users.meta_score AS score, rank_users.gender, rank_user_locations.locality, rank_user_locations.administrative_area, countries.code AS country FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON rank_user_locations.country_id = countries.id LEFT JOIN rank_users AS target_user ON target_user.id = UNHEX(?) WHERE rank_users.meta_score = target_user.meta_score ORDER BY rank_users.creation_time ASC) AS equal_users)" .
				" UNION ALL " .
				"(SELECT 'lower' AS table_name, @i := @i + 1 AS rank, HEX(lower_users.id) AS id, lower_users.score, lower_users.gender, lower_users.locality, lower_users.administrative_area, lower_users.country FROM (SELECT @i := 0) AS initialize, (SELECT rank_users.id, rank_users.meta_score AS score, rank_users.gender, rank_user_locations.locality, rank_user_locations.administrative_area, countries.code AS country FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON countries.id = rank_user_locations.country_id LEFT JOIN rank_users AS target_user ON target_user.id = UNHEX(?) WHERE rank_users.meta_score < target_user.meta_score ORDER BY rank_users.meta_score DESC LIMIT 10) AS lower_users)"
			);
			
			mysqli_stmt_bind_param($stmt, "ssss", $userID, $userID, $userID, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank_above = 0;
				$num_equal = 0;
				$obtained = 0;
				
				$higher = array();
				$equal = array();
				$lower = array();
				
				while ($row = mysqli_fetch_array($recordSet)) {
					if ($row["table_name"] == "higher") {
						$higher[] = $row;
						$rank_above = $row["rank"];
					} else if ($row["table_name"] == "equal") {
						$equal[] = $row;
					} else {
						$lower[] = $row;
					}
				}
				
				// Correct rank numbers in $higher
				for ($i = 0; $i < count($higher); $i++) {
					$higher[$i]["rank"] = $rank_above--;
				}
				$rank_above += count($higher);
				
				// Reverse higher since we obtained it in reverse order
				$higher = array_reverse($higher);
				
				$num_equal = count($equal);
				
				// Correct rank numbers in $equal
				for ($i = 0; $i < $num_equal; $i++) {
					$equal[$i]["rank"] += $rank_above;
				}
				
				// Correct rank numbers in $lower
				for ($i = 0; $i < count($lower); $i++) {
					$lower[$i]["rank"] += $rank_above + $num_equal;
				}
				
				$dataTable = array_merge($higher, $equal, $lower);
				
				// Find target user
				$targetUserIndex = 0;
				for ($i = 0; $i < count($dataTable); $i++) {
					if ($dataTable[$i]["id"] == $userID) {
						$targetUserIndex = $i;
						break;
					}
				}
				
				$harvestedIndices = array($targetUserIndex);
				$offset = -1;
				$iteration = 0;
				
				// Alternate adding from above and below target user until 11 results are returned
				while (count($harvestedIndices) < 11 && count($harvestedIndices) != count($dataTable)) {
					$curIndex = $targetUserIndex + $offset;
					if ($curIndex >= 0 && $curIndex < count($dataTable)) {
						$harvestedIndices[] = $curIndex;
					}
					
					if ($iteration % 2 == 0) {
						$offset = abs($offset);
					} else {
						$offset = -($offset + 1);
					}
					$iteration++;
				}
				
				// Sort the $harvestedIndices and return those rows from $dataTable
				asort($harvestedIndices);
				
				$result = array();
				foreach ($harvestedIndices as $index) {
					$result[] = $dataTable[$index];
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getMetaRankRankHistory($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%d %H:%i:%sZ') AS modified_time, rank FROM rank_global WHERE user_id = UNHEX(?) AND modified_time >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY modified_time ASC");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = array();
				
				$recordSet = mysqli_stmt_get_result($stmt);
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			//echo "<pre>"; print_r($result); echo "</pre>"; die;
			
			return $result;
		}

		public function getMetaRankParticipantsHistory() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%d %H:%i:%sZ') AS modified_time, participants FROM rank_global_participants WHERE modified_time >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY modified_time ASC");
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getMetaRankScoreHistory($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%d %H:%i:%sZ') AS modified_time, score FROM rank_score WHERE user_id = UNHEX(?) AND modified_time >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY modified_time ASC");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = array();
				
				$recordSet = mysqli_stmt_get_result($stmt);
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getMetaRankReportCardDetails($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT rank_users.meta_score AS score, rank_users.bonus_score, IFNULL(rank_users_facebook.score, 0) AS facebook_score, IFNULL(rank_users_linkedin.score, 0) AS linkedin_score, IFNULL(rank_users_twitter.score, 0) AS twitter_score FROM rank_users LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id WHERE rank_users.id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$stmt2 = mysqli_prepare($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%dT%H:%i:%sZ') AS modified_time FROM rank_score WHERE user_id = UNHEX(?) ORDER BY modified_time DESC LIMIT 1");
					mysqli_stmt_bind_param($stmt2, "s", $userID);
					if (mysqli_stmt_execute($stmt2)) {
						$recordSet2 = mysqli_stmt_get_result($stmt2);
						
						while ($row2 = mysqli_fetch_array($recordSet2)) {
							$result = $row + $row2;
						}
					}
					
					mysqli_stmt_close($stmt2);
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getMetaRankPostalCodeRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users AS target_user ON target_user_location.user_id = target_user.id WHERE rank_users.meta_score > target_user.meta_score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.postal_code = target_user_location.postal_code");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, postal_code FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_user_locations.postal_code FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users AS target_user ON target_user_location.user_id = target_user.id WHERE rank_users.meta_score = target_user.meta_score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.postal_code = target_user_location.postal_code ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$postalCode = $row["postal_code"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.postal_code = target_user_location.postal_code");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["postal_code"] = $postalCode;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getMetaRankLocalityRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users AS target_user ON target_user_location.user_id = target_user.id WHERE rank_users.meta_score > target_user.meta_score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area AND rank_user_locations.locality = target_user_location.locality");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, locality FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_user_locations.locality FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users AS target_user ON target_user_location.user_id = target_user.id WHERE rank_users.meta_score = target_user.meta_score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area AND rank_user_locations.locality = target_user_location.locality ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$locality = $row["locality"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area AND rank_user_locations.locality = target_user_location.locality");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["locality"] = $locality;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getMetaRankAdministrativeAreaRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users AS target_user ON target_user_location.user_id = target_user.id WHERE rank_users.meta_score > target_user.meta_score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, administrative_area FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_user_locations.administrative_area FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users AS target_user ON target_user_location.user_id = target_user.id WHERE rank_users.meta_score = target_user.meta_score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$administrativeArea = $row["administrative_area"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["administrative_area"] = $administrativeArea;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getMetaRankCountryRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users AS target_user ON target_user_location.user_id = target_user.id WHERE rank_users.meta_score > target_user.meta_score AND rank_user_locations.country_id = target_user_location.country_id");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, country FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, countries.name AS country FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON rank_user_locations.country_id = countries.id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users AS target_user ON target_user_location.user_id = target_user.id WHERE rank_users.meta_score = target_user.meta_score AND rank_user_locations.country_id = target_user_location.country_id ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$country = $row["country"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["country"] = $country;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getMetaRankGlobalParticipants() {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users");
			while ($row = mysqli_fetch_array($recordSet)) {
				$result = array();
				
				$result["participants"] = $row["participants"];
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getMetaRankGlobalRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_users AS target_user ON target_user.id = UNHEX(?) WHERE rank_users.meta_score > target_user.meta_score");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id FROM rank_users LEFT JOIN rank_users AS target_user ON target_user.id = UNHEX(?) WHERE rank_users.meta_score = target_user.meta_score ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						
						$result = array();
						
						$result["rank"] = $rank;
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}
		
		public function getFacebookLeaderboard($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, 
				"(SELECT 'higher' AS table_name, higher_score.rank, HEX(higher_users.id) AS id, higher_users.score, higher_users.gender, higher_users.locality, higher_users.administrative_area, higher_users.country FROM (SELECT rank_users.id, rank_users_facebook.score, rank_users.gender, rank_user_locations.locality, rank_user_locations.administrative_area, countries.code AS country FROM rank_users LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON countries.id = rank_user_locations.country_id LEFT JOIN rank_users_facebook AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_facebook.score > target_user.score ORDER BY rank_users_facebook.score ASC LIMIT 10) AS higher_users INNER JOIN (SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_facebook AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_facebook.score > target_user.score) AS higher_score)" .
				" UNION ALL " .
				"(SELECT 'equal' AS table_name, @r := @r + 1 AS rank, HEX(equal_users.id) AS id, equal_users.score, equal_users.gender, equal_users.locality, equal_users.administrative_area, equal_users.country FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_users_facebook.score, rank_users.gender, rank_user_locations.locality, rank_user_locations.administrative_area, countries.code AS country FROM rank_users LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON rank_user_locations.country_id = countries.id LEFT JOIN rank_users_facebook AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_facebook.score = target_user.score ORDER BY rank_users.creation_time ASC) AS equal_users)" .
				" UNION ALL " .
				"(SELECT 'lower' AS table_name, @i := @i + 1 AS rank, HEX(lower_users.id) AS id, lower_users.score, lower_users.gender, lower_users.locality, lower_users.administrative_area, lower_users.country FROM (SELECT @i := 0) AS initialize, (SELECT rank_users.id, rank_users_facebook.score, rank_users.gender, rank_user_locations.locality, rank_user_locations.administrative_area, countries.code AS country FROM rank_users LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON countries.id = rank_user_locations.country_id LEFT JOIN rank_users_facebook AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_facebook.score < target_user.score ORDER BY rank_users_facebook.score DESC LIMIT 10) AS lower_users)"
			);
			mysqli_stmt_bind_param($stmt, "ssss", $userID, $userID, $userID, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank_above = 0;
				$num_equal = 0;
				$obtained = 0;
				
				$higher = array();
				$equal = array();
				$lower = array();
				
				while ($row = mysqli_fetch_array($recordSet)) {
					if ($row["table_name"] == "higher") {
						$higher[] = $row;
						$rank_above = $row["rank"];
					} else if ($row["table_name"] == "equal") {
						$equal[] = $row;
					} else {
						$lower[] = $row;
					}
				}
				
				// Correct rank numbers in $higher
				for ($i = 0; $i < count($higher); $i++) {
					$higher[$i]["rank"] = $rank_above--;
				}
				$rank_above += count($higher);
				
				// Reverse higher since we obtained it in reverse order
				$higher = array_reverse($higher);
				
				$num_equal = count($equal);
				
				// Correct rank numbers in $equal
				for ($i = 0; $i < $num_equal; $i++) {
					$equal[$i]["rank"] += $rank_above;
				}
				
				// Correct rank numbers in $lower
				for ($i = 0; $i < count($lower); $i++) {
					$lower[$i]["rank"] += $rank_above + $num_equal;
				}
				
				$dataTable = array_merge($higher, $equal, $lower);
				
				// Find target user
				$targetUserIndex = 0;
				for ($i = 0; $i < count($dataTable); $i++) {
					if ($dataTable[$i]["id"] == $userID) {
						$targetUserIndex = $i;
						break;
					}
				}
				
				$harvestedIndices = array($targetUserIndex);
				$offset = -1;
				$iteration = 0;
				
				// Alternate adding from above and below target user until 11 results are returned
				while (count($harvestedIndices) < 11 && count($harvestedIndices) != count($dataTable)) {
					$curIndex = $targetUserIndex + $offset;
					if ($curIndex >= 0 && $curIndex < count($dataTable)) {
						$harvestedIndices[] = $curIndex;
					}
					
					if ($iteration % 2 == 0) {
						$offset = abs($offset);
					} else {
						$offset = -($offset + 1);
					}
					$iteration++;
				}
				
				// Sort the $harvestedIndices and return those rows from $dataTable
				asort($harvestedIndices);
				
				$result = array();
				foreach ($harvestedIndices as $index) {
					$result[] = $dataTable[$index];
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getFacebookRankHistory($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%d %H:%i:%sZ') AS modified_time, rank FROM rank_global_facebook WHERE user_id = UNHEX(?) AND modified_time >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY modified_time ASC");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = array();
				
				$recordSet = mysqli_stmt_get_result($stmt);
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getFacebookParticipantsHistory() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%d %H:%i:%sZ') AS modified_time, participants FROM rank_global_facebook_participants WHERE modified_time >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY modified_time ASC");
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getFacebookScoreHistory($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%d %H:%i:%sZ') AS modified_time, score FROM rank_score_facebook WHERE user_id = UNHEX(?) AND modified_time >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY modified_time ASC");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = array();
				
				$recordSet = mysqli_stmt_get_result($stmt);
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getFacebookReportCardDetails($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT score, checkins, checkin_likes, status_posts, links_posted, photos_posted, liked_photos, videos_posted, liked_videos, comments_posted, liked_comments, tagged_videos, tagged_checkins, tagged_photos, wall_posts_received, comments_received, unique_users FROM rank_users_facebook WHERE user_id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$stmt2 = mysqli_prepare($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%dT%H:%i:%sZ') AS modified_time FROM rank_score_facebook WHERE user_id = UNHEX(?) ORDER BY modified_time DESC LIMIT 1");
					mysqli_stmt_bind_param($stmt2, "s", $userID);
					if (mysqli_stmt_execute($stmt2)) {
						$recordSet2 = mysqli_stmt_get_result($stmt2);
						
						while ($row2 = mysqli_fetch_array($recordSet2)) {
							$result = $row + $row2;
						}
					}
					
					mysqli_stmt_close($stmt2);
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getFacebookPostalCodeRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_facebook AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_facebook.score > target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.postal_code = target_user_location.postal_code");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, postal_code FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_user_locations.postal_code FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_facebook AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_facebook.score = target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.postal_code = target_user_location.postal_code ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$postalCode = $row["postal_code"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.postal_code = target_user_location.postal_code");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["postal_code"] = $postalCode;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getFacebookLocalityRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_facebook AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_facebook.score > target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area AND rank_user_locations.locality = target_user_location.locality");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, locality FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_user_locations.locality FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_facebook AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_facebook.score = target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area AND rank_user_locations.locality = target_user_location.locality ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$locality = $row["locality"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area AND rank_user_locations.locality = target_user_location.locality");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["locality"] = $locality;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getFacebookAdministrativeAreaRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_facebook AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_facebook.score > target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, administrative_area FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_user_locations.administrative_area FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_facebook AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_facebook.score = target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$administrativeArea = $row["administrative_area"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["administrative_area"] = $administrativeArea;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getFacebookCountryRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_facebook AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_facebook.score > target_user.score AND rank_user_locations.country_id = target_user_location.country_id");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, country FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, countries.name AS country FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON rank_user_locations.country_id = countries.id INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_facebook AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_facebook.score = target_user.score AND rank_user_locations.country_id = target_user_location.country_id ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$country = $row["country"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["country"] = $country;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getFacebookGlobalParticipants() {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id");
			while ($row = mysqli_fetch_array($recordSet)) {
				$result = array();
				
				$result["participants"] = $row["participants"];
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getFacebookGlobalRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_facebook AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_facebook.score > target_user.score");
			
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id FROM rank_users INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_facebook AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_facebook.score = target_user.score ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						
						$result = array();
						
						$result["rank"] = $rank;
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}
		
		public function getLinkedInLeaderboard($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, 
				"(SELECT 'higher' AS table_name, higher_score.rank, HEX(higher_users.id) AS id, higher_users.score, higher_users.gender, higher_users.locality, higher_users.administrative_area, higher_users.country FROM (SELECT rank_users.id, rank_users_linkedin.score, rank_users.gender, rank_user_locations.locality, rank_user_locations.administrative_area, countries.code AS country FROM rank_users LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON countries.id = rank_user_locations.country_id LEFT JOIN rank_users_linkedin AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_linkedin.score > target_user.score ORDER BY rank_users_linkedin.score ASC LIMIT 10) AS higher_users INNER JOIN (SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_linkedin AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_linkedin.score > target_user.score) AS higher_score)" .
				" UNION ALL " .
				"(SELECT 'equal' AS table_name, @r := @r + 1 AS rank, HEX(equal_users.id) AS id, equal_users.score, equal_users.gender, equal_users.locality, equal_users.administrative_area, equal_users.country FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_users_linkedin.score, rank_users.gender, rank_user_locations.locality, rank_user_locations.administrative_area, countries.code AS country FROM rank_users LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON rank_user_locations.country_id = countries.id LEFT JOIN rank_users_linkedin AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_linkedin.score = target_user.score ORDER BY rank_users.creation_time ASC) AS equal_users)" .
				" UNION ALL " .
				"(SELECT 'lower' AS table_name, @i := @i + 1 AS rank, HEX(lower_users.id) AS id, lower_users.score, lower_users.gender, lower_users.locality, lower_users.administrative_area, lower_users.country FROM (SELECT @i := 0) AS initialize, (SELECT rank_users.id, rank_users_linkedin.score, rank_users.gender, rank_user_locations.locality, rank_user_locations.administrative_area, countries.code AS country FROM rank_users LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON countries.id = rank_user_locations.country_id LEFT JOIN rank_users_linkedin AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_linkedin.score < target_user.score ORDER BY rank_users_linkedin.score DESC LIMIT 10) AS lower_users)"
			);
			mysqli_stmt_bind_param($stmt, "ssss", $userID, $userID, $userID, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank_above = 0;
				$num_equal = 0;
				$obtained = 0;
				
				$higher = array();
				$equal = array();
				$lower = array();
				
				while ($row = mysqli_fetch_array($recordSet)) {
					if ($row["table_name"] == "higher") {
						$higher[] = $row;
						$rank_above = $row["rank"];
					} else if ($row["table_name"] == "equal") {
						$equal[] = $row;
					} else {
						$lower[] = $row;
					}
				}
				
				// Correct rank numbers in $higher
				for ($i = 0; $i < count($higher); $i++) {
					$higher[$i]["rank"] = $rank_above--;
				}
				$rank_above += count($higher);
				
				// Reverse higher since we obtained it in reverse order
				$higher = array_reverse($higher);
				
				$num_equal = count($equal);
				
				// Correct rank numbers in $equal
				for ($i = 0; $i < $num_equal; $i++) {
					$equal[$i]["rank"] += $rank_above;
				}
				
				// Correct rank numbers in $lower
				for ($i = 0; $i < count($lower); $i++) {
					$lower[$i]["rank"] += $rank_above + $num_equal;
				}
				
				$dataTable = array_merge($higher, $equal, $lower);
				
				// Find target user
				$targetUserIndex = 0;
				for ($i = 0; $i < count($dataTable); $i++) {
					if ($dataTable[$i]["id"] == $userID) {
						$targetUserIndex = $i;
						break;
					}
				}
				
				$harvestedIndices = array($targetUserIndex);
				$offset = -1;
				$iteration = 0;
				
				// Alternate adding from above and below target user until 11 results are returned
				while (count($harvestedIndices) < 11 && count($harvestedIndices) != count($dataTable)) {
					$curIndex = $targetUserIndex + $offset;
					if ($curIndex >= 0 && $curIndex < count($dataTable)) {
						$harvestedIndices[] = $curIndex;
					}
					
					if ($iteration % 2 == 0) {
						$offset = abs($offset);
					} else {
						$offset = -($offset + 1);
					}
					$iteration++;
				}
				
				// Sort the $harvestedIndices and return those rows from $dataTable
				asort($harvestedIndices);
				
				$result = array();
				foreach ($harvestedIndices as $index) {
					$result[] = $dataTable[$index];
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getLinkedInRankHistory($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%d %H:%i:%sZ') AS modified_time, rank FROM rank_global_linkedin WHERE user_id = UNHEX(?) AND modified_time >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY modified_time ASC");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = array();
				
				$recordSet = mysqli_stmt_get_result($stmt);
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getLinkedInParticipantsHistory() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%d %H:%i:%sZ') AS modified_time, participants FROM rank_global_linkedin_participants WHERE modified_time >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY modified_time ASC");
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getLinkedInScoreHistory($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%d %H:%i:%sZ') AS modified_time, score FROM rank_score_linkedin WHERE user_id = UNHEX(?) AND modified_time >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY modified_time ASC");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = array();
				
				$recordSet = mysqli_stmt_get_result($stmt);
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getLinkedInReportCardDetails($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT score, recommendations, connections, foreign_connections, positions, current_positions, educations FROM rank_users_linkedin WHERE user_id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$stmt2 = mysqli_prepare($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%dT%H:%i:%sZ') AS modified_time FROM rank_score_linkedin WHERE user_id = UNHEX(?) ORDER BY modified_time DESC LIMIT 1");
					mysqli_stmt_bind_param($stmt2, "s", $userID);
					if (mysqli_stmt_execute($stmt2)) {
						$recordSet2 = mysqli_stmt_get_result($stmt2);
						
						while ($row2 = mysqli_fetch_array($recordSet2)) {
							$result = $row + $row2;
						}
					}
					
					mysqli_stmt_close($stmt2);
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getLinkedInPostalCodeRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_linkedin AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_linkedin.score > target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.postal_code = target_user_location.postal_code");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, postal_code FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_user_locations.postal_code FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_linkedin AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_linkedin.score = target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.postal_code = target_user_location.postal_code ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$postalCode = $row["postal_code"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.postal_code = target_user_location.postal_code");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["postal_code"] = $postalCode;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getLinkedInLocalityRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_linkedin AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_linkedin.score > target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area AND rank_user_locations.locality = target_user_location.locality");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, locality FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_user_locations.locality FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_linkedin AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_linkedin.score = target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area AND rank_user_locations.locality = target_user_location.locality ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$locality = $row["locality"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area AND rank_user_locations.locality = target_user_location.locality");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["locality"] = $locality;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getLinkedInAdministrativeAreaRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_linkedin AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_linkedin.score > target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, administrative_area FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_user_locations.administrative_area FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_linkedin AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_linkedin.score = target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$administrativeArea = $row["administrative_area"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["administrative_area"] = $administrativeArea;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getLinkedInCountryRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_linkedin AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_linkedin.score > target_user.score AND rank_user_locations.country_id = target_user_location.country_id");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, country FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, countries.name AS country FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON rank_user_locations.country_id = countries.id INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_linkedin AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_linkedin.score = target_user.score AND rank_user_locations.country_id = target_user_location.country_id ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$country = $row["country"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["country"] = $country;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getLinkedInGlobalParticipants() {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id");
			while ($row = mysqli_fetch_array($recordSet)) {
				$result = array();
				
				$result["participants"] = $row["participants"];
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getLinkedInGlobalRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_linkedin AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_linkedin.score > target_user.score");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id FROM rank_users INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_linkedin AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_linkedin.score = target_user.score ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						
						$result = array();
						
						$result["rank"] = $rank;
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}
		
		public function getTwitterLeaderboard($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, 
				"(SELECT 'higher' AS table_name, higher_score.rank, HEX(higher_users.id) AS id, higher_users.score, higher_users.gender, higher_users.locality, higher_users.administrative_area, higher_users.country FROM (SELECT rank_users.id, rank_users_twitter.score, rank_users.gender, rank_user_locations.locality, rank_user_locations.administrative_area, countries.code AS country FROM rank_users LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON countries.id = rank_user_locations.country_id LEFT JOIN rank_users_twitter AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_twitter.score > target_user.score ORDER BY rank_users_twitter.score ASC LIMIT 10) AS higher_users INNER JOIN (SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_users_twitter AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_twitter.score > target_user.score) AS higher_score)" .
				" UNION ALL " .
				"(SELECT 'equal' AS table_name, @r := @r + 1 AS rank, HEX(equal_users.id) AS id, equal_users.score, equal_users.gender, equal_users.locality, equal_users.administrative_area, equal_users.country FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_users_twitter.score, rank_users.gender, rank_user_locations.locality, rank_user_locations.administrative_area, countries.code AS country FROM rank_users LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON rank_user_locations.country_id = countries.id LEFT JOIN rank_users_twitter AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_twitter.score = target_user.score ORDER BY rank_users.creation_time ASC) AS equal_users)" .
				" UNION ALL " .
				"(SELECT 'lower' AS table_name, @i := @i + 1 AS rank, HEX(lower_users.id) AS id, lower_users.score, lower_users.gender, lower_users.locality, lower_users.administrative_area, lower_users.country FROM (SELECT @i := 0) AS initialize, (SELECT rank_users.id, rank_users_twitter.score, rank_users.gender, rank_user_locations.locality, rank_user_locations.administrative_area, countries.code AS country FROM rank_users LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON countries.id = rank_user_locations.country_id LEFT JOIN rank_users_twitter AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_twitter.score < target_user.score ORDER BY rank_users_twitter.score DESC LIMIT 10) AS lower_users)"
			);
			mysqli_stmt_bind_param($stmt, "ssss", $userID, $userID, $userID, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank_above = 0;
				$num_equal = 0;
				$obtained = 0;
				
				$higher = array();
				$equal = array();
				$lower = array();
				
				while ($row = mysqli_fetch_array($recordSet)) {
					if ($row["table_name"] == "higher") {
						$higher[] = $row;
						$rank_above = $row["rank"];
					} else if ($row["table_name"] == "equal") {
						$equal[] = $row;
					} else {
						$lower[] = $row;
					}
				}
				
				// Correct rank numbers in $higher
				for ($i = 0; $i < count($higher); $i++) {
					$higher[$i]["rank"] = $rank_above--;
				}
				$rank_above += count($higher);
				
				// Reverse higher since we obtained it in reverse order
				$higher = array_reverse($higher);
				
				$num_equal = count($equal);
				
				// Correct rank numbers in $equal
				for ($i = 0; $i < $num_equal; $i++) {
					$equal[$i]["rank"] += $rank_above;
				}
				
				// Correct rank numbers in $lower
				for ($i = 0; $i < count($lower); $i++) {
					$lower[$i]["rank"] += $rank_above + $num_equal;
				}
				
				$dataTable = array_merge($higher, $equal, $lower);
				
				// Find target user
				$targetUserIndex = 0;
				for ($i = 0; $i < count($dataTable); $i++) {
					if ($dataTable[$i]["id"] == $userID) {
						$targetUserIndex = $i;
						break;
					}
				}
				
				$harvestedIndices = array($targetUserIndex);
				$offset = -1;
				$iteration = 0;
				
				// Alternate adding from above and below target user until 11 results are returned
				while (count($harvestedIndices) < 11 && count($harvestedIndices) != count($dataTable)) {
					$curIndex = $targetUserIndex + $offset;
					if ($curIndex >= 0 && $curIndex < count($dataTable)) {
						$harvestedIndices[] = $curIndex;
					}
					
					if ($iteration % 2 == 0) {
						$offset = abs($offset);
					} else {
						$offset = -($offset + 1);
					}
					$iteration++;
				}
				
				// Sort the $harvestedIndices and return those rows from $dataTable
				asort($harvestedIndices);
				
				$result = array();
				foreach ($harvestedIndices as $index) {
					$result[] = $dataTable[$index];
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getTwitterRankHistory($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%d %H:%i:%sZ') AS modified_time, rank FROM rank_global_twitter WHERE user_id = UNHEX(?) AND modified_time >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY modified_time ASC");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = array();
				
				$recordSet = mysqli_stmt_get_result($stmt);
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getTwitterParticipantsHistory() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%d %H:%i:%sZ') AS modified_time, participants FROM rank_global_twitter_participants WHERE modified_time >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY modified_time ASC");
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getTwitterScoreHistory($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%d %H:%i:%sZ') AS modified_time, score FROM rank_score_twitter WHERE user_id = UNHEX(?) AND modified_time >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY modified_time ASC");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = array();
				
				$recordSet = mysqli_stmt_get_result($stmt);
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getTwitterReportCardDetails($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT score, original_tweets, other_tweets, retweeted_total, favorited_total, favorites, statuses, friends, followers, listed, verified FROM rank_users_twitter WHERE user_id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$stmt2 = mysqli_prepare($this->_conn, "SELECT DATE_FORMAT(modified_time, '%Y-%m-%dT%H:%i:%sZ') AS modified_time FROM rank_score_twitter WHERE user_id = UNHEX(?) ORDER BY modified_time DESC LIMIT 1");
					mysqli_stmt_bind_param($stmt2, "s", $userID);
					if (mysqli_stmt_execute($stmt2)) {
						$recordSet2 = mysqli_stmt_get_result($stmt2);
						
						while ($row2 = mysqli_fetch_array($recordSet2)) {
							$result = $row + $row2;
						}
					}
					
					mysqli_stmt_close($stmt2);
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getTwitterPostalCodeRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_twitter AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_twitter.score > target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.postal_code = target_user_location.postal_code");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, postal_code FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_user_locations.postal_code FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_twitter AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_twitter.score = target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.postal_code = target_user_location.postal_code ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$postalCode = $row["postal_code"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.postal_code = target_user_location.postal_code");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["postal_code"] = $postalCode;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getTwitterLocalityRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_twitter AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_twitter.score > target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area AND rank_user_locations.locality = target_user_location.locality");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, locality FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_user_locations.locality FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_twitter AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_twitter.score = target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area AND rank_user_locations.locality = target_user_location.locality ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$locality = $row["locality"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area AND rank_user_locations.locality = target_user_location.locality");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["locality"] = $locality;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getTwitterAdministrativeAreaRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_twitter AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_twitter.score > target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, administrative_area FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, rank_user_locations.administrative_area FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_twitter AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_twitter.score = target_user.score AND rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$administrativeArea = $row["administrative_area"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id AND rank_user_locations.administrative_area = target_user_location.administrative_area");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["administrative_area"] = $administrativeArea;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getTwitterCountryRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_twitter AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_twitter.score > target_user.score AND rank_user_locations.country_id = target_user_location.country_id");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker, country FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id, countries.name AS country FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON rank_user_locations.country_id = countries.id INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) LEFT JOIN rank_users_twitter AS target_user ON target_user_location.user_id = target_user.user_id WHERE rank_users_twitter.score = target_user.score AND rank_user_locations.country_id = target_user_location.country_id ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						$country = $row["country"];
						
						$stmt3 = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_user_locations AS target_user_location ON target_user_location.user_id = UNHEX(?) WHERE rank_user_locations.country_id = target_user_location.country_id");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						if (mysqli_stmt_execute($stmt3)) {
							$recordSet = mysqli_stmt_get_result($stmt3);
							
							while ($row = mysqli_fetch_array($recordSet)) {
								$result = array();
								
								$result["participants"] = $row["participants"];
								$result["rank"] = $rank;
								$result["country"] = $country;
							}
						}
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getTwitterGlobalParticipants() {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id");
			while ($row = mysqli_fetch_array($recordSet)) {
				$result = array();
				
				$result["participants"] = $row["participants"];
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getTwitterGlobalRank($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_users_twitter AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_twitter.score > target_user.score");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$rank = 0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$rank = $row["rank"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id FROM rank_users INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_users_twitter AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_twitter.score = target_user.score ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ss", $userID, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$rank += $row["tie_breaker"];
						
						$result = array();
						
						$result["rank"] = $rank;
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function updateUserLocation($userID, $countryCode, $region, $city, $postalCode, $latitude, $longitude) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_user_locations SET country_id = (SELECT id FROM countries WHERE code = ? LIMIT 1), administrative_area = ?, locality = ?, postal_code = ?, latitude = ?, longitude = ? WHERE user_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "sssssss", $countryCode, $region, $city, $postalCode, $latitude, $longitude, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}
		
		public function updateUserProfilePicture($userID, $profilePicture) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_user_settings SET profile_picture = ? WHERE user_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", $profilePicture, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			return $result;
		}
		
		public function updateUserEmailFlags($userID, $emailFlags) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_user_settings SET email_flags = ? WHERE user_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "is", $emailFlags, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			return $result;
		}
		
		public function updateUserPrivacyFlags($userID, $privacyFlags) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_user_settings SET privacy_flags = ? WHERE user_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "is", $privacyFlags, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			return $result;
		}

		public function disconnectFacebookFromUser($userID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "DELETE FROM rank_users_facebook WHERE user_id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$stmt2 = mysqli_prepare($this->_conn, "DELETE FROM rank_score_facebook WHERE user_id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "s", $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$stmt3 = mysqli_prepare($this->_conn, "DELETE FROM rank_global_facebook WHERE user_id = UNHEX(?)");
					mysqli_stmt_bind_param($stmt3, "s", $userID);
					if (mysqli_stmt_execute($stmt3)) {
						$result = $this->updateMetaScore($userID);
					}
					
					mysqli_stmt_close($stmt3);
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function disconnectLinkedInFromUser($userID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "DELETE FROM rank_users_linkedin WHERE user_id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$stmt2 = mysqli_prepare($this->_conn, "DELETE FROM rank_score_linkedin WHERE user_id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "s", $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$stmt3 = mysqli_prepare($this->_conn, "DELETE FROM rank_global_linkedin WHERE user_id = UNHEX(?)");
					mysqli_stmt_bind_param($stmt3, "s", $userID);
					if (mysqli_stmt_execute($stmt3)) {
						$result = $this->updateMetaScore($userID);
					}
					
					mysqli_stmt_close($stmt3);
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function disconnectTwitterFromUser($userID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "DELETE FROM rank_users_twitter WHERE user_id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$stmt2 = mysqli_prepare($this->_conn, "DELETE FROM rank_score_twitter WHERE user_id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "s", $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$stmt3 = mysqli_prepare($this->_conn, "DELETE FROM rank_global_twitter WHERE user_id = UNHEX(?)");
					mysqli_stmt_bind_param($stmt3, "s", $userID);
					if (mysqli_stmt_execute($stmt3)) {
						$result = $this->updateMetaScore($userID);
					}
					
					mysqli_stmt_close($stmt3);
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function updateFacebookAccessToken($userID, $accessToken, $accessTokenExpireTime) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users_facebook SET access_token = ?, access_token_expire = ? WHERE user_id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "sss", $accessToken, $accessTokenExpireTime->format("Y-m-d H:i:s"), $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function createFacebookUser($userID, $uniqueID, $accessToken, $accessTokenExpireTime) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_users_facebook SET user_id = UNHEX(?), unique_id = ?, access_token = ?, access_token_expire = ?");
			mysqli_stmt_bind_param($stmt, "ssss", $userID, $uniqueID, $accessToken, $accessTokenExpireTime->format("Y-m-d H:i:s"));
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			} else die(mysqli_error($this->_conn));
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getFacebookUserEmail($facebookUniqueID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT rank_users.email FROM rank_users_facebook LEFT JOIN rank_users ON rank_users_facebook.user_id = rank_users.id WHERE rank_users_facebook.unique_id = ?");
			
			mysqli_stmt_bind_param($stmt, "s", $facebookUniqueID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = $row["email"];
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function updateLinkedInAccessToken($userID, $accessToken, $accessTokenExpireTime) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users_linkedin SET oauth_token = ?, oauth_token_expire = ? WHERE user_id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "sss", $accessToken, $accessTokenExpireTime->format("Y-m-d H:i:s"), $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function createLinkedInUser($userID, $uniqueID, $accessToken, $accessTokenExpireTime) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_users_linkedin SET user_id = UNHEX(?), unique_id = ?, oauth_token = ?, oauth_token_expire = ?");
			mysqli_stmt_bind_param($stmt, "ssss", $userID, $uniqueID, $accessToken, $accessTokenExpireTime->format("Y-m-d H:i:s"));
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getLinkedInUserEmail($linkedinUniqueID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT rank_users.email FROM rank_users_linkedin LEFT JOIN rank_users ON rank_users_linkedin.user_id = rank_users.id WHERE rank_users_linkedin.unique_id = ?");
			
			mysqli_stmt_bind_param($stmt, "s", $linkedinUniqueID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = $row["email"];
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function updateTwitterOAuthToken($userID, $oAuthToken, $oAuthTokenSecret) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users_twitter SET oauth_token = ?, oauth_token_secret = ? WHERE user_id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "sss", $oAuthToken, $oAuthTokenSecret, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function createTwitterUser($userID, $uniqueID, $oAuthToken, $oAuthTokenSecret, $profilePictureURL) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_users_twitter SET user_id = UNHEX(?), unique_id = ?, oauth_token = ?, oauth_token_secret = ?, profile_picture_url = ?");
			mysqli_stmt_bind_param($stmt, "sssss", $userID, $uniqueID, $oAuthToken, $oAuthTokenSecret, $profilePictureURL);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getTwitterUserEmail($twitterUniqueID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT rank_users.email FROM rank_users_twitter LEFT JOIN rank_users ON rank_users_twitter.user_id = rank_users.id WHERE rank_users_twitter.unique_id = ?");
			
			mysqli_stmt_bind_param($stmt, "s", $twitterUniqueID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = $row["email"];
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getLock($key, $timeout) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT GET_LOCK(?, ?) AS success");
			
			mysqli_stmt_bind_param($stmt, "si", $key, ($timeout != null ? $timeout : LOCK_TIMEOUT));
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = $row["success"];
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function releaseLock($key) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT RELEASE_LOCK(?) AS success");
			
			mysqli_stmt_bind_param($stmt, "s", $key);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = $row["success"];
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getEligiblePromotionMonths($accountID) {
			$result = 0;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(promotions.id) AS used_promotion_months FROM promotions LEFT JOIN business_promotions ON promotions.id = business_promotions.promotion_id LEFT JOIN businesses ON business_promotions.business_id = businesses.id WHERE businesses.account_id = UNHEX(?) GROUP BY YEAR(promotions.start_time), MONTH(promotions.start_time)");
			
			mysqli_stmt_bind_param($stmt, "s", $accountID);
			
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$usedPromotionMonths = mysqli_num_rows($recordSet);
			}
			
			mysqli_stmt_close($stmt);
			
			if (isset($usedPromotionMonths)) {
				$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(perks.id) AS num_perks FROM perks LEFT JOIN business_perks ON perks.id = business_perks.perk_id LEFT JOIN businesses ON business_perks.business_id = businesses.id WHERE businesses.account_id = UNHEX(?)");
				
				mysqli_stmt_bind_param($stmt, "s", $accountID);
				
				if (mysqli_stmt_execute($stmt)) {
					$recordSet = mysqli_stmt_get_result($stmt);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$result = $row["num_perks"] - $usedPromotionMonths;
					}
				}
				
				mysqli_stmt_close($stmt);
			}
			
			return $result;
		}

		public function getScheduledPerks($businessID) {
			$result = array();
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(perks.id) AS id, perks.name, perks.description, perks.notes, perks.value, businesses.name AS business_name, businesses.website AS business_website, currencies.abbreviation AS currency_abbreviation, DATE_FORMAT(perks.start_time, '%Y-%m-%dT%H:%i:%sZ') AS start_time, perks.duration, perks.eligibility_distance, distance_units.name AS eligibility_distance_unit_name, IFNULL(services.name, 'All Services') AS service_name, perks.max_recipients, perks.signatory, DATE_FORMAT(perks.date_signed, '%Y-%m-%dT%H:%i:%sZ') AS date_signed FROM perks LEFT JOIN business_perks ON perks.id = business_perks.perk_id LEFT JOIN businesses ON business_perks.business_id = businesses.id LEFT JOIN currencies ON perks.value_currency_id = currencies.id LEFT JOIN distance_units ON perks.eligibility_distance_unit_id = distance_units.id LEFT JOIN services ON perks.service_id = services.id WHERE businesses.id = UNHEX(?) AND perks.start_time > NOW() ORDER BY perks.date_signed DESC");
			
			mysqli_stmt_bind_param($stmt, "s", $businessID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					if ($row["eligibility_distance"] != "0") {
						$row["eligibility_addresses"] = array();
						
						$addressRecordSet = mysqli_query($this->_conn, "SELECT HEX(perk_eligibility_addresses.id) AS id, perk_eligibility_addresses.thoroughfare, perk_eligibility_addresses.premise, perk_eligibility_addresses.locality, perk_eligibility_addresses.dependent_locality, perk_eligibility_addresses.administrative_area, perk_eligibility_addresses.postal_code, HEX(perk_eligibility_addresses.country_id) AS country_id, countries.name AS country_name, perk_eligibility_addresses.latitude, perk_eligibility_addresses.longitude FROM perk_eligibility_addresses LEFT JOIN countries ON perk_eligibility_addresses.country_id = countries.id WHERE perk_eligibility_addresses.perk_id = UNHEX('" . $row["id"] . "')");
						
						while ($row2 = mysqli_fetch_array($addressRecordSet)) {
							$row["eligibility_addresses"][] = $row2;
						}
						
						mysqli_free_result($addressRecordSet);
					}
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getActivePerks($businessID) {
			$result = array();
			
			$stmt = mysqli_prepare($this->_conn, "SELECT NOW() as now, HEX(perks.id) AS id, perks.name, perks.description, perks.notes, perks.value, businesses.name AS business_name, businesses.website AS business_website, currencies.abbreviation AS currency_abbreviation, DATE_FORMAT(perks.start_time, '%Y-%m-%dT%H:%i:%sZ') AS start_time, perks.duration, perks.eligibility_distance, distance_units.name AS eligibility_distance_unit_name, IFNULL(services.name, 'All Services') AS service_name, perks.max_recipients, perks.signatory, DATE_FORMAT(perks.date_signed, '%Y-%m-%dT%H:%i:%sZ') AS date_signed FROM perks LEFT JOIN business_perks ON perks.id = business_perks.perk_id LEFT JOIN businesses ON business_perks.business_id = businesses.id LEFT JOIN currencies ON perks.value_currency_id = currencies.id LEFT JOIN distance_units ON perks.eligibility_distance_unit_id = distance_units.id LEFT JOIN services ON perks.service_id = services.id WHERE businesses.id = UNHEX(?) AND perks.start_time > DATE_SUB(NOW(), INTERVAL perks.duration SECOND) AND perks.start_time <= NOW() ORDER BY perks.date_signed DESC");
			
			mysqli_stmt_bind_param($stmt, "s", $businessID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					if ($row["eligibility_distance"] != "0") {
						$row["eligibility_addresses"] = array();
						
						$addressRecordSet = mysqli_query($this->_conn, "SELECT HEX(perk_eligibility_addresses.id) AS id, perk_eligibility_addresses.thoroughfare, perk_eligibility_addresses.premise, perk_eligibility_addresses.locality, perk_eligibility_addresses.dependent_locality, perk_eligibility_addresses.administrative_area, perk_eligibility_addresses.postal_code, HEX(perk_eligibility_addresses.country_id) AS country_id, countries.name AS country_name, perk_eligibility_addresses.latitude, perk_eligibility_addresses.longitude FROM perk_eligibility_addresses LEFT JOIN countries ON perk_eligibility_addresses.country_id = countries.id WHERE perk_eligibility_addresses.perk_id = UNHEX('" . $row["id"] . "')");
						
						while ($row2 = mysqli_fetch_array($addressRecordSet)) {
							$row["eligibility_addresses"][] = $row2;
						}
						
						mysqli_free_result($addressRecordSet);
					}
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getCompletedPerks($businessID) {
			$result = array();
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(perks.id) AS id, perks.name, perks.description, perks.notes, perks.value, businesses.name AS business_name, businesses.website AS business_website, currencies.abbreviation AS currency_abbreviation, DATE_FORMAT(perks.start_time, '%Y-%m-%dT%H:%i:%sZ') AS start_time, perks.duration, perks.eligibility_distance, distance_units.name AS eligibility_distance_unit_name, IFNULL(services.name, 'All Services') AS service_name, perks.max_recipients, perks.signatory, DATE_FORMAT(perks.date_signed, '%Y-%m-%dT%H:%i:%sZ') AS date_signed FROM perks LEFT JOIN business_perks ON perks.id = business_perks.perk_id LEFT JOIN businesses ON business_perks.business_id = businesses.id LEFT JOIN currencies ON perks.value_currency_id = currencies.id LEFT JOIN distance_units ON perks.eligibility_distance_unit_id = distance_units.id LEFT JOIN services ON perks.service_id = services.id WHERE businesses.id = UNHEX(?) AND perks.start_time <= DATE_SUB(NOW(), INTERVAL perks.duration SECOND) ORDER BY perks.date_signed DESC");
			
			mysqli_stmt_bind_param($stmt, "s", $businessID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					if ($row["eligibility_distance"] != "0") {
						$row["eligibility_addresses"] = array();
						
						$addressRecordSet = mysqli_query($this->_conn, "SELECT HEX(perk_eligibility_addresses.id) AS id, perk_eligibility_addresses.thoroughfare, perk_eligibility_addresses.premise, perk_eligibility_addresses.locality, perk_eligibility_addresses.dependent_locality, perk_eligibility_addresses.administrative_area, perk_eligibility_addresses.postal_code, HEX(perk_eligibility_addresses.country_id) AS country_id, countries.name AS country_name, perk_eligibility_addresses.latitude, perk_eligibility_addresses.longitude FROM perk_eligibility_addresses LEFT JOIN countries ON perk_eligibility_addresses.country_id = countries.id WHERE perk_eligibility_addresses.perk_id = UNHEX('" . $row["id"] . "')");
						
						while ($row2 = mysqli_fetch_array($addressRecordSet)) {
							$row["eligibility_addresses"][] = $row2;
						}
						
						mysqli_free_result($addressRecordSet);
					}
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getScheduledPromotions($businessID) {
			$result = array();
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(promotions.id) AS id, promotions.name, promotions.description, promotions.notes, promotions.value, businesses.name AS business_name, businesses.website AS business_website, currencies.abbreviation AS currency_abbreviation, DATE_FORMAT(promotions.start_time, '%Y-%m-%dT%H:%i:%sZ') AS start_time, promotions.duration, promotions.eligibility_distance, distance_units.name AS eligibility_distance_unit_name, services.name AS service_name, promotion_checkin_options.location AS checkin_location, promotion_checkin_options.hashtag AS checkin_hashtag, promotion_post_options.link AS post_link, promotion_like_options.link AS like_link, promotions.max_recipients, promotions.signatory, DATE_FORMAT(promotions.date_signed, '%Y-%m-%dT%H:%i:%sZ') AS date_signed FROM promotions LEFT JOIN business_promotions ON promotions.id = business_promotions.promotion_id LEFT JOIN businesses ON business_promotions.business_id = businesses.id LEFT JOIN currencies ON promotions.value_currency_id = currencies.id LEFT JOIN distance_units ON promotions.eligibility_distance_unit_id = distance_units.id LEFT JOIN services ON promotions.service_id = services.id LEFT JOIN promotion_checkin_options ON promotions.id = promotion_checkin_options.promotion_id LEFT JOIN promotion_post_options ON promotions.id = promotion_post_options.promotion_id LEFT JOIN promotion_like_options ON promotions.id = promotion_like_options.promotion_id WHERE businesses.id = UNHEX(?) AND promotions.start_time > NOW() ORDER BY promotions.date_signed DESC");
			
			mysqli_stmt_bind_param($stmt, "s", $businessID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					if ($row["eligibility_distance"] != "0") {
						$row["eligibility_addresses"] = array();
						
						$addressRecordSet = mysqli_query($this->_conn, "SELECT HEX(promotion_eligibility_addresses.id) AS id, promotion_eligibility_addresses.thoroughfare, promotion_eligibility_addresses.premise, promotion_eligibility_addresses.locality, promotion_eligibility_addresses.dependent_locality, promotion_eligibility_addresses.administrative_area, promotion_eligibility_addresses.postal_code, HEX(promotion_eligibility_addresses.country_id) AS country_id, countries.name AS country_name, promotion_eligibility_addresses.latitude, promotion_eligibility_addresses.longitude FROM promotion_eligibility_addresses LEFT JOIN countries ON promotion_eligibility_addresses.country_id = countries.id WHERE promotion_eligibility_addresses.promotion_id = UNHEX('" . $row["id"] . "')");
						
						while ($row2 = mysqli_fetch_array($addressRecordSet)) {
							$row["eligibility_addresses"][] = $row2;
						}
						
						mysqli_free_result($addressRecordSet);
					}
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getActivePromotions($businessID) {
			$result = array();
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(promotions.id) AS id, promotions.name, promotions.description, promotions.notes, promotions.value, businesses.name AS business_name, businesses.website AS business_website, currencies.abbreviation AS currency_abbreviation, DATE_FORMAT(promotions.start_time, '%Y-%m-%dT%H:%i:%sZ') AS start_time, promotions.duration, promotions.eligibility_distance, distance_units.name AS eligibility_distance_unit_name, services.name AS service_name, promotion_checkin_options.location AS checkin_location, promotion_checkin_options.hashtag AS checkin_hashtag, promotion_post_options.link AS post_link, promotion_like_options.link AS like_link, promotions.max_recipients, promotions.signatory, DATE_FORMAT(promotions.date_signed, '%Y-%m-%dT%H:%i:%sZ') AS date_signed FROM promotions LEFT JOIN business_promotions ON promotions.id = business_promotions.promotion_id LEFT JOIN businesses ON business_promotions.business_id = businesses.id LEFT JOIN currencies ON promotions.value_currency_id = currencies.id LEFT JOIN distance_units ON promotions.eligibility_distance_unit_id = distance_units.id LEFT JOIN services ON promotions.service_id = services.id LEFT JOIN promotion_checkin_options ON promotions.id = promotion_checkin_options.promotion_id LEFT JOIN promotion_post_options ON promotions.id = promotion_post_options.promotion_id LEFT JOIN promotion_like_options ON promotions.id = promotion_like_options.promotion_id WHERE businesses.id = UNHEX(?) AND promotions.start_time > DATE_SUB(NOW(), INTERVAL promotions.duration SECOND) AND promotions.start_time <= NOW() ORDER BY promotions.date_signed DESC");
			
			mysqli_stmt_bind_param($stmt, "s", $businessID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					if ($row["eligibility_distance"] != "0") {
						$row["eligibility_addresses"] = array();
						
						$addressRecordSet = mysqli_query($this->_conn, "SELECT HEX(promotion_eligibility_addresses.id) AS id, promotion_eligibility_addresses.thoroughfare, promotion_eligibility_addresses.premise, promotion_eligibility_addresses.locality, promotion_eligibility_addresses.dependent_locality, promotion_eligibility_addresses.administrative_area, promotion_eligibility_addresses.postal_code, HEX(promotion_eligibility_addresses.country_id) AS country_id, countries.name AS country_name, promotion_eligibility_addresses.latitude, promotion_eligibility_addresses.longitude FROM promotion_eligibility_addresses LEFT JOIN countries ON promotion_eligibility_addresses.country_id = countries.id WHERE promotion_eligibility_addresses.promotion_id = UNHEX('" . $row["id"] . "')");
						
						while ($row2 = mysqli_fetch_array($addressRecordSet)) {
							$row["eligibility_addresses"][] = $row2;
						}
						
						mysqli_free_result($addressRecordSet);
					}
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getCompletedPromotions($businessID) {
			$result = array();
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(promotions.id) AS id, promotions.name, promotions.description, promotions.notes, promotions.value, businesses.name AS business_name, businesses.website AS business_website, currencies.abbreviation AS currency_abbreviation, DATE_FORMAT(promotions.start_time, '%Y-%m-%dT%H:%i:%sZ') AS start_time, promotions.duration, promotions.eligibility_distance, distance_units.name AS eligibility_distance_unit_name, services.name AS service_name, promotion_checkin_options.location AS checkin_location, promotion_checkin_options.hashtag AS checkin_hashtag, promotion_post_options.link AS post_link, promotion_like_options.link AS like_link, promotions.max_recipients, promotions.signatory, DATE_FORMAT(promotions.date_signed, '%Y-%m-%dT%H:%i:%sZ') AS date_signed FROM promotions LEFT JOIN business_promotions ON promotions.id = business_promotions.promotion_id LEFT JOIN businesses ON business_promotions.business_id = businesses.id LEFT JOIN currencies ON promotions.value_currency_id = currencies.id LEFT JOIN distance_units ON promotions.eligibility_distance_unit_id = distance_units.id LEFT JOIN services ON promotions.service_id = services.id LEFT JOIN promotion_checkin_options ON promotions.id = promotion_checkin_options.promotion_id LEFT JOIN promotion_post_options ON promotions.id = promotion_post_options.promotion_id LEFT JOIN promotion_like_options ON promotions.id = promotion_like_options.promotion_id WHERE businesses.id = UNHEX(?) AND promotions.start_time <= DATE_SUB(NOW(), INTERVAL promotions.duration SECOND) ORDER BY promotions.date_signed DESC");
			
			mysqli_stmt_bind_param($stmt, "s", $businessID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					if ($row["eligibility_distance"] != "0") {
						$row["eligibility_addresses"] = array();
						
						$addressRecordSet = mysqli_query($this->_conn, "SELECT HEX(promotion_eligibility_addresses.id) AS id, promotion_eligibility_addresses.thoroughfare, promotion_eligibility_addresses.premise, promotion_eligibility_addresses.locality, promotion_eligibility_addresses.dependent_locality, promotion_eligibility_addresses.administrative_area, promotion_eligibility_addresses.postal_code, HEX(promotion_eligibility_addresses.country_id) AS country_id, countries.name AS country_name, promotion_eligibility_addresses.latitude, promotion_eligibility_addresses.longitude FROM promotion_eligibility_addresses LEFT JOIN countries ON promotion_eligibility_addresses.country_id = countries.id WHERE promotion_eligibility_addresses.promotion_id = UNHEX('" . $row["id"] . "')");
						
						while ($row2 = mysqli_fetch_array($addressRecordSet)) {
							$row["eligibility_addresses"][] = $row2;
						}
						
						mysqli_free_result($addressRecordSet);
					}
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function createPerk($name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned) {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(NEWID()) AS id");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$perkID = $row["id"];
			}
			
			mysqli_free_result($recordSet);
			
			if (isset($perkID)) {
				if ($notes != null) {
					if ($serviceID != null) {
						if ($distanceUnitID != null) {
							$stmt = mysqli_prepare($this->_conn, "INSERT INTO perks SET name = ?, description = ?, notes = ?, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = UNHEX(?), service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
							mysqli_stmt_bind_param($stmt, "sssssssssssssss", $name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
						} else {
							$stmt = mysqli_prepare($this->_conn, "INSERT INTO perks SET name = ?, description = ?, notes = ?, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = NULL, service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
							mysqli_stmt_bind_param($stmt, "ssssssssssssss", $name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
						}
					} else {
						if ($distanceUnitID != null) {
							$stmt = mysqli_prepare($this->_conn, "INSERT INTO perks SET name = ?, description = ?, notes = ?, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = UNHEX(?), service_id = NULL, max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
							mysqli_stmt_bind_param($stmt, "ssssssssssssss", $name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
						} else {
							$stmt = mysqli_prepare($this->_conn, "INSERT INTO perks SET name = ?, description = ?, notes = ?, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = NULL, service_id = NULL, max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
							mysqli_stmt_bind_param($stmt, "sssssssssssss", $name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
						}
					}
				} else {
					if ($serviceID != null) {
						if ($distanceUnitID != null) {
							$stmt = mysqli_prepare($this->_conn, "INSERT INTO perks SET name = ?, description = ?, notes = NULL, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = UNHEX(?), service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
							mysqli_stmt_bind_param($stmt, "ssssssssssssss", $name, $description, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
						} else {
							$stmt = mysqli_prepare($this->_conn, "INSERT INTO perks SET name = ?, description = ?, notes = NULL, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = NULL, service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
							mysqli_stmt_bind_param($stmt, "sssssssssssss", $name, $description, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
						}
					} else {
						if ($distanceUnitID != null) {
							$stmt = mysqli_prepare($this->_conn, "INSERT INTO perks SET name = ?, description = ?, notes = NULL, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = UNHEX(?), service_id = NULL, max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
							mysqli_stmt_bind_param($stmt, "sssssssssssss", $name, $description, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
						} else {
							$stmt = mysqli_prepare($this->_conn, "INSERT INTO perks SET name = ?, description = ?, notes = NULL, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = NULL, service_id = NULL, max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
							mysqli_stmt_bind_param($stmt, "ssssssssssss", $name, $description, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
						}
					}
				}
				
				if (mysqli_stmt_execute($stmt)) {
					$result = $perkID;
				}
				
				mysqli_stmt_close($stmt);
			}
			
			return $result;
		}

		public function createPerkEligibilityAddress($perkID, $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $countryID, $latitude, $longitude) {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(NEWID()) AS id");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$perkEligibilityAddressID = $row["id"];
			}
			
			mysqli_free_result($recordSet);
			
			if (isset($perkEligibilityAddressID)) {
				$stmt = mysqli_prepare($this->_conn, "INSERT INTO perk_eligibility_addresses SET perk_id = UNHEX(?), thoroughfare = ?, premise = ?, locality = ?, dependent_locality = ?, administrative_area = ?, postal_code = ?, country_id = UNHEX(?), latitude = ?, longitude = ?, id = UNHEX(?)");
				
				mysqli_stmt_bind_param($stmt, "sssssssssss", $perkID, $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $countryID, $latitude, $longitude, $perkEligibilityAddressID);
				
				if (mysqli_stmt_execute($stmt)) {
					$result = $perkEligibilityAddressID;
				}
				
				mysqli_stmt_close($stmt);
			}
			
			return $result;
		}

		public function linkPerkToBusiness($perkID, $businessID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "INSERT INTO business_perks SET business_id = UNHEX(?), perk_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", $businessID, $perkID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function createPromotion($name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned) {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(NEWID()) AS id");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$promotionID = $row["id"];
			}
			
			mysqli_free_result($recordSet);
			
			if (isset($promotionID)) {
				if ($notes != null) {
					if ($distanceUnitID != null) {
						$stmt = mysqli_prepare($this->_conn, "INSERT INTO promotions SET name = ?, description = ?, notes = ?, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = UNHEX(?), service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
						mysqli_stmt_bind_param($stmt, "sssssssssssssss", $name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $promotionID);
					} else {
						$stmt = mysqli_prepare($this->_conn, "INSERT INTO promotions SET name = ?, description = ?, notes = ?, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = NULL, service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
						mysqli_stmt_bind_param($stmt, "ssssssssssssss", $name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $promotionID);
					}
				} else {
					if ($distanceUnitID != null) {
						$stmt = mysqli_prepare($this->_conn, "INSERT INTO promotions SET name = ?, description = ?, notes = NULL, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = UNHEX(?), service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
						mysqli_stmt_bind_param($stmt, "ssssssssssssss", $name, $description, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $promotionID);
					} else {
						$stmt = mysqli_prepare($this->_conn, "INSERT INTO promotions SET name = ?, description = ?, notes = NULL, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = NULL, service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
						mysqli_stmt_bind_param($stmt, "sssssssssssss", $name, $description, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $promotionID);
					}
				}
				
				if (mysqli_stmt_execute($stmt)) {
					$result = $promotionID;
				}
				
				mysqli_stmt_close($stmt);
			}
			
			return $result;
		}

		public function createPromotionEligibilityAddress($promotionID, $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $countryID, $latitude, $longitude) {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(NEWID()) AS id");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$promotionEligibilityAddressID = $row["id"];
			}
			
			mysqli_free_result($recordSet);
			
			if (isset($promotionEligibilityAddressID)) {
				$stmt = mysqli_prepare($this->_conn, "INSERT INTO promotion_eligibility_addresses SET promotion_id = UNHEX(?), thoroughfare = ?, premise = ?, locality = ?, dependent_locality = ?, administrative_area = ?, postal_code = ?, country_id = UNHEX(?), latitude = ?, longitude = ?, id = UNHEX(?)");
				
				mysqli_stmt_bind_param($stmt, "sssssssssss", $promotionID, $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $countryID, $latitude, $longitude, $promotionEligibilityAddressID);
				
				if (mysqli_stmt_execute($stmt)) {
					$result = $promotionEligibilityAddressID;
				}
				
				mysqli_stmt_close($stmt);
			}
			
			return $result;
		}

		public function linkPromotionToBusiness($promotionID, $businessID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "INSERT INTO business_promotions SET business_id = UNHEX(?), promotion_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", $businessID, $promotionID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function createPromotionCheckinOption($promotionID, $checkinLocation, $checkinHashtag) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "INSERT INTO promotion_checkin_options SET promotion_id = UNHEX(?), location = ?, hashtag = ?");
			
			mysqli_stmt_bind_param($stmt, "sss", $promotionID, $checkinLocation, $checkinHashtag);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function createPromotionPostOption($promotionID, $link) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "INSERT INTO promotion_post_options SET promotion_id = UNHEX(?), link = ?");
			
			mysqli_stmt_bind_param($stmt, "ss", $promotionID, $link);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function createPromotionLikeOption($promotionID, $link) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "INSERT INTO promotion_like_options SET promotion_id = UNHEX(?), link = ?");
			
			mysqli_stmt_bind_param($stmt, "ss", $promotionID, $link);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getRoles($excludedRoles, $userID) {
			$result = array();
			if ($userID != null) {
				if ($excludedRoles != null && count($excludedRoles) > 0) {
					$stmt = mysqli_prepare($this->_conn, "SELECT HEX(roles.id) AS id, roles.name, IF(user_roles.role_id IS NULL, 0, 1) AS selected FROM roles LEFT JOIN user_roles ON roles.id = user_roles.role_id AND user_roles.user_id = UNHEX(?) WHERE roles.name NOT IN ('" . implode("', '", $excludedRoles) . "')");
				} else {
					$stmt = mysqli_prepare($this->_conn, "SELECT HEX(roles.id) AS id, roles.name, IF(user_roles.role_id IS NULL, 0, 1) AS selected FROM roles LEFT JOIN user_roles ON roles.id = user_roles.role_id AND user_roles.user_id = UNHEX(?)");
				}
				mysqli_stmt_bind_param($stmt, "s", $userID);
			} else {
				if ($excludedRoles != null && count($excludedRoles) > 0) {
					$recordSet = mysqli_query($this->_conn, "SELECT HEX(id) AS id, name FROM roles WHERE name NOT IN ('" . implode("', '", $excludedRoles) . "')");
				} else {
					$recordSet = mysqli_query($this->_conn, "SELECT HEX(id) AS id, name FROM roles");
				}
			}
			if (isset($stmt)) {
				if (mysqli_stmt_execute($stmt)) {
					$recordSet = mysqli_stmt_get_result($stmt);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$result[] = $row;
					}
				}
				
				mysqli_stmt_close($stmt);
			} else {
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
				
				mysqli_free_result($recordSet);
			}
			
			return $result;
		}

		public function checkBusinessInManagedBusinesses($businessID, $managerUserID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT user_id FROM business_users WHERE business_id = UNHEX(?) AND user_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", $businessID, $managerUserID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = true;
				}
			}
			
			return $result;
		}

		public function checkUserInManagedBusiness($userID, $managerUserID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT target.user_id FROM business_users AS target LEFT JOIN business_users AS manager ON target.business_id = manager.business_id WHERE target.user_id = UNHEX(?) AND manager.user_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", $userID, $managerUserID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = true;
				}
			}
			
			return $result;
		}

		public function checkBusinessInAccount($businessID, $accountID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT name FROM businesses WHERE id = UNHEX(?) AND account_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", $businessID, $accountID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = true;
				}
			}
			
			return $result;
		}

		public function checkUserInAccount($userID, $accountID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT email FROM users WHERE id = UNHEX(?) AND account_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", $userID, $accountID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = true;
				}
			}
			
			return $result;
		}

		public function getAccountBusinesses($accountID, $userID) {
			$result = array();
			
			if ($userID != null) {
				$stmt = mysqli_prepare($this->_conn, "SELECT HEX(businesses.id) AS id, businesses.name, IF(business_users.business_id IS NULL, 0, 1) AS selected FROM businesses LEFT JOIN business_users ON businesses.id = business_users.business_id AND business_users.user_id = UNHEX(?) WHERE businesses.account_id = UNHEX(?)");
				
				mysqli_stmt_bind_param($stmt, "ss", $userID, $accountID);
			} else {
				$stmt = mysqli_prepare($this->_conn, "SELECT HEX(id) AS id, name FROM businesses WHERE account_id = UNHEX(?)");
				
				mysqli_stmt_bind_param($stmt, "s", $accountID);
			}
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
					
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getUserBusinesses($userID, $selectedUserID) {
			$result = array();
			
			if ($selectedUserID != null) {
				$stmt = mysqli_prepare($this->_conn, "SELECT HEX(businesses.id) AS id, businesses.name, IF(target.business_id IS NULL, 0, 1) AS selected FROM businesses LEFT JOIN business_users ON businesses.id = business_users.business_id LEFT JOIN business_users AS target ON businesses.id = target.business_id AND target.user_id = UNHEX(?) WHERE business_users.user_id = UNHEX(?)");
				
				mysqli_stmt_bind_param($stmt, "ss", $selectedUserID, $userID);
			} else {
				$stmt = mysqli_prepare($this->_conn, "SELECT HEX(businesses.id) AS id, businesses.name FROM businesses LEFT JOIN business_users ON businesses.id = business_users.business_id WHERE business_users.user_id = UNHEX(?)");
				
				mysqli_stmt_bind_param($stmt, "s", $userID);
			}
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
					
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getAccountUsers($accountID, $userID) {
			$result = array();
			
			if ($userID != null) {
				$stmt = mysqli_prepare($this->_conn, "SELECT HEX(users.id) AS id, users.first_name, users.last_name, users.phone, users.email FROM users LEFT JOIN accounts ON users.account_id = accounts.id WHERE users.account_id = UNHEX(?) AND users.id != accounts.primary_user_id AND users.id != UNHEX(?)");
				mysqli_stmt_bind_param($stmt, "ss", $accountID, $userID);
			} else {
				$stmt = mysqli_prepare($this->_conn, "SELECT HEX(users.id) AS id, users.first_name, users.last_name, users.phone, users.email FROM users LEFT JOIN accounts ON users.account_id = accounts.id WHERE users.account_id = UNHEX(?) AND users.id != accounts.primary_user_id");
				mysqli_stmt_bind_param($stmt, "s", $accountID);
			}
			
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
					
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function changeUserPassword($userID, $password) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users SET password = ? WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", password_hash($password, PASSWORD_BCRYPT), $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function changeUserSecurityQuestion($userID, $securityQuestionID, $securityAnswer) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users SET security_question_id = UNHEX(?), security_question_answer = ? WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "sss", $securityQuestionID, password_hash($securityAnswer, PASSWORD_BCRYPT), $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function updateUserProfile($userID, $firstName, $lastName, $birthday, $gender, $phone) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users SET first_name = ?,  last_name = ?, date_of_birth = ?, gender = ?, phone = ? WHERE id = UNHEX(?)");

			mysqli_stmt_bind_param($stmt, "ssssss", $firstName, $lastName, $birthday, $gender, $phone, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getExpiredSessions() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(id) AS id, session_id FROM rank_active_sessions WHERE expiration_time <= NOW()");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getUserSessions($userID) {
			$result = array();
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(id) AS id, session_id FROM rank_active_sessions WHERE user_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getActiveSessions() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(rank_active_sessions.id) AS id, rank_active_sessions.session_id, HEX(rank_active_sessions.user_id) AS user_id, rank_active_sessions.expiration_time, rank_users.first_name, rank_users.last_name, rank_users.email, rank_users.phone, IF(expiration_time <= NOW(), 1, 0) AS pending_delete FROM rank_active_sessions LEFT JOIN users ON rank_active_sessions.user_id = rank_users.id");

			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function untrackSession($sessionTrackingID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "DELETE FROM rank_active_sessions WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "s", $sessionTrackingID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function updateSession($sessionTrackingID, $sessionID, $userID, $expirationSeconds) {
			$result = false;
			
			if ($userID != null) {
				$stmt = mysqli_prepare($this->_conn, "UPDATE rank_active_sessions SET session_id = ?,  expiration_time = DATE_ADD(NOW(), INTERVAL ? SECOND), user_id = UNHEX(?) WHERE id = UNHEX(?)");
			
				mysqli_stmt_bind_param($stmt, "siss", $sessionID, $expirationSeconds, $userID, $sessionTrackingID);
				if (mysqli_stmt_execute($stmt)) {
					$result = true;
				}
				
				mysqli_stmt_close($stmt);
			} else {
				$stmt = mysqli_prepare($this->_conn, "UPDATE rank_active_sessions SET session_id = ?,  expiration_time = DATE_ADD(NOW(), INTERVAL ? SECOND), user_id = NULL WHERE id = UNHEX(?)");
			
				mysqli_stmt_bind_param($stmt, "sis", $sessionID, $expirationSeconds, $sessionTrackingID);
				if (mysqli_stmt_execute($stmt)) {
					$result = true;
				}
				
				mysqli_stmt_close($stmt);
			}
			
			return $result;
		}

		public function trackSession($sessionID, $expirationSeconds) {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(NEWID()) AS id");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$sessionTrackingID = $row["id"];
			}
			
			mysqli_free_result($recordSet);
			
			if (isset($sessionTrackingID)) {
				$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_active_sessions SET id = UNHEX(?), session_id = ?,  expiration_time = DATE_ADD(NOW(), INTERVAL ? SECOND)");
			
				mysqli_stmt_bind_param($stmt, "ssi", $sessionTrackingID, $sessionID, $expirationSeconds);
				if (mysqli_stmt_execute($stmt)) {
					$result = $sessionTrackingID;
				}
				
				mysqli_stmt_close($stmt);
			}
			return $result;
		}

		public function getAccountPrimaryUser($accountID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(accounts.primary_user_id) AS id, users.first_name, users.last_name, users.email FROM accounts LEFT JOIN users ON accounts.primary_user_id = users.id WHERE accounts.id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "s", $accountID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getAccountPrimaryBusiness($accountID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(businesses.id) AS id, businesses.name, businesses.website, business_addresses.thoroughfare, business_addresses.premise, business_addresses.locality, business_addresses.dependent_locality, business_addresses.administrative_area, business_addresses.postal_code, HEX(business_addresses.country_id) AS country_id, countries.name AS country_name FROM accounts LEFT JOIN businesses ON accounts.primary_business_id = businesses.id LEFT JOIN business_addresses ON businesses.primary_address_id = business_addresses.id LEFT JOIN countries ON business_addresses.country_id = countries.id WHERE accounts.id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "s", $accountID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function resetUserPassword($email, $password) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users SET password = ?, reset_key = NULL, reset_time = NULL WHERE email = ?");
			
			mysqli_stmt_bind_param($stmt, "ss", password_hash($password, PASSWORD_BCRYPT), $email);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function checkPasswordResetKeyValidity($resetPasswordKey) {
			$result  = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT email FROM rank_users WHERE reset_key = UNHEX(?) AND DATE_ADD(reset_time, INTERVAL " . PASSWORD_RESET_MAX_INTERVAL_MINUTES . " MINUTE) >= NOW()");
			
			mysqli_stmt_bind_param($stmt, "s", $resetPasswordKey);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
			
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = $row["email"];
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function checkUserIsLocked($userInfo) {
			$result = array();
			
			$result["locked"] = false;
			$result["modification_reason"] = "";
			
			if (strlen($userInfo) == 32 && ctype_xdigit($userInfo)) {
				$userID = $userInfo;
			} else {
				$stmt = mysqli_prepare($this->_conn, "SELECT HEX(id) AS id FROM rank_users WHERE email = ?");
				
				mysqli_stmt_bind_param($stmt, "s", $userInfo);
				mysqli_stmt_execute($stmt);
				
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$userID = $row["id"];
				}
				
				mysqli_stmt_close($stmt);
			}
			
			if (isset($userID)) {
				$stmt = mysqli_prepare($this->_conn, "SELECT IF((status & " . STATUS_LOCKED . ") = " . STATUS_LOCKED . ", 1, 0) AS locked FROM rank_user_statuses WHERE user_id = UNHEX(?) ORDER BY time DESC LIMIT 1");
				
				mysqli_stmt_bind_param($stmt, "s", $userID);
				mysqli_stmt_execute($stmt);
				
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result["locked"] = $row["locked"] == "1";
				}
				
				mysqli_stmt_close($stmt);
			}
			
			if ($result["locked"]) {
				$stmt = mysqli_prepare($this->_conn, "SELECT time FROM rank_user_statuses WHERE user_id = UNHEX(?) AND (status & " . STATUS_LOCKED . ") != " . STATUS_LOCKED . " ORDER BY time DESC LIMIT 1");
				
				mysqli_stmt_bind_param($stmt, "s", $userID);
				mysqli_stmt_execute($stmt);
				
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$time = $row["time"];
				}
				
				mysqli_stmt_close($stmt);
				
				if (isset($time)) {
					$stmt = mysqli_prepare($this->_conn, "SELECT rank_user_statuses.modification_reason, rank_users.first_name, rank_users.last_name FROM rank_user_statuses LEFT JOIN rank_users ON rank_user_statuses.modifier_id = rank_users.id WHERE rank_user_statuses.user_id = UNHEX(?) AND rank_user_statuses.time > '" . $time . "' ORDER BY rank_user_statuses.time ASC LIMIT 1");
					
					mysqli_stmt_bind_param($stmt, "s", $userID);
					mysqli_stmt_execute($stmt);
					
					$recordSet = mysqli_stmt_get_result($stmt);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$result["modification_reason"] = $row["modification_reason"];
						if ($row["first_name"] != null) {
							$result["modification_reason"] .= " by " . $row["first_name"] . " " . $row["last_name"];
						}
					}
					
					mysqli_stmt_close($stmt);
				} else {
					$result["modification_reason"] = "Unknown";
				}
			} else {
				$stmt = mysqli_prepare($this->_conn, "SELECT time FROM rank_user_statuses WHERE user_id = UNHEX(?) AND (status & " . STATUS_LOCKED . ") = " . STATUS_LOCKED . " ORDER BY time DESC LIMIT 1");
				
				mysqli_stmt_bind_param($stmt, "s", $userID);
				mysqli_stmt_execute($stmt);
				
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$time = $row["time"];
				}
				
				mysqli_stmt_close($stmt);
				
				if (isset($time)) {
					$stmt = mysqli_prepare($this->_conn, "SELECT rank_user_statuses.modification_reason, rank_users.first_name, rank_users.last_name FROM rank_user_statuses LEFT JOIN rank_users ON rank_user_statuses.modifier_id = rank_users.id WHERE rank_user_statuses.user_id = UNHEX(?) AND rank_user_statuses.time > '" . $time . "' ORDER BY rank_user_statuses.time ASC LIMIT 1");
					
					mysqli_stmt_bind_param($stmt, "s", $userID);
					mysqli_stmt_execute($stmt);
					
					$recordSet = mysqli_stmt_get_result($stmt);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$result["modification_reason"] = $row["modification_reason"];
						if ($row["first_name"] != null) {
							$result["modification_reason"] .= " by " . $row["first_name"] . " " . $row["last_name"];
						}
					}
					
					mysqli_stmt_close($stmt);
				} else {
					$result["modification_reason"] = "New User Created";
				}
			}
			
			return $result;
		}

		public function unlockUser($userInfo, $reason, $modifierID) {
			$result = false;
			
			if (strlen($userInfo) == 32 && ctype_xdigit($userInfo)) {
				$userID = $userInfo;
			} else {
				$stmt = mysqli_prepare($this->_conn, "SELECT HEX(id) AS id FROM rank_users WHERE email = ?");
				
				mysqli_stmt_bind_param($stmt, "s", $userInfo);
				mysqli_stmt_execute($stmt);
				
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$userID = $row["id"];
				}
				
				mysqli_stmt_close($stmt);
			}
			
			if (isset($userID)) {
				$stmt = mysqli_prepare($this->_conn, "SELECT status FROM rank_user_statuses WHERE user_id = UNHEX(?) ORDER BY time DESC LIMIT 1");
			
				mysqli_stmt_bind_param($stmt, "s", $userID);
				mysqli_stmt_execute($stmt);
				
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$status = $row["status"];
				}
				
				mysqli_stmt_close($stmt);
				
				if (isset($status)) {
					if ($modifierID != null) {
						$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_user_statuses SET user_id = UNHEX(?), status = " . $status . " & " . getRemoveStatusMask(STATUS_LOCKED) . ", modification_reason = ?, modifier_id = UNHEX(?)");
						
						mysqli_stmt_bind_param($stmt, "sss", $userID, $reason, $modifierID);
					} else {
						$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_user_statuses SET user_id = UNHEX(?), status = " . $status . " & " . getRemoveStatusMask(STATUS_LOCKED) . ", modification_reason = ?");
						
						mysqli_stmt_bind_param($stmt, "ss", $userID, $reason);
					}
					if (mysqli_stmt_execute($stmt)) {
						$result = true;
					}
					
					mysqli_stmt_close($stmt);
				}
			}
			
			return $result;
		}

		public function lockUser($userInfo, $reason, $modifierID) {
			$result = false;
			
			if (strlen($userInfo) == 32 && ctype_xdigit($userInfo)) {
				$userID = $userInfo;
			} else {
				$stmt = mysqli_prepare($this->_conn, "SELECT HEX(id) AS id FROM rank_users WHERE email = ?");
				
				mysqli_stmt_bind_param($stmt, "s", $userInfo);
				mysqli_stmt_execute($stmt);
				
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$userID = $row["id"];
				}
				
				mysqli_stmt_close($stmt);
			}
			
			if (isset($userID)) {
				$stmt = mysqli_prepare($this->_conn, "SELECT status FROM rank_user_statuses WHERE user_id = UNHEX(?) ORDER BY time DESC LIMIT 1");
			
				mysqli_stmt_bind_param($stmt, "s", $userID);
				mysqli_stmt_execute($stmt);
				
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$status = $row["status"];
				}
				
				mysqli_stmt_close($stmt);
				
				if (isset($status)) {
					if ($modifierID != null) {
						$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_user_statuses SET user_id = UNHEX(?), status = " . $status . " | " . STATUS_LOCKED . ", modification_reason = ?, modifier_id = UNHEX(?)");
						
						mysqli_stmt_bind_param($stmt, "sss", $userID, $reason, $modifierID);
					} else {
						$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_user_statuses SET user_id = UNHEX(?), status = " . $status . " | " . STATUS_LOCKED . ", modification_reason = ?");
						
						mysqli_stmt_bind_param($stmt, "ss", $userID, $reason);
					}
					if (mysqli_stmt_execute($stmt)) {
						$result = true;
					}
					
					mysqli_stmt_close($stmt);
				}
			}
			
			return $result;
		}

		public function generateResetKey($email) {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(NEWID()) AS id");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$resetKey = $row["id"];
			}
			
			mysqli_free_result($recordSet);
			
			if (isset($resetKey)) {
				$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users SET reset_key = UNHEX(?), reset_time = NOW() WHERE email = ?");
				
				mysqli_stmt_bind_param($stmt, "ss", $resetKey, $email);
				if (mysqli_stmt_execute($stmt)) {
					$result = $resetKey;
				}
				mysqli_stmt_close($stmt);
			}
			
			return $result;
		}

		public function checkSecurityQuestionAnswer($email, $questionID, $answer) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT security_question_answer FROM rank_users WHERE email = ? AND security_question_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", $email, $questionID);
			mysqli_stmt_execute($stmt);
			
			$recordSet = mysqli_stmt_get_result($stmt);
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$answerHash = $row["security_question_answer"];
			}
			
			mysqli_stmt_close($stmt);
			
			if (isset($answerHash)) {
				return password_verify($answer, $answerHash);
			}
			
			return $result;
		}

		public function getSecurityQuestion($email, $phone) {
			$result  = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(security_questions.id) AS id, security_questions.question FROM rank_users LEFT JOIN security_questions ON rank_users.security_question_id = security_questions.id WHERE rank_users.email = ? AND rank_users.phone = ? AND rank_users.email_verified = '1' AND rank_users.security_question_id IS NOT NULL");
			
			mysqli_stmt_bind_param($stmt, "ss", $email, $phone);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
			
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function deleteAccount($accountID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "DELETE FROM accounts WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "s", $accountID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function deleteBusiness($businessID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "DELETE FROM businesses WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "s", $businessID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function deleteBusinessAddress($businessAddressID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "DELETE FROM business_addresses WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "s", $businessAddressID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function deleteUser($userID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "DELETE FROM rank_users WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function linkUserToBusiness($userID, $businessID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "INSERT INTO business_users SET business_id = UNHEX(?), user_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", $businessID, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function linkAccountToBusiness($accountID, $businessID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE businesses SET account_id = UNHEX(?) WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", $accountID, $businessID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function linkAccountToUser($accountID, $userID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE users SET account_id = UNHEX(?) WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", $accountID, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function unlinkBusinessCategories($businessID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "DELETE FROM business_categories WHERE business_ID = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "s", $businessID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function linkCategoryToBusiness($categoryID, $businessID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "INSERT INTO business_categories SET business_id = UNHEX(?), category_id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", $businessID, $categoryID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function linkBusinessToAddress($businessID, $addressID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE business_addresses SET business_id = UNHEX(?) WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ss", $businessID, $addressID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function giveUserRole($userID, $role) {
			$result = false;
			
			if (ctype_xdigit($role) && strlen($role) == 32) {
				// Is a roleID
				$stmt = mysqli_prepare($this->_conn, "INSERT INTO user_roles SET user_id = UNHEX(?), role_id = UNHEX(?)");
				
				mysqli_stmt_bind_param($stmt, "ss", $userID, $role);
			} else {
				$stmt = mysqli_prepare($this->_conn, "INSERT INTO user_roles SET user_id = UNHEX(?), role_id = (SELECT id FROM roles WHERE name = ?)");
				
				mysqli_stmt_bind_param($stmt, "ss", $userID, $role);
			}
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function createAccount($comments, $primaryUserID, $primaryBusinessID) {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(NEWID()) AS id");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$accountID = $row["id"];
			}
			
			mysqli_free_result($recordSet);
			
			if (isset($accountID)) {
				$stmt = mysqli_prepare($this->_conn, "INSERT INTO accounts SET comments = ?, primary_user_id = UNHEX(?), primary_business_id = UNHEX(?), id = UNHEX(?)");
				
				mysqli_stmt_bind_param($stmt, "ssss", $comments, $primaryUserID, $primaryBusinessID, $accountID);
				if (mysqli_stmt_execute($stmt)) {
					$result = $accountID;
				}
				mysqli_stmt_close($stmt);
			}
			
			return $result;
		}

		public function createBusiness($name, $website, $primaryAddressID) {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(NEWID()) AS id");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$businessID = $row["id"];
			}
			
			mysqli_free_result($recordSet);
			
			if (isset($businessID)) {
				$stmt = mysqli_prepare($this->_conn, "INSERT INTO businesses SET name = ?, website = ?, primary_address_id = UNHEX(?), id = UNHEX(?)");
				
				mysqli_stmt_bind_param($stmt, "ssss", $name, $website, $primaryAddressID, $businessID);
				if (mysqli_stmt_execute($stmt)) {
					$result = $businessID;
				}
				mysqli_stmt_close($stmt);
			}
			
			return $result;
		}

		public function updateBusiness($businessID, $name, $website) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE businesses SET name = ?, website = ? WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "sss", $name, $website, $businessID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function createBusinessAddress($thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $country, $latitude, $longitude) {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(NEWID()) AS id");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$businessAddressID = $row["id"];
			}
			
			mysqli_free_result($recordSet);
			
			if (isset($businessAddressID)) {
				$stmt = mysqli_prepare($this->_conn, "INSERT INTO business_addresses SET thoroughfare = ?, premise = ?, locality = ?, dependent_locality = ?, administrative_area = ?, postal_code = ?, country_id = UNHEX(?), latitude = ?, longitude = ?, id = UNHEX(?)");
				
				mysqli_stmt_bind_param($stmt, "ssssssssss", $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $country, $latitude, $longitude, $businessAddressID);
				if (mysqli_stmt_execute($stmt)) {
					$result = $businessAddressID;
				}
				mysqli_stmt_close($stmt);
			}
			
			return $result;
		}

		public function updateBusinessAddress($businessAddressID, $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $country, $latitude, $longitude) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE business_addresses SET thoroughfare = ?, premise = ?, locality = ?, dependent_locality = ?, administrative_area = ?, postal_code = ?, country_id = UNHEX(?), latitude = ?, longitude = ? WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "ssssssssss", $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $country, $latitude, $longitude, $businessAddressID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function createUser($first_name, $last_name, $birthday, $gender, $phone, $email, $password, $securityQuestionID, $securityAnswer) {
			$result = null;
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(NEWID()) AS id");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$userID = $row["id"];
                                break;
			}
			
			mysqli_free_result($recordSet);
			
			if (isset($userID)) {
				$stmt=null;
				if ($securityQuestionID != null) {
					$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_users SET first_name = ?, last_name = ?, date_of_birth = ?, gender = ?, phone = ?, email = ?, password = ?, security_question_id = UNHEX(?), security_question_answer = ?, id = UNHEX(?), api_key = ''");
				
					mysqli_stmt_bind_param($stmt, "ssssssssss", $first_name, $last_name, $birthday, $gender, $phone, $email, password_hash($password, PASSWORD_BCRYPT), $securityQuestionID, password_hash($securityAnswer, PASSWORD_BCRYPT), $userID);
				} else {
					$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_users SET first_name = ?, last_name = ?, date_of_birth = ?, gender = ?, phone = ?, email = ?, password = ?, id = UNHEX(?), api_key = ''");
				
					mysqli_stmt_bind_param($stmt, "ssssssss", $first_name, $last_name, $birthday, $gender, $phone, $email, password_hash($password, PASSWORD_BCRYPT), $userID);
				}
				if (mysqli_stmt_execute($stmt)) {
					mysqli_stmt_close($stmt);
					$stmt2 = mysqli_prepare($this->_conn, "INSERT INTO rank_user_statuses SET user_id = UNHEX(?), status = 0, modification_reason = 'New User'");
					mysqli_stmt_bind_param($stmt2, "s", $userID);
					
					if (mysqli_stmt_execute($stmt2)) {
						mysqli_stmt_close($stmt2);
						$stmt3 = mysqli_prepare($this->_conn, "INSERT INTO rank_user_settings SET user_id = UNHEX(?)");
						mysqli_stmt_bind_param($stmt3, "s", $userID);
						
						if (mysqli_stmt_execute($stmt3)) {
							mysqli_stmt_close($stmt3);
							$stmt4 = mysqli_prepare($this->_conn, "INSERT INTO rank_user_locations SET user_id = UNHEX(?), latitude = 0, longitude = 0");
							mysqli_stmt_bind_param($stmt4, "s", $userID);
							
							if (mysqli_stmt_execute($stmt4)) {
								$result = $userID;
							}  else die(mysqli_error($this->_conn));
							
							mysqli_stmt_close($stmt4);
						}  else die(mysqli_error($this->_conn));
						
					}  else die(mysqli_error($this->_conn));
					
				} else die(mysqli_error($this->_conn));
			}
			
			return $result;
		}

		public function verifyEmail($userID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users SET email_verified = 1 WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function logFailedElevation($userID) {
			mysqli_query($this->_conn, "INSERT INTO rank_user_elevation_attempts SET user_id = UNHEX('" . $userID . "'), ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0), success = 0");
		}

		public function logSuccessfulElevation($userID) {
			mysqli_query($this->_conn, "INSERT INTO rank_user_elevation_attempts SET user_id = UNHEX('" . $userID . "'), ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0), success = 1");
		}

		public function getElevationFailureCount($userID) {
			$result = 0;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(rank_user_elevation_attempts.success) AS failure_count FROM rank_user_elevation_attempts LEFT JOIN (SELECT time FROM rank_user_elevation_attempts WHERE user_id = UNHEX(?) AND success = 1 ORDER BY time DESC LIMIT 1) AS last_success ON 1 = 1 WHERE rank_user_elevation_attempts.user_id = UNHEX(?) AND rank_user_elevation_attempts.time > IFNULL(last_success.time, '1970-01-01 00:00:00') GROUP BY success");
			
			mysqli_stmt_bind_param($stmt, "ss", $userID, $userID);
			mysqli_stmt_execute($stmt);
			
			$recordSet = mysqli_stmt_get_result($stmt);
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result = $row["failure_count"];
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function logFailedLogin($username) {
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(id) AS id FROM rank_users WHERE email = ?");
			
			mysqli_stmt_bind_param($stmt, "s", $username);
			mysqli_stmt_execute($stmt);
			
			$recordSet = mysqli_stmt_get_result($stmt);
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$userID = $row["id"];
			}
			
			mysqli_stmt_close($stmt);
			
			if (isset($userID)) {
				mysqli_query($this->_conn, "INSERT INTO rank_user_login_attempts SET user_id = UNHEX('" . $userID . "'), ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0), success = 0");
			}
			mysqli_query($this->_conn, "INSERT INTO rank_ip_address_login_attempts SET ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0), success = 0");
		}

		public function logSuccessfulLogin($username) {
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(id) AS id FROM rank_users WHERE email = ?");
			
			mysqli_stmt_bind_param($stmt, "s", $username);
			mysqli_stmt_execute($stmt);
			
			$recordSet = mysqli_stmt_get_result($stmt);
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$userID = $row["id"];
			}
			
			mysqli_stmt_close($stmt);
			
			mysqli_query($this->_conn, "INSERT INTO rank_user_login_attempts SET user_id = UNHEX('" . $userID . "'), ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0), success = 1");
			mysqli_query($this->_conn, "INSERT INTO rank_ip_address_login_attempts SET ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0), success = 1");
		}

		public function loadSessionUserInformation($username) {
			$result = false;
			
			$_SESSION["userEmail"] = $username;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(rank_users.id) AS id, rank_users.first_name, rank_users.last_name, rank_users.phone, rank_users.date_of_birth, rank_users.gender, HEX(rank_users.api_key) AS api_key, rank_user_settings.profile_picture, rank_user_settings.email_flags, rank_user_settings.privacy_flags, rank_users_facebook.unique_id AS facebook_unique_id, rank_users_facebook.access_token AS facebook_access_token, rank_users_facebook.access_token_expire AS facebook_access_token_expiration, rank_users_linkedin.unique_id AS linkedin_unique_id, rank_users_linkedin.oauth_token AS linkedin_oauth_token, rank_users_linkedin.oauth_token_expire AS linkedin_oauth_token_expiration, rank_users_twitter.unique_id AS twitter_unique_id, rank_users_twitter.oauth_token AS twitter_oauth_token, rank_users_twitter.oauth_token_secret AS twitter_oauth_token_secret, rank_users_twitter.profile_picture_url AS twitter_profile_picture_url FROM rank_users LEFT JOIN rank_user_settings on rank_users.id = rank_user_settings.user_id LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id WHERE rank_users.email = ?");
			
			mysqli_stmt_bind_param($stmt, "s", $username);
			mysqli_stmt_execute($stmt);
			
			$recordSet = mysqli_stmt_get_result($stmt);
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result = true;
				
				$_SESSION["userID"] = $row["id"];
				$_SESSION["userFirstName"] = $row["first_name"];
				$_SESSION["userLastName"] = $row["last_name"];
				$_SESSION["userPhone"] = $row["phone"];
				$_SESSION["userDateOfBirth"] = $row["date_of_birth"];
				$_SESSION["userGender"] = $row["gender"];
				$_SESSION["userAPIKey"] = $row["api_key"];
				$_SESSION["userProfilePicture"] = $row["profile_picture"];
				$_SESSION["userEmailFlags"] = $row["email_flags"];
				$_SESSION["userPrivacyFlags"] = $row["privacy_flags"];
				
				$_SESSION["facebookUniqueID"] = $row["facebook_unique_id"];
				$_SESSION["facebookAccessToken"] = $row["facebook_access_token"];
				$_SESSION["facebookAccessTokenExpiration"] = ($row["facebook_access_token_expiration"] != null ? strtotime($row["facebook_access_token_expiration"] . " UTC") : null);
				$_SESSION["linkedInUniqueID"] = $row["linkedin_unique_id"];
				$_SESSION["linkedInOAuthToken"] = $row["linkedin_oauth_token"];
				$_SESSION["linkedInOAuthTokenExpiration"] = ($row["linkedin_oauth_token_expiration"] != null ? strtotime($row["linkedin_oauth_token_expiration"] . " UTC") : null);
				$_SESSION["twitterUniqueID"] = $row["twitter_unique_id"];
				$_SESSION["twitterOAuthToken"] = $row["twitter_oauth_token"];
				$_SESSION["twitterOAuthTokenSecret"] = $row["twitter_oauth_token_secret"];
				$_SESSION["twitterProfilePictureURL"] = $row["twitter_profile_picture_url"];
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function checkUserPassword($username, $password) {
			$stmt = mysqli_prepare($this->_conn, "SELECT password FROM rank_users WHERE email = ?");
			
			mysqli_stmt_bind_param($stmt, "s", $username);
			mysqli_stmt_execute($stmt);
			
			$recordSet = mysqli_stmt_get_result($stmt);
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$passwordHash = $row["password"];
			}
			
			mysqli_stmt_close($stmt);
			
			if (!isset($passwordHash)) {
				return false;
			} else {
				return password_verify($password, $passwordHash);
			}
		}

		public function getLoginFailureCountByRemoteIP() {
			$result = 0;
			
			$recordSet = mysqli_query($this->_conn, "SELECT COUNT(rank_ip_address_login_attempts.success) AS failure_count FROM rank_ip_address_login_attempts LEFT JOIN (SELECT time FROM rank_ip_address_login_attempts WHERE ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0) AND success = 1 ORDER BY time DESC LIMIT 1) AS last_success ON 1 = 1 WHERE rank_ip_address_login_attempts.ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0) AND rank_ip_address_login_attempts.time > IFNULL(last_success.time, '1970-01-01 00:00:00') GROUP BY success");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result = $row["failure_count"];
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getLoginFailureCountByUsername($username) {
			$result = 0;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(rank_user_login_attempts.success) AS failure_count FROM rank_user_login_attempts LEFT JOIN (SELECT rank_users.id AS user_id, rank_user_login_attempts.time FROM rank_users LEFT JOIN rank_user_login_attempts ON rank_users.id = rank_user_login_attempts.user_id WHERE rank_users.email = ? AND rank_user_login_attempts.success = 1 ORDER BY rank_user_login_attempts.time DESC LIMIT 1) AS last_success ON 1 = 1 WHERE rank_user_login_attempts.user_id = last_success.user_id AND rank_user_login_attempts.time > IFNULL(last_success.time, '1970-01-01 00:00:00') GROUP BY success");
			
			mysqli_stmt_bind_param($stmt, "s", $username);
			mysqli_stmt_execute($stmt);
			
			$recordSet = mysqli_stmt_get_result($stmt);
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result = $row["failure_count"];
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getBusinessAddresses($businessID) {
			$result = array();
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(business_addresses.id) AS id, business_addresses.thoroughfare, business_addresses.premise, business_addresses.locality, business_addresses.dependent_locality, business_addresses.administrative_area, business_addresses.postal_code, HEX(business_addresses.country_id) AS country_id, countries.name AS country_name FROM business_addresses LEFT JOIN businesses ON business_addresses.business_id = businesses.id WHERE business_addresses.businesses_id = UNHEX(?) AND business_addresses.id != businesses.primary_address_id");
			
			mysqli_stmt_bind_param($stmt, "s", $businessID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getBusiness($businessID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT HEX(businesses.id) AS id, businesses.name, businesses.website, HEX(businesses.primary_address_id) AS primary_address_id, business_addresses.thoroughfare, business_addresses.premise, business_addresses.locality, business_addresses.dependent_locality, business_addresses.administrative_area, business_addresses.postal_code, HEX(business_addresses.country_id) AS country_id, countries.name AS country_name FROM businesses LEFT JOIN business_addresses ON businesses.primary_address_id = business_addresses.id LEFT JOIN countries ON business_addresses.country_id = countries.id WHERE businesses.id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "s", $businessID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getUser($userID) {
			$result = null;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT first_name, last_name, phone, email FROM rank_users WHERE id = UNHEX(?)");
			
			mysqli_stmt_bind_param($stmt, "s", $userID);
			
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result = $row;
				}
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function getBusinessCategories($businessID) {
			$result = array();
			
			if ($businessID != null) {
				$stmt = mysqli_prepare($this->_conn, "SELECT HEX(categories.id) AS id, categories.name, IF(business_categories.business_id IS NULL, 0, 1) AS selected FROM categories LEFT JOIN business_categories ON categories.id = business_categories.category_id AND business_categories.business_id = UNHEX(?) ORDER BY categories.name");
				
				mysqli_stmt_bind_param($stmt, "s", $businessID);
				if (mysqli_stmt_execute($stmt)) {
					$recordSet = mysqli_stmt_get_result($stmt);
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$result[] = $row;
					}
				}
				
				mysqli_stmt_close($stmt);
			} else {
				$recordSet = mysqli_query($this->_conn, "SELECT HEX(id) AS id, name FROM categories ORDER BY name");
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$result[] = $row;
				}
				
				mysqli_free_result($recordSet);
			}
			
			return $result;
		}

		public function getSecurityQuestions() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(id) AS id, question FROM security_questions ORDER BY question");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getCurrencies() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(id) AS id, name, abbreviation FROM currencies ORDER BY name");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getCountries() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(id) AS id, name, code FROM countries ORDER BY name");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getDistanceUnits() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(id) AS id, name FROM distance_units ORDER BY name");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getAllServices() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(id) AS id, name FROM services ORDER BY name");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getCheckinServices() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(services.id) AS id, services.name FROM features INNER JOIN service_features ON features.id = service_features.feature_id LEFT JOIN services ON service_features.service_id = services.id WHERE features.name = 'checkin' ORDER BY services.name");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getPostServices() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(services.id) AS id, services.name FROM features INNER JOIN service_features ON features.id = service_features.feature_id LEFT JOIN services ON service_features.service_id = services.id WHERE features.name = 'post' ORDER BY services.name");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function getLikeServices() {
			$result = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT HEX(services.id) AS id, services.name FROM features INNER JOIN service_features ON features.id = service_features.feature_id LEFT JOIN services ON service_features.service_id = services.id WHERE features.name = 'like' ORDER BY services.name");
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
			
			mysqli_free_result($recordSet);
			
			return $result;
		}

		public function computeGlobalParticipants() {
			$globalParticipants = 0;
			$recordSet = mysqli_query($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users");
			while ($row = mysqli_fetch_array($recordSet)) {
				$globalParticipants = $row["participants"];
			}
			mysqli_free_result($recordSet);
			
			$prevParticipants = array();
			$prevModifiedTimes = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT participants, modified_time FROM rank_global_participants ORDER BY modified_time DESC LIMIT 2");
			while ($row = mysqli_fetch_array($recordSet)) {
				$prevParticipants[] = $row["participants"];
				$prevModifiedTimes[] = $row["modified_time"];
			}
			mysqli_free_result($recordSet);
			
			if (count($prevParticipants) == 2 && $prevParticipants[0] == $globalParticipants && $prevParticipants[1] == $prevParticipants[0]) {
				$stmt = mysqli_prepare($this->_conn, "UPDATE rank_global_participants SET modified_time = NOW() WHERE participants = ? AND modified_time = ?");
				mysqli_stmt_bind_param($stmt, "is", $prevParticipants[0], $prevModifiedTimes[0]);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_close($stmt);
			} else {
				$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_global_participants SET participants = ?");
				mysqli_stmt_bind_param($stmt, "i", $globalParticipants);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_close($stmt);
			}
		}

		public function rankGlobal($userID) {
			$globalRank = 0;
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users LEFT JOIN rank_users AS target_user ON target_user.id = UNHEX(?) WHERE rank_users.meta_score > target_user.meta_score");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$globalRank = $row["rank"];
				}
			}
			mysqli_stmt_close($stmt);
			
			$stmt = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id FROM rank_users LEFT JOIN rank_users AS target_user ON target_user.id = UNHEX(?) WHERE rank_users.meta_score = target_user.meta_score ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "ss", $userID, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$globalRank += $row["tie_breaker"];
				}
			}
			mysqli_stmt_close($stmt);
			
			$stmt = mysqli_prepare($this->_conn, "SELECT rank, modified_time FROM rank_global WHERE user_id = UNHEX(?) ORDER BY modified_time DESC LIMIT 2");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				$prevRanks = array();
				$prevModifiedTimes = array();
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$prevRanks[] = $row["rank"];
					$prevModifiedTimes[] = $row["modified_time"];
				}
				
				if (count($prevRanks) == 2 && $prevRanks[0] == $globalRank && $prevRanks[1] == $prevRanks[0]) {
					$stmt2 = mysqli_prepare($this->_conn, "UPDATE rank_global SET modified_time = NOW() WHERE user_id = UNHEX(?) AND modified_time = ?");
					mysqli_stmt_bind_param($stmt2, "ss", $userID, $prevModifiedTimes[0]);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				} else {
					$stmt2 = mysqli_prepare($this->_conn, "INSERT INTO rank_global SET user_id = UNHEX(?), rank = ?");
					mysqli_stmt_bind_param($stmt2, "ss", $userID, $globalRank);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				}
			}
			mysqli_stmt_close($stmt);
		}
		
		public function scoreMetaRankBonus($userID) {
			$result = false;
			
			$stmt = mysqli_prepare($this->_conn, "SELECT ((IFNULL(rank_users.email_verified, 0) * 420) + IFNULL(countries.score, 0) + IFNULL(rank_bonus_administrative_area.score, 0) + IFNULL(rank_bonus_postal_code.score, 0)) * 0.10 AS score FROM rank_users LEFT JOIN rank_user_locations ON rank_users.id = rank_user_locations.user_id LEFT JOIN countries ON rank_user_locations.country_id = countries.id LEFT JOIN rank_bonus_administrative_area ON countries.id = rank_bonus_administrative_area.country_id AND rank_user_locations.administrative_area = rank_bonus_administrative_area.administrative_area LEFT JOIN rank_bonus_postal_code ON countries.id = rank_bonus_postal_code.country_id AND rank_user_locations.postal_code = rank_bonus_postal_code.postal_code WHERE rank_users.id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				$score = 0.0;
				while ($row = mysqli_fetch_array($recordSet)) {
					$score = doubleval($row["score"]);
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "UPDATE rank_users SET bonus_score = ? WHERE id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt2, "ds", $score, $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$result = true;
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}

		public function scoreFacebook($userID, $accessToken) {			
			$my_comment_rv = 0.500;
			$my_comment_dr = 0.02;
			$my_comment_du = 1;
			$my_comment_fv = 0.005;
			$my_comment_bump = 0.85;
			$my_pic_rv = 0.700;
			$my_pic_dr = 0.01;
			$my_pic_du = 2;
			$my_pic_fv = 0.010;
			$my_pic_bump = 0.85;
			$my_checkin_rv = 3.000;
			$my_checkin_dr = 0.01;
			$my_checkin_du = 2;
			$my_checkin_fv = 0.500;
			$my_checkin_bump = 0.90;
			$my_video_rv = 1.500;
			$my_video_dr = 0.01;
			$my_video_du = 7;
			$my_video_fv = 0.500;
			$my_video_bump = 0.85;
			$tagged_checkin_rv = 3.500;
			$tagged_checkin_dr = 0.01;
			$tagged_checkin_du = 7;
			$tagged_checkin_fv = 0.600;
			$tagged_checkin_bump = 0.85;
			$tagged_photo_rv = 2.000;
			$tagged_photo_dr = 0.01;
			$tagged_photo_du = 7;
			$tagged_photo_fv = 0.500;
			$tagged_photo_bump = 0.85;
			$tagged_video_rv = 0.000;
			$tagged_video_dr = 0.00;
			$tagged_video_du = 0;
			$tagged_video_fv = 0.000;
			$tagged_video_bump = 0.85;

			$end_time = strtotime("-180 day");

			$limit = 999;
			$offset = 0;
			$score = 0;
			$last_time = strtotime("now");
			$my_link_post = 0;
			$my_video_post = 0;
			$my_photo_post = 0;
			$my_checkin_post = 0;
			$my_status_post = 0;
			$my_sub_comment_post = 0;
			$receive_comment = 0;
			$receive_sub_comment = 0;
			$receive_like_comment = 0;
			$receive_like_checkin = 0;
			$receive_like_video = 0;
			$receive_like_photo = 0;
			$tagged_checkin = 0;
			$tagged_photo = 0;
			$tagged_video = 0;
			$uniques = array();
			
			$ending_date = date('Y-m-d',(strtotime ( '-180 day' , $last_time ) ));
			$ending_date = strtotime($ending_date);
			
			$graph_url = "https://graph.facebook.com/me/feed?since=".$ending_date."&until=".$last_time."&limit=".$limit."&offset=".$offset."&date_format=U&access_token=".$accessToken;
			//$graph_url = "https://graph.facebook.com/".$userID."/feed?access_token=".$accessToken.'<br/>'; limit="'.$limit.'"&

			while($end_time < $last_time)
			{
				if ($graph_url=="" || !$graph_url)
				{
					break;
				}
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $graph_url);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
				curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
				curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/lib/fb/src/Facebook/HttpClients/certs/DigiCertHighAssuranceEVRootCA.pem');
				curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
				$response = curl_exec($ch);
				curl_close($ch);
				$feed = json_decode($response);
				$nr =  count(@$feed->data);
				
				if (isset($feed->error->message))
				{
					return false;
				}
				
				for ($i = 0; $i < $nr; $i++)
				{
					$from_id = $feed->data[$i]->from->id;
					$type = $feed->data[$i]->type;
					//$type= "status";
					@$uniques[$from_id]++;
					
					if (isset($feed->data[$i]->story_tags)) {
						foreach ( @$feed->data[$i]->story_tags as $key => $value)
						{
							@$uniques[$value[0]->id]++;
							if ($value[0]->id!=$from_id)
							{
								$my_sub_comment_post++;
							}
						}
					}
					
					if (isset($feed->data[$i]->likes) && isset($feed->data[$i]->likes->data)) {
						foreach ( @$feed->data[$i]->likes->data as $key => $value)
						{
							$id = $value->id;
							@$uniques[$id]++;
						}
					}
					
					if (isset($feed->data[$i]->comments) && isset($feed->data[$i]->comments->data)) {
						foreach ( @$feed->data[$i]->comments->data as $key => $value)
						{
							if (isset($value->from->id) )
							{
							//print_r($value);
								$id = $value->from->id;
								@$uniques[$id]++;
							}
						}	
					}
					
					if ($type=="link")
					{
						$my_link_post++;
						$receive_like_comment += (isset($feed->data[$i]->likes) ? count($feed->data[$i]->likes->data) : 0);
						$age = time() - $feed->data[$i]->created_time;
						$decay = ($age / (86400 * $my_comment_du)) * $my_comment_dr;
						$adj_score = ($my_comment_rv * 1) - $decay;
						if (isset($feed->data[$i]->comments) || isset($feed->data[$i]->likes))
						{
							$bump_score = ($my_comment_bump * $my_comment_rv);
							if ($adj_score < $bump_score) $adj_score = $bump_score;
						}
						
						$score += ($my_comment_fv>$adj_score ? $my_comment_fv : $adj_score);
					}
					
					if ($type=="video")
					{
						$my_video_post++;
						$receive_like_video += (isset($feed->data[$i]->likes) ? count($feed->data[$i]->likes->data) : 0);
						$age = time() - $feed->data[$i]->created_time;
						$decay = ($age / (86400 * $my_pic_du)) * $my_pic_dr;
						$adj_score = ($my_video_rv * 1) - $decay;
						if (isset($feed->data[$i]->comments) || isset($feed->data[$i]->likes))
						{
							$bump_score = ($my_video_bump * $my_video_rv);
							if ($adj_score < $bump_score) $adj_score = $bump_score;
						}
						
						$score += ($my_video_fv>$adj_score ? $my_video_fv : $adj_score);
					}
					
					if ($type=="photo")
					{
						$my_photo_post++;
						$receive_like_photo += (isset($feed->data[$i]->likes) ? count($feed->data[$i]->likes->data) : 0);
						$age = time() - $feed->data[$i]->created_time;
						$decay = ($age / (86400 * $my_pic_du)) * $my_pic_dr;
						$adj_score = ($my_pic_rv * 1) - $decay;
						if (isset($feed->data[$i]->comments) || isset($feed->data[$i]->likes))
						{
							$bump_score = ($my_pic_bump * $my_pic_rv);
							if ($adj_score < $bump_score) $adj_score = $bump_score;
						}
						
						$score += ($my_pic_fv>$adj_score ? $my_pic_fv : $adj_score);
					}
					if ($type=="status")
					{
						$my_status_post++;
						$receive_like_comment += (isset($feed->data[$i]->likes) ? count($feed->data[$i]->likes->data) : 0);
						$age = time() - $feed->data[$i]->created_time;
						$decay = ($age / (86400 * $my_comment_du)) * $my_comment_dr;
						$adj_score = ($my_comment_rv * 1) - $decay;
						if (isset($feed->data[$i]->comments) || isset($feed->data[$i]->likes))
						{
							$bump_score = ($my_comment_bump * $my_comment_rv);
							if ($adj_score < $bump_score) $adj_score = $bump_score;
						}
						
						$score += ($my_comment_fv>$adj_score ? $my_comment_fv : $adj_score);
					}
					
					if ($type=="checkin")
					{
						$my_checkin_post++;
						$receive_like_checkin += (isset($feed->data[$i]->likes) ? count($feed->data[$i]->likes->data) : 0);
						$age = time() - $feed->data[$i]->created_time;
						$decay = ($age / (86400 * $my_checkin_du)) * $my_checkin_dr;
						$adj_score = ($my_checkin_rv * 1) - $decay;
						if (isset($feed->data[$i]->comments) || isset($feed->data[$i]->likes))
						{
							$bump_score = ($my_checkin_bump * $my_checkin_rv);
							if ($adj_score < $bump_score) $adj_score = $bump_score;
						}
						
						$score += ($my_checkin_fv>$adj_score ? $my_checkin_fv : $adj_score);
					}
					
					$receive_sub_comment += (isset($feed->data[$i]->comments) ? count($feed->data[$i]->comments->data) : 0);
					
					if ($type=="status")
					{
						$receive_comment++;
					}
					else if ($type=="checkin")
					{
						$tagged_checkin++;
						$age = time() - $feed->data[$i]->created_time;
						$decay = ($age / (86400 * $my_checkin_du)) * $tagged_checkin_dr;
						$adj_score = ($tagged_checkin_rv * 1) - $decay;
						if (isset($feed->data[$i]->comments) || isset($feed->data[$i]->likes))
						{
							$bump_score = ($tagged_checkin_bump * $tagged_checkin_rv);
							if ($adj_score < $bump_score) $adj_score = $bump_score;
						}
						
						$score += ($tagged_checkin_fv>$adj_score ? $tagged_checkin_fv : $adj_score);
					}
					else if ($type=="photo")
					{
						$tagged_photo++;
						$age = time() - $feed->data[$i]->created_time;
						$decay = ($age / (86400 * $tagged_photo_du)) * $tagged_photo_dr;
						$adj_score = ($tagged_photo_rv * 1) - $decay;
						if (isset($feed->data[$i]->comments) || isset($feed->data[$i]->likes))
						{
							$bump_score = ($tagged_photo_bump * $tagged_photo_rv);
							if ($adj_score < $bump_score) $adj_score = $bump_score;
						}
						
						$score += ($tagged_photo_fv>$adj_score ? $tagged_photo_fv : $adj_score);
					}
					else if ($type=="video")
					{
						$tagged_video++;
						$age = time() - $feed->data[$i]->created_time;
						$decay = ($age / (86400 * $my_video_du)) * $tagged_video_dr;
						$adj_score = ($tagged_video_rv * 1) - $decay;
						if (isset($feed->data[$i]->comments) || isset($feed->data[$i]->likes))
						{
							$bump_score = ($tagged_video_bump * $tagged_video_rv);
							if ($adj_score < $bump_score) $adj_score = $bump_score;
						}
						
						$score += ($tagged_video_fv>$adj_score ? $tagged_video_fv : $adj_score);
					}
					
					$last_time = strtotime($feed->data[$i]->created_time);
					$offset++;
				}
			}

			$limit = 500;
			$offset = 0;

			$graph_url = "https://graph.facebook.com/me/friends?limit=".$limit."&offset=".$offset."&date_format=U&access_token=".$accessToken;
			//$graph_url = "https://graph.facebook.com/".$userID."/feed?access_token=".$accessToken;
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $graph_url);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
			curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/lib/fb/src/Facebook/HttpClients/certs/DigiCertHighAssuranceEVRootCA.pem');
			curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
			$response = curl_exec($ch);
			curl_close($ch);
			$feed = json_decode($response);
			/*echo '<pre>';
			print_r($feed);
			echo '</pre>';
			die;*/
			
			$nr_friends = $feed->summary->total_count;

			$score += $nr_friends * 0.01;
			
			$score = round($score, 15);

			$unique_users = count($uniques);

			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users_facebook SET checkins = ?, checkin_likes = ?, status_posts = ?, links_posted = ?, photos_posted = ?, liked_photos = ?, videos_posted = ?, liked_videos = ?, comments_posted = ?, liked_comments = ?, tagged_videos = ?, tagged_checkins = ?, tagged_photos = ?, wall_posts_received = ?, comments_received = ?, unique_users = ? WHERE user_id = UNHEX(?)");
			
			//$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users_facebook SET checkins = ?, checkin_likes = ?, status_posts = ?, links_posted = ?, photos_posted = ?, liked_photos = ?, videos_posted = ?, liked_videos = ?, comments_posted = ?, liked_comments = ?, tagged_videos = ?, tagged_checkins = ?, tagged_photos = ?, wall_posts_received = ?, comments_received = ?, unique_users = ? WHERE unique_id = ?");
			
			mysqli_stmt_bind_param($stmt, "iiiiiiiiiiiiiiiis", $my_checkin_post, $receive_like_checkin, $my_status_post, $my_link_post, $my_photo_post, $receive_like_photo, $my_video_post, $receive_like_video, $my_sub_comment_post, $receive_like_comment, $tagged_video, $tagged_checkin, $tagged_photo, $receive_comment, $receive_sub_comment, $unique_users, $userID);
			
			mysqli_stmt_execute($stmt);
			mysqli_stmt_close($stmt);

			$stmt = mysqli_prepare($this->_conn, "SELECT score, modified_time FROM rank_score_facebook WHERE user_id = UNHEX(?) ORDER BY modified_time DESC LIMIT 2");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				$prevScores = array();
				$prevModifiedTimes = array();
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$prevScores[] = $row["score"];
					$prevModifiedTimes[] = $row["modified_time"];
				}
				
				if (count($prevScores) == 2 && $prevScores[0] == $score && $prevScores[1] == $prevScores[0]) {
					$stmt2 = mysqli_prepare($this->_conn, "UPDATE rank_score_facebook SET modified_time = NOW() WHERE user_id = UNHEX(?) AND modified_time = ?");
					mysqli_stmt_bind_param($stmt2, "ss", $userID, $prevModifiedTimes[0]);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				} else {
					$stmt2 = mysqli_prepare($this->_conn, "INSERT INTO rank_score_facebook SET user_id = UNHEX(?), score = ?");
					mysqli_stmt_bind_param($stmt2, "sd", $userID, $score);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				}
				
				if (count($prevScores) < 1 || $prevScores[0] != $score) {
					$stmt2 = mysqli_prepare($this->_conn, "UPDATE rank_users_facebook SET score = ? WHERE user_id = UNHEX(?)");
					mysqli_stmt_bind_param($stmt2, "ds", $score, $userID);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				}
				
			}
			
			mysqli_stmt_close($stmt);
		}

		public function computeGlobalFacebookParticipants() {
			$globalFacebookParticipants = 0;
			$recordSet = mysqli_query($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id");
			while ($row = mysqli_fetch_array($recordSet)) {
				$globalFacebookParticipants = $row["participants"];
			}
			mysqli_free_result($recordSet);
			
			$prevParticipants = array();
			$prevModifiedTimes = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT participants, modified_time FROM rank_global_facebook_participants ORDER BY modified_time DESC LIMIT 2");
			while ($row = mysqli_fetch_array($recordSet)) {
				$prevParticipants[] = $row["participants"];
				$prevModifiedTimes[] = $row["modified_time"];
			}
			mysqli_free_result($recordSet);
			
			if (count($prevParticipants) == 2 && $prevParticipants[0] == $globalFacebookParticipants && $prevParticipants[1] == $prevParticipants[0]) {
				$stmt = mysqli_prepare($this->_conn, "UPDATE rank_global_facebook_participants SET modified_time = NOW() WHERE participants = ? AND modified_time = ?");
				mysqli_stmt_bind_param($stmt, "is", $prevParticipants[0], $prevModifiedTimes[0]);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_close($stmt);
			} else {
				$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_global_facebook_participants SET participants = ?");
				mysqli_stmt_bind_param($stmt, "i", $globalFacebookParticipants);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_close($stmt);
			}
		}

		public function rankGlobalFacebook($userID) {
			$globalFacebookRank = 0;
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_facebook AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_facebook.score > target_user.score");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$globalFacebookRank = $row["rank"];
				}
			}
			mysqli_stmt_close($stmt);
			
			$stmt = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id FROM rank_users INNER JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_facebook AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_facebook.score = target_user.score ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "ss", $userID, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$globalFacebookRank += $row["tie_breaker"];
				}
			}
			mysqli_stmt_close($stmt);
			
			$stmt = mysqli_prepare($this->_conn, "SELECT rank, modified_time FROM rank_global_facebook WHERE user_id = UNHEX(?) ORDER BY modified_time DESC LIMIT 2");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				$prevRanks = array();
				$prevModifiedTimes = array();
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$prevRanks[] = $row["rank"];
					$prevModifiedTimes[] = $row["modified_time"];
				}
				
				if (count($prevRanks) == 2 && $prevRanks[0] == $globalFacebookRank && $prevRanks[1] == $prevRanks[0]) {
					$stmt2 = mysqli_prepare($this->_conn, "UPDATE rank_global_facebook SET modified_time = NOW() WHERE user_id = UNHEX(?) AND modified_time = ?");
					mysqli_stmt_bind_param($stmt2, "ss", $userID, $prevModifiedTimes[0]);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				} else {
					$stmt2 = mysqli_prepare($this->_conn, "INSERT INTO rank_global_facebook SET user_id = UNHEX(?), rank = ?");
					mysqli_stmt_bind_param($stmt2, "ss", $userID, $globalFacebookRank);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				}
			}
			mysqli_stmt_close($stmt);
		}

		public function computeGlobalLinkedInParticipants() {
			$globalLinkedInParticipants = 0;
			$recordSet = mysqli_query($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id");
			while ($row = mysqli_fetch_array($recordSet)) {
				$globalLinkedInParticipants = $row["participants"];
			}
			mysqli_free_result($recordSet);
			
			$prevParticipants = array();
			$prevModifiedTimes = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT participants, modified_time FROM rank_global_linkedin_participants ORDER BY modified_time DESC LIMIT 2");
			while ($row = mysqli_fetch_array($recordSet)) {
				$prevParticipants[] = $row["participants"];
				$prevModifiedTimes[] = $row["modified_time"];
			}
			mysqli_free_result($recordSet);
			
			if (count($prevParticipants) == 2 && $prevParticipants[0] == $globalLinkedInParticipants && $prevParticipants[1] == $prevParticipants[0]) {
				$stmt = mysqli_prepare($this->_conn, "UPDATE rank_global_linkedin_participants SET modified_time = NOW() WHERE participants = ? AND modified_time = ?");
				mysqli_stmt_bind_param($stmt, "is", $prevParticipants[0], $prevModifiedTimes[0]);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_close($stmt);
			} else {
				$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_global_linkedin_participants SET participants = ?");
				mysqli_stmt_bind_param($stmt, "i", $globalLinkedInParticipants);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_close($stmt);
			}
		}

		public function rankGlobalLinkedIn($userID) {
			$globalLinkedInRank = 0;
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_linkedin AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_linkedin.score > target_user.score");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$globalLinkedInRank = $row["rank"];
				}
			}
			mysqli_stmt_close($stmt);
			
			$stmt = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id FROM rank_users INNER JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_linkedin AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_linkedin.score = target_user.score ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "ss", $userID, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$globalLinkedInRank += $row["tie_breaker"];
				}
			}
			mysqli_stmt_close($stmt);
			
			$stmt = mysqli_prepare($this->_conn, "SELECT rank, modified_time FROM rank_global_linkedin WHERE user_id = UNHEX(?) ORDER BY modified_time DESC LIMIT 2");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				$prevRanks = array();
				$prevModifiedTimes = array();
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$prevRanks[] = $row["rank"];
					$prevModifiedTimes[] = $row["modified_time"];
				}
				
				if (count($prevRanks) == 2 && $prevRanks[0] == $globalLinkedInRank && $prevRanks[1] == $prevRanks[0]) {
					$stmt2 = mysqli_prepare($this->_conn, "UPDATE rank_global_linkedin SET modified_time = NOW() WHERE user_id = UNHEX(?) AND modified_time = ?");
					mysqli_stmt_bind_param($stmt2, "ss", $userID, $prevModifiedTimes[0]);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				} else {
					$stmt2 = mysqli_prepare($this->_conn, "INSERT INTO rank_global_linkedin SET user_id = UNHEX(?), rank = ?");
					mysqli_stmt_bind_param($stmt2, "ss", $userID, $globalLinkedInRank);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				}
			}
			mysqli_stmt_close($stmt);
		}

		public function scoreLinkedIn($userID, $information) {
			$score = 0;
			
			// $recommendationsReceived = $information["recommendationsReceived"]["_total"];
			$recommendationsReceived = '';
			// $connections = $information["connections"]["_total"];
			$connections = '';
			$foreignConnections = 0;
			$positions = $information["positions"]["_total"];
			$currentPositions = 0;
			// $educations = $information["educations"]["_total"];
			$educations = '';
			
			if ($recommendationsReceived > 0) {
				$score += $information["recommendationsReceived"]["_total"] * 2;
			}
			
			for($i = 0; $i < $connections; $i++)
			{
				$score += 0.10;
				if($information["connections"]["values"][$i]["location"]["country"]["code"] != "us") {
					$score += 0.02;
					$foreignConnections++;
				}
			}
			
			for($i = 0; $i < $positions; $i++)
			{
				$score += 1.00;

				if ($information["positions"]["values"][$i]["isCurrent"] == "1") {
					$score += 100.00;
					$currentPositions++;
				}
			}
					
			for($i = 0; $i < $educations; $i++)
			{
				$score += 0.10;
			}
			
			$score = round($score, 15);
			
			$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users_linkedin SET recommendations = ?, connections = ?, foreign_connections = ?, positions = ?, current_positions = ?, educations = ? WHERE user_id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "iiiiiis", $recommendationsReceived, $connections, $foreignConnections, $positions, $currentPositions, $educations, $userID);
			mysqli_stmt_execute($stmt);
			mysqli_stmt_close($stmt);

			$stmt = mysqli_prepare($this->_conn, "SELECT score, modified_time FROM rank_score_linkedin WHERE user_id = UNHEX(?) ORDER BY modified_time DESC LIMIT 2");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				$prevScores = array();
				$prevModifiedTimes = array();
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$prevScores[] = $row["score"];
					$prevModifiedTimes[] = $row["modified_time"];
				}
				
				if (count($prevScores) == 2 && $prevScores[0] == $score && $prevScores[1] == $prevScores[0]) {
					$stmt2 = mysqli_prepare($this->_conn, "UPDATE rank_score_linkedin SET modified_time = NOW() WHERE user_id = UNHEX(?) AND modified_time = ?");
					mysqli_stmt_bind_param($stmt2, "ss", $userID, $prevModifiedTimes[0]);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				} else {
					$stmt2 = mysqli_prepare($this->_conn, "INSERT INTO rank_score_linkedin SET user_id = UNHEX(?), score = ?");
					mysqli_stmt_bind_param($stmt2, "sd", $userID, $score);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				}
				
				if (count($prevScores) < 1 || $prevScores[0] != $score) {
					$stmt2 = mysqli_prepare($this->_conn, "UPDATE rank_users_linkedin SET score = ? WHERE user_id = UNHEX(?)");
					mysqli_stmt_bind_param($stmt2, "ds", $score, $userID);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				}
			}
			
			mysqli_stmt_close($stmt);
		}

		public function computeGlobalTwitterParticipants() {
			$globalTwitterParticipants = 0;
			$recordSet = mysqli_query($this->_conn, "SELECT COUNT(*) AS participants FROM rank_users INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id");
			while ($row = mysqli_fetch_array($recordSet)) {
				$globalTwitterParticipants = $row["participants"];
			}
			mysqli_free_result($recordSet);
			
			$prevParticipants = array();
			$prevModifiedTimes = array();
			
			$recordSet = mysqli_query($this->_conn, "SELECT participants, modified_time FROM rank_global_twitter_participants ORDER BY modified_time DESC LIMIT 2");
			while ($row = mysqli_fetch_array($recordSet)) {
				$prevParticipants[] = $row["participants"];
				$prevModifiedTimes[] = $row["modified_time"];
			}
			mysqli_free_result($recordSet);
			
			if (count($prevParticipants) == 2 && $prevParticipants[0] == $globalTwitterParticipants && $prevParticipants[1] == $prevParticipants[0]) {
				$stmt = mysqli_prepare($this->_conn, "UPDATE rank_global_twitter_participants SET modified_time = NOW() WHERE participants = ? AND modified_time = ?");
				mysqli_stmt_bind_param($stmt, "is", $prevParticipants[0], $prevModifiedTimes[0]);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_close($stmt);
			} else {
				$stmt = mysqli_prepare($this->_conn, "INSERT INTO rank_global_twitter_participants SET participants = ?");
				mysqli_stmt_bind_param($stmt, "i", $globalTwitterParticipants);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_close($stmt);
			}
		}

		public function rankGlobalTwitter($userID) {
			$globalTwitterRank = 0;
			$stmt = mysqli_prepare($this->_conn, "SELECT COUNT(*) AS rank FROM rank_users INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_users_twitter AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_twitter.score > target_user.score");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$globalTwitterRank = $row["rank"];
				}
			}
			mysqli_stmt_close($stmt);
			
			$stmt = mysqli_prepare($this->_conn, "SELECT @r := @r + 1 AS tie_breaker FROM (SELECT @r := 0) AS initialize, (SELECT rank_users.id FROM rank_users INNER JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id LEFT JOIN rank_users_twitter AS target_user ON target_user.user_id = UNHEX(?) WHERE rank_users_twitter.score = target_user.score ORDER BY rank_users.creation_time ASC) AS tie_ranks WHERE tie_ranks.id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "ss", $userID, $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$globalTwitterRank += $row["tie_breaker"];
				}
			}
			mysqli_stmt_close($stmt);
			
			$stmt = mysqli_prepare($this->_conn, "SELECT rank, modified_time FROM rank_global_twitter WHERE user_id = UNHEX(?) ORDER BY modified_time DESC LIMIT 2");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				$prevRanks = array();
				$prevModifiedTimes = array();
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$prevRanks[] = $row["rank"];
					$prevModifiedTimes[] = $row["modified_time"];
				}
				
				if (count($prevRanks) == 2 && $prevRanks[0] == $globalTwitterRank && $prevRanks[1] == $prevRanks[0]) {
					$stmt2 = mysqli_prepare($this->_conn, "UPDATE rank_global_twitter SET modified_time = NOW() WHERE user_id = UNHEX(?) AND modified_time = ?");
					mysqli_stmt_bind_param($stmt2, "ss", $userID, $prevModifiedTimes[0]);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				} else {
					$stmt2 = mysqli_prepare($this->_conn, "INSERT INTO rank_global_twitter SET user_id = UNHEX(?), rank = ?");
					mysqli_stmt_bind_param($stmt2, "ss", $userID, $globalTwitterRank);
					mysqli_stmt_execute($stmt2);
					mysqli_stmt_close($stmt2);
				}
			}
			mysqli_stmt_close($stmt);
		}

		public function scoreTwitter($userID, $oAuthToken, $oAuthTokenSecret) {
			$score = 0;
			
			$originalTweets = 0;
			$otherTweets = 0;
			$retweetedTotal = 0;
			$favoritedTotal = 0;
			$favorites = 0;
			$statuses = 0;
			$friends = 0;
			$followers = 0;
			$listed = 0;
			$verified = 0;
			require(BASE_PATH . "/../phpinc/lib/tw/twitter.php");
			$twitterSession = new TwitterOAuth(TWITTER_APP_ID, TWITTER_SECRET, $oAuthToken, $oAuthTokenSecret);
			$account = $twitterSession->get("account/verify_credentials");
			
			if ($twitterSession->http_code == 200 && isset($account->id)) {
				// Also update their profile picture in the database and in any active sessions for that user
				$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users_twitter SET profile_picture_url = ? WHERE user_id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt, "ss", $account->profile_image_url_https, $userID);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_close($stmt);
				
				if (isset($_SESSION)) {
					// Preserve current session's ID
					$tmpSessionID = session_id();
					session_write_close();
				}
				
				$stmt = mysqli_prepare($this->_conn, "SELECT session_id FROM rank_active_sessions WHERE user_id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt, "s", $userID);
				if (mysqli_stmt_execute($stmt)) {
					$recordSet = mysqli_stmt_get_result($stmt);
					while ($row = mysqli_fetch_array($recordSet)) {
						session_id($row["session_id"]);
						session_start();
						
						$_SESSION["twitterProfilePictureURL"] = $account->profile_image_url_https;
						
						session_write_close();
					}
				}
				mysqli_stmt_close($stmt);
				
				if (isset($tmpSessionID)) {
					// Restart the current session
					session_id($tmpSessionID);
					session_start();
				}
				
				$tweets = $twitterSession->get('statuses/user_timeline', array('include_entities' => '1', 'include_rts' => '1', 'screen_name' => $account->screen_name, 'count' => 400));
				
				if ($twitterSession->http_code == 200) {
					$nr_tweets = count($tweets);
					
					$now = new DateTime();
					for ($x = 0; $x < $nr_tweets; $x++)
					{
						$tweet_score = 0;
						$tweetTime = new DateTime($tweets[$x]->created_at);
						$days = $now->diff($tweetTime)->days;

						if (!isset($tweets[$x]->retweeted_status) && !$tweets[$x]->in_reply_to_user_id)
						{
							$originalTweets++;
							$tweet_score += 0.10;
						}
						else if ($tweets[$x]->in_reply_to_user_id || isset($tweets[$x]->retweeted_status)) 
						{
							$otherTweets++;
							$tweet_score += 0.02;
						}
						if ($tweets[$x]->retweet_count > 0)
						{
							$retweetedTotal += $tweets[$x]->retweet_count;
							$tweet_score += (($tweets[$x]->retweet_count) / 2) / 100;
						}
						if ($tweets[$x]->favorited > 0)
						{
							$favoritedTotal += $tweets[$x]->favorited;
							$tweet_score += $tweets[$x]->favorited / 100;
						}
						
						$tweet_score -= ($days / 100);
						if ($tweet_score > 0) {
							$score += $tweet_score;
						}
					}
				}
				
				if ($account->favourites_count > 0) {
					$favorites = $account->favourites_count;
					$score += $account->favourites_count * 1;
				}
				if ($account->statuses_count > 0) {
					$statuses = $account->statuses_count;
					$score += $account->statuses_count * 0.01;
				}
				if ($account->friends_count > 0) {
					$friends = $account->friends_count;
					$score += $account->friends_count* 0.02;
				}
				if ($account->followers_count > 0) {
					$followers = $account->followers_count;
					$friendsPerFollower = $account->friends_count / $account->followers_count;
					if ($friendsPerFollower > 1) {
						$score += $account->followers_count * 0.20;	
					} else if ($friendsPerFollower >= 0.20 && $friendsPerFollower <= 1) {
						$score += $account->followers_count * 0.40;
					} else if ($friendsPerFollower < 0.20) {
						$score += $account->followers_count * 1;
					} else {
						$score += $account->followers_count * 0.30;
					}
				}
				if ($account->listed_count > 0) {
					$listed = $account->listed_count;
					$score += $account->listed_count * 1;
				}
				
				if ($account->verified > 0) {
					$verified = $account->verified;
					$score += ($score * 1.25);
				}
				
				$score = $score / 100;
				
				$score = round($score, 15);
				
				$stmt = mysqli_prepare($this->_conn, "UPDATE rank_users_twitter SET original_tweets = ?, other_tweets = ?, retweeted_total = ?, favorited_total = ?, favorites = ?, statuses = ?, friends = ?, followers = ?, listed = ?, verified = ? WHERE user_id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt, "iiiiiiiiiis", $originalTweets, $otherTweets, $retweetedTotal, $favoritedTotal, $favorites, $statuses, $friends, $followers, $listed, $verified, $userID);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_close($stmt);

				$stmt = mysqli_prepare($this->_conn, "SELECT score, modified_time FROM rank_score_twitter WHERE user_id = UNHEX(?) ORDER BY modified_time DESC LIMIT 2");
				mysqli_stmt_bind_param($stmt, "s", $userID);
				if (mysqli_stmt_execute($stmt)) {
					$recordSet = mysqli_stmt_get_result($stmt);
					$prevScores = array();
					$prevModifiedTimes = array();
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$prevScores[] = $row["score"];
						$prevModifiedTimes[] = $row["modified_time"];
					}
					
					if (count($prevScores) == 2 && $prevScores[0] == $score && $prevScores[1] == $prevScores[0]) {
						$stmt2 = mysqli_prepare($this->_conn, "UPDATE rank_score_twitter SET modified_time = NOW() WHERE user_id = UNHEX(?) AND modified_time = ?");
						mysqli_stmt_bind_param($stmt2, "ss", $userID, $prevModifiedTimes[0]);
						mysqli_stmt_execute($stmt2);
						mysqli_stmt_close($stmt2);
					} else {
						$stmt2 = mysqli_prepare($this->_conn, "INSERT INTO rank_score_twitter SET user_id = UNHEX(?), score = ?");
						mysqli_stmt_bind_param($stmt2, "sd", $userID, $score);
						mysqli_stmt_execute($stmt2);
						mysqli_stmt_close($stmt2);
					}
					
					if (count($prevScores) < 1 || $prevScores[0] != $score) {
						$stmt2 = mysqli_prepare($this->_conn, "UPDATE rank_users_twitter SET score = ? WHERE user_id = UNHEX(?)");
						mysqli_stmt_bind_param($stmt2, "ds", $score, $userID);
						mysqli_stmt_execute($stmt2);
						mysqli_stmt_close($stmt2);
					}
				}
				
				mysqli_stmt_close($stmt);
			}
		}

		public function updateMetaScore($userID) {
			$result = false;
			
			$metaScore = 0.0;
			$stmt = mysqli_prepare($this->_conn, "SELECT rank_users.bonus_score + IFNULL(rank_users_facebook.score, 0) + IFNULL(rank_users_linkedin.score, 0) + IFNULL(rank_users_twitter.score, 0) AS meta_score FROM rank_users LEFT JOIN rank_users_facebook ON rank_users.id = rank_users_facebook.user_id LEFT JOIN rank_users_linkedin ON rank_users.id = rank_users_linkedin.user_id LEFT JOIN rank_users_twitter ON rank_users.id = rank_users_twitter.user_id WHERE rank_users.id = UNHEX(?)");
			mysqli_stmt_bind_param($stmt, "s", $userID);
			if (mysqli_stmt_execute($stmt)) {
				$recordSet = mysqli_stmt_get_result($stmt);
				
				while ($row = mysqli_fetch_array($recordSet)) {
					$metaScore = $row["meta_score"];
				}
				
				$stmt2 = mysqli_prepare($this->_conn, "SELECT score, modified_time FROM rank_score WHERE user_id = UNHEX(?) ORDER BY modified_time DESC LIMIT 2");
				mysqli_stmt_bind_param($stmt2, "s", $userID);
				if (mysqli_stmt_execute($stmt2)) {
					$recordSet = mysqli_stmt_get_result($stmt2);
					$prevScores = array();
					$prevModifiedTimes = array();
					
					while ($row = mysqli_fetch_array($recordSet)) {
						$prevScores[] = $row["score"];
						$prevModifiedTimes[] = $row["modified_time"];
					}
					
					if (count($prevScores) == 2 && $prevScores[0] == $metaScore && $prevScores[1] == $prevScores[0]) {
						$stmt3 = mysqli_prepare($this->_conn, "UPDATE rank_score SET modified_time = NOW() WHERE user_id = UNHEX(?) AND modified_time = ?");
						mysqli_stmt_bind_param($stmt3, "ss", $userID, $prevModifiedTimes[0]);
						$result = mysqli_stmt_execute($stmt3);
						mysqli_stmt_close($stmt3);
					} else {
						$stmt3 = mysqli_prepare($this->_conn, "INSERT INTO rank_score SET user_id = UNHEX(?), score = ?");
						mysqli_stmt_bind_param($stmt3, "sd", $userID, $metaScore);
						$result = mysqli_stmt_execute($stmt3);
						mysqli_stmt_close($stmt3);
					}
					
					if ($result && count($prevScores) < 1 || $prevScores[0] != $metaScore) {
						$stmt3 = mysqli_prepare($this->_conn, "UPDATE rank_users SET meta_score = ? WHERE id = UNHEX(?)");
						mysqli_stmt_bind_param($stmt3, "ds", $metaScore, $userID);
						$result = mysqli_stmt_execute($stmt3);
						mysqli_stmt_close($stmt3);
					}
				}
				
				mysqli_stmt_close($stmt2);
			}
			
			mysqli_stmt_close($stmt);
			
			return $result;
		}
		
		public function randomPassword($length = 6) {
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$password = substr( str_shuffle( $chars ), 0, $length );
			return $password;
		}
		
		public function getFacebookScore() {			
			$AllFBScore = mysqli_query($this->_conn, "SELECT DISTINCT(score) FROM rank_users_facebook");
			$AllFacebookScore = array();
			while($AFBS = mysqli_fetch_assoc($AllFBScore)) {
				$AllFacebookScore[] = $AFBS['score'];
			}
			return $AllFacebookScore;	
		}
		
		public function getTwitterScore() {			
			$AllTWScore = mysqli_query($this->_conn, "SELECT DISTINCT(score) FROM rank_users_twitter");
			$AllTwitterScore = array();
			while($ATWS = mysqli_fetch_assoc($AllTWScore)) {
				$AllTwitterScore[] = $ATWS['score'];
			}
			return $AllTwitterScore;	
		}
		
		public function getLinkedinScore() {			
			$AllLIScore = mysqli_query($this->_conn, "SELECT DISTINCT(score) FROM rank_users_linkedin");
			$AllLinkedinScore = array();
			while($ALIS = mysqli_fetch_assoc($AllLIScore)) {
				$AllLinkedinScore[] = $ALIS['score'];
			}
			return $AllLinkedinScore;	
		}
	}
	
	$metaRankDatabase = new MetaRankDatabase();
?>
