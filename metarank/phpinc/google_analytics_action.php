<?php

if (isset($_SESSION["googleAnalyticsAction"])) {
	switch ($_SESSION["googleAnalyticsAction"]) {
		case "Facebook Login":
?>
			<script type="text/javascript">
				// Set the user ID using signed-in user_id.
				ga('set', '&uid', '<?php echo $_SESSION["userID"]; ?>');
				
				ga('send', 'event', 'Process', 'Login', 'Facebook', 0);
			</script><?php
			break;
		case "Facebook Connected":
?>
			<script type="text/javascript">
				ga('send', 'event', 'Process', 'ConnectService', 'Facebook', 0);
			</script><?php
			break;
		case "Facebook Reauthorized":
?>
			<script type="text/javascript">
				ga('send', 'event', 'Process', 'ReauthorizeService', 'Facebook', 0);
			</script><?php
			break;
		case "Facebook Login Failure":
?>
			<script type="text/javascript">
				ga('send', 'event', 'Process', 'Login', 'Facebook', 1);
			</script><?php
			break;
		case "Facebook Connection Failure":
?>
			<script type="text/javascript">
				ga('send', 'event', 'Process', 'ConnectService', 'Facebook', 1);
			</script><?php
			break;
		case "Facebook Reauthorization Failure":
?>
			<script type="text/javascript">
				ga('send', 'event', 'Process', 'ReauthorizeService', 'Facebook', 1);
			</script><?php
			break;
		case "LinkedIn Login":
?>
			<script type="text/javascript">
				// Set the user ID using signed-in user_id.
				ga('set', '&uid', '<?php echo $_SESSION["userID"]; ?>');
				
				ga('send', 'event', 'Process', 'Login', 'LinkedIn', 0);
			</script><?php
			break;
		case "LinkedIn Connected":
?>
			<script type="text/javascript">
				ga('send', 'event', 'Process', 'ConnectService', 'LinkedIn', 0);
			</script><?php
			break;
		case "LinkedIn Reauthorized":
?>
			<script type="text/javascript">
				ga('send', 'event', 'Process', 'ReauthorizeService', 'LinkedIn', 0);
			</script><?php
			break;
		case "LinkedIn Login Failure":
?>
			<script type="text/javascript">
				ga('send', 'event', 'Process', 'Login', 'LinkedIn', 1);
			</script><?php
			break;
		case "LinkedIn Connection Failure":
?>
			<script type="text/javascript">
				ga('send', 'event', 'Process', 'ConnectService', 'LinkedIn', 1);
			</script><?php
			break;
		case "LinkedIn Reauthorization Failure":
?>
			<script type="text/javascript">
				ga('send', 'event', 'Process', 'ReauthorizeService', 'LinkedIn', 1);
			</script><?php
			break;
		case "Twitter Login":
?>
			<script type="text/javascript">
				// Set the user ID using signed-in user_id.
				ga('set', '&uid', '<?php echo $_SESSION["userID"]; ?>');
				
				ga('send', 'event', 'Process', 'Login', 'Twitter', 0);
			</script><?php
			break;
		case "Twitter Connected":
?>
			<script type="text/javascript">
				ga('send', 'event', 'Process', 'ConnectService', 'Twitter', 0);
			</script><?php
			break;
		case "Twitter Reauthorized":
?>
			<script type="text/javascript">
				ga('send', 'event', 'Process', 'ReauthorizeService', 'Twitter', 0);
			</script><?php
			break;
		default:
			break;
	}
}

?>