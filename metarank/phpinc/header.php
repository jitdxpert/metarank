<!DOCTYPE html>
<html>
<head>
<title><?php echo $pageTitle; ?></title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta property="og:title" content="MetaRank - The Human Hierarchy Project" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.metarank.com" />
<meta property="og:image" content="//www.metarank.com/images/gt-logo-hierarchy-100.png" />
<meta property="og:site_name" content="MetaRank" />
<meta property="fb:admins" content="675125767" />
<meta name="description" content="MetaRank: A way to apply a rank to each human based on all aspects of ones' life." />
<meta name="keywords" content="score, social media, rank, metarank, hierarchy, charts, global, popularity, leaderboard, metascore" />

<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css" />
<link rel="stylesheet" href="<?php echo BASE_URL; ?>/lib/jquery-mobile/plugins/datetimepicker/DateTimePicker.css" />
<link rel="stylesheet" href="<?php echo BASE_URL; ?>/lib/image-picker/image-picker.css" />
<link rel="stylesheet" src="<?php echo BASE_URL; ?>/lib/jqPlot/jquery.jqplot.css" />

<!-- For iPhone 4 -->
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo BASE_URL; ?>/img/h/apple-touch-icon.png" />
<!-- For iPad 1-->
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo BASE_URL; ?>/img/m/apple-touch-icon.png" />
<!-- For iPhone 3G, iPod Touch and Android -->
<link rel="apple-touch-icon-precomposed" href="<?php echo BASE_URL; ?>/img/l/apple-touch-icon-precomposed.png" />
<!-- For Nokia -->
<link rel="shortcut icon" href="<?php echo BASE_URL; ?>/img/l/apple-touch-icon.png" />
<!-- For everything else -->
<link rel="shortcut icon" href="<?php echo BASE_URL; ?>/favicon.ico" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
<script src="<?php echo BASE_URL; ?>/lib/jquery-mobile/plugins/datetimepicker/DateTimePicker.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
<script src="<?php echo BASE_URL; ?>/lib/image-picker/image-picker.min.js"></script>
<script src="<?php echo BASE_URL; ?>/lib/jqPlot/jquery.jqplot.min.js"></script>
<script src="<?php echo BASE_URL; ?>/lib/jqPlot/plugins/jqplot.highlighter.min.js"></script>
<script src="<?php echo BASE_URL; ?>/lib/jqPlot/plugins/jqplot.cursor.min.js"></script>
<script src="<?php echo BASE_URL; ?>/lib/jqPlot/plugins/jqplot.dateAxisRenderer.min.js"></script>
<script src="<?php echo BASE_URL; ?>/lib/jqPlot/plugins/jqplot.canvasTextRenderer.min.js"></script>
<script src="<?php echo BASE_URL; ?>/lib/jqPlot/plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>
<script type="text/javascript">
window['GoogleAnalyticsObject'] = 'ga';
window['ga'] = window['ga'] || function() {
	(window['ga'].q = window['ga'].q || []).push(arguments);
};
window['ga'].l = 1 * new Date();
ga('create', 'UA-57609810-1', 'auto');
ga('require', 'displayfeatures');
ga('require', 'linkid', 'linkid.js');
<?php if (isset($_SESSION["isLoggedIn"]) && $_SESSION["isLoggedIn"]) { ?>
	// Set the user ID using signed-in user_id.
	ga('set', '&uid', '<?php echo $_SESSION["userID"]; ?>');
<?php } ?>
</script>
<script src="//www.google-analytics.com/analytics.js" async></script>
<!--<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCBJQL9dOp-VUMP8MOFS2DD105wMQMHzNo&sensor=true"></script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyAvsScjpr38CJiGwvivtPDgl96pVRqrBD0&sensor=true"></script>-->
<script type="text/javascript" src="http://maps.google.com/maps?file=api&v=2&sensor=false&key=AIzaSyAvsScjpr38CJiGwvivtPDgl96pVRqrBD0"></script>
<script src="//www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
<!--For testing: <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBpB8yfgQPnAHQbyb4kTfCZv8txgq2bi8A"></script>-->
<script src="//platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>
<script id="facebook-jssdk" src="//connect.facebook.net/en_US/all.js#xfbml=1&appId=332319773559364"></script>
<script type="text/javascript">
window["twttr"] = window["twttr"] || (t = {_e: [], ready: function (f) {t._e.push(f)}});
</script>
<script id="twitter-wjs" src="//platform.twitter.com/widgets.js" async></script>
<link rel="stylesheet" href="<?php echo BASE_URL; ?>/css/core.css" />
<input type="hidden" value="<?php echo BASE_URL; ?>" id="BASE_URL" name="BASE_URL" />
<script src="<?php echo BASE_URL; ?>/js/core.js"></script>
</head>
<body>
<section data-role="page">
	<header data-role="header" data-theme="a">
    	<div class="content-wrapper">
        	<img src="<?php echo BASE_URL; ?>/img/logo.png" class="logo" alt="MetaRank" />
            <?php if (isset($_SESSION["isLoggedIn"]) || $_SESSION["isLoggedIn"]) { ?>
            	<a id="mobile-menu" href="#mobile_menu">
                	<img src="<?php echo BASE_URL; ?>/img/bars.png" alt="Menu" />
                </a>
            <?php } ?>
        </div>
    </header>