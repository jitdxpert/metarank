
						<div class="content-box">
							<p class="left larger-message">Leaderboard</p>
							<table class="leaderboard">
								<thead>
									<tr>
										<td><?php echo ($service == "" ? "Metarank" : "Rank"); ?></td>
										<td><?php echo ($service == "" ? "Metascore" : "Score"); ?></td>
										<td>Gender</td>
										<td>Location</td>
									</tr>
								</thead>
								<tbody><?php
	foreach ($leaderboard as $entry) {
?>
								<tr<?php echo ($entry["id"] == $_SESSION["userID"] ? ' class="selected"' : ''); ?>>
									<td><?php echo $entry["rank"]; ?><span class="superscript"><?php echo getOrdinal($entry["rank"]); ?></span></td>
									<td><?php echo round($entry["score"], 0); ?></td>
									<td><img src="<?php echo BASE_URL; ?>/img/<?php echo ($entry["gender"] == "F" ? "female" : ($entry["gender"] == "M" ? "male" : "question")); ?>.png" alt="<?php echo ($entry["gender"] != null ? $entry["gender"] : "?"); ?>"></td>
									<td><?php echo $entry["locality"] . ", " . $entry["administrative_area"] . ", " . $entry["country"]; ?></td>
								</tr><?php
	}
?>
								</tbody>
							</table>
						</div>