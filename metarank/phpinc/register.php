<section data-role="main" class="ui-content">
    <script type="text/javascript">
    ga('send', 'event', 'Register', 'view', '<?php echo (isset($service) ? $service : 'email'); ?>');
    </script>
    <div id="register_page" data-title="<?php echo $pageTitle; ?>">
        <div class="content-wrapper">
            <br />
            <p class="orange-text larger-message">User Information</p>
            <form action="<?php echo BASE_URL; ?>/ajax/register.php" method="POST" class="left">
				<?php if (isset($service)) { ?>
	                <input type="hidden" name="service" value="<?php echo $service; ?>" />
				<?php } ?>
                <label for="first_name">First Name:</label>
                <input type="text" id="first_name" name="first_name" required value="<?php echo $registrationInformation["first_name"]; ?>" />
                <label for="last_name">Last Name:</label>
                <input type="text" id="last_name" name="last_name" required  value="<?php echo $registrationInformation["last_name"]; ?>" />
                <label for="date_of_birth">Date of Birth:</label>
                <input type="date" id="date_of_birth" placeholder="YYYY-mm-dd" name="date_of_birth"  value="<?php echo $registrationInformation["date_of_birth"]; ?>" />
                <label>Gender:</label>
                <label><input type="radio" name="gender" value="F"<?php echo ($registrationInformation["gender"] == "F" ? " checked" : ""); ?>> Female</label>
                <label><input type="radio" name="gender" value="M"<?php echo ($registrationInformation["gender"] == "M" ? " checked" : ""); ?>> Male</label>
                </label>
                <label for="phone">Phone:</label>
                <input type="tel" id="phone" name="phone" required  value="<?php echo $registrationInformation["phone"]; ?>" />
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" required  value="<?php echo $registrationInformation["email"]; ?>" />
				
				
				
				<?php if(isset($service) == "Facebook" || isset($service) == "Twitter" || isset($service) == "LinkedIn"){ } else{ ?>
                <label for="password">Password:</label>
                <input type="password" id="password" name="password" required pattern="<?php echo PASSWORD_HTML5_REGULAR_EXPRESSION; ?>" onchange="
                    this.setCustomValidity(this.validity.patternMismatch ? 'Password must be at least 6 characters long and contain at least one digit, one uppercase letter, one lowercase letter' : '');
                    if(this.checkValidity()) document.getElementById('password_confirm').pattern = this.value.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
                "/>
                <label for="password_confirm">Confirm Password:</label>
                <input type="password" id="password_confirm" name="password_confirm" required pattern="<?php echo PASSWORD_HTML5_REGULAR_EXPRESSION; ?>" onchange="
                    this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above.' : '');
                " />
                <label for="security_question">Security Question:</label>
                <select id="security_question" name="security_question">
					<?php $securityQuestions = $metaRankDatabase->getSecurityQuestions();
					foreach ($securityQuestions as $row) { ?>
	                    <option value="<?php echo $row["id"]; ?>"><?php echo $row["question"]; ?></option>
					<?php } ?>
                </select>
                <label for="security_answer">Answer:</label>
                <input type="text" id="security_answer" name="security_answer" required />
				<?php } ?>
				
				
				
                <div id="status_message" style="display:none;"></div>
                <div id="navigation_box">
                    <p>
                        <button style="float:right;" type="submit">Register</button>
                        <a href="<?php echo BASE_URL; ?>/login/" data-ajax="false">
                        	<button style="background-color:#777777;float:left;color:#fff;display:block;font-weight:400;text-decoration:none;" type="button">Back</button>
                        </a>
                    </p>
                </div>
            </form>
            <script type="text/javascript">
            $("#register_page form").on("submit", register);
            </script>
        </div>
    </div>
</section>