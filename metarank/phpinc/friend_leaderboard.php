<div class="content-box">
	<p class="left larger-message">Leaderboard</p>
    <table class="friend-leaderboard">
    	<thead>
        	<tr>
            	<td>&nbsp;</td>
                <td>Name</td>
                <td>Rank</td>
                <td>Metascore</td>
            </tr>
        </thead>
        <tbody>
			<?php if ($leaderboard !== null && count($leaderboard) > 0) {
				foreach ($leaderboard as $entry) {
					if ($entry["profile_picture"] == "facebook") {
						$profilePictureURL = "https://graph.facebook.com/" . $entry["facebook_id"] . "/picture";
					} else if ($entry["profile_picture"] == "linkedin") {
						require_once(dirname(__FILE__) . '/lib/li/linkedin.php');
						$linkedinSession->setAccessToken($entry["linkedin_oauth_token"]);
						try {
							$information = $linkedinSession->get("/people/~:(picture-url)");
							$profilePictureURL = $information["pictureUrl"];
						} catch (\RuntimeException $re) {
							//TODO: Handle this error
							$profilePictureURL = BASE_URL . "/img/anonymous.png";
						}
					} else if ($entry["profile_picture"] == "twitter") {
						$profilePictureURL = $entry["twitter_profile_picture_url"];
					} else {
						$profilePictureURL = BASE_URL . "/img/anonymous.png";
					} ?>
                    <tr<?php echo ($entry["id"] == $_SESSION["userID"] ? ' class="selected"' : ''); ?>>
                    	<td align="center"><img src="<?php echo $profilePictureURL; ?>" alt="<?php echo $entry["name"]; ?>"/></td>
                        <td align="center"><?php echo $entry["name"]; ?></td>
                        <td align="center"><?php echo $entry["rank"]; ?><span class="superscript"><?php echo getOrdinal($entry["rank"]); ?></span></td>
                        <td align="center"><?php echo round($entry["score"], 0); ?></td>
                    </tr>
				<?php
				}
			} else { ?>
            	<tr>
                	<td colspan="4" style="text-align: center;">No friends in leaderboard.</td>
				</tr>
			<?php
			} ?>
		</tbody>
	</table>
</div>