<?php
	$pageTitle = "MetaRank - Error";
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
		require_once(dirname(dirname(__FILE__)) . "/../phpinc/header.php");
	}
?>
		<section data-role="page" id="error_page" data-title="<?php echo $pageTitle; ?>">
			<div role="main" class="ui-content">
				<div class="content-wrapper">
					<p class="black-text larger-message"><?php echo $message; ?></p>
					<br />
					<br />
					<div id="navigation_box" class="grey-box">
						<p>
							<a href="<?php echo BASE_URL; ?>/">
								<button>Return to Home Page</button>
							</a>
						</p>
					</div>
				</div>
			</div><!-- /content -->
		</section><!-- /page -->
<?php
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
		require_once(BASE_PATH . "/../phpinc/footer.php");
	}
?>