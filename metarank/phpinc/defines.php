<?php
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASSWORD", "wisely");
define("DB_DATABASE", "metarank");

define ("LOCK_TIMEOUT", "5");

define("MAX_LOGIN_FAILURES", "2");
define("MAX_ELEVATION_FAILURES", "2");

//define("RECAPTCHA_PUBLIC_KEY", "6LcmpvsSAAAAADIoufW7T5UJDR-xIGAYoLOOrTxt");
//define("RECAPTCHA_PRIVATE_KEY", "6LcmpvsSAAAAAL7CRFuU2_fMEo3E0dJfp3sOdeXF");
define("RECAPTCHA_PUBLIC_KEY", "6LekgwgTAAAAAGA7JT_0NrNUJvS23iAr-CTj1jje");
define("RECAPTCHA_PRIVATE_KEY", "6LekgwgTAAAAAIXeTfX4Qd2jIcZjcRvZ5h3uvVCQ");

define("STATUS_LOCKED", "1");

define("PASSWORD_RESET_MAX_INTERVAL_MINUTES", "20");
define("PASSWORD_HTML5_REGULAR_EXPRESSION", "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}");

define("MYSQL_MAX_UNSIGNED_INTEGER", "4294967295");

define('FACEBOOK_APP_ID', '400386843419323'); //[MetaRank Real Test App]
define('FACEBOOK_SECRET', 'f13ff20cff1453aa24806add11bc254e');

/*define('FACEBOOK_APP_ID', '102028313479541'); // MetaRank[Login With Facebook]
define('FACEBOOK_SECRET', '75659adf19b6b813b66ba9c2ba77bf6f');*/

/*define('FACEBOOK_APP_ID', '332319773559364'); // for live site
define('FACEBOOK_SECRET', 'fec42a37cd1b1c134158f44ad338b44c');*/

/*define('FACEBOOK_APP_ID', '448775481963198'); For wisely.co
define('FACEBOOK_SECRET', 'a2e66b9ee9c147dafec8d0ae40ba305c');*/

/*define('LINKEDIN_APP_ID', '75yfis9bim1475'); // for live site
define('LINKEDIN_SECRET', '5kA8d3XNTjHOCFgu');*/
define('LINKEDIN_APP_ID', '75v3dxkelppk9u'); //For wisely.co
define('LINKEDIN_SECRET', '4Zc7HgNQ0xuWHMUE');

/*define('TWITTER_APP_ID', 'hskktIVLscdrwytjKewH8TR5b'); // for live site
define('TWITTER_SECRET', 'biD0G2MKrdwjGXrJ3BwhaailTpFtvZWUFX9VG6RBvKJnKdgzmk');*/
define('TWITTER_APP_ID', 'KNivemYiEAxTL6estnbzKUPcc');  //For wisely.co
define('TWITTER_SECRET', 'hPDGaMwEGBjMxF4a52VSPMHFL280sUouudZLncZTMA1Ootsmz0');

define('BASE_URL', 'https://dev.metarank.com');
define('BASE_PATH', dirname(dirname(__FILE__)) . '/htdocs');

function getRemoveStatusMask($statusToRemove) {
	return MYSQL_MAX_UNSIGNED_INTEGER - $statusToRemove;
}

function getOrdinal($number) {
	$n1 = $number % 100; //first remove all but the last two digits 
	$n2 = ($n1 > 3 && $n1 < 21 ? 4 : $number % 10); //remove all but last digit unless the number is in the teens, which all should be 'th'

	//$n is now used to determine the suffix. 
	return ($n2 == 1 ? 'st' : (($n2 == 2 ? 'nd' : ($n2 == 3 ? 'rd' : 'th')))); 
}

function getGUID() {
	mt_srand((double) microtime() * 10000); //optional for PHP 4.2.0 and up.
	$charid = strtoupper(md5(uniqid(rand(), true)));
	return substr($charid, 0, 8) . substr($charid, 8, 4) . substr($charid, 12, 4) . substr($charid, 16, 4) . substr($charid, 20, 12);
	return $uuid;
}

function curHostURL() {
	if (empty($_SERVER["SERVER_NAME"])) {
		//return "https://www.metarank.com";
		return "https://dev.metarank.com/";
	}
	
	$hostURL = 'http';
	if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
		$hostURL .= "s";
	}
	$hostURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443") {
		$hostURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
	} else {
		$hostURL .= $_SERVER["SERVER_NAME"];
	}
	return $hostURL;
}

function curPageURL() {
	if (empty($_SERVER["SERVER_NAME"])) {
		//return "https://www.metarank.com/";
		return "https://dev.metarank.com/";
	}
	
	$pageURL = 'http';
	if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
		$pageURL .= "s";
	}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

function sendEmailVerification($email, $userID, $password) {
	if(isset($password)){
		$password_string = "Your metarank login password is = ". $password;
	} else {
		$password_string ="";
	}
	$headers =  "From: email_verification@metarank.com\r\n";
	$headers .= "Reply-To: do_not_reply@metarank.com\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= 'Content-Type: multipart/related; boundary="m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y"; type="text/html"\r\n';

	$metaRankBottomPNG = base64_encode(fread(fopen(dirname(__FILE__) . "/../htdocs/img/MetaRankBottom.png", "r"), filesize(dirname(__FILE__) . "/../htdocs/img/MetaRankBottom.png")));
	$logoPNG = base64_encode(fread(fopen(dirname(__FILE__) . "/../htdocs/img/logo.png", "r"), filesize(dirname(__FILE__) . "/../htdocs/img/logo.png")));

	$emailContent = <<<EOD
--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y
Content-Type: text/html; charset="UTF-8"

<!DOCTYPE html>
<html>
	<head>
		<title>MetaRank</title>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style type="text/css">
			html, body {
				font-family: Verdana;
				margin: 0;
				padding: 0;
				height: 100%;
			}
			body {
				background: url(cid:MetaRankBottom.png) no-repeat bottom left;
			}
			.meta-logotext {
				color: #00B200;
				font-weight: bold;
			}
			.rank-logotext {
				color: #000000;
				font-weight: bold;
			}

			.black-text {
				color: #000000;
			}

			.orange-text {
				color: #FF7F00;
			}

			.larger-message {
				font-size: 1.1em;
			}

			.smaller-message {
				font-size: .9em;
			}

			.grey-box {
				background-color: rgba(238, 238, 238, .75);
				margin-left: -3em;
				margin-right: -3em;
				padding-left: 3em;
				padding-right: 3em;
			}

			.green-header {
				background-color: #00B200;
				margin-left: -3em;
				margin-right: -3em;
				padding-left: 3em;
				padding-right: 3em;
				color: #FFFFFF;
				text-shadow: none;
				padding: 1px;
				-webkit-border-radius: 10px;
				-moz-border-radius: 10px;
				border-radius: 10px;
			}

			#header {
				position: absolute;
				top: 0;
				left: 0;
				right: 0;
				text-align: center;
				height: 70px;
				z-index: 1;
				background-color: #F6F6F6;
				-webkit-box-shadow: 0px 5px 5px 0px rgba(50, 50, 50, 0.4);
				-moz-box-shadow:    0px 5px 5px 0px rgba(50, 50, 50, 0.4);
				box-shadow:         0px 5px 5px 0px rgba(50, 50, 50, 0.4);
			}
			#header img {
				width: 320px;
			}
			#main {
				bottom: 0;
				background-color: transparent;
			}

			#footer {
				position: absolute;
				left: 0;
				right: 0;
				bottom: 0;
				height: 18px;
				box-sizing: border-box;
				padding-left: 15%;
				padding-right: 15%;
				font-size: .9em;
				background-color: #00B200;
			}
			#footer span:last-child {
				display: inline-block;
				float: right;
			}
			#header, #footer {
				color: #FFFFFF;
				font-weight: normal;
				text-shadow: none;
			}
			
			#main_content {
				position: absolute;
				top: 70px;
				bottom: 17px;
				left: 0;
				right: 0;
				overflow-y: auto;
				padding:2em 0 2em 0;
			}

			.content-wrapper {
				margin: auto;
				max-width: 70%;
				color: #888888;
				text-align: center;
				box-sizing: border-box;
				background-color: rgba(246, 246, 246, .75);
				margin-top: 0;
				margin-bottom: 0;
				padding: 1em 3em 1em 3em;
				min-height: 100%;
			}

			a {
				color: #FFFFFF;
				text-decoration: none;
			}

			a:hover {
				color: #AAAAAA;
				text-decoration: none;
			}

			a:active {
				color: #CCCCCC;
				text-decoration: none;
			}

			a:visited {
				color: #888888;
				text-decoration: none;
			}
			
			#main a {
				text-decoration: none;
				color: #0080C0;
			}
			#main a:hover {
				text-decoration: none;
				color: #0080C0;
			}
			#main a:active {
				text-decoration: none;
				color: #0080C0;
			}
			#main a:visited {
				text-decoration: none;
				color: #0080C0;
			}
		</style>
	</head>
	<body>
		<div id="header">
			<img src="cid:logo.png" alt="MetaRank" />
		</div><!-- /header -->
		<div id="main">
			<div id="main_content" class="ui-content">
				<div class="content-wrapper">
					<p class="black-text larger-message">Verify Email</p>
					<br />
					<div class="grey-box">
						<br />
						<p>Please verify your email by clicking or pasting into your browser address bar the following link:</p>
						<blockquote><a href="https://dev.metarank.com/account/verify/?uuid=$userID">https://dev.metarank.com/account/verify/?uuid=$userID</a></blockquote>
						<br />
					</div>
					<p>Thank you for using <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>!</p>
				</div>
				$password_string
				<div></div>
			</div>
		</div>
		<div id="footer">
			<span>Use confirms acceptance of <a href="https://dev.metarank.com/tou/">Terms of Use</a> and <a href="https://dev.metarank.com/privacy/">Privacy Policy</a>.</span>
			<span>MetaRank &copy; 2015</span>
		</div><!-- /footer -->
	</body>
</html>

--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y
Content-Location: CID:ignored0000
Content-ID: <MetaRankBottom.png>
Content-Type: IMAGE/PNG
Content-Transfer-Encoding: BASE64

$metaRankBottomPNG

--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y
Content-Location: CID:ignored0001
Content-ID: <logo.png>
Content-Type: IMAGE/PNG
Content-Transfer-Encoding: BASE64

$logoPNG

--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y--
EOD;
	mail($email, "MetaRank - Verify Email", $emailContent, $headers);
}

?>
