<?php
	require_once(dirname(__FILE__) . '/LinkedIn/LinkedIn.php');
	
	class MRLinkedIn extends LinkedIn {
		/**
		 * @var string Prefix to use for session variables
		 */
		private $sessionPrefix = 'LIRLH_';
		
		/**
		 * @var string State token for CSRF validation
		 */
		protected $state;
		
		public function getLoginUrl(array $scope = array(), $state = null) {
			$this->state = "linkedIn_" . $this->random(16);
			$this->storeState($this->state);
			return parent::getLoginUrl($scope, $this->state);
		}
		
		public function processRedirect() {
			$this->loadState();
			if ($this->isValidRedirect()) {
				return parent::getAccessToken($_GET["code"]);
			} else {
				return null;
			}
		}
		 
		/**
		 * Stores a state string in session storage for CSRF protection.
		 * Developers should subclass and override this method if they want to store
		 *   this state in a different location.
		 *
		 * @param string $state
		 */
		protected function storeState($state)
		{
			$_SESSION[$this->sessionPrefix . 'state'] = $state;
		}
		
		/**
		 * Loads a state string from session storage for CSRF validation.  May return
		 *   null if no object exists.  Developers should subclass and override this
		 *   method if they want to load the state from a different location.
		 *
		 * @return string|null
		 *
		 * @throws FacebookSDKException
		 */
		protected function loadState()
		{
			if (isset($_SESSION[$this->sessionPrefix . 'state'])) {
				$this->state = $_SESSION[$this->sessionPrefix . 'state'];
				return $this->state;
			}
			return null;
		}
		 
		/**
		 * Check if a redirect has a valid state.
		 *
		 * @return bool
		 */
		protected function isValidRedirect()
		{
			return $this->getCode() && isset($_GET['state']) && $_GET['state'] == $this->state;
		}
		
		/**
		 * Return the code.
		 *
		 * @return string|null
		 */
		protected function getCode()
		{
			return isset($_GET['code']) ? $_GET['code'] : null;
		}
		
		  /**
		  * Generate a cryptographically secure pseudrandom number
		  * 
		  * @param integer $bytes - number of bytes to return
		  * 
		  * @return string
		  * 
		  * @todo Support Windows platforms
		  */
		public function random($bytes)
		{
			if (!is_numeric($bytes)) {
				return "";
			}
			if ($bytes < 1) {
				return "";
			}
			$buf = '';
			// http://sockpuppet.org/blog/2014/02/25/safely-generate-random-numbers/
			if (!ini_get('open_basedir') && is_readable('/dev/urandom')) {
				$fp = fopen('/dev/urandom', 'rb');
				if ($fp !== FALSE) {
					$buf = fread($fp, $bytes);
					fclose($fp);
					if($buf !== FALSE) {
						return bin2hex($buf);
					}
				}
			}

			if (function_exists('mcrypt_create_iv')) {
				$buf = mcrypt_create_iv($bytes, MCRYPT_DEV_URANDOM);
				if ($buf !== FALSE) {
					return bin2hex($buf);
				}
			}

			while (strlen($buf) < $bytes) {
				$buf .= md5(uniqid(mt_rand(), true), true); 
				// We are appending raw binary
			}
			return bin2hex(substr($buf, 0, $bytes));
		}
	}
	
	$linkedinSession = new MRLinkedIn(
		array(
			'api_key' => LINKEDIN_APP_ID, 
			'api_secret' => LINKEDIN_SECRET, 
			'callback_url' => curHostURL() . "/metarank/htdocs/"
		)
	);
?>