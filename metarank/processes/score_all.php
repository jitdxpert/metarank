#!/usr/bin/php
<?php
	require_once(dirname(__FILE__) . '/../phpinc/db.php');
	require_once(BASE_PATH . '/../phpinc/lib/li/linkedin.php');
	require_once(BASE_PATH . '/../phpinc/lib/tw/twitter.php');
	
	class Connect extends Worker {
		public function __construct() {
		}
		
		public function start($options = NULL) {
			return parent::start(isset($options) ? $options : array(PTHREADS_INHERIT_ALL|PTHREADS_ALLOW_HEADERS, PTHREADS_INHERIT_ALL|PTHREADS_ALLOW_HEADERS, PTHREADS_INHERIT_ALL|PTHREADS_ALLOW_HEADERS));
		}
		
		public function getDatabase() {
			if (!self::$db) {
				self::$db = new MetaRankDatabase();
			}
			
			return self::$db;
		}
		
		/**
		 * Note that the link is stored statically, which for pthreads, means thread local
		 **/
		protected static $db;
	}
	
	class FacebookThread extends Thread {
		public $returnCode;
		
		protected $_userID;
		protected $_accessToken;
		
		public function __construct($userID, $accessToken) {
			$this->_userID = $userID;
			$this->_accessToken = $accessToken;
		}

		public function run() {
			if ($this->_accessToken !== null) {
				$this->worker->getDatabase()->scoreFacebook($this->_userID, $this->_accessToken);
				
				$this->worker->getDatabase()->commitChanges();
			}

			$this->returnCode = 0;
		}
	}
	
	class LinkedInThread extends Thread {
		public $returnCode;
		
		protected $_userID;
		protected $_accessToken;
		
		public function __construct($userID, $accessToken) {
			$this->_userID = $userID;
			$this->_accessToken = $accessToken;
		}

		public function run() {
			if ($this->_accessToken !== null) {
				$session = new MRLinkedIn(
					array(
						'api_key' => LINKEDIN_APP_ID, 
						'api_secret' => LINKEDIN_SECRET, 
						'callback_url' => "/" // This is not used for scoring, so it does not have to be accurate
					)
				);
				$session->setAccessToken($this->_accessToken);
		
				try {
					$information = $session->get("/people/~:(email-address,last-modified-timestamp,id,first-name,last-name,maiden-name,formatted-name,headline,location:(name),industry,current-share,num-connections,num-connections-capped,summary,specialties,positions,picture-url,site-standard-profile-request,api-standard-profile-request:(headers),public-profile-url,phone-numbers,bound-account-types,im-accounts,main-address,twitter-accounts,primary-twitter-account,connections,group-memberships,proposal-comments,associations,honors,interests,publications,patents,languages,skills,certifications,educations,courses,volunteer,three-current-positions,three-past-positions,num-recommenders,recommendations-received,mfeed-rss-url,following,job-bookmarks,suggestions,date-of-birth,member-url-resources,related-profile-views,network)");
					
					$this->worker->getDatabase()->scoreLinkedIn($this->_userID, $information);
					
					$this->worker->getDatabase()->commitChanges();
					
					$this->returnCode = 0;
				} catch (\RuntimeException $re) {
					//TODO: Log this error
					
					$this->returnCode = 1;
				}
			} else {
				$this->returnCode = 0;
			}
		}
	}
	
	class TwitterThread extends Thread {
		public $returnCode;
		
		protected $_userID;
		protected $_oAuthToken;
		protected $_oAuthTokenSecret;
		
		public function __construct($userID, $oAuthToken, $oAuthTokenSecret) {
			$this->_userID = $userID;
			$this->_oAuthToken = $oAuthToken;
			$this->_oAuthTokenSecret = $oAuthTokenSecret;
		}

		public function run() {
			if ($this->_oAuthToken !== null && $this->_oAuthTokenSecret !== null) {
				$this->worker->getDatabase()->scoreTwitter($this->_userID, $this->_oAuthToken, $this->_oAuthTokenSecret);
				
				$this->worker->getDatabase()->commitChanges();
			}
			
			$this->returnCode = 0;
		}
	}
	
	$userScoringDetails = $metaRankDatabase->getAllUserScoringDetails();
	if ($userScoringDetails != null && count($userScoringDetails) > 0) {
		$pool = new Pool(3, "Connect");
		
		foreach ($userScoringDetails as $userScoringDetail) {
			if ($userScoringDetail["facebook_access_token"] != null) {
				$facebookThread = new FacebookThread($userScoringDetail["id"], $userScoringDetail["facebook_access_token"]);
				$pool->submit($facebookThread);
			} else {
				$facebookThread = null;
			}
			
			if ($userScoringDetail["linkedin_oauth_token"] != null) {
				$linkedinThread = new LinkedInThread($userScoringDetail["id"], $userScoringDetail["linkedin_oauth_token"]);
				$pool->submit($linkedinThread);
			} else {
				$linkedinThread = null;
			}
			
			if ($userScoringDetail["twitter_oauth_token"] != null) {
				$twitterThread = new TwitterThread($userScoringDetail["id"], $userScoringDetail["twitter_oauth_token"], $userScoringDetail["twitter_oauth_token_secret"]);
				$pool->submit($twitterThread);
			} else {
				$twitterThread = null;
			}
			
			if ($facebookThread != null) {
				$facebookThread->join();
			}
			
			if ($linkedinThread != null) {
				$linkedinThread->join();
			}
			
			if ($twitterThread != null) {
				$twitterThread->join();
			}
			
			if ($facebookThread != null || $linkedinThread != null || $twitterThread != null) {
				$metaRankDatabase->updateMetaScore($userScoringDetail["id"]);
				
				$metaRankDatabase->commitChanges();
			}
		}
		
		foreach ($userScoringDetails as $userScoringDetail) {
			$metaRankDatabase->rankGlobalFacebook($userScoringDetail["id"]);
			$metaRankDatabase->rankGlobalLinkedIn($userScoringDetail["id"]);
			$metaRankDatabase->rankGlobalTwitter($userScoringDetail["id"]);
			
			$metaRankDatabase->rankGlobal($userScoringDetail["id"]);
		}
		
		$metaRankDatabase->computeGlobalFacebookParticipants();
		$metaRankDatabase->computeGlobalLinkedInParticipants();
		$metaRankDatabase->computeGlobalTwitterParticipants();
		
		$metaRankDatabase->computeGlobalParticipants();
	
		$metaRankDatabase->commitChanges();
		
		$pool->shutdown();
	}

	exit;
?>
