<?php
        // If we spit out an error, it wont let us change sessions below!
        error_reporting(0);

        require_once(dirname(__FILE__) . "/../phpinc/db.php");

        $expiredSessions = $metaRankDatabase->getExpiredSessions();

        foreach ($expiredSessions as $session) {
			$metaRankDatabase->untrackSession($session["id"]);

			session_id($session["session_id"]);
			session_start();

			$_SESSION = array();

			session_destroy();

			$metaRankDatabase->commitChanges();
        }
        exit();
?>
