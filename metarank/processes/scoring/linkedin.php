#!/usr/bin/php
<?php
if (!isset($argv[1]) || !isset($argv[2]))
{
	exit;
}
else
{
	require_once(dirname(dirname(__FILE__)) . '/../phpinc/db.php');
	require_once(BASE_PATH . '/../phpinc/lib/li/linkedin.php');
	
	$linkedinSession->setAccessToken($argv[2]);
	
	try {
		$information = $linkedinSession->get("/people/~:(email-address,last-modified-timestamp,id,first-name,last-name,maiden-name,formatted-name,headline,location:(name),industry,current-share,num-connections,num-connections-capped,summary,specialties,positions,picture-url,site-standard-profile-request,api-standard-profile-request:(headers),public-profile-url,phone-numbers,bound-account-types,im-accounts,main-address,twitter-accounts,primary-twitter-account,connections,group-memberships,proposal-comments,associations,honors,interests,publications,patents,languages,skills,certifications,educations,courses,volunteer,three-current-positions,three-past-positions,num-recommenders,recommendations-received,mfeed-rss-url,following,job-bookmarks,suggestions,date-of-birth,member-url-resources,related-profile-views,network)");
		
		$metaRankDatabase->scoreLinkedIn($argv[1], $information);
	
		$metaRankDatabase->commitChanges();
	} catch (\RuntimeException $re) {
		//TODO: Log this error
	}

	exit;
}
?>