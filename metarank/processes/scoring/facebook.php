#!/usr/bin/php
<?php
if (!isset($argv[1]) || !isset($argv[2]))
{
	exit;
}
else
{
	require_once(dirname(dirname(__FILE__)) . '/../phpinc/db.php');
	
	$metaRankDatabase->scoreFacebook($argv[1], $argv[2]);
	
	$metaRankDatabase->commitChanges();

	exit;
}
?>