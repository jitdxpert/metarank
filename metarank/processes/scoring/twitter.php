#!/usr/bin/php
<?php
if (!isset($argv[1]) || !isset($argv[2]) || !isset($argv[3]))
{
	exit;
}
else
{
	require_once(dirname(dirname(__FILE__)) . '/../phpinc/db.php');
	require_once(BASE_PATH . "/../phpinc/lib/tw/twitter.php");
	
	$metaRankDatabase->scoreTwitter($argv[1], $argv[2], $argv[3]);
	
	$metaRankDatabase->commitChanges();

	exit;
}
?>