<?php
        error_reporting(0);

        if ($argc == 0) {
                exit();
        }

        require_once(dirname(__FILE__) . "/../phpinc/db.php");

        $userSessions = $metaRankDatabase->getUserSessions($argv[1]);

        foreach ($userSessions as $session) {
                $metaRankDatabase->untrackSession($session["id"]);

                session_id($session["session_id"]);
                session_start();

                $_SESSION = array();

                session_destroy();

                $metaRankDatabase->commitChanges();
        }

        exit();
?>
