<?php
	// Turn off errors so sessions will always be able to start without complaining about headers already being sent
	error_reporting(0);

	if ($argc == 0) {
		exit();
	}
	
	session_id($argv[1]);
	session_start();
	
	if ($argv[2] == 'true') {
		// Update their session variables
		$_SESSION["userFirstName"] = $argv[3];
		$_SESSION["userLastName"] = $argv[4];
		$_SESSION["userPhone"] = $argv[[5];
	} else {
		// Log them out
		require_once(dirname(dirname(__FILE__)) .  "/phpinc/db.php");
		
		untrackSession($_SESSION["sessionTrackingID"]);

		$_SESSION = array();

		session_destroy();

		commitChanges();
	}
?>
