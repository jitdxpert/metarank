<?php
	error_reporting(0);

	if ($argc == 1) {
		exit();
	}

	require_once(dirname(dirname(__FILE__)) .  "/phpinc/db.php");

	$userSessions = getUserSessions($argv[1]);

	foreach ($userSessions as $session) {
		untrackSession($session["id"]);

		session_id($session["session_id"]);
		session_start();

		$_SESSION = array();

		session_destroy();

		commitChanges();		
	}

	exit();
?>
