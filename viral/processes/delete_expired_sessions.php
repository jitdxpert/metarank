<?php
	// If we spit out an error, it wont let us change sessions below!
	error_reporting(0);

	require_once(dirname(dirname(__FILE__)) .  "/phpinc/db.php");
	
	$expiredSessions = getExpiredSessions();

	foreach ($expiredSessions as $session) {
		untrackSession($session["id"]);
		
		session_id($session["session_id"]);
		session_start();

		$_SESSION = array();
		
		session_destroy();

		commitChanges();
	}
	exit();
?>
