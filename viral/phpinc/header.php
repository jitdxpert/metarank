<!DOCTYPE html>
<html>
	<head>
		<title>Viral by MetaRank</title>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="//code.jquery.com/mobile/1.4.4/jquery.mobile-1.4.4.min.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/lib/jquery-mobile/plugins/datetimepicker/DateTimePicker.css">
		<script src="//code.jquery.com/jquery-1.11.1.min.js"></script> 
		<script src="//code.jquery.com/mobile/1.4.4/jquery.mobile-1.4.4.js"></script>
		<script src="<?php echo BASE_URL; ?>/lib/jquery-mobile/plugins/datetimepicker/DateTimePicker.js"></script>
		<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
		<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyAVeqwKYjgYhuu8VkYVcnCn1TJxlyNy9Ro"></script>
		<script src="//www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
		<!--For testing: <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBpB8yfgQPnAHQbyb4kTfCZv8txgq2bi8A"></script>-->
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/css/core.css" />
        <input type="hidden" value="<?php echo BASE_URL; ?>" id="BASE_URL" name="BASE_URL" />
		<script src="<?php echo BASE_URL; ?>/js/core.js"></script>
	</head>
	<body>
		<header data-role="header" data-position="fixed" data-theme="a" data-tap-toggle="false">
			<img src="<?php echo BASE_URL; ?>/img/logo.png" alt="MetaRank" />
		</header><!-- /header -->