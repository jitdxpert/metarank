<?php

require_once(dirname(__FILE__) . "/defines.php");

$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);

// Check connection
if (mysqli_connect_errno()) {
	if (isset($success) && isset($message) && isset($commands)) {
		$success = false;
		$message = "We are currently experiencing technical difficulties, and are unable to complete your request. Please try again later.";
		
		header('Content-Type: application/json');
	
		echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
	} else {
		include(dirname(__FILE__) . "/errors/technical_difficulties.php");
	}
	die("Failed to connect to MySQL: " . mysqli_connect_error());
}

mysqli_autocommit($conn, false);

// We want UTC timezone
mysqli_query($conn, "SET time_zone = '+0:00'");

function commitChanges() {
	global $conn;
	
	return mysqli_commit($conn);
}

function rollbackChanges() {
	global $conn;
	
	return mysqli_rollback($conn);
}

function getLock($key, $timeout) {
	global $conn;
	
	$result = null;
	
	$stmt = mysqli_prepare($conn, "SELECT GET_LOCK(?, ?) AS success");
	
	mysqli_stmt_bind_param($stmt, "si", $key, ($timeout != null ? $timeout : LOCK_TIMEOUT));
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result = $row["success"];
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function releaseLock($key) {
	global $conn;
	
	$result = null;
	
	$stmt = mysqli_prepare($conn, "SELECT RELEASE_LOCK(?) AS success");
	
	mysqli_stmt_bind_param($stmt, "s", $key);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result = $row["success"];
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getEligiblePromotionMonths($accountID) {
	global $conn;
	
	$result = 0;
	
	$stmt = mysqli_prepare($conn, "SELECT COUNT(promotions.id) AS used_promotion_months FROM promotions LEFT JOIN business_promotions ON promotions.id = business_promotions.promotion_id LEFT JOIN businesses ON business_promotions.business_id = businesses.id WHERE businesses.account_id = UNHEX(?) GROUP BY YEAR(promotions.start_time), MONTH(promotions.start_time)");
	
	mysqli_stmt_bind_param($stmt, "s", $accountID);
	
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		$usedPromotionMonths = mysqli_num_rows($recordSet);
	}
	
	mysqli_stmt_close($stmt);
	
	if (isset($usedPromotionMonths)) {
		$stmt = mysqli_prepare($conn, "SELECT COUNT(perks.id) AS num_perks FROM perks LEFT JOIN business_perks ON perks.id = business_perks.perk_id LEFT JOIN businesses ON business_perks.business_id = businesses.id WHERE businesses.account_id = UNHEX(?)");
		
		mysqli_stmt_bind_param($stmt, "s", $accountID);
		
		if (mysqli_stmt_execute($stmt)) {
			$recordSet = mysqli_stmt_get_result($stmt);
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result = $row["num_perks"] - $usedPromotionMonths;
			}
		}
		
		mysqli_stmt_close($stmt);
	}
	
	return $result;
}

function getScheduledPerks($businessID) {
	global $conn;
	
	$result = array();
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(perks.id) AS id, perks.name, perks.description, perks.notes, perks.value, businesses.name AS business_name, businesses.website AS business_website, currencies.abbreviation AS currency_abbreviation, DATE_FORMAT(perks.start_time, '%Y-%m-%dT%H:%i:%sZ') AS start_time, perks.duration, perks.eligibility_distance, distance_units.name AS eligibility_distance_unit_name, IFNULL(services.name, 'All Services') AS service_name, perks.max_recipients, perks.signatory, DATE_FORMAT(perks.date_signed, '%Y-%m-%dT%H:%i:%sZ') AS date_signed FROM perks LEFT JOIN business_perks ON perks.id = business_perks.perk_id LEFT JOIN businesses ON business_perks.business_id = businesses.id LEFT JOIN currencies ON perks.value_currency_id = currencies.id LEFT JOIN distance_units ON perks.eligibility_distance_unit_id = distance_units.id LEFT JOIN services ON perks.service_id = services.id WHERE businesses.id = UNHEX(?) AND perks.start_time > NOW() ORDER BY perks.date_signed DESC");
	
	mysqli_stmt_bind_param($stmt, "s", $businessID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			if ($row["eligibility_distance"] != "0") {
				$row["eligibility_addresses"] = array();
				
				$addressRecordSet = mysqli_query($conn, "SELECT HEX(perk_eligibility_addresses.id) AS id, perk_eligibility_addresses.thoroughfare, perk_eligibility_addresses.premise, perk_eligibility_addresses.locality, perk_eligibility_addresses.dependent_locality, perk_eligibility_addresses.administrative_area, perk_eligibility_addresses.postal_code, HEX(perk_eligibility_addresses.country_id) AS country_id, countries.name AS country_name, perk_eligibility_addresses.latitude, perk_eligibility_addresses.longitude FROM perk_eligibility_addresses LEFT JOIN countries ON perk_eligibility_addresses.country_id = countries.id WHERE perk_eligibility_addresses.perk_id = UNHEX('" . $row["id"] . "')");
				
				while ($row2 = mysqli_fetch_array($addressRecordSet)) {
					$row["eligibility_addresses"][] = $row2;
				}
				
				mysqli_free_result($addressRecordSet);
			}
			$result[] = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getActivePerks($businessID) {
	global $conn;
	
	$result = array();
	
	$stmt = mysqli_prepare($conn, "SELECT NOW() as now, HEX(perks.id) AS id, perks.name, perks.description, perks.notes, perks.value, businesses.name AS business_name, businesses.website AS business_website, currencies.abbreviation AS currency_abbreviation, DATE_FORMAT(perks.start_time, '%Y-%m-%dT%H:%i:%sZ') AS start_time, perks.duration, perks.eligibility_distance, distance_units.name AS eligibility_distance_unit_name, IFNULL(services.name, 'All Services') AS service_name, perks.max_recipients, perks.signatory, DATE_FORMAT(perks.date_signed, '%Y-%m-%dT%H:%i:%sZ') AS date_signed FROM perks LEFT JOIN business_perks ON perks.id = business_perks.perk_id LEFT JOIN businesses ON business_perks.business_id = businesses.id LEFT JOIN currencies ON perks.value_currency_id = currencies.id LEFT JOIN distance_units ON perks.eligibility_distance_unit_id = distance_units.id LEFT JOIN services ON perks.service_id = services.id WHERE businesses.id = UNHEX(?) AND perks.start_time > DATE_SUB(NOW(), INTERVAL perks.duration SECOND) AND perks.start_time <= NOW() ORDER BY perks.date_signed DESC");
	
	mysqli_stmt_bind_param($stmt, "s", $businessID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			if ($row["eligibility_distance"] != "0") {
				$row["eligibility_addresses"] = array();
				
				$addressRecordSet = mysqli_query($conn, "SELECT HEX(perk_eligibility_addresses.id) AS id, perk_eligibility_addresses.thoroughfare, perk_eligibility_addresses.premise, perk_eligibility_addresses.locality, perk_eligibility_addresses.dependent_locality, perk_eligibility_addresses.administrative_area, perk_eligibility_addresses.postal_code, HEX(perk_eligibility_addresses.country_id) AS country_id, countries.name AS country_name, perk_eligibility_addresses.latitude, perk_eligibility_addresses.longitude FROM perk_eligibility_addresses LEFT JOIN countries ON perk_eligibility_addresses.country_id = countries.id WHERE perk_eligibility_addresses.perk_id = UNHEX('" . $row["id"] . "')");
				
				while ($row2 = mysqli_fetch_array($addressRecordSet)) {
					$row["eligibility_addresses"][] = $row2;
				}
				
				mysqli_free_result($addressRecordSet);
			}
			$result[] = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getCompletedPerks($businessID) {
	global $conn;
	
	$result = array();
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(perks.id) AS id, perks.name, perks.description, perks.notes, perks.value, businesses.name AS business_name, businesses.website AS business_website, currencies.abbreviation AS currency_abbreviation, DATE_FORMAT(perks.start_time, '%Y-%m-%dT%H:%i:%sZ') AS start_time, perks.duration, perks.eligibility_distance, distance_units.name AS eligibility_distance_unit_name, IFNULL(services.name, 'All Services') AS service_name, perks.max_recipients, perks.signatory, DATE_FORMAT(perks.date_signed, '%Y-%m-%dT%H:%i:%sZ') AS date_signed FROM perks LEFT JOIN business_perks ON perks.id = business_perks.perk_id LEFT JOIN businesses ON business_perks.business_id = businesses.id LEFT JOIN currencies ON perks.value_currency_id = currencies.id LEFT JOIN distance_units ON perks.eligibility_distance_unit_id = distance_units.id LEFT JOIN services ON perks.service_id = services.id WHERE businesses.id = UNHEX(?) AND perks.start_time <= DATE_SUB(NOW(), INTERVAL perks.duration SECOND) ORDER BY perks.date_signed DESC");
	
	mysqli_stmt_bind_param($stmt, "s", $businessID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			if ($row["eligibility_distance"] != "0") {
				$row["eligibility_addresses"] = array();
				
				$addressRecordSet = mysqli_query($conn, "SELECT HEX(perk_eligibility_addresses.id) AS id, perk_eligibility_addresses.thoroughfare, perk_eligibility_addresses.premise, perk_eligibility_addresses.locality, perk_eligibility_addresses.dependent_locality, perk_eligibility_addresses.administrative_area, perk_eligibility_addresses.postal_code, HEX(perk_eligibility_addresses.country_id) AS country_id, countries.name AS country_name, perk_eligibility_addresses.latitude, perk_eligibility_addresses.longitude FROM perk_eligibility_addresses LEFT JOIN countries ON perk_eligibility_addresses.country_id = countries.id WHERE perk_eligibility_addresses.perk_id = UNHEX('" . $row["id"] . "')");
				
				while ($row2 = mysqli_fetch_array($addressRecordSet)) {
					$row["eligibility_addresses"][] = $row2;
				}
				
				mysqli_free_result($addressRecordSet);
			}
			$result[] = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getScheduledPromotions($businessID) {
	global $conn;
	
	$result = array();
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(promotions.id) AS id, promotions.name, promotions.description, promotions.notes, promotions.value, businesses.name AS business_name, businesses.website AS business_website, currencies.abbreviation AS currency_abbreviation, DATE_FORMAT(promotions.start_time, '%Y-%m-%dT%H:%i:%sZ') AS start_time, promotions.duration, promotions.eligibility_distance, distance_units.name AS eligibility_distance_unit_name, services.name AS service_name, promotion_checkin_options.location AS checkin_location, promotion_checkin_options.hashtag AS checkin_hashtag, promotion_post_options.link AS post_link, promotion_like_options.link AS like_link, promotions.max_recipients, promotions.signatory, DATE_FORMAT(promotions.date_signed, '%Y-%m-%dT%H:%i:%sZ') AS date_signed FROM promotions LEFT JOIN business_promotions ON promotions.id = business_promotions.promotion_id LEFT JOIN businesses ON business_promotions.business_id = businesses.id LEFT JOIN currencies ON promotions.value_currency_id = currencies.id LEFT JOIN distance_units ON promotions.eligibility_distance_unit_id = distance_units.id LEFT JOIN services ON promotions.service_id = services.id LEFT JOIN promotion_checkin_options ON promotions.id = promotion_checkin_options.promotion_id LEFT JOIN promotion_post_options ON promotions.id = promotion_post_options.promotion_id LEFT JOIN promotion_like_options ON promotions.id = promotion_like_options.promotion_id WHERE businesses.id = UNHEX(?) AND promotions.start_time > NOW() ORDER BY promotions.date_signed DESC");
	
	mysqli_stmt_bind_param($stmt, "s", $businessID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			if ($row["eligibility_distance"] != "0") {
				$row["eligibility_addresses"] = array();
				
				$addressRecordSet = mysqli_query($conn, "SELECT HEX(promotion_eligibility_addresses.id) AS id, promotion_eligibility_addresses.thoroughfare, promotion_eligibility_addresses.premise, promotion_eligibility_addresses.locality, promotion_eligibility_addresses.dependent_locality, promotion_eligibility_addresses.administrative_area, promotion_eligibility_addresses.postal_code, HEX(promotion_eligibility_addresses.country_id) AS country_id, countries.name AS country_name, promotion_eligibility_addresses.latitude, promotion_eligibility_addresses.longitude FROM promotion_eligibility_addresses LEFT JOIN countries ON promotion_eligibility_addresses.country_id = countries.id WHERE promotion_eligibility_addresses.promotion_id = UNHEX('" . $row["id"] . "')");
				
				while ($row2 = mysqli_fetch_array($addressRecordSet)) {
					$row["eligibility_addresses"][] = $row2;
				}
				
				mysqli_free_result($addressRecordSet);
			}
			$result[] = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getActivePromotions($businessID) {
	global $conn;
	
	$result = array();
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(promotions.id) AS id, promotions.name, promotions.description, promotions.notes, promotions.value, businesses.name AS business_name, businesses.website AS business_website, currencies.abbreviation AS currency_abbreviation, DATE_FORMAT(promotions.start_time, '%Y-%m-%dT%H:%i:%sZ') AS start_time, promotions.duration, promotions.eligibility_distance, distance_units.name AS eligibility_distance_unit_name, services.name AS service_name, promotion_checkin_options.location AS checkin_location, promotion_checkin_options.hashtag AS checkin_hashtag, promotion_post_options.link AS post_link, promotion_like_options.link AS like_link, promotions.max_recipients, promotions.signatory, DATE_FORMAT(promotions.date_signed, '%Y-%m-%dT%H:%i:%sZ') AS date_signed FROM promotions LEFT JOIN business_promotions ON promotions.id = business_promotions.promotion_id LEFT JOIN businesses ON business_promotions.business_id = businesses.id LEFT JOIN currencies ON promotions.value_currency_id = currencies.id LEFT JOIN distance_units ON promotions.eligibility_distance_unit_id = distance_units.id LEFT JOIN services ON promotions.service_id = services.id LEFT JOIN promotion_checkin_options ON promotions.id = promotion_checkin_options.promotion_id LEFT JOIN promotion_post_options ON promotions.id = promotion_post_options.promotion_id LEFT JOIN promotion_like_options ON promotions.id = promotion_like_options.promotion_id WHERE businesses.id = UNHEX(?) AND promotions.start_time > DATE_SUB(NOW(), INTERVAL promotions.duration SECOND) AND promotions.start_time <= NOW() ORDER BY promotions.date_signed DESC");
	
	mysqli_stmt_bind_param($stmt, "s", $businessID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			if ($row["eligibility_distance"] != "0") {
				$row["eligibility_addresses"] = array();
				
				$addressRecordSet = mysqli_query($conn, "SELECT HEX(promotion_eligibility_addresses.id) AS id, promotion_eligibility_addresses.thoroughfare, promotion_eligibility_addresses.premise, promotion_eligibility_addresses.locality, promotion_eligibility_addresses.dependent_locality, promotion_eligibility_addresses.administrative_area, promotion_eligibility_addresses.postal_code, HEX(promotion_eligibility_addresses.country_id) AS country_id, countries.name AS country_name, promotion_eligibility_addresses.latitude, promotion_eligibility_addresses.longitude FROM promotion_eligibility_addresses LEFT JOIN countries ON promotion_eligibility_addresses.country_id = countries.id WHERE promotion_eligibility_addresses.promotion_id = UNHEX('" . $row["id"] . "')");
				
				while ($row2 = mysqli_fetch_array($addressRecordSet)) {
					$row["eligibility_addresses"][] = $row2;
				}
				
				mysqli_free_result($addressRecordSet);
			}
			$result[] = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getCompletedPromotions($businessID) {
	global $conn;
	
	$result = array();
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(promotions.id) AS id, promotions.name, promotions.description, promotions.notes, promotions.value, businesses.name AS business_name, businesses.website AS business_website, currencies.abbreviation AS currency_abbreviation, DATE_FORMAT(promotions.start_time, '%Y-%m-%dT%H:%i:%sZ') AS start_time, promotions.duration, promotions.eligibility_distance, distance_units.name AS eligibility_distance_unit_name, services.name AS service_name, promotion_checkin_options.location AS checkin_location, promotion_checkin_options.hashtag AS checkin_hashtag, promotion_post_options.link AS post_link, promotion_like_options.link AS like_link, promotions.max_recipients, promotions.signatory, DATE_FORMAT(promotions.date_signed, '%Y-%m-%dT%H:%i:%sZ') AS date_signed FROM promotions LEFT JOIN business_promotions ON promotions.id = business_promotions.promotion_id LEFT JOIN businesses ON business_promotions.business_id = businesses.id LEFT JOIN currencies ON promotions.value_currency_id = currencies.id LEFT JOIN distance_units ON promotions.eligibility_distance_unit_id = distance_units.id LEFT JOIN services ON promotions.service_id = services.id LEFT JOIN promotion_checkin_options ON promotions.id = promotion_checkin_options.promotion_id LEFT JOIN promotion_post_options ON promotions.id = promotion_post_options.promotion_id LEFT JOIN promotion_like_options ON promotions.id = promotion_like_options.promotion_id WHERE businesses.id = UNHEX(?) AND promotions.start_time <= DATE_SUB(NOW(), INTERVAL promotions.duration SECOND) ORDER BY promotions.date_signed DESC");
	
	mysqli_stmt_bind_param($stmt, "s", $businessID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			if ($row["eligibility_distance"] != "0") {
				$row["eligibility_addresses"] = array();
				
				$addressRecordSet = mysqli_query($conn, "SELECT HEX(promotion_eligibility_addresses.id) AS id, promotion_eligibility_addresses.thoroughfare, promotion_eligibility_addresses.premise, promotion_eligibility_addresses.locality, promotion_eligibility_addresses.dependent_locality, promotion_eligibility_addresses.administrative_area, promotion_eligibility_addresses.postal_code, HEX(promotion_eligibility_addresses.country_id) AS country_id, countries.name AS country_name, promotion_eligibility_addresses.latitude, promotion_eligibility_addresses.longitude FROM promotion_eligibility_addresses LEFT JOIN countries ON promotion_eligibility_addresses.country_id = countries.id WHERE promotion_eligibility_addresses.promotion_id = UNHEX('" . $row["id"] . "')");
				
				while ($row2 = mysqli_fetch_array($addressRecordSet)) {
					$row["eligibility_addresses"][] = $row2;
				}
				
				mysqli_free_result($addressRecordSet);
			}
			$result[] = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function createPerk($name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned) {
	global $conn;
	
	$result = null;
	
	$recordSet = mysqli_query($conn, "SELECT HEX(NEWID()) AS id");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$perkID = $row["id"];
	}
	
	mysqli_free_result($recordSet);
	
	if (isset($perkID)) {
		if ($notes != null) {
			if ($serviceID != null) {
				if ($distanceUnitID != null) {
					$stmt = mysqli_prepare($conn, "INSERT INTO perks SET name = ?, description = ?, notes = ?, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = UNHEX(?), service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
					mysqli_stmt_bind_param($stmt, "sssssssssssssss", $name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
				} else {
					$stmt = mysqli_prepare($conn, "INSERT INTO perks SET name = ?, description = ?, notes = ?, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = NULL, service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
					mysqli_stmt_bind_param($stmt, "ssssssssssssss", $name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
				}
			} else {
				if ($distanceUnitID != null) {
					$stmt = mysqli_prepare($conn, "INSERT INTO perks SET name = ?, description = ?, notes = ?, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = UNHEX(?), service_id = NULL, max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
					mysqli_stmt_bind_param($stmt, "ssssssssssssss", $name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
				} else {
					$stmt = mysqli_prepare($conn, "INSERT INTO perks SET name = ?, description = ?, notes = ?, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = NULL, service_id = NULL, max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
					mysqli_stmt_bind_param($stmt, "sssssssssssss", $name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
				}
			}
		} else {
			if ($serviceID != null) {
				if ($distanceUnitID != null) {
					$stmt = mysqli_prepare($conn, "INSERT INTO perks SET name = ?, description = ?, notes = NULL, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = UNHEX(?), service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
					mysqli_stmt_bind_param($stmt, "ssssssssssssss", $name, $description, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
				} else {
					$stmt = mysqli_prepare($conn, "INSERT INTO perks SET name = ?, description = ?, notes = NULL, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = NULL, service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
					mysqli_stmt_bind_param($stmt, "sssssssssssss", $name, $description, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
				}
			} else {
				if ($distanceUnitID != null) {
					$stmt = mysqli_prepare($conn, "INSERT INTO perks SET name = ?, description = ?, notes = NULL, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = UNHEX(?), service_id = NULL, max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
					mysqli_stmt_bind_param($stmt, "sssssssssssss", $name, $description, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
				} else {
					$stmt = mysqli_prepare($conn, "INSERT INTO perks SET name = ?, description = ?, notes = NULL, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = NULL, service_id = NULL, max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
					mysqli_stmt_bind_param($stmt, "ssssssssssss", $name, $description, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $perkID);
				}
			}
		}
		
		if (mysqli_stmt_execute($stmt)) {
			$result = $perkID;
		}
		
		mysqli_stmt_close($stmt);
	}
	
	return $result;
}

function createPerkEligibilityAddress($perkID, $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $countryID, $latitude, $longitude) {
	global $conn;
	
	$result = null;
	
	$recordSet = mysqli_query($conn, "SELECT HEX(NEWID()) AS id");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$perkEligibilityAddressID = $row["id"];
	}
	
	mysqli_free_result($recordSet);
	
	if (isset($perkEligibilityAddressID)) {
		$stmt = mysqli_prepare($conn, "INSERT INTO perk_eligibility_addresses SET perk_id = UNHEX(?), thoroughfare = ?, premise = ?, locality = ?, dependent_locality = ?, administrative_area = ?, postal_code = ?, country_id = UNHEX(?), latitude = ?, longitude = ?, id = UNHEX(?)");
		
		mysqli_stmt_bind_param($stmt, "sssssssssss", $perkID, $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $countryID, $latitude, $longitude, $perkEligibilityAddressID);
		
		if (mysqli_stmt_execute($stmt)) {
			$result = $perkEligibilityAddressID;
		}
		
		mysqli_stmt_close($stmt);
	}
	
	return $result;
}

function linkPerkToBusiness($perkID, $businessID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "INSERT INTO business_perks SET business_id = UNHEX(?), perk_id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ss", $businessID, $perkID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function createPromotion($name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned) {
	global $conn;

	$result = null;
	
	$recordSet = mysqli_query($conn, "SELECT HEX(NEWID()) AS id");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$promotionID = $row["id"];
	}
	
	mysqli_free_result($recordSet);
	
	if (isset($promotionID)) {
		if ($notes != null) {
			if ($distanceUnitID != null) {
				$stmt = mysqli_prepare($conn, "INSERT INTO promotions SET name = ?, description = ?, notes = ?, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = UNHEX(?), service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt, "sssssssssssssss", $name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $promotionID);
			} else {
				$stmt = mysqli_prepare($conn, "INSERT INTO promotions SET name = ?, description = ?, notes = ?, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = NULL, service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt, "ssssssssssssss", $name, $description, $notes, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $promotionID);
			}
		} else {
			if ($distanceUnitID != null) {
				$stmt = mysqli_prepare($conn, "INSERT INTO promotions SET name = ?, description = ?, notes = NULL, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = UNHEX(?), service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt, "ssssssssssssss", $name, $description, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $distanceUnitID, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $promotionID);
			} else {
				$stmt = mysqli_prepare($conn, "INSERT INTO promotions SET name = ?, description = ?, notes = NULL, value = ?, value_currency_id = UNHEX(?), start_time = ?, duration = ?, eligibility_distance = ?, eligibility_distance_unit_id = NULL, service_id = UNHEX(?), max_recipients = ?, signatory = ?, signatory_ip_address = LPAD(INET6_ATON(?), 16, 0x0), date_signed = ?, id = UNHEX(?)");
				mysqli_stmt_bind_param($stmt, "sssssssssssss", $name, $description, $value, $currencyID, $startTime, $duration, $eligibilityDistance, $serviceID, $maxRecipients, $signatory, $signatoryIPAddress, $dateSigned, $promotionID);
			}
		}
		
		if (mysqli_stmt_execute($stmt)) {
			$result = $promotionID;
		}
		
		mysqli_stmt_close($stmt);
	}
	
	return $result;
}

function createPromotionEligibilityAddress($promotionID, $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $countryID, $latitude, $longitude) {
	global $conn;
	
	$result = null;
	
	$recordSet = mysqli_query($conn, "SELECT HEX(NEWID()) AS id");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$promotionEligibilityAddressID = $row["id"];
	}
	
	mysqli_free_result($recordSet);
	
	if (isset($promotionEligibilityAddressID)) {
		$stmt = mysqli_prepare($conn, "INSERT INTO promotion_eligibility_addresses SET promotion_id = UNHEX(?), thoroughfare = ?, premise = ?, locality = ?, dependent_locality = ?, administrative_area = ?, postal_code = ?, country_id = UNHEX(?), latitude = ?, longitude = ?, id = UNHEX(?)");
		
		mysqli_stmt_bind_param($stmt, "sssssssssss", $promotionID, $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $countryID, $latitude, $longitude, $promotionEligibilityAddressID);
		
		if (mysqli_stmt_execute($stmt)) {
			$result = $promotionEligibilityAddressID;
		}
		
		mysqli_stmt_close($stmt);
	}
	
	return $result;
}

function linkPromotionToBusiness($promotionID, $businessID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "INSERT INTO business_promotions SET business_id = UNHEX(?), promotion_id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ss", $businessID, $promotionID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function createPromotionCheckinOption($promotionID, $checkinLocation, $checkinHashtag) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "INSERT INTO promotion_checkin_options SET promotion_id = UNHEX(?), location = ?, hashtag = ?");
	
	mysqli_stmt_bind_param($stmt, "sss", $promotionID, $checkinLocation, $checkinHashtag);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function createPromotionPostOption($promotionID, $link) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "INSERT INTO promotion_post_options SET promotion_id = UNHEX(?), link = ?");
	
	mysqli_stmt_bind_param($stmt, "ss", $promotionID, $link);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function createPromotionLikeOption($promotionID, $link) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "INSERT INTO promotion_like_options SET promotion_id = UNHEX(?), link = ?");
	
	mysqli_stmt_bind_param($stmt, "ss", $promotionID, $link);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getRoles($excludedRoles, $userID) {
	global $conn;
	
	$result = array();
	if ($userID != null) {
		if ($excludedRoles != null && count($excludedRoles) > 0) {
			$stmt = mysqli_prepare($conn, "SELECT HEX(roles.id) AS id, roles.name, IF(user_roles.role_id IS NULL, 0, 1) AS selected FROM roles LEFT JOIN user_roles ON roles.id = user_roles.role_id AND user_roles.user_id = UNHEX(?) WHERE roles.name NOT IN ('" . implode("', '", $excludedRoles) . "')");
		} else {
			$stmt = mysqli_prepare($conn, "SELECT HEX(roles.id) AS id, roles.name, IF(user_roles.role_id IS NULL, 0, 1) AS selected FROM roles LEFT JOIN user_roles ON roles.id = user_roles.role_id AND user_roles.user_id = UNHEX(?)");
		}
		mysqli_stmt_bind_param($stmt, "s", $userID);
	} else {
		if ($excludedRoles != null && count($excludedRoles) > 0) {
			$recordSet = mysqli_query($conn, "SELECT HEX(id) AS id, name FROM roles WHERE name NOT IN ('" . implode("', '", $excludedRoles) . "')");
		} else {
			$recordSet = mysqli_query($conn, "SELECT HEX(id) AS id, name FROM roles");
		}
	}
	if (isset($stmt)) {
		if (mysqli_stmt_execute($stmt)) {
			$recordSet = mysqli_stmt_get_result($stmt);
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
		}
		
		mysqli_stmt_close($stmt);
	} else {
		while ($row = mysqli_fetch_array($recordSet)) {
			$result[] = $row;
		}
		
		mysqli_free_result($recordSet);
	}
	
	return $result;
}

function checkBusinessInManagedBusinesses($businessID, $managerUserID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "SELECT user_id FROM business_users WHERE business_id = UNHEX(?) AND user_id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ss", $businessID, $managerUserID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result = true;
		}
	}
	
	return $result;
}

function checkUserInManagedBusiness($userID, $managerUserID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "SELECT target.user_id FROM business_users AS target LEFT JOIN business_users AS manager ON target.business_id = manager.business_id WHERE target.user_id = UNHEX(?) AND manager.user_id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ss", $userID, $managerUserID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result = true;
		}
	}
	
	return $result;
}

function checkBusinessInAccount($businessID, $accountID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "SELECT name FROM businesses WHERE id = UNHEX(?) AND account_id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ss", $businessID, $accountID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result = true;
		}
	}
	
	return $result;
}

function checkUserInAccount($userID, $accountID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "SELECT email FROM users WHERE id = UNHEX(?) AND account_id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ss", $userID, $accountID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result = true;
		}
	}
	
	return $result;
}

function getAccountBusinesses($accountID, $userID) {
	global $conn;
	
	$result = array();
	
	if ($userID != null) {
		$stmt = mysqli_prepare($conn, "SELECT HEX(businesses.id) AS id, businesses.name, IF(business_users.business_id IS NULL, 0, 1) AS selected FROM businesses LEFT JOIN business_users ON businesses.id = business_users.business_id AND business_users.user_id = UNHEX(?) WHERE businesses.account_id = UNHEX(?)");
		
		mysqli_stmt_bind_param($stmt, "ss", $userID, $accountID);
	} else {
		$stmt = mysqli_prepare($conn, "SELECT HEX(id) AS id, name FROM businesses WHERE account_id = UNHEX(?)");
		
		mysqli_stmt_bind_param($stmt, "s", $accountID);
	}
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
			
		while ($row = mysqli_fetch_array($recordSet)) {
			$result[] = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getUserBusinesses($userID, $selectedUserID) {
	global $conn;
	
	$result = array();
	
	if ($selectedUserID != null) {
		$stmt = mysqli_prepare($conn, "SELECT HEX(businesses.id) AS id, businesses.name, IF(target.business_id IS NULL, 0, 1) AS selected FROM businesses LEFT JOIN business_users ON businesses.id = business_users.business_id LEFT JOIN business_users AS target ON businesses.id = target.business_id AND target.user_id = UNHEX(?) WHERE business_users.user_id = UNHEX(?)");
		
		mysqli_stmt_bind_param($stmt, "ss", $selectedUserID, $userID);
	} else {
		$stmt = mysqli_prepare($conn, "SELECT HEX(businesses.id) AS id, businesses.name FROM businesses LEFT JOIN business_users ON businesses.id = business_users.business_id WHERE business_users.user_id = UNHEX(?)");
		
		mysqli_stmt_bind_param($stmt, "s", $userID);
	}
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
			
		while ($row = mysqli_fetch_array($recordSet)) {
			$result[] = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getAccountUsers($accountID, $userID) {
	global $conn;
	
	$result = array();
	
	if ($userID != null) {
		$stmt = mysqli_prepare($conn, "SELECT HEX(users.id) AS id, users.first_name, users.last_name, users.phone, users.email FROM users LEFT JOIN accounts ON users.account_id = accounts.id WHERE users.account_id = UNHEX(?) AND users.id != accounts.primary_user_id AND users.id != UNHEX(?)");
		mysqli_stmt_bind_param($stmt, "ss", $accountID, $userID);
	} else {
		$stmt = mysqli_prepare($conn, "SELECT HEX(users.id) AS id, users.first_name, users.last_name, users.phone, users.email FROM users LEFT JOIN accounts ON users.account_id = accounts.id WHERE users.account_id = UNHEX(?) AND users.id != accounts.primary_user_id");
		mysqli_stmt_bind_param($stmt, "s", $accountID);
	}
	
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
			
		while ($row = mysqli_fetch_array($recordSet)) {
			$result[] = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function changeUserPassword($userID, $password) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "UPDATE users SET password = ? WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ss", password_hash($password, PASSWORD_BCRYPT), $userID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	mysqli_stmt_close($stmt);
	
	return $result;
}

function changeUserSecurityQuestion($userID, $securityQuestionID, $securityAnswer) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "UPDATE users SET security_question_id = UNHEX(?), security_question_answer = ? WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "sss", $securityQuestionID, password_hash($securityAnswer, PASSWORD_BCRYPT), $userID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	mysqli_stmt_close($stmt);
	
	return $result;
}

function updateUserProfile($userID, $firstName, $lastName, $phone) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "UPDATE users SET first_name = ?,  last_name = ?, phone = ? WHERE id = UNHEX(?)");

	mysqli_stmt_bind_param($stmt, "ssss", $firstName, $lastName, $phone, $userID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getExpiredSessions() {
	global $conn;
	
	$result = array();
	
	$recordSet = mysqli_query($conn, "SELECT HEX(id) AS id, session_id FROM active_sessions WHERE expiration_time <= NOW()");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$result[] = $row;
	}
	
	mysqli_free_result($recordSet);
	
	return $result;
}

function getUserSessions($userID) {
	global $conn;
	
	$result = array();
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(id) AS id, session_id FROM active_sessions WHERE user_id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "s", $userID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result[] = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getActiveSessions($accountID) {
	global $conn;
	
	$result = array();
	
	if ($accountID == null) {
		$recordSet = mysqli_query($conn, "SELECT HEX(active_sessions.id) AS id, active_sessions.session_id, HEX(active_sessions.user_id) AS user_id, active_sessions.expiration_time, users.first_name, users.last_name, users.email, users.phone, IF(expiration_time <= NOW(), 1, 0) AS pending_delete FROM active_sessions LEFT JOIN users ON active_sessions.user_id = users.id");
	
		while ($row = mysqli_fetch_array($recordSet)) {
			$result[] = $row;
		}
		
		mysqli_free_result($recordSet);
	} else {
		$stmt = mysqli_prepare($conn, "SELECT HEX(active_sessions.id) AS id, active_sessions.session_id, HEX(active_sessions.user_id) AS user_id, active_sessions.expiration_time, users.first_name, users.last_name, users.email, users.phone, IF(expiration_time <= NOW(), 1, 0) AS pending_delete FROM active_sessions LEFT JOIN users ON active_sessions.user_id = users.id WHERE users.account_id = UNHEX(?)");
		
		mysqli_stmt_bind_param($stmt, "s", $accountID);
		if (mysqli_stmt_execute($stmt)) {
			$recordSet = mysqli_stmt_get_result($stmt);
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
		}
		
		mysqli_stmt_close($stmt);
	}
	
	return $result;
}

function untrackSession($sessionTrackingID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "DELETE FROM active_sessions WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "s", $sessionTrackingID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function updateSession($sessionTrackingID, $sessionID, $userID, $expirationSeconds) {
	global $conn;
	
	$result = false;
	
	if ($userID != null) {
		$stmt = mysqli_prepare($conn, "UPDATE active_sessions SET session_id = ?,  expiration_time = DATE_ADD(NOW(), INTERVAL ? SECOND), user_id = UNHEX(?) WHERE id = UNHEX(?)");
	
		mysqli_stmt_bind_param($stmt, "siss", $sessionID, $expirationSeconds, $userID, $sessionTrackingID);
		if (mysqli_stmt_execute($stmt)) {
			$result = true;
		}
		
		mysqli_stmt_close($stmt);
	} else {
		$stmt = mysqli_prepare($conn, "UPDATE active_sessions SET session_id = ?,  expiration_time = DATE_ADD(NOW(), INTERVAL ? SECOND), user_id = NULL WHERE id = UNHEX(?)");
	
		mysqli_stmt_bind_param($stmt, "sis", $sessionID, $expirationSeconds, $sessionTrackingID);
		if (mysqli_stmt_execute($stmt)) {
			$result = true;
		}
		
		mysqli_stmt_close($stmt);
	}
	
	return $result;
}

function trackSession($sessionID, $expirationSeconds) {
	global $conn;
	
	$result = null;
	
	$recordSet = mysqli_query($conn, "SELECT HEX(NEWID()) AS id");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$sessionTrackingID = $row["id"];
	}
	
	mysqli_free_result($recordSet);
	
	if (isset($sessionTrackingID)) {
		$stmt = mysqli_prepare($conn, "INSERT INTO active_sessions SET id = UNHEX(?), session_id = ?,  expiration_time = DATE_ADD(NOW(), INTERVAL ? SECOND)");
	
		mysqli_stmt_bind_param($stmt, "ssi", $sessionTrackingID, $sessionID, $expirationSeconds);
		if (mysqli_stmt_execute($stmt)) {
			$result = $sessionTrackingID;
		}
		
		mysqli_stmt_close($stmt);
	}
	
	return $result;
}

function getAccountPrimaryUser($accountID) {
	global $conn;
	
	$result = null;
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(accounts.primary_user_id) AS id, users.first_name, users.last_name, users.email FROM accounts LEFT JOIN users ON accounts.primary_user_id = users.id WHERE accounts.id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "s", $accountID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getAccountPrimaryBusiness($accountID) {
	global $conn;
	
	$result = null;
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(businesses.id) AS id, businesses.name, businesses.website, business_addresses.thoroughfare, business_addresses.premise, business_addresses.locality, business_addresses.dependent_locality, business_addresses.administrative_area, business_addresses.postal_code, HEX(business_addresses.country_id) AS country_id, countries.name AS country_name FROM accounts LEFT JOIN businesses ON accounts.primary_business_id = businesses.id LEFT JOIN business_addresses ON businesses.primary_address_id = business_addresses.id LEFT JOIN countries ON business_addresses.country_id = countries.id WHERE accounts.id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "s", $accountID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function resetUserPassword($email, $password) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "UPDATE users SET password = ?, reset_key = NULL, reset_time = NULL WHERE email = ?");
	
	mysqli_stmt_bind_param($stmt, "ss", password_hash($password, PASSWORD_BCRYPT), $email);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	mysqli_stmt_close($stmt);
	
	return $result;
}

function checkPasswordResetKeyValidity($resetPasswordKey) {
	global $conn;
	
	$result  = null;
	
	$stmt = mysqli_prepare($conn, "SELECT email FROM users WHERE reset_key = UNHEX(?) AND DATE_ADD(reset_time, INTERVAL " . PASSWORD_RESET_MAX_INTERVAL_MINUTES . " MINUTE) >= NOW()");
	
	mysqli_stmt_bind_param($stmt, "s", $resetPasswordKey);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
	
		while ($row = mysqli_fetch_array($recordSet)) {
			$result = $row["email"];
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function checkUserIsLocked($userInfo) {
	global $conn;
	
	$result = array();
	
	$result["locked"] = false;
	$result["modification_reason"] = "";
	
	if (strlen($userInfo) == 32 && ctype_xdigit($userInfo)) {
		$userID = $userInfo;
	} else {
		$stmt = mysqli_prepare($conn, "SELECT HEX(id) AS id FROM users WHERE email = ?");
		
		mysqli_stmt_bind_param($stmt, "s", $userInfo);
		mysqli_stmt_execute($stmt);
		
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$userID = $row["id"];
		}
		
		mysqli_stmt_close($stmt);
	}
	
	if (isset($userID)) {
		$stmt = mysqli_prepare($conn, "SELECT IF((status & " . STATUS_LOCKED . ") = " . STATUS_LOCKED . ", 1, 0) AS locked FROM user_statuses WHERE user_id = UNHEX(?) ORDER BY time DESC LIMIT 1");
		
		mysqli_stmt_bind_param($stmt, "s", $userID);
		mysqli_stmt_execute($stmt);
		
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result["locked"] = $row["locked"] == "1";
		}
		
		mysqli_stmt_close($stmt);
	}
	
	if ($result["locked"]) {
		$stmt = mysqli_prepare($conn, "SELECT time FROM user_statuses WHERE user_id = UNHEX(?) AND (status & " . STATUS_LOCKED . ") != " . STATUS_LOCKED . " ORDER BY time DESC LIMIT 1");
		
		mysqli_stmt_bind_param($stmt, "s", $userID);
		mysqli_stmt_execute($stmt);
		
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$time = $row["time"];
		}
		
		mysqli_stmt_close($stmt);
		
		if (isset($time)) {
			$stmt = mysqli_prepare($conn, "SELECT user_statuses.modification_reason, users.first_name, users.last_name FROM user_statuses LEFT JOIN users ON user_statuses.modifier_id = users.id WHERE user_statuses.user_id = UNHEX(?) AND user_statuses.time > '" . $time . "' ORDER BY user_statuses.time ASC LIMIT 1");
			
			mysqli_stmt_bind_param($stmt, "s", $userID);
			mysqli_stmt_execute($stmt);
			
			$recordSet = mysqli_stmt_get_result($stmt);
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result["modification_reason"] = $row["modification_reason"];
				if ($row["first_name"] != null) {
					$result["modification_reason"] .= " by " . $row["first_name"] . " " . $row["last_name"];
				}
			}
			
			mysqli_stmt_close($stmt);
		} else {
			$result["modification_reason"] = "Unknown";
		}
	} else {
		$stmt = mysqli_prepare($conn, "SELECT time FROM user_statuses WHERE user_id = UNHEX(?) AND (status & " . STATUS_LOCKED . ") = " . STATUS_LOCKED . " ORDER BY time DESC LIMIT 1");
		
		mysqli_stmt_bind_param($stmt, "s", $userID);
		mysqli_stmt_execute($stmt);
		
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$time = $row["time"];
		}
		
		mysqli_stmt_close($stmt);
		
		if (isset($time)) {
			$stmt = mysqli_prepare($conn, "SELECT user_statuses.modification_reason, users.first_name, users.last_name FROM user_statuses LEFT JOIN users ON user_statuses.modifier_id = users.id WHERE user_statuses.user_id = UNHEX(?) AND user_statuses.time > '" . $time . "' ORDER BY user_statuses.time ASC LIMIT 1");
			
			mysqli_stmt_bind_param($stmt, "s", $userID);
			mysqli_stmt_execute($stmt);
			
			$recordSet = mysqli_stmt_get_result($stmt);
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result["modification_reason"] = $row["modification_reason"];
				if ($row["first_name"] != null) {
					$result["modification_reason"] .= " by " . $row["first_name"] . " " . $row["last_name"];
				}
			}
			
			mysqli_stmt_close($stmt);
		} else {
			$result["modification_reason"] = "New User Created";
		}
	}
	
	return $result;
}

function unlockUser($userInfo, $reason, $modifierID) {
	global $conn;
	
	$result = false;
	
	if (strlen($userInfo) == 32 && ctype_xdigit($userInfo)) {
		$userID = $userInfo;
	} else {
		$stmt = mysqli_prepare($conn, "SELECT HEX(id) AS id FROM users WHERE email = ?");
		
		mysqli_stmt_bind_param($stmt, "s", $userInfo);
		mysqli_stmt_execute($stmt);
		
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$userID = $row["id"];
		}
		
		mysqli_stmt_close($stmt);
	}
	
	if (isset($userID)) {
		$stmt = mysqli_prepare($conn, "SELECT status FROM user_statuses WHERE user_id = UNHEX(?) ORDER BY time DESC LIMIT 1");
	
		mysqli_stmt_bind_param($stmt, "s", $userID);
		mysqli_stmt_execute($stmt);
		
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$status = $row["status"];
		}
		
		mysqli_stmt_close($stmt);
		
		if (isset($status)) {
			if ($modifierID != null) {
				$stmt = mysqli_prepare($conn, "INSERT INTO user_statuses SET user_id = UNHEX(?), status = " . $status . " & " . getRemoveStatusMask(STATUS_LOCKED) . ", modification_reason = ?, modifier_id = UNHEX(?)");
				
				mysqli_stmt_bind_param($stmt, "sss", $userID, $reason, $modifierID);
			} else {
				$stmt = mysqli_prepare($conn, "INSERT INTO user_statuses SET user_id = UNHEX(?), status = " . $status . " & " . getRemoveStatusMask(STATUS_LOCKED) . ", modification_reason = ?");
				
				mysqli_stmt_bind_param($stmt, "ss", $userID, $reason);
			}
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
		}
	}
	
	return $result;
}

function lockUser($userInfo, $reason, $modifierID) {
	global $conn;
	
	$result = false;
	
	if (strlen($userInfo) == 32 && ctype_xdigit($userInfo)) {
		$userID = $userInfo;
	} else {
		$stmt = mysqli_prepare($conn, "SELECT HEX(id) AS id FROM users WHERE email = ?");
		
		mysqli_stmt_bind_param($stmt, "s", $userInfo);
		mysqli_stmt_execute($stmt);
		
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$userID = $row["id"];
		}
		
		mysqli_stmt_close($stmt);
	}
	
	if (isset($userID)) {
		$stmt = mysqli_prepare($conn, "SELECT status FROM user_statuses WHERE user_id = UNHEX(?) ORDER BY time DESC LIMIT 1");
	
		mysqli_stmt_bind_param($stmt, "s", $userID);
		mysqli_stmt_execute($stmt);
		
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$status = $row["status"];
		}
		
		mysqli_stmt_close($stmt);
		
		if (isset($status)) {
			if ($modifierID != null) {
				$stmt = mysqli_prepare($conn, "INSERT INTO user_statuses SET user_id = UNHEX(?), status = " . $status . " | " . STATUS_LOCKED . ", modification_reason = ?, modifier_id = UNHEX(?)");
				
				mysqli_stmt_bind_param($stmt, "sss", $userID, $reason, $modifierID);
			} else {
				$stmt = mysqli_prepare($conn, "INSERT INTO user_statuses SET user_id = UNHEX(?), status = " . $status . " | " . STATUS_LOCKED . ", modification_reason = ?");
				
				mysqli_stmt_bind_param($stmt, "ss", $userID, $reason);
			}
			if (mysqli_stmt_execute($stmt)) {
				$result = true;
			}
			
			mysqli_stmt_close($stmt);
		}
	}
	
	return $result;
}

function generateResetKey($email) {
	global $conn;
	
	$result = null;
	
	$recordSet = mysqli_query($conn, "SELECT HEX(NEWID()) AS id");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$resetKey = $row["id"];
	}
	
	mysqli_free_result($recordSet);
	
	if (isset($resetKey)) {
		$stmt = mysqli_prepare($conn, "UPDATE users SET reset_key = UNHEX(?), reset_time = NOW() WHERE email = ?");
		
		mysqli_stmt_bind_param($stmt, "ss", $resetKey, $email);
		if (mysqli_stmt_execute($stmt)) {
			$result = $resetKey;
		}
		mysqli_stmt_close($stmt);
	}
	
	return $result;
}

function checkSecurityQuestionAnswer($email, $questionID, $answer) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "SELECT security_question_answer FROM users WHERE email = ? AND security_question_id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ss", $email, $questionID);
	mysqli_stmt_execute($stmt);
	
	$recordSet = mysqli_stmt_get_result($stmt);
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$answerHash = $row["security_question_answer"];
	}
	
	mysqli_stmt_close($stmt);
	
	if (isset($answerHash)) {
		return password_verify($answer, $answerHash);
	}
	
	return $result;
}

function getSecurityQuestion($email, $phone) {
	global $conn;
	
	$result  = null;
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(security_questions.id) AS id, security_questions.question FROM users LEFT JOIN security_questions ON users.security_question_id = security_questions.id WHERE users.email = ? AND users.phone = ? AND users.email_verified = '1' AND users.security_question_id IS NOT NULL");
	
	mysqli_stmt_bind_param($stmt, "ss", $email, $phone);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
	
		while ($row = mysqli_fetch_array($recordSet)) {
			$result = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function deleteAccount($accountID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "DELETE FROM accounts WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "s", $accountID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function deleteBusiness($businessID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "DELETE FROM businesses WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "s", $businessID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function deleteBusinessAddress($businessAddressID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "DELETE FROM business_addresses WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "s", $businessAddressID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function deleteUser($userID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "DELETE FROM users WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "s", $userID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function linkUserToBusiness($userID, $businessID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "INSERT INTO business_users SET business_id = UNHEX(?), user_id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ss", $businessID, $userID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function linkAccountToBusiness($accountID, $businessID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "UPDATE businesses SET account_id = UNHEX(?) WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ss", $accountID, $businessID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function linkAccountToUser($accountID, $userID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "UPDATE users SET account_id = UNHEX(?) WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ss", $accountID, $userID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function unlinkBusinessCategories($businessID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "DELETE FROM business_categories WHERE business_ID = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "s", $businessID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function linkCategoryToBusiness($categoryID, $businessID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "INSERT INTO business_categories SET business_id = UNHEX(?), category_id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ss", $businessID, $categoryID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function linkBusinessToAddress($businessID, $addressID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "UPDATE business_addresses SET business_id = UNHEX(?) WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ss", $businessID, $addressID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function giveUserRole($userID, $role) {
	global $conn;
	
	$result = false;
	
	if (ctype_xdigit($role) && strlen($role) == 32) {
		// Is a roleID
		$stmt = mysqli_prepare($conn, "INSERT INTO user_roles SET user_id = UNHEX(?), role_id = UNHEX(?)");
		
		mysqli_stmt_bind_param($stmt, "ss", $userID, $role);
	} else {
		$stmt = mysqli_prepare($conn, "INSERT INTO user_roles SET user_id = UNHEX(?), role_id = (SELECT id FROM roles WHERE name = ?)");
		
		mysqli_stmt_bind_param($stmt, "ss", $userID, $role);
	}
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function createAccount($comments, $primaryUserID, $primaryBusinessID) {
	global $conn;
	
	$result = null;
	
	$recordSet = mysqli_query($conn, "SELECT HEX(NEWID()) AS id");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$accountID = $row["id"];
	}
	
	mysqli_free_result($recordSet);
	
	if (isset($accountID)) {
		$stmt = mysqli_prepare($conn, "INSERT INTO accounts SET comments = ?, primary_user_id = UNHEX(?), primary_business_id = UNHEX(?), id = UNHEX(?)");
		
		mysqli_stmt_bind_param($stmt, "ssss", $comments, $primaryUserID, $primaryBusinessID, $accountID);
		if (mysqli_stmt_execute($stmt)) {
			$result = $accountID;
		}
		mysqli_stmt_close($stmt);
	}
	
	return $result;
}

function createBusiness($name, $website, $primaryAddressID) {
	global $conn;
	
	$result = null;
	
	$recordSet = mysqli_query($conn, "SELECT HEX(NEWID()) AS id");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$businessID = $row["id"];
	}
	
	mysqli_free_result($recordSet);
	
	if (isset($businessID)) {
		$stmt = mysqli_prepare($conn, "INSERT INTO businesses SET name = ?, website = ?, primary_address_id = UNHEX(?), id = UNHEX(?)");
		
		mysqli_stmt_bind_param($stmt, "ssss", $name, $website, $primaryAddressID, $businessID);
		if (mysqli_stmt_execute($stmt)) {
			$result = $businessID;
		}
		mysqli_stmt_close($stmt);
	}
	
	return $result;
}

function updateBusiness($businessID, $name, $website) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "UPDATE businesses SET name = ?, website = ? WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "sss", $name, $website, $businessID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	mysqli_stmt_close($stmt);
	
	return $result;
}

function createBusinessAddress($thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $country, $latitude, $longitude) {
	global $conn;
	
	$result = null;
	
	$recordSet = mysqli_query($conn, "SELECT HEX(NEWID()) AS id");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$businessAddressID = $row["id"];
	}
	
	mysqli_free_result($recordSet);
	
	if (isset($businessAddressID)) {
		$stmt = mysqli_prepare($conn, "INSERT INTO business_addresses SET thoroughfare = ?, premise = ?, locality = ?, dependent_locality = ?, administrative_area = ?, postal_code = ?, country_id = UNHEX(?), latitude = ?, longitude = ?, id = UNHEX(?)");
		
		mysqli_stmt_bind_param($stmt, "ssssssssss", $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $country, $latitude, $longitude, $businessAddressID);
		if (mysqli_stmt_execute($stmt)) {
			$result = $businessAddressID;
		}
		mysqli_stmt_close($stmt);
	}
	
	return $result;
}

function updateBusinessAddress($businessAddressID, $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $country, $latitude, $longitude) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "UPDATE business_addresses SET thoroughfare = ?, premise = ?, locality = ?, dependent_locality = ?, administrative_area = ?, postal_code = ?, country_id = UNHEX(?), latitude = ?, longitude = ? WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "ssssssssss", $thoroughfare, $premise, $locality, $dependentLocality, $administrativeArea, $postalCode, $country, $latitude, $longitude, $businessAddressID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	mysqli_stmt_close($stmt);
	
	return $result;
}

function createUser($first_name, $last_name, $phone, $email, $password, $securityQuestionID, $securityAnswer) {
	global $conn;

	$result = null;
	
	$recordSet = mysqli_query($conn, "SELECT HEX(NEWID()) AS id");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$userID = $row["id"];
	}
	
	mysqli_free_result($recordSet);
	
	if (isset($userID)) {
		if ($securityQuestionID != null) {
			$stmt = mysqli_prepare($conn, "INSERT INTO users SET first_name = ?, last_name = ?, phone = ?, email = ?, password = ?, security_question_id = UNHEX(?), security_question_answer = ?, id = UNHEX(?), api_key = ''");
		
			mysqli_stmt_bind_param($stmt, "ssssssss", $first_name, $last_name, $phone, $email, password_hash($password, PASSWORD_BCRYPT), $securityQuestionID, password_hash($securityAnswer, PASSWORD_BCRYPT), $userID);
		} else {
			$stmt = mysqli_prepare($conn, "INSERT INTO users SET first_name = ?, last_name = ?, phone = ?, email = ?, password = ?, id = UNHEX(?), api_key = ''");
		
			mysqli_stmt_bind_param($stmt, "ssssss", $first_name, $last_name, $phone, $email, password_hash($password, PASSWORD_BCRYPT), $userID);
		}
		if (mysqli_stmt_execute($stmt)) {
			$stmt2 = mysqli_prepare($conn, "INSERT INTO user_statuses SET user_id = UNHEX(?), status = 0, modification_reason = 'New User'");
			mysqli_stmt_bind_param($stmt2, "s", $userID);
			
			if (mysqli_stmt_execute($stmt2)) {
				$result = $userID;
			}
			
			mysqli_stmt_close($stmt2);
		}
		mysqli_stmt_close($stmt);
	}
	
	return $result;
}

function verifyEmail($userID) {
	global $conn;
	
	$result = false;
	
	$stmt = mysqli_prepare($conn, "UPDATE users SET email_verified = 1 WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "s", $userID);
	if (mysqli_stmt_execute($stmt)) {
		$result = true;
	}
	mysqli_stmt_close($stmt);
	
	return $result;
}

function logFailedElevation($userID) {
	global $conn;
	
	mysqli_query($conn, "INSERT INTO user_elevation_attempts SET user_id = UNHEX('" . $userID . "'), ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0), success = 0");
}

function logSuccessfulElevation($userID) {
	global $conn;
	
	mysqli_query($conn, "INSERT INTO user_elevation_attempts SET user_id = UNHEX('" . $userID . "'), ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0), success = 1");
}

function getElevationFailureCount($userID) {
	global $conn;
	
	$result = 0;
	
	$stmt = mysqli_prepare($conn, "SELECT COUNT(user_elevation_attempts.success) AS failure_count FROM user_elevation_attempts LEFT JOIN (SELECT time FROM user_elevation_attempts WHERE user_id = UNHEX(?) AND success = 1 ORDER BY time DESC LIMIT 1) AS last_success ON 1 = 1 WHERE user_elevation_attempts.user_id = UNHEX(?) AND user_elevation_attempts.time > IFNULL(last_success.time, '1970-01-01 00:00:00') GROUP BY success");
	
	mysqli_stmt_bind_param($stmt, "ss", $userID, $userID);
	mysqli_stmt_execute($stmt);
	
	$recordSet = mysqli_stmt_get_result($stmt);
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$result = $row["failure_count"];
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function logFailedLogin($username) {
	global $conn;
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(id) AS id FROM users WHERE email = ?");
	
	mysqli_stmt_bind_param($stmt, "s", $username);
	mysqli_stmt_execute($stmt);
	
	$recordSet = mysqli_stmt_get_result($stmt);
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$userID = $row["id"];
	}
	
	mysqli_stmt_close($stmt);
	
	if (isset($userID)) {
		mysqli_query($conn, "INSERT INTO user_login_attempts SET user_id = UNHEX('" . $userID . "'), ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0), success = 0");
	}
	mysqli_query($conn, "INSERT INTO ip_address_login_attempts SET ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0), success = 0");
}

function logSuccessfulLogin($username) {
	global $conn;
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(id) AS id FROM users WHERE email = ?");
	
	mysqli_stmt_bind_param($stmt, "s", $username);
	mysqli_stmt_execute($stmt);
	
	$recordSet = mysqli_stmt_get_result($stmt);
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$userID = $row["id"];
	}
	
	mysqli_stmt_close($stmt);
	
	mysqli_query($conn, "INSERT INTO user_login_attempts SET user_id = UNHEX('" . $userID . "'), ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0), success = 1");
	mysqli_query($conn, "INSERT INTO ip_address_login_attempts SET ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0), success = 1");
}

function loadSessionUserInformation($username) {
	global $conn;
	
	$_SESSION["userEmail"] = $username;
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(id) AS id, HEX(account_id) AS account_id, first_name, last_name, phone, HEX(api_key) AS api_key FROM users WHERE email = ?");
	
	mysqli_stmt_bind_param($stmt, "s", $username);
	mysqli_stmt_execute($stmt);
	
	$recordSet = mysqli_stmt_get_result($stmt);
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$_SESSION["userID"] = $row["id"];
		$_SESSION["accountID"] = $row["account_id"];
		$_SESSION["userFirstName"] = $row["first_name"];
		$_SESSION["userLastName"] = $row["last_name"];
		$_SESSION["userPhone"] = $row["phone"];
		$_SESSION["userAPIKey"] = $row["api_key"];
	}
	
	mysqli_stmt_close($stmt);
	
	$recordSet = mysqli_query($conn, "SELECT roles.permissions FROM user_roles LEFT JOIN roles ON user_roles.role_id = roles.id WHERE user_roles.user_id = UNHEX('" . $_SESSION["userID"] . "')");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$_SESSION["userPermissions"] |= $row["permissions"];
	}
	
	mysqli_free_result($recordSet);
}

function checkUserPassword($username, $password) {
	global $conn;
	
	$stmt = mysqli_prepare($conn, "SELECT password FROM users WHERE email = ?");
	
	mysqli_stmt_bind_param($stmt, "s", $username);
	mysqli_stmt_execute($stmt);
	
	$recordSet = mysqli_stmt_get_result($stmt);
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$passwordHash = $row["password"];
	}
	
	mysqli_stmt_close($stmt);
	
	if (!isset($passwordHash)) {
		return false;
	} else {
		return password_verify($password, $passwordHash);
	}
}

function getLoginFailureCountByRemoteIP() {
	global $conn;
	
	$result = 0;
	
	$recordSet = mysqli_query($conn, "SELECT COUNT(ip_address_login_attempts.success) AS failure_count FROM ip_address_login_attempts LEFT JOIN (SELECT time FROM ip_address_login_attempts WHERE ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0) AND success = 1 ORDER BY time DESC LIMIT 1) AS last_success ON 1 = 1 WHERE ip_address_login_attempts.ip_address = LPAD(INET6_ATON('" . $_SERVER["REMOTE_ADDR"] . "'), 16, 0x0) AND ip_address_login_attempts.time > IFNULL(last_success.time, '1970-01-01 00:00:00') GROUP BY success");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$result = $row["failure_count"];
	}
	
	mysqli_free_result($recordSet);
	
	return $result;
}

function getLoginFailureCountByUsername($username) {
	global $conn;
	
	$result = 0;
	
	$stmt = mysqli_prepare($conn, "SELECT COUNT(user_login_attempts.success) AS failure_count FROM user_login_attempts LEFT JOIN (SELECT users.id AS user_id, user_login_attempts.time FROM users LEFT JOIN user_login_attempts ON users.id = user_login_attempts.user_id WHERE users.email = ? AND user_login_attempts.success = 1 ORDER BY user_login_attempts.time DESC LIMIT 1) AS last_success ON 1 = 1 WHERE user_login_attempts.user_id = last_success.user_id AND user_login_attempts.time > IFNULL(last_success.time, '1970-01-01 00:00:00') GROUP BY success");
	
	mysqli_stmt_bind_param($stmt, "s", $username);
	mysqli_stmt_execute($stmt);
	
	$recordSet = mysqli_stmt_get_result($stmt);
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$result = $row["failure_count"];
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getBusinessAddresses($businessID) {
	global $conn;
	
	$result = array();
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(business_addresses.id) AS id, business_addresses.thoroughfare, business_addresses.premise, business_addresses.locality, business_addresses.dependent_locality, business_addresses.administrative_area, business_addresses.postal_code, HEX(business_addresses.country_id) AS country_id, countries.name AS country_name FROM business_addresses LEFT JOIN businesses ON business_addresses.business_id = businesses.id WHERE business_addresses.businesses_id = UNHEX(?) AND business_addresses.id != businesses.primary_address_id");
	
	mysqli_stmt_bind_param($stmt, "s", $businessID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result[] = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getBusiness($businessID) {
	global $conn;
	
	$result = null;
	
	$stmt = mysqli_prepare($conn, "SELECT HEX(businesses.id) AS id, businesses.name, businesses.website, HEX(businesses.primary_address_id) AS primary_address_id, business_addresses.thoroughfare, business_addresses.premise, business_addresses.locality, business_addresses.dependent_locality, business_addresses.administrative_area, business_addresses.postal_code, HEX(business_addresses.country_id) AS country_id, countries.name AS country_name FROM businesses LEFT JOIN business_addresses ON businesses.primary_address_id = business_addresses.id LEFT JOIN countries ON business_addresses.country_id = countries.id WHERE businesses.id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "s", $businessID);
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getUser($userID) {
	global $conn;
	
	$result = null;
	
	$stmt = mysqli_prepare($conn, "SELECT first_name, last_name, phone, email FROM users WHERE id = UNHEX(?)");
	
	mysqli_stmt_bind_param($stmt, "s", $userID);
	
	if (mysqli_stmt_execute($stmt)) {
		$recordSet = mysqli_stmt_get_result($stmt);
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result = $row;
		}
	}
	
	mysqli_stmt_close($stmt);
	
	return $result;
}

function getBusinessCategories($businessID) {
	global $conn;
	
	$result = array();
	
	if ($businessID != null) {
		$stmt = mysqli_prepare($conn, "SELECT HEX(categories.id) AS id, categories.name, IF(business_categories.business_id IS NULL, 0, 1) AS selected FROM categories LEFT JOIN business_categories ON categories.id = business_categories.category_id AND business_categories.business_id = UNHEX(?) ORDER BY categories.name");
		
		mysqli_stmt_bind_param($stmt, "s", $businessID);
		if (mysqli_stmt_execute($stmt)) {
			$recordSet = mysqli_stmt_get_result($stmt);
			
			while ($row = mysqli_fetch_array($recordSet)) {
				$result[] = $row;
			}
		}
		
		mysqli_stmt_close($stmt);
	} else {
		$recordSet = mysqli_query($conn, "SELECT HEX(id) AS id, name FROM categories ORDER BY name");
		
		while ($row = mysqli_fetch_array($recordSet)) {
			$result[] = $row;
		}
		
		mysqli_free_result($recordSet);
	}
	
	return $result;
}

function getSecurityQuestions() {
	global $conn;
	
	$result = array();
	
	$recordSet = mysqli_query($conn, "SELECT HEX(id) AS id, question FROM security_questions ORDER BY question");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$result[] = $row;
	}
	
	mysqli_free_result($recordSet);
	
	return $result;
}

function getCurrencies() {
	global $conn;
	
	$result = array();
	
	$recordSet = mysqli_query($conn, "SELECT HEX(id) AS id, name, abbreviation FROM currencies ORDER BY name");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$result[] = $row;
	}
	
	mysqli_free_result($recordSet);
	
	return $result;
}

function getCountries() {
	global $conn;
	
	$result = array();
	
	$recordSet = mysqli_query($conn, "SELECT HEX(id) AS id, name, code FROM countries ORDER BY name");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$result[] = $row;
	}
	
	mysqli_free_result($recordSet);
	
	return $result;
}

function getDistanceUnits() {
	global $conn;
	
	$result = array();
	
	$recordSet = mysqli_query($conn, "SELECT HEX(id) AS id, name FROM distance_units ORDER BY name");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$result[] = $row;
	}
	
	mysqli_free_result($recordSet);
	
	return $result;
}

function getAllServices() {
	global $conn;
	
	$result = array();
	
	$recordSet = mysqli_query($conn, "SELECT HEX(id) AS id, name FROM services ORDER BY name");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$result[] = $row;
	}
	
	mysqli_free_result($recordSet);
	
	return $result;
}

function getCheckinServices() {
	global $conn;
	
	$result = array();
	
	$recordSet = mysqli_query($conn, "SELECT HEX(services.id) AS id, services.name FROM features INNER JOIN service_features ON features.id = service_features.feature_id LEFT JOIN services ON service_features.service_id = services.id WHERE features.name = 'checkin' ORDER BY services.name");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$result[] = $row;
	}
	
	mysqli_free_result($recordSet);
	
	return $result;
}

function getPostServices() {
	global $conn;
	
	$result = array();
	
	$recordSet = mysqli_query($conn, "SELECT HEX(services.id) AS id, services.name FROM features INNER JOIN service_features ON features.id = service_features.feature_id LEFT JOIN services ON service_features.service_id = services.id WHERE features.name = 'post' ORDER BY services.name");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$result[] = $row;
	}
	
	mysqli_free_result($recordSet);
	
	return $result;
}

function getLikeServices() {
	global $conn;
	
	$result = array();
	
	$recordSet = mysqli_query($conn, "SELECT HEX(services.id) AS id, services.name FROM features INNER JOIN service_features ON features.id = service_features.feature_id LEFT JOIN services ON service_features.service_id = services.id WHERE features.name = 'like' ORDER BY services.name");
	
	while ($row = mysqli_fetch_array($recordSet)) {
		$result[] = $row;
	}
	
	mysqli_free_result($recordSet);
	
	return $result;
}
?>
