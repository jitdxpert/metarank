<?php
	require_once(dirname(__FILE__) . "/db.php");
	
	if (!isset($_SESSION)) {
		session_start();
	}

	/**
	 * User Session Variables:
	 * redirectURL: string (url to load on successful login)
	 * isLoggedIn: boolean
	 * userID: v4 uuid
	 * userPermissions: int(10) (see /inc/defines.php)
	 */
	 
	 // Set Defaults
	 if (!isset($_SESSION["redirectURL"])) {
		$_SESSION["redirectURL"] = BASE_URL . "/account/";
	 }
	 
	 if (!isset($_SESSION["isLoggedIn"])) {
		$_SESSION["isLoggedIn"] = false;
	 }
	 
	 if (!isset($_SESSION["userID"])) {
		$_SESSION["userID"] = null;
	 }
	 
	 if (!isset($_SESSION["accountID"])) {
		$_SESSION["accountID"] = null;
	}
	 
	 if (!isset($_SESSION["userPermissions"])) {
		$_SESSION["userPermissions"] = 0;
	}
	
	if (!isset($_SESSION["sessionTrackingID"])) {
		$sessionTrackingID = trackSession(session_id(), 0 + ini_get("session.gc_maxlifetime"));
		if ($sessionTrackingID != null) {
			commitChanges();
			$_SESSION["sessionTrackingID"] = $sessionTrackingID;
		} else {
			rollbackChanges();
		}
	} else if (!isset($isLoggingOut) || !$isLoggingOut) {
		if (updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"))) {
			commitChanges();
		} else {
			rollbackChanges();
		}
	}
?>
