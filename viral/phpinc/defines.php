<?php

define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASSWORD", "wisely");
define("DB_DATABASE", "metarank");

define ("LOCK_TIMEOUT", "8");

define("LOCK_NEW_PROMOTION", DB_DATABASE . ".NEW_PROMOTION.");

define("MAX_LOGIN_FAILURES", "2");
define("MAX_ELEVATION_FAILURES", "2");

//define("RECAPTCHA_PUBLIC_KEY", "6LcmpvsSAAAAADIoufW7T5UJDR-xIGAYoLOOrTxt");
//define("RECAPTCHA_PRIVATE_KEY", "6LcmpvsSAAAAAL7CRFuU2_fMEo3E0dJfp3sOdeXF");

define("RECAPTCHA_PUBLIC_KEY", "6LekgwgTAAAAAGA7JT_0NrNUJvS23iAr-CTj1jje");
define("RECAPTCHA_PRIVATE_KEY", "6LekgwgTAAAAAIXeTfX4Qd2jIcZjcRvZ5h3uvVCQ");

define("STATUS_LOCKED", "1");
define("STATUS_INACTIVE_NONPAYMENT", "2");
define("STATUS_PAYMENT_INFORMATION_MISSING", "4");
define("STATUS_PAYMENT_INFORMATION_INVALID", "8");
define("STATUS_CANCELLED", "16");

define("PERMISSION_ADMINISTRATOR", "1");
define("PERMISSION_MANAGER", "2");
define("PERMISSION_PROMOTER", "3");

define("PASSWORD_RESET_MAX_INTERVAL_MINUTES", "20");
define("PASSWORD_HTML5_REGULAR_EXPRESSION", "(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!#$%&()*+,-./:;<=>?@\[\\\\\\]\^_`{|}~]).{14,}");

define("MYSQL_MAX_UNSIGNED_INTEGER", "4294967295");

define('BASE_URL', 'http://viral.dev.metarank.com');
define('BASE_PATH', dirname(dirname(__FILE__)) . '/htdocs');

function getRemoveStatusMask($statusToRemove) {
	return MYSQL_MAX_UNSIGNED_INTEGER - $statusToRemove;
}

function curPageURL() {
	$pageURL = 'http';
	if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
		$pageURL .= "s";
	}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

?>
