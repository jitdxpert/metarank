<?php
if (!isset($primaryAccountHolder)) {
	require_once(dirname(dirname(__FILE__)) . "/db.php");
	$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);
}

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(dirname(dirname(__FILE__)) . "/header.php");
}
?>
	<div data-role="page" id="insufficient_permissions_page">
		<div role="main" class="ui-content">
			<div class="content-wrapper">
				<p>Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
				<br />
				<p class="black-text larger-message">You do not have permission to view this page.</p>
				<p class="black-text larger-message">If you believe you have received this message in error, please contact the account's primary user (<?php echo $primaryAccountHolder["first_name"] . " ". $primaryAccountHolder["last_name"] . " &lt;" . $primaryAccountHolder["email"] . "&gt;"; ?>) and provide them with your username (<?php echo $_SESSION["userEmail"]; ?>) and the URL of this page (<?php echo curPageURL(); ?>).</p>
				<p class="black-text larger-message">If you are the account's primary user, please contact technical support.</p>
				<br />
				<br />
				<div id="navigation_box" class="grey-box">
					<p>
						<a href="<?php echo $backPageURL; ?>">
							<button>Back</button>
						</a>
					</p>
				</div>
				<p class="smaller-message">
					Want to talk with a specialist?
					<br>
					Call 818.588.6836
				</p>
			</div>
		</div><!-- /content -->
	</div><!-- /page -->
<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(dirname(dirname(__FILE__)) . "/footer.php");
}
?>
