<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(dirname(dirname(__FILE__)) . "/header.php");
}
?>

<section data-role="page" id="insufficient_permissions_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p>Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br />
            <p class="black-text larger-message">We are currently experiencing technical difficulties, and are unable to complete your request. We apologize for any inconvenience this may cause you, and are working to correct the problem.</p>
            <p class="black-text larger-message">Please check back again later.</p>
            <br />
            <br />
            <div id="navigation_box" class="grey-box">
                <p>
                    <a href="<?php echo BASE_URL; ?>/">
                        <button>Return to Home Page</button>
                    </a>
                </p>
            </div>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(dirname(dirname(__FILE__)) . "/footer.php");
}
?>