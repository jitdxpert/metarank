var BASE_URL = $('#BASE_URL').val();

// INFO: We don't want jqm to keep the first page indefinitely, we need to break the cache, even if we are doing ajax
$(document).bind("pagechange", function (toPage, info) {
        if ($.mobile.firstPage && info.options.fromPage && (typeof info.options.target == "undefined" || info.options.target[0] != "#") && ($.mobile.firstPage == info.options.fromPage) && info.toPage && ($.mobile.firstPage[0] != info.toPage[0])) {
                $.mobile.firstPage.remove();

                // We only need to remove 1 time from DOM, so unbind the unused event
                $(document).unbind("pagechange", this);
        }
});

function formatLocalDate(date) {
	tzo = -date.getTimezoneOffset(),
	dif = tzo >= 0 ? '+' : '-',
	pad = function(num) {
		norm = Math.abs(Math.floor(num));
		return (norm < 10 ? '0' : '') + norm;
	};
	return date.getFullYear() 
		+ '-' + pad(date.getMonth()+1)
		+ '-' + pad(date.getDate())
		+ 'T00:00:00';/*
		+ 'T' + pad(date.getHours())
		+ ':' + pad(date.getMinutes()));*/
}

function updateAddressCountry(evt) {
	var addressFieldNumber = this.id.slice(-4);
	var curFieldSet = $("fieldset[data-addressfield-number=" + addressFieldNumber + "]", this.form);
	var data = curFieldSet.serialize();
	data = data.replace(/%5B%5D=/g, "=");
	data += "&addressFieldNumber=" + addressFieldNumber + "&" + $(this.form.removeFields).serialize();
	$.ajax({
		url: BASE_URL + '/ajax/update_address_country.php',
		data: data,
		type: "POST",
		success: function(theForm, addressHTML, textStatus, jqXHR) {
			curFieldSet.replaceWith(addressHTML);
			$("fieldset[data-addressfield-number=" + addressFieldNumber + "]", theForm).trigger("create");
		}.bind(window, this.form),
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Country selection error: " + errorThrown);
		}
	});
}

function updateScheduledPerks(evt) {
	$.ajax({
		url: BASE_URL + '/ajax/update_scheduled_perks.php',
		data: $(this).serialize(),
		type: "POST",
		success: function(scheduledPerksHTML, textStatus, jqXHR) {
			$("#scheduled_perks").empty().append(scheduledPerksHTML);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Business selection error: " + errorThrown);
		}
	});
}

function updateActivePerks(evt) {
	$.ajax({
		url: BASE_URL + '/ajax/update_active_perks.php',
		data: $(this).serialize(),
		type: "POST",
		success: function(activePerksHTML, textStatus, jqXHR) {
			$("#active_perks").empty().append(activePerksHTML);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Business selection error: " + errorThrown);
		}
	});
}

function updateCompletedPerks(evt) {
	$.ajax({
		url: BASE_URL + '/ajax/update_completed_perks.php',
		data: $(this).serialize(),
		type: "POST",
		success: function(completedPerksHTML, textStatus, jqXHR) {
			$("#completed_perks").empty().append(completedPerksHTML);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Business selection error: " + errorThrown);
		}
	});
}

function updateScheduledPromotions(evt) {
	$.ajax({
		url: BASE_URL + '/ajax/update_scheduled_promotions.php',
		data: $(this).serialize(),
		type: "POST",
		success: function(scheduledPromotionsHTML, textStatus, jqXHR) {
			$("#scheduled_promotions").empty().append(scheduledPromotionsHTML);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Business selection error: " + errorThrown);
		}
	});
}

function updateActivePromotions(evt) {
	$.ajax({
		url: BASE_URL + '/ajax/update_active_promotions.php',
		data: $(this).serialize(),
		type: "POST",
		success: function(activePromotionsHTML, textStatus, jqXHR) {
			$("#active_promotions").empty().append(activePromotionsHTML);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Business selection error: " + errorThrown);
		}
	});
}

function updateCompletedPromotions(evt) {
	$.ajax({
		url: BASE_URL + '/ajax/update_completed_promotions.php',
		data: $(this).serialize(),
		type: "POST",
		success: function(completedPromotionsHTML, textStatus, jqXHR) {
			$("#completed_promotions").empty().append(completedPromotionsHTML);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Business selection error: " + errorThrown);
		}
	});
}

$(document).ready(function(evt) {
	$( "[data-role='header'], [data-role='footer']" ).toolbar();
});

var RecaptchaOptions = {theme:'clean'};
function showRecaptcha(element) {
	Recaptcha.create("6LekgwgTAAAAAGA7JT_0NrNUJvS23iAr-CTj1jje", element, RecaptchaOptions);
}
function reloadRecaptcha() {
	Recaptcha.reload();
}
function removeRecaptcha() {
	Recaptcha.destroy();
}

/* Update Business Related */
function updateBusiness(evt) {
	var geocodeAddresses = new Array();
				
	var addressFieldsets = $("#edit_business_page fieldset");
	addressFieldsets.each(function(index) {
		var address = "";
		fields = $("input[type=text]:enabled, select:enabled", this);
		for (var i = 0; i < fields.length; i++) {
			curField = $(fields[i]);
			if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
				address += ", ";
			} else {
				address += " ";
			}
			address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
		}
		
		if (fields.length != 0) {
			curLatitude = $("input[name='latitude[]']", this);
			curLongitude = $("input[name='longitude[]']", this);
			
			runGeocode = false;
			
			if (curLatitude.length == 0) {
				latitudeElem = document.createElement("input");
				latitudeElem.setAttribute("type", "hidden");
				latitudeElem.setAttribute("name", "latitude[]");
				this.appendChild(latitudeElem);
				curLatitude = $(latitudeElem);
				runGeocode = true;
			}
			
			if (curLongitude.length == 0) {
				longitudeElem = document.createElement("input");
				longitudeElem.setAttribute("type", "hidden");
				longitudeElem.setAttribute("name", "longitude[]");
				this.appendChild(longitudeElem);
				curLongitude = $(longitudeElem);
				runGeocode = true;
			}
			
			if (!runGeocode) {
				if (curLatitude.val() == "" || curLongitude.val() == "") {
					runGeocode = true;
				}
			}
			
			if (runGeocode) {
				geocodeAddresses.push({address: address, fieldSet: this});
			}
		}
		
		if (index == addressFieldsets.length - 1) {
			if (geocodeAddresses.length > 0) {
				var runningGeocodes = geocodeAddresses.length;
				for (var i = 0; i < geocodeAddresses.length; i++) {
					geocodeInformation = geocodeAddresses[i];
					
					geocoder = new google.maps.Geocoder();
					geocoder.geocode({address: geocodeInformation.address}, function(address, fieldSet, results, status) {
						curLatitude = $("input[name='latitude[]']", fieldSet);
						curLongitude = $("input[name='longitude[]']", fieldSet);
						
						if (status == google.maps.GeocoderStatus.OK) {
							if (results[0].geometry.location_type.toUpperCase() == "ROOFTOP") {
								curLatitude.val(results[0].geometry.location.lat());
								curLongitude.val(results[0].geometry.location.lng());
							} else {
								curLatitude.val("");
								curLongitude.val("");
								console.log('Geocode for "' + address + '" was not successful for the following reason: Location type was not specific.');
							}
						} else {
							curLatitude.val("");
							curLongitude.val("");
							console.log('Geocode for "' + address + '" was not successful for the following reason: ' + status);
						}
						
						runningGeocodes--;
						
						if (runningGeocodes == 0) {
							theForm = $(fieldSet).closest("form");
							latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
							for (var j = 0; j < latLngs.length; j++) {
								if (latLngs[j].value == "") {
									var address = "";
									fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
									for (var k = 0; k < fields.length; k++) {
										curField = $(fields[k]);
										if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
											address += ", ";
										} else {
											address += " ";
										}
										address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
									}
									var statusMessage = $("#status_message");
									statusMessage.removeClass("info");
									statusMessage.addClass("error");
									statusMessage.text("Invalid address: " + address);
									statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
									setTimeout(function() {
										statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
									}, 3000);
									return false;
								}
							}
							$.ajax({
								url: theForm[0].action,
								data: theForm.serialize(),
								type: "POST",
								success: function(data, textStatus, jqXHR) {
									if (data.success) {
										var statusMessage = $("#status_message");
										statusMessage.removeClass("error");
										statusMessage.addClass("info");
										statusMessage.text(data.message);
										statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
										setTimeout(function() {
											statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
										}, 3000);
									} else {
										var statusMessage = $("#status_message");
										statusMessage.removeClass("info");
										statusMessage.addClass("error");
										statusMessage.text(data.message);
										statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
										setTimeout(function() {
											statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
										}, 3000);
										for (var i = 0; i < data.commands.length; i++) {
											/*switch (data.commands[i].action) {
												case "command...":
													break;
											}*/
										}
									}
								},
								error: function(jqXHR, textStatus, errorThrown) {
									var statusMessage = $("#status_message");
									statusMessage.removeClass("info");
									statusMessage.addClass("error");
									statusMessage.text(errorThrown);
									statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
									setTimeout(function() {
										statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
									}, 3000);
								}
							});
						}
					}.bind(window, geocodeInformation.address, geocodeInformation.fieldSet));
				}
			} else {
				theForm = $(this).closest("form");
				latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
				for (var i = 0; i < latLngs.length; i++) {
					if (latLngs[i].value == "") {
						var address = "";
						fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
						for (var i = 0; i < fields.length; i++) {
							curField = $(fields[i]);
							if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
								address += ", ";
							} else {
								address += " ";
							}
							address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
						}
						var statusMessage = $("#status_message");
						statusMessage.removeClass("info");
						statusMessage.addClass("error");
						statusMessage.text("Invalid address: " + address);
						statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
						setTimeout(function() {
							statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						}, 3000);
						return false;
					}
				}
				$.ajax({
					url: theForm[0].action,
					data: theForm.serialize(),
					type: "POST",
					success: function(data, textStatus, jqXHR) {
						if (data.success) {
							var statusMessage = $("#status_message");
							statusMessage.removeClass("error");
							statusMessage.addClass("info");
							statusMessage.text(data.message);
							statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							setTimeout(function() {
								statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
							}, 3000);
						} else {
							var statusMessage = $("#status_message");
							statusMessage.removeClass("info");
							statusMessage.addClass("error");
							statusMessage.text(data.message);
							statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							setTimeout(function() {
								statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
							}, 3000);
							for (var i = 0; i < data.commands.length; i++) {
								/*switch (data.commands[i].action) {
									case "command...":
										break;
								}*/
							}
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						var statusMessage = $("#status_message");
						statusMessage.removeClass("info");
						statusMessage.addClass("error");
						statusMessage.text(errorThrown);
						statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
						setTimeout(function() {
							statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						}, 3000);
					}
				});
				
			}
		}
	});
	
	evt.preventDefault();
	return false;
}

/* Add Business Related */
function addBusiness(evt) {
	var geocodeAddresses = new Array();
				
	var addressFieldsets = $("#add_business_page fieldset");
	addressFieldsets.each(function(index) {
		var address = "";
		fields = $("input[type=text]:enabled, select:enabled", this);
		for (var i = 0; i < fields.length; i++) {
			curField = $(fields[i]);
			if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
				address += ", ";
			} else {
				address += " ";
			}
			address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
		}
		
		if (fields.length != 0) {
			curLatitude = $("input[name='latitude[]']", this);
			curLongitude = $("input[name='longitude[]']", this);
			
			runGeocode = false;
			
			if (curLatitude.length == 0) {
				latitudeElem = document.createElement("input");
				latitudeElem.setAttribute("type", "hidden");
				latitudeElem.setAttribute("name", "latitude[]");
				this.appendChild(latitudeElem);
				curLatitude = $(latitudeElem);
				runGeocode = true;
			}
			
			if (curLongitude.length == 0) {
				longitudeElem = document.createElement("input");
				longitudeElem.setAttribute("type", "hidden");
				longitudeElem.setAttribute("name", "longitude[]");
				this.appendChild(longitudeElem);
				curLongitude = $(longitudeElem);
				runGeocode = true;
			}
			
			if (!runGeocode) {
				if (curLatitude.val() == "" || curLongitude.val() == "") {
					runGeocode = true;
				}
			}
			
			if (runGeocode) {
				geocodeAddresses.push({address: address, fieldSet: this});
			}
		}
		
		if (index == addressFieldsets.length - 1) {
			if (geocodeAddresses.length > 0) {
				var runningGeocodes = geocodeAddresses.length;
				for (var i = 0; i < geocodeAddresses.length; i++) {
					geocodeInformation = geocodeAddresses[i];
					
					geocoder = new google.maps.Geocoder();
					geocoder.geocode({address: geocodeInformation.address}, function(address, fieldSet, results, status) {
						curLatitude = $("input[name='latitude[]']", fieldSet);
						curLongitude = $("input[name='longitude[]']", fieldSet);
						
						if (status == google.maps.GeocoderStatus.OK) {
							if (results[0].geometry.location_type.toUpperCase() == "ROOFTOP") {
								curLatitude.val(results[0].geometry.location.lat());
								curLongitude.val(results[0].geometry.location.lng());
							} else {
								curLatitude.val("");
								curLongitude.val("");
								console.log('Geocode for "' + address + '" was not successful for the following reason: Location type was not specific.');
							}
						} else {
							curLatitude.val("");
							curLongitude.val("");
							console.log('Geocode for "' + address + '" was not successful for the following reason: ' + status);
						}
						
						runningGeocodes--;
						
						if (runningGeocodes == 0) {
							theForm = $(fieldSet).closest("form");
							latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
							for (var j = 0; j < latLngs.length; j++) {
								if (latLngs[j].value == "") {
									var address = "";
									fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
									for (var k = 0; k < fields.length; k++) {
										curField = $(fields[k]);
										if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
											address += ", ";
										} else {
											address += " ";
										}
										address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
									}
									var statusMessage = $("#status_message");
									statusMessage.removeClass("info");
									statusMessage.addClass("error");
									statusMessage.text("Invalid address: " + address);
									statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
									setTimeout(function() {
										statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
									}, 3000);
									return false;
								}
							}
							$.ajax({
								url: theForm[0].action,
								data: theForm.serialize(),
								type: "POST",
								success: function(data, textStatus, jqXHR) {
									if (data.success) {
										var statusMessage = $("#status_message");
										statusMessage.removeClass("error");
										statusMessage.addClass("info");
										statusMessage.text(data.message);
										statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
										setTimeout(function() {
											statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
										}, 3000);
									} else {
										var statusMessage = $("#status_message");
										statusMessage.removeClass("info");
										statusMessage.addClass("error");
										statusMessage.text(data.message);
										statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
										setTimeout(function() {
											statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
										}, 3000);
										for (var i = 0; i < data.commands.length; i++) {
											/*switch (data.commands[i].action) {
												case "command...":
													break;
											}*/
										}
									}
								},
								error: function(jqXHR, textStatus, errorThrown) {
									var statusMessage = $("#status_message");
									statusMessage.removeClass("info");
									statusMessage.addClass("error");
									statusMessage.text(errorThrown);
									statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
									setTimeout(function() {
										statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
									}, 3000);
								}
							});
						}
					}.bind(window, geocodeInformation.address, geocodeInformation.fieldSet));
				}
			} else {
				theForm = $(this).closest("form");
				latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
				for (var i = 0; i < latLngs.length; i++) {
					if (latLngs[i].value == "") {
						var address = "";
						fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
						for (var i = 0; i < fields.length; i++) {
							curField = $(fields[i]);
							if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
								address += ", ";
							} else {
								address += " ";
							}
							address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
						}
						var statusMessage = $("#status_message");
						statusMessage.removeClass("info");
						statusMessage.addClass("error");
						statusMessage.text("Invalid address: " + address);
						statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
						setTimeout(function() {
							statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						}, 3000);
						return false;
					}
				}
				$.ajax({
					url: theForm[0].action,
					data: theForm.serialize(),
					type: "POST",
					success: function(data, textStatus, jqXHR) {
						if (data.success) {
							var statusMessage = $("#status_message");
							statusMessage.removeClass("error");
							statusMessage.addClass("info");
							statusMessage.text(data.message);
							statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							setTimeout(function() {
								statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
							}, 3000);
						} else {
							var statusMessage = $("#status_message");
							statusMessage.removeClass("info");
							statusMessage.addClass("error");
							statusMessage.text(data.message);
							statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							setTimeout(function() {
								statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
							}, 3000);
							for (var i = 0; i < data.commands.length; i++) {
								/*switch (data.commands[i].action) {
									case "command...":
										break;
								}*/
							}
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						var statusMessage = $("#status_message");
						statusMessage.removeClass("info");
						statusMessage.addClass("error");
						statusMessage.text(errorThrown);
						statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
						setTimeout(function() {
							statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						}, 3000);
					}
				});
				
			}
		}
	});
	
	evt.preventDefault();
	return false;
}

/* Add User Related */
function addUser(evt) {
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for(var i=0;i<data.commands.length;i++) {
					/*switch (data.commands[i].action) {
						case "command...":
							break;
					}*/
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}

/* Update User Related */
function updateUser(evt) {
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					/*switch (data.commands[i].action) {
						case "command...":
							break;
					}*/
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}

/* Change Credentials Related */
function changeCredentials(evt) {
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i=0;i<data.commands.length;i++) {
					switch (data.commands[i].action) {
						case "removeCaptcha":
							removeRecaptcha();
							break;
					}
				}
				$("#change_credentials_page input, #change_credentials_page select").val("");
				$("#change_credentials_page select").selectmenu("refresh");
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i=0;i<data.commands.length;i++) {
					switch (data.commands[i].action) {
						case "loadCaptcha":
							var elevationCaptcha = $("#elevation_captcha")[0];
							if (elevationCaptcha.innerHTML == "") {
								showRecaptcha(elevationCaptcha);
							} else {
								reloadRecaptcha();
							}
							break;
						case "removeCaptcha":
							removeRecaptcha();
							break;
					}
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}

/* Profile Update Related */
function updateProfile(evt) {
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					/*switch (data.commands[i].action) {
						case "command...":
							break;
					}*/
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}

/* Forgot Password Related */
function resetPassword(evt) {
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				 if (data.message != '') {
					var statusMessage = $("#status_message");
					statusMessage.removeClass("error");
					statusMessage.addClass("info");
					statusMessage.text(data.message);
					statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
					if (data.commands.length == 0 || data.commands[0].action != "removeFields") {
						setTimeout(function() {
							statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						}, 3000);
					}
				}
				for (var i = 0; i < data.commands.length; i++) {
					switch (data.commands[i].action) {
						case "showPhase2":
							$("#password_reset_phase2 p").text(data.commands[i].params[0]);
							$("#password_reset_phase2 input").textinput("enable");
							$("#password_reset_phase1 input").textinput("disable");
							$("#password_reset_phase2 input").val("");
							$("#password_reset_phase1").stop(true, true).fadeOut({queue: false}).slideUp();
							$("#password_reset_phase2").stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							break;
						case "removeFields":
							$("#password_reset_phase2").stop(true, true).fadeOut({queue: false}).slideUp();
							$("#navigation_box input[type=submit]").parent().fadeOut();
							$("#navigation_box .ui-btn:first-of-type").css('margin-right', 'auto');
							break;
					}
				}
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					switch (data.commands[i].action) {
						case "showPhase1":
							$("#password_reset_phase2 p").text("");
							$("#password_reset_phase2 input").textinput("disable");
							$("#password_reset_phase1 input").textinput("enable");
							$("#password_reset_phase1 input").val("");
							$("#password_reset_phase2").stop(true, true).fadeOut({queue: false}).slideUp();
							$("#password_reset_phase1").stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							break;
					}
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}

function resetPasswordCompletion(evt) {
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				 if (data.message != '') {
					var statusMessage = $("#status_message");
					statusMessage.removeClass("error");
					statusMessage.addClass("info");
					statusMessage.text(data.message);
					statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
					if (data.commands.length == 0 || data.commands[0].action != "removeFields") {
						setTimeout(function() {
							statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						}, 3000);
					}
				}
				for (var i = 0; i < data.commands.length; i++) {
					switch (data.commands[i].action) {
						case "showPhase2":
							$("#password_reset_completion_phase2 input").textinput("enable");
							$("#password_reset_completion_phase1 input").textinput("disable");
							$("#password_reset_completion_phase1").stop(true, true).fadeOut({queue: false}).slideUp();
							$("#password_reset_completion_phase2").stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							break;
						case "removeFields":
							$("#password_reset_completion_phase2").stop(true, true).fadeOut({queue: false}).slideUp();
							$("#navigation_box input[type=submit]").parent().fadeOut();
							$("#navigation_box .ui-btn:first-of-type").css('margin-right', 'auto');
							break;
					}
				}
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i = 0; i < data.commands.length; i++) {
					switch (data.commands[i].action) {
						case "showPhase1":
							$("#password_reset_completion_phase2 input").textinput("disable");
							$("#password_reset_completion_phase1 input").textinput("enable");
							$("#password_reset_completion_phase2").stop(true, true).fadeOut({queue: false}).slideUp();
							$("#password_reset_completion_phase1").stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							break;
					}
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	evt.preventDefault();
	return false;
}

/* Registration Related */
function register(evt) {
	evt.preventDefault();
	
	try {
		var txt = $('#website').val();
		var re = /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
		if(!re.test(txt)) {
			$('#website').css('border', '1px solid #C23939');
			$('#website').parent('div').css('border', '1px solid #C23939');
			$('#website').focus();
			return false;
		} else {
			$('#website').css('border', '1px solid #00B200');
			$('#website').parent('div').css('border', '1px solid #00B200');
		}
		var geocodeAddresses = new Array();
		
		var addressFieldsets = $("#register_page fieldset");
		addressFieldsets.each(function(index) {
			var address = "";
			fields = $("input[type=text]:enabled, select:enabled", this);
			for (var i = 0; i < fields.length; i++) {
				curField = $(fields[i]);
				if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
					address += ", ";
				} else {
					address += " ";
				}
				address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
			}
			
			if (fields.length != 0) {
				curLatitude = $("input[name='latitude[]']", this);
				curLongitude = $("input[name='longitude[]']", this);
				
				runGeocode = false;
				
				if (curLatitude.length == 0) {
					latitudeElem = document.createElement("input");
					latitudeElem.setAttribute("type", "hidden");
					latitudeElem.setAttribute("name", "latitude[]");
					this.appendChild(latitudeElem);
					curLatitude = $(latitudeElem);
					runGeocode = true;
				}
				
				if (curLongitude.length == 0) {
					longitudeElem = document.createElement("input");
					longitudeElem.setAttribute("type", "hidden");
					longitudeElem.setAttribute("name", "longitude[]");
					this.appendChild(longitudeElem);
					curLongitude = $(longitudeElem);
					runGeocode = true;
				}
				
				if (!runGeocode) {
					if (curLatitude.val() == "" || curLongitude.val() == "") {
						runGeocode = true;
					}
				}
				
				if (runGeocode) {
					geocodeAddresses.push({address: address, fieldSet: this});
				}
			}
			
			if (index == addressFieldsets.length - 1) {
				if (geocodeAddresses.length > 0) {
					var runningGeocodes = geocodeAddresses.length;
					for (var i = 0; i < geocodeAddresses.length; i++) {
						geocodeInformation = geocodeAddresses[i];
						
						geocoder = new google.maps.Geocoder();
						geocoder.geocode({address: geocodeInformation.address}, function(address, fieldSet, results, status) {
							curLatitude = $("input[name='latitude[]']", fieldSet);
							curLongitude = $("input[name='longitude[]']", fieldSet);
							
							if (status == google.maps.GeocoderStatus.OK) {
								if (results[0].geometry.location_type.toUpperCase() == "ROOFTOP") {
									curLatitude.val(results[0].geometry.location.lat());
									curLongitude.val(results[0].geometry.location.lng());
								} else {
									curLatitude.val("");
									curLongitude.val("");
									console.log('Geocode for "' + address + '" was not successful for the following reason: Location type was not specific.');
								}
							} else {
								curLatitude.val("");
								curLongitude.val("");
								console.log('Geocode for "' + address + '" was not successful for the following reason: ' + status);
							}
							
							runningGeocodes--;
							
							if (runningGeocodes == 0) {
								theForm = $(fieldSet).closest("form");
								latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
								for (var j = 0; j < latLngs.length; j++) {
									if (latLngs[j].value == "") {
										var address = "";
										fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
										for (var k = 0; k < fields.length; k++) {
											curField = $(fields[k]);
											if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
												address += ", ";
											} else {
												address += " ";
											}
											address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
										}
										alert("Invalid Address: " + address);
										return false;
									}
								}
								theForm = $("#register_page form");
								$.ajax({
									url: theForm[0].action,
									data: theForm.serialize(),
									type: "POST",
									success: function(data, textStatus, jqXHR) {
										if (data.success) {
											var statusMessage = $("#status_message");
											statusMessage.removeClass("error");
											statusMessage.addClass("info");
											statusMessage.text(data.message);
											statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
											setTimeout(function() {
												statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
											}, 3000);
											$(document.body).pagecontainer("change", BASE_URL + '/account/');
										} else {
											var statusMessage = $("#status_message");
											statusMessage.removeClass("info");
											statusMessage.addClass("error");
											statusMessage.text(data.message);
											statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
											setTimeout(function() {
												statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
											}, 3000);
											for (var i = 0; i < data.commands.length; i++) {
												/*switch (data.commands[i].action) {
													case "command...":
														break;
												}*/
											}
										}
									},
									error: function(jqXHR, textStatus, errorThrown) {
										var statusMessage = $("#status_message");
										statusMessage.removeClass("info");
										statusMessage.addClass("error");
										statusMessage.text(errorThrown);
										statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
										setTimeout(function() {
											statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
										}, 3000);
									}
								});
							}
						}.bind(window, geocodeInformation.address, geocodeInformation.fieldSet));
					}
				} else {
					theForm = $(this).closest("form");
					latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
					for (var i = 0; i < latLngs.length; i++) {
						if (latLngs[i].value == "") {
							var address = "";
							fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
							for (var i = 0; i < fields.length; i++) {
								curField = $(fields[i]);
								if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
									address += ", ";
								} else {
									address += " ";
								}
								address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
							}
							alert("Invalid Address: " + address);
							return false;
						}
					}

					$.ajax({
						url: theForm[0].action,
						data: theForm.serialize(),
						type: "POST",
						success: function(data, textStatus, jqXHR) {
							
							if(data.success) {
								var statusMessage = $("#status_message");
								statusMessage.removeClass("error");
								statusMessage.addClass("info");
								statusMessage.text(data.message);
								statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
								setTimeout(function() {
									statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
								}, 3000);
								$(document.body).pagecontainer("change", BASE_URL + '/account/');
							} else {
								var statusMessage = $("#status_message");
								statusMessage.removeClass("info");
								statusMessage.addClass("error");
								statusMessage.text(data.message);
								statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
								setTimeout(function() {
									statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
								}, 3000);
								for (var i=0;i<data.commands.length;i++) {
									/*switch (data.commands[i].action) {
										case "command...":
											break;
									}*/
								}
							}
						},
						error: function(jqXHR, textStatus, errorThrown) {
							var statusMessage = $("#status_message");
							statusMessage.removeClass("info");
							statusMessage.addClass("error");
							statusMessage.text(errorThrown);
							statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
							setTimeout(function() {
								statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
							}, 3000);
						}
					});
				}
			}
		});
	} catch (e) {
		console.log(e);
	}

	return false;
}

/* Login Related */
function login(evt) {
	$("#navigation_box input[type=submit]").parent().fadeOut();
	$.ajax({
		url: evt.target.action,
		data: $(evt.target).serialize(),
		type: "POST",
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("error");
				statusMessage.addClass("info");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
					var loginRedirectURL = $("#login_redirect_url");
					if (loginRedirectURL.val() == BASE_URL + "/account/promotions/new/" || loginRedirectURL.val() == BASE_URL + "/account/perks/new/") {
						window.location.href = loginRedirectURL.val();
					} else {
						$(document.body).pagecontainer("change", loginRedirectURL.val());
					}
				}, 3000);
			} else {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(data.message);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
				for (var i=0;i<data.commands.length;i++) {
					switch (data.commands[i].action) {
						case "loadCaptcha":
							var loginCaptcha = $("#login_captcha")[0];
							if (loginCaptcha.innerHTML == "") {
								showRecaptcha(loginCaptcha);
							} else {
								reloadRecaptcha();
							}
							break;
					}
				}
				$("#navigation_box input[type=submit]").parent().fadeIn();
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			var statusMessage = $("#status_message");
			statusMessage.removeClass("info");
			statusMessage.addClass("error");
			statusMessage.text(errorThrown);
			statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
			setTimeout(function() {
				statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
			}, 3000);
		}
	});
	
	evt.preventDefault();
	return false;
}

/* New Promotion Related */
function getLocalTimezone(date) {
	tzo = -date.getTimezoneOffset(),
	dif = tzo >= 0 ? '+' : '-',
	pad = function(num) {
		norm = Math.abs(Math.floor(num));
		return (norm < 10 ? '0' : '') + norm;
	};
	return dif + pad(tzo / 60) + ':' + pad(tzo % 60);
}

function handleMSForm(e) {
	var next = "";
	
	var pageID = $(e.target).closest("section[data-role=page]").attr("id");

	// Validate...
	// If we got this far, we are either valid according to HTML5, or the browser does not support HTML5 form validation...
	if (typeof e.target.checkValidity !== "function") {
		var cancel = false;
		var message;
		if (pageID == "new_promotion_step2") {
			if ($("#promotion_name").val() == "") {
				cancel = true;
				message = "Promotion name is required.";
			} else if ($("#promotion_description").val() == "") {
				cancel = true;
				message = "Promotion description is required.";
			} else if ($("#promotion_value").val() == "") {
				cancel = true;
				message = "Promotion value is required.";
			} else if (parseFloat($("#promotion_value").val()).toFixed(2) == "") {
				cancel = true;
				message = "Promotion value must be a number to two decimal places.";
			} else if ($("#promotion_max_recipients").val() == "") {
				cancel = true;
				message = "Promotion maximum recipients is required.";
			} else if (parseInt($("#promotion_max_recipients").val()) == "") {
				cancel = true;
				message = "Promotion maximum recipients must be a positive number.";
			}
		} else if (pageID == "new_promotion_step3") {
			if ($("#eligibility_distance").val == "") {
				cancel = true;
				message = "Eligibility distance is required.";
			} else if (parseInt($("#eligibility_distance").val()) == "") {
				cancel = true;
				message = "Eligibility distance must be a positive number or zero.";
			} else if ($("#eligibility_distance").val() != "0") {
				latLngs = $("#" + pageID + " input[name='latitude[]'], #" + pageID + " input[name='longitude[]']");
				for (var i = 0; i < latLngs.length; i++) {
					if (latLngs[i].value == "") {
						var address = "";
						fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
						for (var i = 0; i < fields.length; i++) {
							curField = $(fields[i]);
							if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
								address += ", ";
							} else {
								address += " ";
							}
							address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
						}
						cancel = true;
						message = "Invalid Address: " + address;
					}
				}
			}
			
			if (cancel) {
				e.preventDefault();
				alert(message);
				return false;
			}
			
			if ($("#eligibility_distance").val() != "0") {
				reviewEligibilityFromAddresses.val("");
				
				var geocodeAddresses = new Array();
				
				var addressFieldsets = $("#new_promotion_step3 fieldset");
				addressFieldsets.each(function(index) {
					var address = "";
					fields = $("input[type=text]:enabled, select:enabled", this);
					for (var i = 0; i < fields.length; i++) {
						curField = $(fields[i]);
						if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
							address += ", ";
						} else {
							address += " ";
						}
						address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
					}
					
					if (fields.length != 0) {
						reviewEligibilityFromAddresses.val(reviewEligibilityFromAddresses.val() + address + "\n");
						
						curLatitude = $("input[name='latitude[]']", this);
						curLongitude = $("input[name='longitude[]']", this);
						
						runGeocode = false;
						
						if (curLatitude.length == 0) {
							latitudeElem = document.createElement("input");
							latitudeElem.setAttribute("type", "hidden");
							latitudeElem.setAttribute("name", "latitude[]");
							this.appendChild(latitudeElem);
							curLatitude = $(latitudeElem);
							runGeocode = true;
						}
						
						if (curLongitude.length == 0) {
							longitudeElem = document.createElement("input");
							longitudeElem.setAttribute("type", "hidden");
							longitudeElem.setAttribute("name", "longitude[]");
							this.appendChild(longitudeElem);
							curLongitude = $(longitudeElem);
							runGeocode = true;
						}
						
						if (!runGeocode) {
							if (curLatitude.val() == "" || curLongitude.val() == "") {
								runGeocode = true;
							}
						}
						
						if (runGeocode) {
							geocodeAddresses.push({address: address, fieldSet: this});
						}
					}
					
					if (index == addressFieldsets.length - 1) {
						if (geocodeAddresses.length > 0) {
							var runningGeocodes = geocodeAddresses.length;
							for (var i = 0; i < geocodeAddresses.length; i++) {
								geocodeInformation = geocodeAddresses[i];
								
								geocoder = new google.maps.Geocoder();
								geocoder.geocode({address: geocodeInformation.address}, function(address, fieldSet, results, status) {
									curLatitude = $("input[name='latitude[]']", fieldSet);
									curLongitude = $("input[name='longitude[]']", fieldSet);
									
									if (status == google.maps.GeocoderStatus.OK) {
										if (results[0].geometry.location_type.toUpperCase() == "ROOFTOP") {
											curLatitude.val(results[0].geometry.location.lat());
											curLongitude.val(results[0].geometry.location.lng());
										} else {
											curLatitude.val("");
											curLongitude.val("");
											console.log('Geocode for "' + address + '" was not successful for the following reason: Location type was not specific.');
										}
									} else {
										curLatitude.val("");
										curLongitude.val("");
										console.log('Geocode for "' + address + '" was not successful for the following reason: ' + status);
									}
									
									runningGeocodes--;
									
									if (runningGeocodes == 0) {
										theForm = $(fieldSet).closest("form");
										latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
										for (var j = 0; j < latLngs.length; j++) {
											if (latLngs[j].value == "") {
												var address = "";
												fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
												for (var k = 0; k < fields.length; k++) {
													curField = $(fields[k]);
													if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
														address += ", ";
													} else {
														address += " ";
													}
													address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
												}
												alert("Invalid Address: " + address);
												return false;
											}
										}
										
										$(document.body).pagecontainer("change", "#new_promotion_step4", {transition: "flip"});
									}
								}.bind(window, geocodeInformation.address, geocodeInformation.fieldSet));
							}
						} else {
							theForm = $(this).closest("form");
							latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
							for (var i = 0; i < latLngs.length; i++) {
								if (latLngs[i].value == "") {
									var address = "";
									fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
									for (var i = 0; i < fields.length; i++) {
										curField = $(fields[i]);
										if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
											address += ", ";
										} else {
											address += " ";
										}
										address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
									}
									alert("Invalid Address: " + address);
									return false;
								}
							}
							
							$(document.body).pagecontainer("change", "#new_promotion_step4", {transition: "flip"});
						}
					}
				});
						
				e.preventDefault();
				return false;
			}
		} else if (pageID == "new_promotion_step4") {
			if ($("#checkin_option option:selected").val() != "") {
				if ($("#checkin_location").val() == "") {
					cancel = true;
					message = "Checkin Location is required.";
				} else if ($("#checkin_hashtag").val() == "") {
					cancel = true;
					message = "Checkin Hashtag is required.";
				} else if ($("#checkin_hashtag").val().indexOf(" ") != -1) {
					cancel = true;
					message = "Checkin Hashtag may not contain spaces.";
				}
			} else if ($("#post_option option:selected").val() != "") {
				if ($("#post_link").val() == "") {
					cancel = true;
					message = "Post Link is required.";
				}
			} else if ($("#like_option option:selected").val() != "") {
				if ($("#like_link").val() == "") {
					cancel = true;
					message = "Like Link is required.";
				}
			} else {
				cancel = true;
				message = "At least one option must be selected.";
			}
		} else if (pageID == "new_promotion_step5") {
			if (!$("#agrees_to_terms")[0].checked) {
				cancel = true;
				message = "You must indicate your agreement to the above terms.";
			} else if ($("#signatory").val() == "") {
				cancel = true;
				message = "Your e-signature is required.";
			}
		} else if (pageID == "new_perk_step2") {
			if ($("#perk_name").val() == "") {
				cancel = true;
				message = "Perk name is required.";
			} else if ($("#perk_description").val() == "") {
				cancel = true;
				message = "Perk description is required.";
			} else if ($("#perk_value").val() == "") {
				cancel = true;
				message = "Perk value is required.";
			} else if (parseFloat($("#perk_value").val()).toFixed(2) == "") {
				cancel = true;
				message = "Perk value must be a number to two decimal places.";
			}
			
		} else if (pageID == "new_perk_step3") {
			if ($("#eligibility_distance").val == "") {
				cancel = true;
				message = "Eligibility distance is required.";
			} else if (parseInt($("#eligibility_distance").val()) == "") {
				cancel = true;
				message = "Eligibility distance must be a positive number or zero.";
			} else if ($("#eligibility_distance").val() != "0") {
				latLngs = $("#" + pageID + " input[name='latitude[]'], #" + pageID + " input[name='longitude[]']");
				for (var i = 0; i < latLngs.length; i++) {
					if (latLngs[i].value == "") {
						var address = "";
						fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
						for (var i = 0; i < fields.length; i++) {
							curField = $(fields[i]);
							if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
								address += ", ";
							} else {
								address += " ";
							}
							address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
						}
						cancel = true;
						message = "Invalid Address: " + address;
					}
				}
			}
			
			if (cancel) {
				e.preventDefault();
				alert(message);
				return false;
			}
			
			// First run geocodes
			if ($("#eligibility_distance").val() != "0") {
				reviewEligibilityFromAddresses.val("");
				
				var geocodeAddresses = new Array();
				
				var addressFieldsets = $("#new_perk_step3 fieldset");
				addressFieldsets.each(function(index) {
					var address = "";
					fields = $("input[type=text]:enabled, select:enabled", this);
					for (var i = 0; i < fields.length; i++) {
						curField = $(fields[i]);
						if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
							address += ", ";
						} else {
							address += " ";
						}
						address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
					}
					
					if (fields.length != 0) {
						reviewEligibilityFromAddresses.val(reviewEligibilityFromAddresses.val() + address + "\n");
						
						curLatitude = $("input[name='latitude[]']", this);
						curLongitude = $("input[name='longitude[]']", this);
						
						runGeocode = false;
						
						if (curLatitude.length == 0) {
							latitudeElem = document.createElement("input");
							latitudeElem.setAttribute("type", "hidden");
							latitudeElem.setAttribute("name", "latitude[]");
							this.appendChild(latitudeElem);
							curLatitude = $(latitudeElem);
							runGeocode = true;
						}
						
						if (curLongitude.length == 0) {
							longitudeElem = document.createElement("input");
							longitudeElem.setAttribute("type", "hidden");
							longitudeElem.setAttribute("name", "longitude[]");
							this.appendChild(longitudeElem);
							curLongitude = $(longitudeElem);
							runGeocode = true;
						}
						
						if (!runGeocode) {
							if (curLatitude.val() == "" || curLongitude.val() == "") {
								runGeocode = true;
							}
						}
						
						if (runGeocode) {
							geocodeAddresses.push({address: address, fieldSet: this});
						}
					}
					
					if (index == addressFieldsets.length - 1) {
						if (geocodeAddresses.length > 0) {
							var runningGeocodes = geocodeAddresses.length;
							for (var i = 0; i < geocodeAddresses.length; i++) {
								geocodeInformation = geocodeAddresses[i];
								
								geocoder = new google.maps.Geocoder();
								geocoder.geocode({address: geocodeInformation.address}, function(address, fieldSet, results, status) {
									curLatitude = $("input[name='latitude[]']", fieldSet);
									curLongitude = $("input[name='longitude[]']", fieldSet);
									
									if (status == google.maps.GeocoderStatus.OK) {
										if (results[0].geometry.location_type.toUpperCase() == "ROOFTOP") {
											curLatitude.val(results[0].geometry.location.lat());
											curLongitude.val(results[0].geometry.location.lng());
										} else {
											curLatitude.val("");
											curLongitude.val("");
											console.log('Geocode for "' + address + '" was not successful for the following reason: Location type was not specific.');
										}
									} else {
										curLatitude.val("");
										curLongitude.val("");
										console.log('Geocode for "' + address + '" was not successful for the following reason: ' + status);
									}
									
									runningGeocodes--;
									
									if (runningGeocodes == 0) {
										theForm = $(fieldSet).closest("form");
										latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
										for (var j = 0; j < latLngs.length; j++) {
											if (latLngs[j].value == "") {
												var address = "";
												fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
												for (var k = 0; k < fields.length; k++) {
													curField = $(fields[k]);
													if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
														address += ", ";
													} else {
														address += " ";
													}
													address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
												}
												alert("Invalid Address: " + address);
												return false;
											}
										}

										$(document.body).pagecontainer("change", "#new_perk_step4", {transition: "flip"});
									}
								}.bind(window, geocodeInformation.address, geocodeInformation.fieldSet));
							}
						} else {
							theForm = $(this).closest("form");
							latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
							for (var i = 0; i < latLngs.length; i++) {
								if (latLngs[i].value == "") {
									var address = "";
									fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
									for (var i = 0; i < fields.length; i++) {
										curField = $(fields[i]);
										if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
											address += ", ";
										} else {
											address += " ";
										}
										address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
									}
									alert("Invalid Address: " + address);
									return false;
								}
							}
							
							$(document.body).pagecontainer("change", "#new_perk_step4", {transition: "flip"});
						}
					}
				});
						
				e.preventDefault();
				return false;
			}
		} else if (pageID == "new_perk_step4") {
			if (!$("#agrees_to_terms")[0].checked) {
				cancel = true;
				message = "You must indicate your agreement to the above terms.";
			} else if ($("#signatory").val() == "") {
				cancel = true;
				message = "Your e-signature is required.";
			}
		}
		
		if (cancel) {
			e.preventDefault();
			alert(message);
			return false;
		}
	} else {
		if (pageID == "new_promotion_step3") {
			if ($("#eligibility_distance").val() != "0") {
				reviewEligibilityFromAddresses.val("");
				
				var geocodeAddresses = new Array();
				
				var addressFieldsets = $("#new_promotion_step3 fieldset");
				addressFieldsets.each(function(index) {
					var address = "";
					fields = $("input[type=text]:enabled, select:enabled", this);
					for (var i = 0; i < fields.length; i++) {
						curField = $(fields[i]);
						if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
							address += ", ";
						} else {
							address += " ";
						}
						address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
					}
					
					if (fields.length != 0) {
						reviewEligibilityFromAddresses.val(reviewEligibilityFromAddresses.val() + address + "\n");
						
						curLatitude = $("input[name='latitude[]']", this);
						curLongitude = $("input[name='longitude[]']", this);
						
						runGeocode = false;
						
						if (curLatitude.length == 0) {
							latitudeElem = document.createElement("input");
							latitudeElem.setAttribute("type", "hidden");
							latitudeElem.setAttribute("name", "latitude[]");
							this.appendChild(latitudeElem);
							curLatitude = $(latitudeElem);
							runGeocode = true;
						}
						
						if (curLongitude.length == 0) {
							longitudeElem = document.createElement("input");
							longitudeElem.setAttribute("type", "hidden");
							longitudeElem.setAttribute("name", "longitude[]");
							this.appendChild(longitudeElem);
							curLongitude = $(longitudeElem);
							runGeocode = true;
						}
						
						if (!runGeocode) {
							if (curLatitude.val() == "" || curLongitude.val() == "") {
								runGeocode = true;
							}
						}
						
						if (runGeocode) {
							geocodeAddresses.push({address: address, fieldSet: this});
						}
					}
					
					if (index == addressFieldsets.length - 1) {
						if (geocodeAddresses.length > 0) {
							var runningGeocodes = geocodeAddresses.length;
							for (var i = 0; i < geocodeAddresses.length; i++) {
								geocodeInformation = geocodeAddresses[i];
								
								geocoder = new google.maps.Geocoder();
								geocoder.geocode({address: geocodeInformation.address}, function(address, fieldSet, results, status) {
									curLatitude = $("input[name='latitude[]']", fieldSet);
									curLongitude = $("input[name='longitude[]']", fieldSet);
									
									if (status == google.maps.GeocoderStatus.OK) {
										if (results[0].geometry.location_type.toUpperCase() == "ROOFTOP") {
											curLatitude.val(results[0].geometry.location.lat());
											curLongitude.val(results[0].geometry.location.lng());
										} else {
											curLatitude.val("");
											curLongitude.val("");
											console.log('Geocode for "' + address + '" was not successful for the following reason: Location type was not specific.');
										}
									} else {
										curLatitude.val("");
										curLongitude.val("");
										console.log('Geocode for "' + address + '" was not successful for the following reason: ' + status);
									}
									
									runningGeocodes--;
									
									if (runningGeocodes == 0) {
										theForm = $(fieldSet).closest("form");
										latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
										for (var j = 0; j < latLngs.length; j++) {
											if (latLngs[j].value == "") {
												var address = "";
												fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
												for (var k = 0; k < fields.length; k++) {
													curField = $(fields[k]);
													if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
														address += ", ";
													} else {
														address += " ";
													}
													address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
												}
												alert("Invalid Address: " + address);
												return false;
											}
										}
										
										$(document.body).pagecontainer("change", "#new_promotion_step4", {transition: "flip"});
									}
								}.bind(window, geocodeInformation.address, geocodeInformation.fieldSet));
							}
						} else {
							theForm = $(this).closest("form");
							latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
							for (var i = 0; i < latLngs.length; i++) {
								if (latLngs[i].value == "") {
									var address = "";
									fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
									for (var i = 0; i < fields.length; i++) {
										curField = $(fields[i]);
										if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
											address += ", ";
										} else {
											address += " ";
										}
										address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
									}
									alert("Invalid Address: " + address);
									return false;
								}
							}
							
							$(document.body).pagecontainer("change", "#new_promotion_step4", {transition: "flip"});
						}
					}
				});
						
				e.preventDefault();
				return false;
			}
		} else if (pageID == "new_promotion_step4") {
			// We have HTML5 validation, lets use it for the complex step4 navigation instead of the above!... Someday...
			if ($("#checkin_option option:selected").val() == "" && $("#post_option option:selected").val() == "" && $("#like_option option:selected").val() == "") {
				alert("At least one option must be selected.");
				e.preventDefault();
				return false;
			}
		} else if (pageID == "new_perk_step3") {
			if ($("#eligibility_distance").val() != "0") {
				reviewEligibilityFromAddresses.val("");
				
				var geocodeAddresses = new Array();
				
				var addressFieldsets = $("#new_perk_step3 fieldset");
				addressFieldsets.each(function(index) {
					var address = "";
					fields = $("input[type=text]:enabled, select:enabled", this);
					for (var i = 0; i < fields.length; i++) {
						curField = $(fields[i]);
						if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
							address += ", ";
						} else {
							address += " ";
						}
						address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
					}
					
					if (fields.length != 0) {
						reviewEligibilityFromAddresses.val(reviewEligibilityFromAddresses.val() + address + "\n");
						
						curLatitude = $("input[name='latitude[]']", this);
						curLongitude = $("input[name='longitude[]']", this);
						
						runGeocode = false;
						
						if (curLatitude.length == 0) {
							latitudeElem = document.createElement("input");
							latitudeElem.setAttribute("type", "hidden");
							latitudeElem.setAttribute("name", "latitude[]");
							this.appendChild(latitudeElem);
							curLatitude = $(latitudeElem);
							runGeocode = true;
						}
						
						if (curLongitude.length == 0) {
							longitudeElem = document.createElement("input");
							longitudeElem.setAttribute("type", "hidden");
							longitudeElem.setAttribute("name", "longitude[]");
							this.appendChild(longitudeElem);
							curLongitude = $(longitudeElem);
							runGeocode = true;
						}
						
						if (!runGeocode) {
							if (curLatitude.val() == "" || curLongitude.val() == "") {
								runGeocode = true;
							}
						}
						
						if (runGeocode) {
							geocodeAddresses.push({address: address, fieldSet: this});
						}
					}
					
					if (index == addressFieldsets.length - 1) {
						if (geocodeAddresses.length > 0) {
							var runningGeocodes = geocodeAddresses.length;
							for (var i = 0; i < geocodeAddresses.length; i++) {
								geocodeInformation = geocodeAddresses[i];
								
								geocoder = new google.maps.Geocoder();
								geocoder.geocode({address: geocodeInformation.address}, function(address, fieldSet, results, status) {
									curLatitude = $("input[name='latitude[]']", fieldSet);
									curLongitude = $("input[name='longitude[]']", fieldSet);
									
									if (status == google.maps.GeocoderStatus.OK) {
										if (results[0].geometry.location_type.toUpperCase() == "ROOFTOP") {
											curLatitude.val(results[0].geometry.location.lat());
											curLongitude.val(results[0].geometry.location.lng());
										} else {
											curLatitude.val("");
											curLongitude.val("");
											console.log('Geocode for "' + address + '" was not successful for the following reason: Location type was not specific.');
										}
									} else {
										curLatitude.val("");
										curLongitude.val("");
										console.log('Geocode for "' + address + '" was not successful for the following reason: ' + status);
									}
									
									runningGeocodes--;
									
									if (runningGeocodes == 0) {
										theForm = $(fieldSet).closest("form");
										latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
										for (var j = 0; j < latLngs.length; j++) {
											if (latLngs[j].value == "") {
												var address = "";
												fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
												for (var k = 0; k < fields.length; k++) {
													curField = $(fields[k]);
													if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
														address += ", ";
													} else {
														address += " ";
													}
													address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
												}
												alert("Invalid Address: " + address);
												return false;
											}
										}

										$(document.body).pagecontainer("change", "#new_perk_step4", {transition: "flip"});
									}
								}.bind(window, geocodeInformation.address, geocodeInformation.fieldSet));
							}
						} else {
							theForm = $(this).closest("form");
							latLngs = $("input[name='latitude[]'], input[name='longitude[]']", theForm);
							for (var i = 0; i < latLngs.length; i++) {
								if (latLngs[i].value == "") {
									var address = "";
									fields = $("input[type=text]:enabled, select:enabled", latLngs[i].parentNode);
									for (var i = 0; i < fields.length; i++) {
										curField = $(fields[i]);
										if (curField[0].name == "locality[]" || curField[0].name =="postal_code[]") {
											address += ", ";
										} else {
											address += " ";
										}
										address += (curField.prop("tagName").toUpperCase() == "INPUT" ? curField.val() : $(curField[0].options[curField[0].options.selectedIndex]).text());
									}
									alert("Invalid Address: " + address);
									return false;
								}
							}
							
							$(document.body).pagecontainer("change", "#new_perk_step4", {transition: "flip"});
						}
					}
				});
						
				e.preventDefault();
				return false;
			}
		}
	}
	
	next = this.nextStep.value;

	//now - we need to go the next page...
	//if next step isn't a full url, we assume internal link
	//logic will be, if something.something, do a post
	if (next.indexOf(".") == -1) {
		var nextPage = "#" + next;
		$(document.body).pagecontainer("change", nextPage, {transition: "flip"});
	} else {
		//gather the fields
		var formData = {};
		
		var forms = $("form");
		for (var i = 0; i < forms.length; i++) {
			theForm = forms[i];
			var data = $(theForm).serializeArray();

			//store them - assumes unique names
			for (var j=0; j < data.length; j++) {
				//If nextStep, it's our metadata, don't store it in formdata
				if (data[j].name == "nextStep") {
					continue;
				}
				
				if (data[j].name.indexOf("[]") == data[j].name.length - 2) {
					if (!formData.hasOwnProperty(data[j].name)) {
						formData[data[j].name] = new Array();
					}
					formData[data[j].name].push(data[j].value);
				} else {
					formData[data[j].name] = data[j].value;
				}
			}
		}
		
		var encodedData = "";
		for (var key in formData) {
			if (formData.hasOwnProperty(key)) {
				if (typeof formData[key] == "string") {
					encodedData += key + "=" + formData[key] + "&";
				} else {
					for (var i = 0; i < formData[key].length; i++) {
						encodedData += key + "=" + formData[key][i] + "&";
					}
				}
			}
		}
		
		if (encodedData != "") {
			encodedData = encodedData.substring(0, encodedData.length - 1);
		}
		
		$("#navigation_box").stop(true, true).fadeOut({queue: false}).slideUp();
		
		$.ajax({
			url: next,
			type: "POST",
			data: encodedData,
			success: function(data, textStatus, jqXHR) {
				if(data.success) {
					var statusMessage = $("#status_message");
					statusMessage.removeClass("error");
					statusMessage.addClass("info");
					statusMessage.text(data.message);
					statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
					setTimeout(function() {
						statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
						window.location.href = BASE_URL + '/account/';
					}, 3000);
				} else {
					var statusMessage = $("#status_message");
					statusMessage.removeClass("info");
					statusMessage.addClass("error");
					statusMessage.text(data.message);
					statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
					$("#navigation_box").stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
					setTimeout(function() {
						statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
					}, 3000);
					for (var i=0;i<data.commands.length;i++) {
						/*switch (data.commands[i].action) {
							case "command...":
								break;
						}*/
					}
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				var statusMessage = $("#status_message");
				statusMessage.removeClass("info");
				statusMessage.addClass("error");
				statusMessage.text(errorThrown);
				statusMessage.stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				$("#navigation_box").stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
				setTimeout(function() {
					statusMessage.stop(true, true).fadeOut({queue: false}).slideUp();
				}, 3000);
			}
		});
	}
	
	e.preventDefault();
	return false;
}
