<?php
require_once(dirname(dirname(__FILE)) . "/../phpinc/db.php");
if (!isset($_GET["uuid"]) || strlen($_GET["uuid"]) != 32 || !ctype_xdigit($_GET["uuid"])) {
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>

<section data-role="page" id="verify_email_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p>Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br />
            <div class="green-header">
                <p>Email Verification</p>
            </div>
            <div class="grey-box">
			<?php
			require_once(BASE_PATH . "/../phpinc/db.php");
			require_once(BASE_PATH . "/../phpinc/session.php");

if (verifyEmail($_GET["uuid"])) {
commitChanges();
?>
                <p>Email successfully verified!</p><?php
if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
    $_SESSION['redirectURL'] = BASE_URL . '/account/';
?>
                <p>Redirecting to login page...</p>
                <script type="text/javascript">
                    setTimeout(function() {
                        $(document.body).pagecontainer('change', '<?php echo BASE_URL; ?>/login/');
                    }, 3000);
                </script><?php
} else {
?>
                <p>Redirecting to Account Portal...</p>
                <script type="text/javascript">
                    setTimeout(function() {
                        $(document.body).pagecontainer('change', '<?php echo BASE_URL; ?>/account/');
                    }, 3000);
                </script><?php
}
} else {
?>
                <p>Unable to verify email address. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.</p><?php
}
?>
            </div>
            <div id="navigation_box">
                <a href="<?php echo BASE_URL; ?>/account/">
                    <button>Account Portal</button>
                </a>
            </div>
            <br />
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>
