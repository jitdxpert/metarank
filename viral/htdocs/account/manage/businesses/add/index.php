<?php
require_once(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/manage/businesses/add/';
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

require_once(BASE_PATH . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if ($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR) {
	$backPageURL = BASE_URL . "/account/manage/";
	include(BASE_PATH . "/../phpinc/errors/insufficient_permissions.php");
	exit();
}

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>

<section data-role="page" id="add_business_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <div id="step_header" class="green-box">
                <p>Add Business</p>
            </div>
            <br />
            <form action="<?php echo BASE_URL; ?>/ajax/add_business.php" method="POST">
                <div id="fields">
                    <?php echo render_address_form(array(), array("first_name", "last_name"), "0000"); ?>
                    <label for="business_category">Business Category:</label>
                    <select id="business_category" name="business_category">
						<?php
						$businessCategories = getBusinessCategories(null);
						foreach ($businessCategories as $businessCategory) { ?>
                        	<option value="<?php echo $businessCategory["id"]; ?>"><?php echo $businessCategory["name"]; ?></option>
						<?php
						} ?>
                    </select>
                    <label for="website">Website:</label>
                    <input type="url" id="website" name="website" required="required" />
                </div>
                <div id="status_message" style="display:none;"></div>
                <div id="navigation_box" class="grey-box">
                    <p>
                    	<button type="submit">Add</button>
                        <button type="button" onclick="window.history.back()">Back</button>
                    </p>
                </div>
            </form>
            <script type="text/javascript">
            $("#add_business_page form").on("submit", addBusiness);
            </script>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>