<?php
require_once(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/manage/businesses/edit/';
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

require_once(BASE_PATH . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if (($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR) || !checkBusinessInAccount($_GET["b"], $_SESSION["accountID"])) {
	$backPageURL = BASE_URL . "/account/manage/";
	include(BASE_PATH . "/../phpinc/errors/insufficient_permissions.php");
	exit();
}

$editBusiness = getBusiness($_GET["b"]);

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>
		
<section data-role="page" id="edit_business_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <div id="step_header" class="green-box">
                <p>Edit Business</p>
            </div>
            <br />
            <form action="<?php echo BASE_URL; ?>/ajax/update_business.php" method="POST">
                <div id="fields">
                    <input type="hidden" name="b" value="<?php echo $_GET["b"]; ?>">
                    <input type="hidden" name="address_id[]" value="<?php echo $editBusiness["primary_address_id"]; ?>">
					<?php echo render_address_form(array("organization_name" => $editBusiness["name"], "thoroughfare" => $editBusiness["thoroughfare"], "premise" => $editBusiness["premise"], "locality" => $editBusiness["locality"], "dependent_locality" => $editBusiness["dependent_locality"], "administrative_area" => $editBusiness["administrative_area"], "postal_code" => $editBusiness["postal_code"], "country" => $editBusiness["country_id"]), array("first_name", "last_name"), "0000");
					$businessAddresses = getBusinessAddresses($_GET["b"]);
					if (count($businessAddresses) > 0) { ?>
	                    <h1>Additional Addresses</h1>
						<?php
                        $addressIndex = 1;
                        foreach ($businessAddresses as $businessAddress) { ?>
                            <input type="hidden" name="address_id[]" value="<?php echo $businessAddress["id"]; ?>">
                            <?php
                            echo render_address_form(array("thoroughfare" => $businessAddress["thoroughfare"], "premise" => $businessAddress["premise"], "locality" => $businessAddress["locality"], "dependent_locality" => $businessAddress["dependent_locality"], "administrative_area" => $businessAddress["administrative_area"], "postal_code" => $businessAddress["postal_code"], "country" => $businessAddress["country_id"]), array("first_name", "last_name", "organization_name"), str_pad($addressIndex, 4, "0", STR_PAD_LEFT));
                            $addressIndex++;
                        }
                    } ?>
                    <label for="business_category">Business Category:</label>
                    <select id="business_category" name="business_category">
						<?php
						$businessCategories = getBusinessCategories($_GET["b"]);
						foreach ($businessCategories as $businessCategory) { ?>
                        	<option value="<?php echo $businessCategory["id"]; ?>"<?php echo ($businessCategory["selected"] == "1" ? " selected" : ""); ?>><?php echo $businessCategory["name"]; ?></option>
						<?php
						} ?>
                    </select>
                    <label for="website">Website:</label>
                    <input type="url" id="website" name="website" required="required" value="<?php echo $editBusiness["website"]; ?>" />
                </div>
                <div id="status_message" style="display:none;"></div>
                <div id="navigation_box" class="grey-box">
                    <p>
                    	<button type="submit">Update</button>
                        <button type="button" onclick="window.history.back()">Back</button>
                    </p>
                </div>
            </form>
            <script type="text/javascript">
            $("#edit_business_page form").on("submit", updateBusiness);
            </script>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section>><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>