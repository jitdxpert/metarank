<?php
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/manage/businesses/';
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

require_once(BASE_PATH . "/../phpinc/db.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);
if ($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR) {
	$backPageURL = "/account/manage/";
	include(BASE_PATH . "/../phpinc/errors/insufficient_permissions.php");
	exit();
}

$accountBusinesses = getAccountBusinesses($_SESSION["accountID"], null);
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>

<section data-role="page" id="account_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <div class="green-header">
                <p>Business Management</p>
            </div>
			<?php if (count($accountBusinesses) > 0) { ?>
                <div class="grey-box">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2">
                                    <p class="orange-text larger-message">Edit Businesses</p>
                                </th>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <th>Operation</th>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="2">&nbsp;</th>
                            </tr>
                        </tfoot>
                        <tbody>
							<?php foreach ($accountBusinesses as $accountBusiness) { ?>
                                <tr>
                                    <td><?php echo $accountBusiness["name"]; ?></td>
                                    <td>
                                    	<a href="<?php echo BASE_URL; ?>/account/manage/businesses/edit/?b=<?php echo $accountBusiness["id"]; ?>">
                                    		<button class="edit_business">Edit</button>
                                        </a>
                                    </td>
                                </tr>
							<?php } ?>
                        </tbody>
                    </table>
                </div>
			<?php } ?>
            <br /><br />
            <div id="manage_businesses_navigation">
                <a href="<?php echo BASE_URL; ?>/account/manage/businesses/add/">
                    <button>Add Business</button>
                </a>
                <br /><br />
                <br />
                <a onclick="window.history.back()">
                    <button>Back</button>
                </a>
            </div>
            <br />
            <br />
            <div id="social_media">
                <p>Follow Us On...</p>
                <p>
                    <a href="//www.facebook.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/facebook.png" alt="Facebook" title="Facebook" /></a>
                    <a href="//www.twitter.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/twitter.png" alt="Twitter" title="Twitter" /></a>
                    <a href="//metarank.tumblr.com" target="_blank"><img src="<?php echo BASE_URL; ?>/img/tumblr.png" alt="Tumblr" title="Tumblr" /></a>
                    <a href="//www.pinterest.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/pinterest.png" alt="Pinterest" title="Pinterest" /></a>
                    <a href="//www.google.com/+MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/gplus.png" alt="Google+" title="Google+" /></a>
                    <a href="//www.instagram.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/instagram.png" alt="Instagram" title="Instagram" /></a>
                </p>
            </div>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>