<?php
require_once(dirname(dirname(dirname(__FILE__))) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/manage/';
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

require_once(BASE_PATH . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);
$primaryBusiness = getAccountPrimaryBusiness($_SESSION["accountID"]);

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>

<section data-role="page" id="account_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <div class="green-header">
                <p>Manage Account</p>
            </div>
			<?php if ($_SESSION["userID"] == $primaryAccountHolder["id"] || ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) == PERMISSION_ADMINISTRATOR) { ?>
                <div class="grey-box">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="5">
                                    <p class="orange-text larger-message">Active Users</p>
                                </th>
                            </tr>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Session Status</th>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="5">&nbsp;</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php
                            $accountActiveSessions = getActiveSessions($_SESSION["accountID"]);
                            foreach ($accountActiveSessions as $activeSession) { ?>
                                <tr>
                                    <td><?php echo $activeSession["first_name"]; ?></td>
                                    <td><?php echo $activeSession["last_name"]; ?></td>
                                    <td><?php echo $activeSession["email"]; ?></td>
                                    <td><?php echo $activeSession["phone"]; ?></td>
                                    <td><?php echo ($activeSession["pending_delete"] == "1" ? "Pending Delete" : "Active"); ?></td>
                                </tr>
                            <?php 
                            } ?>
                        </tbody>
                    </table>
                </div>
			<?php } ?>
            <div id="manage_account_navigation">
                <p class="orange-text larger-message bolded">Actions</p>
				<?php if ($_SESSION["userID"] == $primaryAccountHolder["id"]) { ?>
                    <a href="<?php echo BASE_URL; ?>/account/manage/billing/">
                        <button>Billing Management</button>
                    </a>
                    <br /><br />
                    <a href="<?php echo BASE_URL; ?>/account/manage/businesses/">
                        <button>Business Management</button>
                    </a>
                    <br /><br />
                <?php } 
				if ($_SESSION["userID"] == $primaryAccountHolder["id"] || ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) == PERMISSION_ADMINISTRATOR || ($_SESSION["userPermissions"] & PERMISSION_MANAGER) == PERMISSION_MANAGER) { ?>
	                <a href="<?php echo BASE_URL; ?>/account/manage/users/">
    	                <button>User Management</button>
        	        </a>
                    <br /><br />
				<?php } ?>
                <a href="<?php echo BASE_URL; ?>/account/manage/me/">
                    <button>Update Profile Information</button>
                </a>
                <br /><br/>
                <a href="<?php echo BASE_URL; ?>/account/manage/credentials/">
                    <button>Change Security Credentials</button>
                </a>
                <br /><br />
                <br />
                <a onclick="window.history.back()">
                    <button>Back</button>
                </a>
            </div>
            <br />
            <br />
            <div id="social_media">
                <p>Follow Us On...</p>
                <p>
                    <a href="//www.facebook.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/facebook.png" alt="Facebook" title="Facebook" /></a>
                    <a href="//www.twitter.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/twitter.png" alt="Twitter" title="Twitter" /></a>
                    <a href="//metarank.tumblr.com" target="_blank"><img src="<?php echo BASE_URL; ?>/img/tumblr.png" alt="Tumblr" title="Tumblr" /></a>
                    <a href="//www.pinterest.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/pinterest.png" alt="Pinterest" title="Pinterest" /></a>
                    <a href="//www.google.com/+MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/gplus.png" alt="Google+" title="Google+" /></a>
                    <a href="//www.instagram.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/instagram.png" alt="Instagram" title="Instagram" /></a>
                </p>
            </div>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section>><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>