<?php
require_once(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/manage/users/add/';
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

require_once(BASE_PATH . "/../phpinc/db.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if (($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR) && ($_SESSION["userPermissions"] & PERMISSION_MANAGER) != PERMISSION_MANAGER) {
	$backPageURL = BASE_URL . "/account/manage/";
	include(BASE_PATH . "/../phpinc/errors/insufficient_permissions.php");
	exit();
}

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>

<section data-role="page" id="add_user_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <div id="step_header" class="green-box">
                <p>Add User</p>
            </div>
            <br />
            <form action="<?php echo BASE_URL; ?>/ajax/add_user.php" method="POST">
                <div id="fields">
                    <label for="first_name">First Name:</label>
                    <input type="text" id="first_name" name="first_name" required />
                    <label for="last_name">Last Name:</label>
                    <input type="text" id="last_name" name="last_name" required />
                    <label for="phone">Phone:</label>
                    <input type="tel" id="phone" name="phone" required />
                    <label for="email">Email:</label>
                    <input type="email" id="email" name="email" />
                    <label for="password">Password:</label>
                    <input type="password" id="password" name="password" pattern="<?php echo PASSWORD_HTML5_REGULAR_EXPRESSION; ?>" onchange="
                        this.setCustomValidity(this.validity.patternMismatch ? 'Password must be at least 14 characters long and contain at least one digit, one uppercase letter, one lowercase letter, and one of the following special characters: !#$%&()*+,-./:;<=>?@\\\^_{|}~' : '');
                        if(this.checkValidity()) document.getElementById('password_confirm').pattern = this.value.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
                    "/>
                    <label for="password_confirm">Confirm Password:</label>
                    <input type="password" id="password_confirm" name="password_confirm" pattern="<?php echo PASSWORD_HTML5_REGULAR_EXPRESSION; ?>" onchange="
                        this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above.' : '');
                    " />
                    <label for="role">Role:</label>
                    <select id="role" name="role">
                        <?php
                        if ($_SESSION["userID"] == $primaryAccountHolder["id"]) {
                            $roles = getRoles(null, null);
                        } else if (($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) == PERMISSION_ADMINISTRATOR) {
                            $roles = getRoles(array('Administrator'), null);
                        } else {
                            $roles = getRoles(array('Administrator', 'Manager'), null);
                        }
                        foreach($roles as $role) { ?>
                            <option value="<?php echo $role["id"]; ?>"><?php echo $role["name"]; ?></option>
                        <?php 
                        } ?>
                    </select>
                    <label for="businesses">Business(es):</label>
                    <div id="businesses">
                        <?php
                        if ($_SESSION["userID"] == $primaryAccountHolder["id"] || ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) == PERMISSION_ADMINISTRATOR) {
                            $businesses = getAccountBusinesses($_SESSION["accountID"], null);
                        } else {
                            $businesses = getUserBusinesses($_SESSION["userID"], null);
                        }
                        foreach($businesses as $business) { ?>
                            <label for="business_<?php echo $business["id"]; ?>"><?php echo $business["name"]; ?></label>
                            <input type="checkbox" id="business_<?php echo $business["id"]; ?>" name="businesses[]" value="<?php echo $business["id"]; ?>" />
                        <?php
                        } ?>
                    </div>
                </div>
                <div id="status_message" style="display:none;"></div>
                <div id="navigation_box" class="grey-box">
                    <p>
                    	<button type="submit">Add</button>
                        <button type="button" onclick="window.history.back()">Back</button>
                    </p>
                </div>
            </form>
            <script type="text/javascript">
            $("#add_user_page form").on("submit", addUser);
            </script>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>