<?php
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/manage/users/';
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

require_once(BASE_PATH . "/../phpinc/db.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if ($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR && ($_SESSION["userPermissions"] & PERMISSION_MANAGER) != PERMISSION_MANAGER) {
	$backPageURL = BASE_URL . "/account/manage/";
	include(BASE_PATH . "/../phpinc/errors/insufficient_permissions.php");
	exit();
}

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}

$accountUsers = getAccountUsers($_SESSION["accountID"], $_SESSION["userID"]);
?>

<section data-role="page" id="account_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <div class="green-header">
                <p>User Management</p>
            </div>
            <?php if (count($accountUsers) > 0) { ?>
                <div class="grey-box">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="5">
                                    <p class="orange-text larger-message">Edit Users</p>
                                </th>
                            </tr>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Operation</th>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="5">&nbsp;</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($accountUsers as $accountUser) { ?>
                                <tr>
                                    <td><?php echo $accountUser["first_name"]; ?></td>
                                    <td><?php echo $accountUser["last_name"]; ?></td>
                                    <td><?php echo $accountUser["email"]; ?></td>
                                    <td><?php echo $accountUser["phone"]; ?></td>
                                    <td><a href="<?php echo BASE_URL; ?>/account/manage/users/edit/?u=<?php echo $accountUser["id"]; ?>"><button class="edit_user">Edit</button></a></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>
            <br /><br />
            <div id="manage_users_navigation">
                <a href="<?php echo BASE_URL; ?>/account/manage/users/add/">
                    <button>Add User</button>
                </a>
                <br /><br />
                <br />
                <a onclick="window.history.back()">
                    <button>Back</button>
                </a>
            </div>
            <br />
            <br />
            <div id="social_media">
                <p>Follow Us On...</p>
                <p>
                    <a href="//www.facebook.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/facebook.png" alt="Facebook" title="Facebook" /></a>
                    <a href="//www.twitter.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/twitter.png" alt="Twitter" title="Twitter" /></a>
                    <a href="//metarank.tumblr.com" target="_blank"><img src="<?php echo BASE_URL; ?>/img/tumblr.png" alt="Tumblr" title="Tumblr" /></a>
                    <a href="//www.pinterest.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/pinterest.png" alt="Pinterest" title="Pinterest" /></a>
                    <a href="//www.google.com/+MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/gplus.png" alt="Google+" title="Google+" /></a>
                    <a href="//www.instagram.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/instagram.png" alt="Instagram" title="Instagram" /></a>
                </p>
            </div>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->
    
<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>