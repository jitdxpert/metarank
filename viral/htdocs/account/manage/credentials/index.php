<?php
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/../phpinc/defines.php");
require_once(BASE_PATH . "/../phpinc/session.php");
require_once(BASE_PATH . "/../phpinc/db.php");

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>

<section data-role="page" id="change_credentials_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <div id="step_header" class="green-box">
                <p>Change Password/Security Questions</p>
            </div>
            <br />
            <form action="<?php echo BASE_URL; ?>/ajax/change_credentials.php" method="POST">
                <div id="fields">
                    <label for="password">Current Password:</label>
                    <input type="password" id="password" name="password" required="required" />
                    <p>Leave new password fields blank to retain current password.</p>
                    <label for="new_password">New Password:</label>
                    <input type="password" id="new_password" name="new_password" pattern="<?php echo PASSWORD_HTML5_REGULAR_EXPRESSION; ?>" onchange="
                        this.setCustomValidity(this.validity.patternMismatch ? 'Password must be at least 14 characters long and contain at least one digit, one uppercase letter, one lowercase letter, and one of the following special characters: !#$%&()*+,-./:;<=>?@\\\^_{|}~' : '');
                        if(this.checkValidity()) document.getElementById('new_password_confirm').pattern = this.value.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
                    "/>
                    <label for="new_password_confirm">Confirm New Password:</label>
                    <input type="password" id="new_password_confirm" name="new_password_confirm" pattern="<?php echo PASSWORD_HTML5_REGULAR_EXPRESSION; ?>" onchange="
                        this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above.' : '');
                    " />
                    <label for="security_question">Security Question:</label>
                    <select id="security_question" name="security_question">
                        <option value="">Select a question if you wish to change your security question/answer.</option>
						<?php
                        $securityQuestions = getSecurityQuestions();
                        foreach ($securityQuestions as $row) { ?>
                            <option value="<?php echo $row["id"]; ?>"><?php echo $row["question"]; ?></option>
                        <?php 
                        } ?>
                    </select>
                    <label for="security_answer">Answer:</label>
                    <input type="text" id="security_answer" name="security_answer" />
                </div>
                <div id="elevation_captcha">
					<?php if (getElevationFailureCount($_SESSION["userID"]) > MAX_ELEVATION_FAILURES) { ?>
						<script type="text/javascript">
                        showRecaptcha($("#elevation_captcha")[0]);
                        </script>
	                <?php } ?>
                </div>
                <div id="status_message" style="display:none;"></div>
                <div id="navigation_box" class="grey-box">
                    <p>
                    	<button type="submit">Change Information</button>
                        <button type="button" onclick="window.history.back()">Back</button>
                    </p>
                </div>
            </form>
            <script type="text/javascript">
            $("#change_credentials_page form").on("submit", changeCredentials);
            </script>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>