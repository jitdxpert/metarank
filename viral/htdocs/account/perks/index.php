<?php
require_once(dirname(dirname(dirname(__FILE__))) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/perks/scheduled/';
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

require_once(BASE_PATH . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if ($_SESSION["userID"] == $primaryAccountHolder["id"] || ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) == PERMISSION_ADMINISTRATOR) {
	$businesses = getAccountBusinesses($_SESSION["accountID"], null);
	$primaryBusiness = getAccountPrimaryBusiness($_SESSION["accountID"]);
} else {
	$businesses = getUserBusinesses($_SESSION["userID"], null);
	$primaryBusiness = array("id" => $businesses[0]["id"]);
}

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>

<section data-role="page" id="scheduled_perks_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <div class="green-header">
                <p>Perks</p>
            </div>
            
            <div id="account_navigation">
                <p class="orange-text larger-message bolded">Actions</p>
                <a href="<?php echo BASE_URL; ?>/account/perks/new/" data-ajax="false">
                    <button>Add Perk</button>
                </a>
                <br /><br />
                <a href="<?php echo BASE_URL; ?>/account/perks/drafts/" data-ajax="false">
                    <button>Drafts</button>
                </a>
                <br /><br />
                <a href="<?php echo BASE_URL; ?>/account/perks/scheduled/" data-ajax="false">
                    <button>Scheduled</button>
                </a>
                <br /><br />
                <a href="<?php echo BASE_URL; ?>/account/perks/active/" data-ajax="false">
                    <button>Active</button>
                </a>
                <br /><br />
                <a href="<?php echo BASE_URL; ?>/account/perks/completed/" data-ajax="false">
                    <button>Completed</button>
                </a>
                <br /><br />
                <a href="<?php echo BASE_URL; ?>/account/" data-ajax="true">
                    <button>Back to Account</button>
                </a>
            </div>
            <br />
            <br />
            <div id="social_media">
                <p>Follow Us On...</p>
                <p>
                    <a href="//www.facebook.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/facebook.png" alt="Facebook" title="Facebook" /></a>
                    <a href="//www.twitter.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/twitter.png" alt="Twitter" title="Twitter" /></a>
                    <a href="//metarank.tumblr.com" target="_blank"><img src="<?php echo BASE_URL; ?>/img/tumblr.png" alt="Tumblr" title="Tumblr" /></a>
                    <a href="//www.pinterest.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/pinterest.png" alt="Pinterest" title="Pinterest" /></a>
                    <a href="//www.google.com/+MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/gplus.png" alt="Google+" title="Google+" /></a>
                    <a href="//www.instagram.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/instagram.png" alt="Instagram" title="Instagram" /></a>
                </p>
            </div>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>