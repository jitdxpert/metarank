<?php
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/perks/completed/';
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

require_once(BASE_PATH . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if ($_SESSION["userID"] == $primaryAccountHolder["id"] || ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) == PERMISSION_ADMINISTRATOR) {
	$businesses = getAccountBusinesses($_SESSION["accountID"], null);
	$primaryBusiness = getAccountPrimaryBusiness($_SESSION["accountID"]);
} else {
	$businesses = getUserBusinesses($_SESSION["userID"], null);
	$primaryBusiness = array("id" => $businesses[0]["id"]);
}

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>

<section data-role="page" id="completed_perks_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <div class="green-header">
                <p>Completed Perks</p>
            </div>
            <br />
			<?php if (count($businesses) > 1) { ?>
	            <p class="orange-text larger-message">Select Business</p>
                <select id="completed_perks_business" name="business">
                    <?php foreach($businesses as $business) { ?>
                        <option value="<?php echo $business["id"]; ?>"<?php echo ($primaryBusiness["id"] == $business["id"] ? " selected" : ""); ?>><?php echo $business["name"]; ?></option>
                    <?php } ?>
                </select>
                <script type="text/javascript">
                $("#completed_perks_business").on("change", updateCompletedPerks);
                </script>
                <br />
                <p class="orange-text larger-message">Perks</p>
                <br />
            <?php } ?>
            <div id="completed_perks">
				<?php 
				$completedPerks = getCompletedPerks($primaryBusiness["id"]);
				if (count($completedPerks) == 0) { ?>
                	<div class="completed-perk">
		                <p>There are no completed perks.</p>
        		    </div>
				<?php } else {
					foreach ($completedPerks as $completedPerk) { ?>
                        <div class="perk-header"><?php echo $completedPerk["name"]; ?></div>
                        <div class="completed-perk">
                            <div class="completed-perk-main">
                                <div class="by-line">by <a href="<?php echo $completedPerk["business_website"]; ?>" target="_blank"><?php echo $completedPerk["business_name"]; ?></a></div>
                                <p><?php echo $completedPerk["description"]; ?></p>
								<?php if ($completedPerk["notes"] != "") { ?>
	                                <p><small>DISCLAIMER: <?php echo $completedPerk["notes"]; ?></small></p>
								<?php } ?>
                            </div>
		                    <div class="completed-perk-side">
                                <table>
                                    <tbody>
                                        <tr>
                                            <th>Start Time:</th>
                                            <td id="<?php echo $completedPerk["id"]; ?>_start_time"></td>
                                        </tr>
                                        <tr>
                                            <th>End Time:</th>
                                            <td id="<?php echo $completedPerk["id"]; ?>_end_time"></td>
                                        </tr>
                                        <tr>
                                            <th>Awarded To:</th>
                                            <td>Top <?php echo $completedPerk["max_recipients"]; ?> on <?php echo $completedPerk["service_name"]; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Value:</th>
                                            <td><?php echo $completedPerk["value"] . " " . $completedPerk["currency_abbreviation"]; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Eligibility:</th>
                                            <td>
												<?php if ($completedPerk["eligibility_distance"] == "0") { ?>
	                                                Global
												<?php } else { ?>
                                                	Within <?php echo $completedPerk["eligibility_distance"] . " " . $completedPerk["eligibility_distance_unit_name"]; ?> of:
													<?php foreach($completedPerk["eligibility_addresses"] as $eligibilityAddress) { ?>
                                                    	<p><?php echo render_address($eligibilityAddress + array("country" => $eligibilityAddress["country_id"]), array("first_name", "last_name", "organization_name")); ?></p>
													<?php
													}
												} ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
		                <div class="perk-footer">Added on <span id="<?php echo $completedPerk["id"]; ?>_date_signed"></span> by <?php echo $completedPerk["signatory"]; ?></div>
						<script type="text/javascript">
                        startDate = new Date('<?php echo $completedPerk["start_time"]; ?>');
                        endDate = new Date(startDate);
                        endDate.setSeconds(startDate.getSeconds() + <?php echo $completedPerk["duration"]; ?>);
                        $("#<?php echo $completedPerk["id"]; ?>_start_time")[0].innerText = startDate.toLocaleString();
                        $("#<?php echo $completedPerk["id"]; ?>_end_time")[0].innerText = endDate.toLocaleString();
                        $("#<?php echo $completedPerk["id"]; ?>_date_signed")[0].innerText = new Date('<?php echo $completedPerk["date_signed"]; ?>').toLocaleString();
		                </script>
		                <br />
					<?php
					}
				} ?>
            </div>
            <div id="completed_perks_navigation">
                <a onclick="window.history.back()">
                    <button>Back</button>
                </a>
            </div>
            <br />
            <br />
            <div id="social_media">
                <p>Follow Us On...</p>
                <p>
                    <a href="//www.facebook.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/facebook.png" alt="Facebook" title="Facebook" /></a>
                    <a href="//www.twitter.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/twitter.png" alt="Twitter" title="Twitter" /></a>
                    <a href="//metarank.tumblr.com" target="_blank"><img src="<?php echo BASE_URL; ?>/img/tumblr.png" alt="Tumblr" title="Tumblr" /></a>
                    <a href="//www.pinterest.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/pinterest.png" alt="Pinterest" title="Pinterest" /></a>
                    <a href="//www.google.com/+MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/gplus.png" alt="Google+" title="Google+" /></a>
                    <a href="//www.instagram.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/instagram.png" alt="Instagram" title="Instagram" /></a>
                </p>
            </div>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>