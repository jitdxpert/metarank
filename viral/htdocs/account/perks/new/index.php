<?php
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/../phpinc/session.php");
	
if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/promotions/new/';
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

require_once(BASE_PATH . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if ($_SESSION["userID"] == $primaryAccountHolder["id"] || ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) == PERMISSION_ADMINISTRATOR) {
	$businesses = getAccountBusinesses($_SESSION["accountID"], null);
} else {
	$businesses = getUserBusinesses($_SESSION["userID"], null);
}

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
	// Enforce initial page to always be step 1
?>
	<script type="text/javascript">
	$(document).ready(function() {
		if (window.location.hash.replace("#", "") != "") {
			window.location.hash = "";
		}
	});
	</script>
<?php
} ?>

<section data-role="page" id="new_perk_step1">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <div class="green-header">
                <p>New Perk Setup - Step 1</p>
            </div>

            <form method="POST" class="msform" data-ajax="false">
                <input type="hidden" name="nextStep" value="new_perk_step2">
                <input type="hidden" name="start_time" id="start_time" />
                <input type="hidden" name="duration" id="duration" />
                <br />
				<?php if (count($businesses) > 1) { ?>					
                	<p class="orange-text larger-message">Choose Business</p>
	                <label for="business">Business:</label>
    	            <select id="business" name="business">
						<?php foreach($businesses as $business) { ?>
		                    <option value="<?php echo $business["id"]; ?>"><?php echo $business["name"]; ?></option>
						<?php } ?>					
                    </select>
	                <br />
				<?php } else { ?>
	                <input type="hidden" id="business" name="business" value="<?php echo $businesses[0]["id"]; ?>">
				<?php } ?>					
                <p class="orange-text larger-message">Choose length of Perk</p>
                <br />
                <label for="start_time_display">Start Time:</label>
                <input type="text" id="start_time_display" required data-field="datetime" data-title="Set Start Date/Time" readonly="readonly" />
                <br />
                <label for="end_time_display">End Time:</label>
                <input type="text" id="end_time_display" required data-field="datetime" data-title="Set End Date/Time" data-startend="end" data-startendelem="#start_time_display" readonly="readonly" />
                <div id="navigation_box" class="grey-box">
                    <p>
                    	<button type="submit">Continue</button>
                       	<button type="button" onclick="window.location.href='<?php echo BASE_URL; ?>/account/perks/'">Back</button>
                    </p>
                </div>
                <div id="dtBox"></div>
            </form>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->
        
<section data-role="page" id="new_perk_step2">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p>Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br />
            <div class="green-header">
                <p>New Perk Setup - Step 2</p>
            </div>
            <br />
            <p class="orange-text larger-message">Describe your product</p>
            <form method="POST" class="msform" data-ajax="false">
                <input type="hidden" name="nextStep" value="new_perk_step3">
                <label for="perk_name">Name the perk the winner(s) receive:</label>
                <input type="text" id="perk_name" name="perk_name" required />
                <label for="perk_description">Description:</label>
                <textarea id="perk_description" name="perk_description" required></textarea>
                <label for="perk_disclaimer">Disclaimer, exclusion or special instructions:</label>
                <textarea id="perk_disclaimer" name="perk_disclaimer"></textarea>
                <label for="perk_value">This perk is valued at:</label>
                <input type="number" step="0.01" id="perk_value" name="perk_value" required onchange="this.value = parseFloat(this.value).toFixed(2)" />
                <label for="perk_currency">Valuation Currency:</label>
                <select id="perk_currency" name="perk_currency">
					<?php 
					$currencies = getCurrencies();
					foreach ($currencies as $row) { ?>
	                    <option value="<?php echo $row["id"]; ?>"<?php if ($row["abbreviation"] == "USD") echo " selected"; ?>>
							<?php echo $row["name"]; ?> (<?php echo $row["abbreviation"]; ?>)
                        </option>
					<?php
					} ?>
                </select>
                <label for="perk_max_recipients">Winners:</label>
                <select id="perk_max_recipients" name="perk_max_recipients">
                    <option value="5">Top 5</option>
                    <option value="10">Top 10</option>
                    <option value="20">Top 20</option>
                </select>
                <label for="perk_service">Ranked on:</label>
                <select id="perk_service" name="perk_service">
                    <option value="">Choose One</option>
					<?php 
					$allServices = getAllServices();
					foreach ($allServices as $row) { ?>
	                    <option value="<?php echo $row["id"]; ?>"><?php echo $row["name"]; ?></option>
					<?php
					} ?>
                </select>
                <div id="navigation_box" class="grey-box">
                    <p>
                    	<button type="submit">Continue</button>
                    	<button type="button" onclick="$(document.body).pagecontainer('change', '#new_perk_step1', {transition: 'flip', reverse: true});">Back</button>
                    </p>
                </div>
            </form>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->
    
<section data-role="page" id="new_perk_step3">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p>Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br />
            <div class="green-header">
                <p>New Perk Setup - Step 3</p>
            </div>
            <br />
            <p class="orange-text larger-message">Eligibility by Distance</p>
            <form method="POST" class="msform" data-ajax="false">
                <input type="hidden" name="nextStep" value="new_perk_step4">
                <input type="hidden" name="removeFields" value="first_name,last_name,organization_name">
                <label for="eligibility_distance">Eligibility Radius: (0 means Global)</label>
                <input type="number" id="eligibility_distance" name="eligibility_distance" value="0" min="0" pattern="0|[1-9][0-9]*" required />
                <label for="eligibility_distance_unit">Distance Unit:</label>
                <select id="eligibility_distance_unit" name="eligibility_distance_unit">
					<?php
                    $distanceUnits = getDistanceUnits();
					foreach ($distanceUnits as $row) { ?>
	                    <option value="<?php echo $row["id"]; ?>"<?php if ($row["name"] == "Miles") echo " selected"; ?>><?php echo $row["name"]; ?></option>
					<?php
					} ?>
                </select>
                <label>From Address(es):</label>
                <?php echo render_address_form(array(), array("first_name", "last_name", "organization_name"), "0000"); ?>
                <a href="javascript:addAddressForm()">Add Address</a>
                <div id="navigation_box" class="grey-box">
                    <p>
                    	<button type="submit">Continue</button>
                        <button type="button" onclick="$(document.body).pagecontainer('change', '#new_perk_step2', {transition: 'flip', reverse: true});">Back</button>
                    </p>
                </div>
            </form>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<section data-role="page" id="new_perk_step4">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p>Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br />
            <div class="green-header">
                <p>New Perk Setup - Step 4</p>
            </div>
            <br />
            <p class="orange-text larger-message">Review Perk and Confirm</p>
            <div style="text-align:left;" class="grey-box">
                <br />
                <label for="review_start_time_display">Start Time:</label>
                <input type="text" id="review_start_time_display" readonly />
                <br />
                <label for="review_end_time_display">End Time:</label>
                <input type="text" id="review_end_time_display" readonly />
                <br /><br />
                <label for="review_perk_name">Name the perk the winner(s) receive:</label>
                <input type="text" id="review_perk_name" readonly />
                <label for="review_perk_description">Description:</label>
                <textarea id="review_perk_description" readonly></textarea>
                <label for="review_perk_disclaimer">Disclaimer, exclusion or special instructions:</label>
                <textarea id="review_perk_disclaimer" readonly></textarea>
                <label for="review_perk_value">This perk is valued at:</label>
                <input type="number" step="0.01" id="review_perk_value" readonly />
                <label for="review_perk_currency">Valuation Currency:</label>
                <input type="text" id="review_perk_currency" readonly />
                <br /><br />
                <label for="review_eligibility_distance">Eligibility Radius:</label>
                <input type="text" id="review_eligibility_distance" readonly />
                <label for="review_eligibility_distance_unit">Distance Unit:</label>
                <input type="text" id="review_eligibility_distance_unit" readonly />
                <label for="review_eligibility_from_addresses">From Address(es):</label>
                <textarea id="review_eligibility_from_addresses" readonly></textarea>
                <br />
            </div>
            <br />
            <p>Please carefully read the contract below that details our agreement and provides our terms of service.<br />Once you have digitally signed this agreement, a copy will be emailed to you.</p>
            <p>I am a duly authorized representative of Merchant and my electronic signature binds Merchant to this Agreement including <a href="/terms">Merchant Terms and Conditions</a>.
            <form method="POST" class="msform" data-ajax="false">
                <input type="hidden" name="nextStep" value="<?php echo BASE_URL; ?>/ajax/new_perk.php">
                <input type="hidden" id="date_signed" name="date_signed">
                <label for="agrees_to_terms">I have read and agree to these terms</label>
                <input type="checkbox" id="agrees_to_terms" name="agrees_to_terms" value="yes" required />
                <label for="signatory">Name:</label>
                <input type="text" id="signatory" name="signatory" required />
                <label for="date_signed_display">Date:</label>
                <input type="text" id="date_signed_display" readonly />
                <div id="status_message"></div>
                <div id="navigation_box" class="grey-box">
                    <p>
                    	<button type="submit">Confirm</button>
                    	<button type="button" onclick="$(document.body).pagecontainer('change', '#new_perk_step3', {transition: 'flip', reverse: true});">Back</button>
                    </p>
                </div>
            </form>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<script type="text/javascript">
    var startTime = $("#start_time");
    var duration = $("#duration");
    var startTimeDisplay = $("#start_time_display");
    var endTimeDisplay = $("#end_time_display");
    var perkName = $("#perk_name");
    var perkDescription = $("#perk_description");
    var perkDisclaimer = $("#perk_disclaimer");
    var perkValue = $("#perk_value");
    var perkCurrency = $("#perk_currency");
    var eligibilityDistance = $("#eligibility_distance");
    var eligibilityDistanceUnit = $("#eligibility_distance_unit");
    var signatory = $("#signatory");
    var dateSigned = $("#date_signed");
    var dateSignedDisplay = $("#date_signed_display");
    
    var reviewStartTimeDisplay = $("#review_start_time_display");
    var reviewEndTimeDisplay = $("#review_end_time_display");
    var reviewPerkName = $("#review_perk_name");
    var reviewPerkDescription = $("#review_perk_description");
    var reviewPerkDisclaimer = $("#review_perk_disclaimer");
    var reviewPerkValue = $("#review_perk_value");
    var reviewPerkCurrency = $("#review_perk_currency");
    var reviewEligibilityDistance = $("#review_eligibility_distance");
    var reviewEligibilityDistanceUnit = $("#review_eligibility_distance_unit");
    var reviewEligibilityDistanceUnitLabel = $("#new_perk_step4 label[for=review_eligibility_distance_unit]");
    var reviewEligibilityFromAddresses = $("#review_eligibility_from_addresses");
    var reviewEligibilityFromAddressesLabel = $("#new_perk_step4 label[for=review_eligibility_from_addresses]");
    
    // Pre-initialize these fields for jQuery Mobile enhancement
    reviewEligibilityDistanceUnit.textinput();
    
    reviewEligibilityDistanceUnit.parent().hide();
    reviewEligibilityDistanceUnitLabel.hide();
    reviewEligibilityFromAddresses.hide();
    reviewEligibilityFromAddressesLabel.hide();
    
    var curDate = new Date();
    
    $("#dtBox").DateTimePicker({
        buttonsToDisplay: ["HeaderCloseButton", "SetButton"],
        dateTimeFormat : "yyyy-MM-dd hh:mm:ss",
        minDateTime:curDate.getFullYear() + "-" + ("0" + (curDate.getMonth()+1)).slice(-2) + "-" + ("0" + curDate.getDate()).slice(-2) + " " + ("0" + curDate.getHours()).slice(-2) + ":" + ("0" + curDate.getMinutes()).slice(-2)
    });
    startTimeDisplay.on("change", function(evt) {
        date = new Date(this.value);
        date.setDate(date.getDate() + 5);
        endTimeDisplay.data("max", date.getFullYear() + "-" + ("0" + (date.getMonth()+1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2) + " " + ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2));
        endTimeDisplay.val(this.value);
    });
    endTimeDisplay.on("change", function(evt) {
        startDate = new Date(startTimeDisplay.val());
        endDate = new Date(this.value);
        startTime.val(startDate.toISOString().replace("T", " ").replace("Z", ""));
        duration.val((endDate - startDate) / 1000);
    });
    startTimeDisplay.on("dtPickerHideComplete", function(evt) {
        dtPickerObj = evt.data;
        if (typeof dtPickerObj === "undefined") {
            dtPickerObj = evt.originalEvent.data;
        }
        if(dtPickerObj.dataObject.oInputElement == null)
        {
            dtPickerObj.showDateTimePicker(endTimeDisplay[0]);
        }
    });
    tomorrowDate = new Date(curDate);
    tomorrowDate.setDate(curDate.getDate() + 1);
    startTimeDisplay.val(tomorrowDate.getFullYear() + "-" + ("0" + (tomorrowDate.getMonth()+1)).slice(-2) + "-" + ("0" + tomorrowDate.getDate()).slice(-2) + " 00:00");
    startTime.val(tomorrowDate.toISOString().replace("T", " ").replace("Z", ""));
    maxDate = new Date(tomorrowDate);
    maxDate.setDate(tomorrowDate.getDate() + 5);
    endTimeDisplay.val(maxDate.getFullYear() + "-" + ("0" + (maxDate.getMonth()+1)).slice(-2) + "-" + ("0" + maxDate.getDate()).slice(-2) + " 00:00");
    endTimeDisplay.data("max", maxDate.getFullYear() + "-" + ("0" + (maxDate.getMonth()+1)).slice(-2) + "-" + ("0" + maxDate.getDate()).slice(-2) + " 00:00");
    duration.val((maxDate - tomorrowDate) / 1000);
    
    $("#new_perk_step3 form").on("change", "fieldset[data-addressfield-number] select.country", updateAddressCountry);
    
    function addAddressForm() {
        var lastFieldSet = $("#new_perk_step3 form fieldset:last-of-type");
        if (lastFieldSet.data("addressfield-number") == "9999") {
            alert("Maximum address limit reached.");
            return;
        }
        var nextAddressFieldNumber = ("0000" + (parseInt(lastFieldSet.data("addressfield-number")) + 1)).slice(-4);
        var data = "addressFieldNumber=" + nextAddressFieldNumber + "&" + $("#new_perk_step3 input[name=removeFields]").serialize();
        $.ajax({
            url: '<?php echo BASE_URL; ?>/ajax/update_address_country.php',
            data: data,
            type: "POST",
            success: function(addressHTML, textStatus, jqXHR) {
                lastFieldSet.after(addressHTML);
                newFieldSet = $("#new_perk_step3 fieldset[data-addressfield-number=" + nextAddressFieldNumber + "]");
                newFieldSet.trigger("create");
                deleteLink = document.createElement("a");
                deleteLink.appendChild(document.createTextNode("Delete Address"));
                deleteLink.setAttribute("href", "javascript:void(0);");
                deleteLink.setAttribute("style", "color:red;float:right;");
                $(deleteLink).on("click", function(evt) {
                    $(this).parent().remove();
                });
                newFieldSet.append(deleteLink);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        })
    }
    
    eligibilityDistance.on("change", function(evt) {
        if (this.value == "0") {
            eligibilityDistanceUnit.selectmenu("disable");
            $("fieldset input[type=hidden]", this.form).each(function(index) {
                $(this).prop("disabled", true);
            });
            $("fieldset input[type=text]", this.form).each(function(index) {
                $(this).textinput("disable");
            });
            $("fieldset select", this.form).each(function(index) {
                $(this).selectmenu("disable");
            });
            
            reviewEligibilityDistanceUnit.parent().hide();
            reviewEligibilityDistanceUnitLabel.hide();
            reviewEligibilityFromAddresses.hide();
            reviewEligibilityFromAddressesLabel.hide();
        } else if (isNaN(parseInt(this.value, 10))){
            this.value = "0";
            eligibilityDistanceUnit.selectmenu("disable");
            $("fieldset input[type=hidden]", this.form).each(function(index) {
                $(this).prop("disabled", true);
            });
            $("fieldset input[type=text]", this.form).each(function(index) {
                $(this).textinput("disable");
            });
            $("fieldset select", this.form).each(function(index) {
                $(this).selectmenu("disable");
            });
            
            reviewEligibilityDistanceUnit.parent().hide();
            reviewEligibilityDistanceUnitLabel.hide();
            reviewEligibilityFromAddresses.hide();
            reviewEligibilityFromAddressesLabel.hide();
        } else {
            eligibilityDistanceUnit.selectmenu("enable");
            $("fieldset input[type=hidden]", this.form).each(function(index) {
                $(this).prop("disabled", false);
            });
            $("fieldset input[type=text]", this.form).each(function(index) {
                $(this).textinput("enable");
            });
            $("fieldset select", this.form).each(function(index) {
                $(this).selectmenu("enable");
            });
            
            reviewEligibilityDistanceUnit.parent().show();
            reviewEligibilityDistanceUnitLabel.show();
            reviewEligibilityFromAddresses.show();
            reviewEligibilityFromAddressesLabel.show();
        }
    });
    
    $("#new_perk_step3").on("pagecreate", function(event, ui) {
        eligibilityDistanceUnit.selectmenu("disable");
        $("#new_perk_step3 fieldset input[type=text]").each(function(index) {
            $(this).textinput("disable");
        });
        $("#new_perk_step3 fieldset input[type=hidden]").each(function(index) {
            $(this).prop("disabled", true);
        });
        $("#new_perk_step3 fieldset select").each(function(index) {
            $(this).selectmenu("disable");
        });
    });
    
    $("form.msform").on("submit", handleMSForm);
    
    $(document.body).on("pagecontainerchange", function(event, ui) {
        if (ui.toPage.attr("id").toUpperCase() == "NEW_PERK_STEP4") {
            signDate = new Date();
            dateSigned.val(signDate.toISOString().replace("T", " ").replace("Z", ""));
            dateSignedDisplay.val(signDate.toLocaleDateString());
            
            reviewStartTimeDisplay.val(startTimeDisplay.val());
            reviewEndTimeDisplay.val(endTimeDisplay.val());
            reviewPerkName.val(perkName.val());
            reviewPerkDescription.val(perkDescription.val());
            reviewPerkDisclaimer.val(perkDisclaimer.val());
            reviewPerkValue.val(perkValue.val());
            reviewPerkCurrency.val($(perkCurrency[0].options[perkCurrency[0].options.selectedIndex]).text());
            reviewEligibilityDistance.val((eligibilityDistance.val() == "0" ? "Global" : eligibilityDistance.val()));
            reviewEligibilityDistanceUnit.val($(eligibilityDistanceUnit[0].options[eligibilityDistanceUnit[0].options.selectedIndex]).text());
        }
    });
</script>

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>