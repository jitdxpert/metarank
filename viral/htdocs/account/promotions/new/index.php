<?php
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/promotions/new/';
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

require_once(BASE_PATH . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if ($_SESSION["userID"] == $primaryAccountHolder["id"] || ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) == PERMISSION_ADMINISTRATOR) {
	$businesses = getAccountBusinesses($_SESSION["accountID"], null);
} else {
	$businesses = getUserBusinesses($_SESSION["userID"], null);
}

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
	// Enforce initial page to always be step 1
?>
	<script type="text/javascript">
		$(document).ready(function() {
			if (window.location.hash.replace("#", "") != "") {
				window.location.hash = "";
			}
		});
	</script><?php
}
?>

<section data-role="page" id="new_promotion_step1">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p>Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br />
            <div class="green-header">
                <p>New Promotion Setup - Step 1</p>
            </div>
            
            <form method="POST" class="msform" data-ajax="false">
                <input type="hidden" name="nextStep" value="new_promotion_step2">
                <input type="hidden" name="start_time" id="start_time" />
                <input type="hidden" name="duration" id="duration" />
                <br /><?php
if (count($businesses) > 1) {
?>					<p class="orange-text larger-message">Choose Business</p>
                <label for="business">Business:</label>
                <select id="business" name="business"><?php
foreach($businesses as $business) {
?>
                    <option value="<?php echo $business["id"]; ?>"><?php echo $business["name"]; ?></option><?php
}
?>					</select>
                <br /><?php
} else {
?>
                <input type="hidden" id="business" name="business" value="<?php echo $businesses[0]["id"]; ?>"><?php
}
?>					<p class="orange-text larger-message">Choose length of Promotion</p>
                <label for="start_time_display">Start Time:</label>
                <input type="text" id="start_time_display" required data-field="datetime" data-title="Set Start Date/Time" readonly="readonly" />
                <br />
                <label for="end_time_display">End Time:</label>
                <input type="text" id="end_time_display" required data-field="datetime" data-title="Set End Date/Time" data-startend="end" data-startendelem="#start_time_display" readonly="readonly" />
                <div id="navigation_box" class="grey-box">
                    <p><button type="submit">Continue</button> <button type="button" onclick="window.location.href = '<?php echo BASE_URL; ?>/account/';">Back</button></p>
                </div>
                <div id="dtBox"></div>
            </form>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->
        
<section data-role="page" id="new_promotion_step2">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p>Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br />
            <div class="green-header">
                <p>New Promotion Setup - Step 2</p>
            </div>
            <br />
            <p class="orange-text larger-message">Describe your product</p>
            <form method="POST" class="msform" data-ajax="false">
                <input type="hidden" name="nextStep" value="new_promotion_step3">
                <label for="promotion_name">Name the promotion:</label>
                <input type="text" id="promotion_name" name="promotion_name" required />
                <label for="promotion_description">Description:</label>
                <textarea id="promotion_description" name="promotion_description" required></textarea>
                <label for="promotion_disclaimer">Disclaimer, exclusion or special instructions:</label>
                <textarea id="promotion_disclaimer" name="promotion_disclaimer"></textarea>
                <label for="promotion_value">This promotion is valued at:</label>
                <input type="number" step="0.01" id="promotion_value" name="promotion_value" required onchange="this.value = parseFloat(this.value).toFixed(2)" />
                <label for="promotion_currency">Valuation Currency:</label>
                <select id="promotion_currency" name="promotion_currency"><?php
$currencies = getCurrencies();
foreach ($currencies as $row) {
?>
                    <option value="<?php echo $row["id"]; ?>"<?php if ($row["abbreviation"] == "USD") echo " selected"; ?>><?php echo $row["name"]; ?> (<?php echo $row["abbreviation"]; ?>)</option><?php
}
?>
                </select>
                <label for="promotion_max_recipients">Maximum Recipients:</label>
                <input type="number" id="promotion_max_recipients" name="promotion_max_recipients" required value="100" />
                <div id="navigation_box" class="grey-box">
                    <p><button type="submit">Continue</button> <button type="button" onclick="$(document.body).pagecontainer('change', '#new_promotion_step1', {transition: 'flip', reverse: true});">Back</button></p>
                </div>
            </form>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->
    
<section data-role="page" id="new_promotion_step3">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p>Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br />
            <div class="green-header">
                <p>New Promotion Setup - Step 3</p>
            </div>
            <br />
            <p class="orange-text larger-message">Eligibility by Distance</p>
            <form method="POST" class="msform" data-ajax="false">
                <input type="hidden" name="nextStep" value="new_promotion_step4">
                <input type="hidden" name="removeFields" value="first_name,last_name,organization_name">
                <label for="eligibility_distance">Eligibility Radius: (0 means Global)</label>
                <input type="number" id="eligibility_distance" name="eligibility_distance" value="0" min="0" pattern="0|[1-9][0-9]*" required />
                <label for="eligibility_distance_unit">Distance Unit:</label>
                <select id="eligibility_distance_unit" name="eligibility_distance_unit"><?php
$distanceUnits = getDistanceUnits();
foreach ($distanceUnits as $row) {
?>
                    <option value="<?php echo $row["id"]; ?>"<?php if ($row["name"] == "Miles") echo " selected"; ?>><?php echo $row["name"]; ?></option><?php
}
?>
                </select>
                <label>From Address(es):</label>
                <?php echo render_address_form(array(), array("first_name", "last_name", "organization_name"), "0000"); ?>
                <a href="javascript:addAddressForm()">Add Address</a>
                <div id="navigation_box" class="grey-box">
                    <p><button type="submit">Continue</button> <button type="button" onclick="$(document.body).pagecontainer('change', '#new_promotion_step2', {transition: 'flip', reverse: true});">Back</button></p>
                </div>
            </form>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->
    
<section data-role="page" id="new_promotion_step4">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p>Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br />
            <div class="green-header">
                <p>New Promotion Setup - Step 4</p>
            </div>
            <br />
            <p class="orange-text larger-message">Choose one of the following options</p>
            <form method="POST" class="msform" data-ajax="false">
                <input type="hidden" name="nextStep" value="new_promotion_step5">
                <label for="checkin_option">Who Check-In with:</label>
                <select id="checkin_option" name="checkin_option">
                    <option value="">- Off -</option><?php
$checkinServices = getCheckinServices();
foreach ($checkinServices as $row) {
?>
                    <option value="<?php echo $row["id"]; ?>"><?php echo $row["name"]; ?></option><?php
}
?>
                </select>
                <label for="checkin_location">to this location:</label>
                <input type="text" id="checkin_location" name="checkin_location" placeholder="Exact location as it appears on the selected network." />
                <label for="checkin_hashtag">and use this hashtag (#):</label>
                <input type="text" id="checkin_hashtag" name="checkin_hashtag" title="Must not contain spaces or hashtags (except as the first character)." pattern="(#[^ #]+)|[^ #]+"/>
                <div class="or-bubble">
                    <span>OR</span>
                </div>
                <div class="grey-box">
                    <br /><br />
                    <label for="post_option">Who share/post to:</label>
                    <select id="post_option" name="post_option">
                        <option value="">- Off -</option><?php
$postServices = getPostServices();
foreach ($postServices as $row) {
?>
                    <option value="<?php echo $row["id"]; ?>"><?php echo $row["name"]; ?></option><?php
}
?>
                    </select>
                    <label for="post_link">This link:</label>
                    <input type="url" id="post_link" name="post_link" placeholder="Facebook page, website, tweet, Yelp profile, any review, etc." />
                    <div class="or-bubble">
                        <span>OR</span>
                    </div>
                </div>
                <br /><br />
                <label for="like_option">Who likes on:</label>
                <select id="like_option" name="like_option">
                    <option value="">- Off -</option><?php
$likeServices = getLikeServices();
foreach ($likeServices as $row) {
?>
                    <option value="<?php echo $row["id"]; ?>"><?php echo $row["name"]; ?></option><?php
}
?>
                </select>
                <label for="like_link">This link:</label>
                <input type="url" id="like_link" name="like_link" placeholder="Facebook page, website, tweet, Yelp profile, any review, etc." />
                <div id="navigation_box" class="grey-box">
                    <p><button type="submit">Continue</button> <button type="button" onclick="$(document.body).pagecontainer('change', '#new_promotion_step3', {transition: 'flip', reverse: true});">Back</button></p>
                </div>
            </form>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<section data-role="page" id="new_promotion_step5">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p>Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br />
            <div class="green-header">
                <p>New Promotion Setup - Step 5</p>
            </div>
            <br />
            <p class="orange-text larger-message">Review Promotion and Confirm</p>
            <div style="text-align:left;" class="grey-box">
                <br />
                <label for="review_start_time_display">Start Time:</label>
                <input type="text" id="review_start_time_display" readonly />
                <br />
                <label for="review_end_time_display">End Time:</label>
                <input type="text" id="review_end_time_display" readonly />
                <br /><br />
                <label for="review_promotion_name">Name the promotion the winner(s) receive:</label>
                <input type="text" id="review_promotion_name" readonly />
                <label for="review_promotion_description">Description:</label>
                <textarea id="review_promotion_description" readonly></textarea>
                <label for="review_promotion_disclaimer">Disclaimer, exclusion or special instructions:</label>
                <textarea id="review_promotion_disclaimer" readonly></textarea>
                <label for="review_promotion_value">This promotion is valued at:</label>
                <input type="number" step="0.01" id="review_promotion_value" readonly />
                <label for="review_promotion_currency">Valuation Currency:</label>
                <input type="text" id="review_promotion_currency" readonly />
                <br /><br />
                <label for="review_eligibility_distance">Eligibility Radius:</label>
                <input type="text" id="review_eligibility_distance" readonly />
                <label for="review_eligibility_distance_unit">Distance Unit:</label>
                <input type="text" id="review_eligibility_distance_unit" readonly />
                <label for="review_eligibility_from_addresses">From Address(es):</label>
                <textarea id="review_eligibility_from_addresses" readonly></textarea>
                <br /><br />
                <label for="review_checkin_option">Who Check-In with:</label>
                <input type="text" id="review_checkin_option" readonly />
                <label for="review_checkin_location">to this location:</label>
                <input type="text" id="review_checkin_location" readonly />
                <label for="review_checkin_hashtag">and use this hashtag (#):</label>
                <input type="text" id="review_checkin_hashtag" readonly />
                <label for="review_post_option">Who share/post to:</label>
                <input type="text" id="review_post_option" readonly />
                <label for="review_post_link">This link:</label>
                <input type="text" id="review_post_link" readonly />
                <label for="review_like_option">Who likes on:</label>
                <input type="text" id="review_like_option" readonly />
                <label for="review_like_link">This link:</label>
                <input type="text" id="review_like_link" readonly />
                <br />
            </div>
            <br />
            <p>Please carefully read the contract below that details our agreement and provides our terms of service.<br />Once you have digitally signed this agreement, a copy will be emailed to you.</p>
            <p>I am a duly authorized representative of Merchant and my electronic signature binds Merchant to this Agreement including <a href="<?php echo BASE_URL; ?>/terms">Merchant Terms and Conditions</a>.
            <form method="POST" class="msform" data-ajax="false">
                <input type="hidden" name="nextStep" value="<?php echo BASE_URL; ?>/ajax/new_promotion.php">
                <input type="hidden" id="date_signed" name="date_signed">
                <label for="agrees_to_terms">I have read and agree to these terms</label>
                <input type="checkbox" id="agrees_to_terms" name="agrees_to_terms" value="yes" required />
                <label for="signatory">Name:</label>
                <input type="text" id="signatory" name="signatory" required />
                <label for="date_signed_display">Date:</label>
                <input type="text" id="date_signed_display" readonly />
                <div id="status_message"></div>
                <div id="navigation_box" class="grey-box">
                    <p><button type="submit">Confirm</button> <button type="button" onclick="$(document.body).pagecontainer('change', '#new_promotion_step4', {transition: 'flip', reverse: true});">Back</button></p>
                </div>
            </form>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<script type="text/javascript">
var startTime = $("#start_time");
var duration = $("#duration");
var startTimeDisplay = $("#start_time_display");
var endTimeDisplay = $("#end_time_display");
var promotionName = $("#promotion_name");
var promotionDescription = $("#promotion_description");
var promotionDisclaimer = $("#promotion_disclaimer");
var promotionValue = $("#promotion_value");
var promotionCurrency = $("#promotion_currency");
var eligibilityDistance = $("#eligibility_distance");
var eligibilityDistanceUnit = $("#eligibility_distance_unit");
var checkinOption = $("#checkin_option");
var checkinLocation = $("#checkin_location");
var checkinHashtag = $("#checkin_hashtag");
var postOption = $("#post_option");
var postLink = $("#post_link");
var likeOption = $("#like_option");
var likeLink = $("#like_link");
var signatory = $("#signatory");
var dateSigned = $("#date_signed");
var dateSignedDisplay = $("#date_signed_display");

var reviewStartTimeDisplay = $("#review_start_time_display");
var reviewEndTimeDisplay = $("#review_end_time_display");
var reviewPromotionName = $("#review_promotion_name");
var reviewPromotionDescription = $("#review_promotion_description");
var reviewPromotionDisclaimer = $("#review_promotion_disclaimer");
var reviewPromotionValue = $("#review_promotion_value");
var reviewPromotionCurrency = $("#review_promotion_currency");
var reviewEligibilityDistance = $("#review_eligibility_distance");
var reviewEligibilityDistanceUnit = $("#review_eligibility_distance_unit");
var reviewEligibilityDistanceUnitLabel = $("#new_promotion_step5 label[for=review_eligibility_distance_unit]");
var reviewEligibilityFromAddresses = $("#review_eligibility_from_addresses");
var reviewEligibilityFromAddressesLabel = $("#new_promotion_step5 label[for=review_eligibility_from_addresses]");
var reviewCheckinOption = $("#review_checkin_option");
var reviewCheckinOptionLabel = $("#new_promotion_step5 label[for=review_checkin_option]");
var reviewCheckinLocation = $("#review_checkin_location");
var reviewCheckinLocationLabel = $("#new_promotion_step5 label[for=review_checkin_location]");
var reviewCheckinHashtag = $("#review_checkin_hashtag");
var reviewCheckinHashtagLabel = $("#new_promotion_step5 label[for=review_checkin_hashtag]");
var reviewPostOption = $("#review_post_option");
var reviewPostOptionLabel = $("#new_promotion_step5 label[for=review_post_option]");
var reviewPostLink = $("#review_post_link");
var reviewPostLinkLabel = $("#new_promotion_step5 label[for=review_post_link]");
var reviewLikeOption = $("#review_like_option");
var reviewLikeOptionLabel = $("#new_promotion_step5 label[for=review_like_option]");
var reviewLikeLink = $("#review_like_link");
var reviewLikeLinkLabel = $("#new_promotion_step5 label[for=review_like_link]");

// Pre-initialize these fields for jQuery Mobile enhancement
reviewEligibilityDistanceUnit.textinput();
reviewCheckinOption.textinput();
reviewCheckinLocation.textinput();
reviewCheckinHashtag.textinput();
reviewPostOption.textinput();
reviewPostLink.textinput();
reviewLikeOption.textinput();
reviewLikeLink.textinput();

reviewEligibilityDistanceUnit.parent().hide();
reviewEligibilityDistanceUnitLabel.hide();
reviewEligibilityFromAddresses.hide();
reviewEligibilityFromAddressesLabel.hide();

reviewCheckinOption.parent().hide();
reviewCheckinOptionLabel.hide();
reviewCheckinLocation.parent().hide();
reviewCheckinLocationLabel.hide();
reviewCheckinHashtag.parent().hide();
reviewCheckinHashtagLabel.hide();
reviewPostOption.parent().hide();
reviewPostOptionLabel.hide();
reviewPostLink.parent().hide();
reviewPostLinkLabel.hide();
reviewLikeOption.parent().hide();
reviewLikeOptionLabel.hide();
reviewLikeLink.parent().hide();
reviewLikeLinkLabel.hide();

var curDate = new Date();

$("#dtBox").DateTimePicker({
	buttonsToDisplay: ["HeaderCloseButton", "SetButton"],
	dateTimeFormat : "yyyy-MM-dd hh:mm:ss",
	minDateTime:curDate.getFullYear() + "-" + ("0" + (curDate.getMonth()+1)).slice(-2) + "-" + ("0" + curDate.getDate()).slice(-2) + " " + ("0" + curDate.getHours()).slice(-2) + ":" + ("0" + curDate.getMinutes()).slice(-2)
});
startTimeDisplay.on("change", function(evt) {
	date = new Date(this.value);
	date.setDate(date.getDate() + 5);
	endTimeDisplay.data("max", date.getFullYear() + "-" + ("0" + (date.getMonth()+1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2) + " " + ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2));
	endTimeDisplay.val(this.value);
});
endTimeDisplay.on("change", function(evt) {
	startDate = new Date(startTimeDisplay.val());
	endDate = new Date(this.value);
	startTime.val(startDate.toISOString().replace("T", " ").replace("Z", ""));
	duration.val((endDate - startDate) / 1000);
});
startTimeDisplay.on("dtPickerHideComplete", function(evt) {
	dtPickerObj = evt.data;
	if (typeof dtPickerObj === "undefined") {
		dtPickerObj = evt.originalEvent.data;
	}
	if(dtPickerObj.dataObject.oInputElement == null)
	{
		dtPickerObj.showDateTimePicker(endTimeDisplay[0]);
	}
});
tomorrowDate = new Date(curDate);
tomorrowDate.setDate(curDate.getDate() + 1);
startTimeDisplay.val(tomorrowDate.getFullYear() + "-" + ("0" + (tomorrowDate.getMonth()+1)).slice(-2) + "-" + ("0" + tomorrowDate.getDate()).slice(-2) + " 00:00");
startTime.val(tomorrowDate.toISOString().replace("T", " ").replace("Z", ""));
maxDate = new Date(tomorrowDate);
maxDate.setDate(tomorrowDate.getDate() + 5);
endTimeDisplay.val(maxDate.getFullYear() + "-" + ("0" + (maxDate.getMonth()+1)).slice(-2) + "-" + ("0" + maxDate.getDate()).slice(-2) + " 00:00");
endTimeDisplay.data("max", maxDate.getFullYear() + "-" + ("0" + (maxDate.getMonth()+1)).slice(-2) + "-" + ("0" + maxDate.getDate()).slice(-2) + " 00:00");
duration.val((maxDate - tomorrowDate) / 1000);

$("#new_promotion_step3 form").on("change", "fieldset[data-addressfield-number] select.country", updateAddressCountry);
$("#new_promotion_step3 form").on("change", "fieldset[data-addressfield-number] input, fieldset[data-addressfield-number] select:not(.country)", function(evt) {
	fieldSet = "fieldset[data-addressfield-number=" + this.id.slice(-4) + "]";
	$(fieldSet + " input[name='latitude[]'], " + fieldSet + " input[name='longitude[]']", this.form).val("");
});

function addAddressForm() {
	var lastFieldSet = $("#new_promotion_step3 form fieldset:last-of-type");
	if (lastFieldSet.data("addressfield-number") == "9999") {
		alert("Maximum address limit reached.");
		return;
	}
	var nextAddressFieldNumber = ("0000" + (parseInt(lastFieldSet.data("addressfield-number")) + 1)).slice(-4);
	var data = "addressFieldNumber=" + nextAddressFieldNumber + "&" + $("#new_promotion_step3 input[name=removeFields]").serialize();
	$.ajax({
		url: '<?php echo BASE_URL; ?>/ajax/update_address_country.php',
		data: data,
		type: "POST",
		success: function(addressHTML, textStatus, jqXHR) {
			lastFieldSet.after(addressHTML);
			newFieldSet = $("#new_promotion_step3 fieldset[data-addressfield-number=" + nextAddressFieldNumber + "]");
			newFieldSet.trigger("create");
			deleteLink = document.createElement("a");
			deleteLink.appendChild(document.createTextNode("Delete Address"));
			deleteLink.setAttribute("href", "javascript:void(0);");
			deleteLink.setAttribute("style", "color:red;float:right;");
			$(deleteLink).on("click", function(evt) {
				$(this).parent().remove();
			});
			newFieldSet.append(deleteLink);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert(errorThrown);
		}
	})
}

eligibilityDistance.on("change", function(evt) {
	if (this.value == "0") {
		eligibilityDistanceUnit.selectmenu("disable");
		$("fieldset input[type=hidden]", this.form).each(function(index) {
			$(this).prop("disabled", true);
		});
		$("fieldset input[type=text]", this.form).each(function(index) {
			$(this).textinput("disable");
		});
		$("fieldset select", this.form).each(function(index) {
			$(this).selectmenu("disable");
		});
		
		reviewEligibilityDistanceUnit.parent().hide();
		reviewEligibilityDistanceUnitLabel.hide();
		reviewEligibilityFromAddresses.hide();
		reviewEligibilityFromAddressesLabel.hide();
	} else if (isNaN(parseInt(this.value, 10))){
		this.value = "0";
		eligibilityDistanceUnit.selectmenu("disable");
		$("fieldset input[type=hidden]", this.form).each(function(index) {
			$(this).prop("disabled", true);
		});
		$("fieldset input[type=text]", this.form).each(function(index) {
			$(this).textinput("disable");
		});
		$("fieldset select", this.form).each(function(index) {
			$(this).selectmenu("disable");
		});
		
		reviewEligibilityDistanceUnit.parent().hide();
		reviewEligibilityDistanceUnitLabel.hide();
		reviewEligibilityFromAddresses.hide();
		reviewEligibilityFromAddressesLabel.hide();
	} else {
		eligibilityDistanceUnit.selectmenu("enable");
		$("fieldset input[type=hidden]", this.form).each(function(index) {
			$(this).prop("disabled", false);
		});
		$("fieldset input[type=text]", this.form).each(function(index) {
			$(this).textinput("enable");
		});
		$("fieldset select", this.form).each(function(index) {
			$(this).selectmenu("enable");
		});
		
		reviewEligibilityDistanceUnit.parent().show();
		reviewEligibilityDistanceUnitLabel.show();
		reviewEligibilityFromAddresses.show();
		reviewEligibilityFromAddressesLabel.show();
	}
});

$("#new_promotion_step3").on("pagecreate", function(event, ui) {
	eligibilityDistanceUnit.selectmenu("disable");
	$("#new_promotion_step3 fieldset input[type=text]").each(function(index) {
		$(this).textinput("disable");
	});
	$("#new_promotion_step3 fieldset input[type=hidden]").each(function(index) {
		$(this).prop("disabled", true);
	});
	$("#new_promotion_step3 fieldset select").each(function(index) {
		$(this).selectmenu("disable");
	});
});
$("#new_promotion_step4").on("pagecreate", function(event, ui) {
	checkinLocation.textinput("disable");
	checkinHashtag.textinput("disable");
	postLink.textinput("disable");
	likeLink.textinput("disable");
});

checkinOption.on("change", function(evt) {
	if (this.options.selectedIndex != 0) {
		postOption.val("");
		postOption.selectmenu("refresh");
		postLink.textinput("disable");
		postLink.prop("required", false);
		likeOption.val("");
		likeOption.selectmenu("refresh");
		likeLink.textinput("disable");
		likeLink.prop("required", false);
		checkinLocation.textinput("enable");
		checkinLocation.prop("required", true);
		checkinHashtag.textinput("enable");
		checkinHashtag.prop("required", true);
		
		reviewCheckinOption.parent().show();
		reviewCheckinOptionLabel.show();
		reviewCheckinLocation.parent().show();
		reviewCheckinLocationLabel.show();
		reviewCheckinHashtag.parent().show();
		reviewCheckinHashtagLabel.show();
		
		reviewPostOption.parent().hide();
		reviewPostOptionLabel.hide();
		reviewPostLink.parent().hide();
		reviewPostLinkLabel.hide();
		reviewLikeOption.parent().hide();
		reviewLikeOptionLabel.hide();
		reviewLikeLink.parent().hide();
		reviewLikeLinkLabel.hide();
	} else {
		checkinLocation.textinput("disable");
		checkinLocation.prop("required", false);
		checkinHashtag.textinput("disable");
		checkinHashtag.prop("required", false);
		
		reviewCheckinOption.parent().hide();
		reviewCheckinOptionLabel.hide();
		reviewCheckinLocation.parent().hide();
		reviewCheckinLocationLabel.hide();
		reviewCheckinHashtag.parent().hide();
		reviewCheckinHashtagLabel.hide();
	}
});
postOption.on("change", function(evt) {
	if (this.options.selectedIndex != 0) {
		checkinOption.val("");
		checkinOption.selectmenu("refresh");
		checkinLocation.textinput("disable");
		checkinLocation.prop("required", false);
		checkinHashtag.textinput("disable");
		checkinHashtag.prop("required", false);
		likeOption.val("");
		likeOption.selectmenu("refresh");
		likeLink.textinput("disable");
		likeLink.prop("required", false);
		postLink.textinput("enable");
		postLink.prop("required", true);
		
		reviewPostOption.parent().show();
		reviewPostOptionLabel.show();
		reviewPostLink.parent().show();
		reviewPostLinkLabel.show();
		
		reviewCheckinOption.parent().hide();
		reviewCheckinOptionLabel.hide();
		reviewCheckinLocation.parent().hide();
		reviewCheckinLocationLabel.hide();
		reviewCheckinHashtag.parent().hide();
		reviewCheckinHashtagLabel.hide();
		reviewLikeOption.parent().hide();
		reviewLikeOptionLabel.hide();
		reviewLikeLink.parent().hide();
		reviewLikeLinkLabel.hide();
	} else {
		postLink.textinput("disable");
		postLink.prop("required", false);
		
		reviewPostOption.parent().hide();
		reviewPostOptionLabel.hide();
		reviewPostLink.parent().hide();
		reviewPostLinkLabel.hide();
	}
});
likeOption.on("change", function(evt) {
	if (this.options.selectedIndex != 0) {
		checkinOption.val("");
		checkinOption.selectmenu("refresh");
		checkinLocation.textinput("disable");
		checkinLocation.prop("required", false);
		checkinHashtag.textinput("disable");
		checkinHashtag.prop("required", false);
		postOption.val("");
		postOption.selectmenu("refresh");
		postLink.textinput("disable");
		postLink.prop("required", false);
		likeLink.textinput("enable");
		likeLink.prop("required", true);
		
		reviewLikeOption.parent().show();
		reviewLikeOptionLabel.show();
		reviewLikeLink.parent().show();
		reviewLikeLinkLabel.show()
		
		reviewCheckinOption.parent().hide();
		reviewCheckinOptionLabel.hide();
		reviewCheckinLocation.parent().hide();
		reviewCheckinLocationLabel.hide();
		reviewCheckinHashtag.parent().hide();
		reviewCheckinHashtagLabel.hide();
		reviewPostOption.parent().hide();
		reviewPostOptionLabel.hide();
		reviewPostLink.parent().hide();
		reviewPostLinkLabel.hide();
	} else {
		likeLink.textinput("disable");
		likeLink.prop("required", false);
		
		reviewLikeOption.parent().hide();
		reviewLikeOptionLabel.hide();
		reviewLikeLink.parent().hide();
		reviewLikeLinkLabel.hide();
	}
});

$("form.msform").on("submit", handleMSForm);

$(document.body).on("pagecontainerchange", function(event, ui) {
	if (ui.toPage.attr("id").toUpperCase() == "NEW_PROMOTION_STEP5") {
		signDate = new Date();
		dateSigned.val(signDate.toISOString().replace("T", " ").replace("Z", ""));
		dateSignedDisplay.val(signDate.toLocaleDateString());
		
		reviewStartTimeDisplay.val(startTimeDisplay.val());
		reviewEndTimeDisplay.val(endTimeDisplay.val());
		reviewPromotionName.val(promotionName.val());
		reviewPromotionDescription.val(promotionDescription.val());
		reviewPromotionDisclaimer.val(promotionDisclaimer.val());
		reviewPromotionValue.val(promotionValue.val());
		reviewPromotionCurrency.val($(promotionCurrency[0].options[promotionCurrency[0].options.selectedIndex]).text());
		reviewEligibilityDistance.val((eligibilityDistance.val() == "0" ? "Global" : eligibilityDistance.val()));
		reviewEligibilityDistanceUnit.val($(eligibilityDistanceUnit[0].options[eligibilityDistanceUnit[0].options.selectedIndex]).text());
		reviewCheckinOption.val($(checkinOption[0].options[checkinOption[0].options.selectedIndex]).text());
		reviewCheckinLocation.val(checkinLocation.val());
		reviewCheckinHashtag.val(checkinHashtag.val());
		reviewPostOption.val($(postOption[0].options[postOption[0].options.selectedIndex]).text());
		reviewPostLink.val(postLink.val());
		reviewLikeOption.val($(likeOption[0].options[likeOption[0].options.selectedIndex]).text());
		reviewLikeLink.val(likeLink.val());
	}
});
</script>
        
<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>