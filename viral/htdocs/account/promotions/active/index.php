<?php
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/../phpinc/session.php");
	
if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/promotions/active/';
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

require_once(BASE_PATH . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if ($_SESSION["userID"] == $primaryAccountHolder["id"] || ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) == PERMISSION_ADMINISTRATOR) {
	$businesses = getAccountBusinesses($_SESSION["accountID"], null);
	$primaryBusiness = getAccountPrimaryBusiness($_SESSION["accountID"]);
} else {
	$businesses = getUserBusinesses($_SESSION["userID"], null);
	$primaryBusiness = array("id" => $businesses[0]["id"]);
}

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>

<section data-role="page" id="active_promotions_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p>Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br />
            <div class="green-header">
                <p>Active Promotions</p>
            </div>
            <br /><?php
if (count($businesses) > 1) {
?>
            <p class="orange-text larger-message">Select Business</p>
            <select id="active_promotions_business" name="business"><?php
foreach($businesses as $business) {
?>
            <option value="<?php echo $business["id"]; ?>"<?php echo ($primaryBusiness["id"] == $business["id"] ? " selected" : ""); ?>><?php echo $business["name"]; ?></option><?php
}
?>
            </select>
            <script type="text/javascript">
                $("#active_promotions_business").on("change", updateActivePromotions);
            </script>
            <br />
            <p class="orange-text larger-message">Promotions</p>
            <br /><?php
}
?>
            
            <div id="active_promotions"><?php
$activePromotions = getActivePromotions($primaryBusiness["id"]);
if (count($activePromotions) == 0) {
?>
            <div class="active-promotion">
                <p>There are no active promotions.</p>
            </div><?php
} else {
foreach ($activePromotions as $activePromotion) {
?>
                <div class="promotion-header"><?php echo $activePromotion["name"]; ?></div>
                <div class="active-promotion">
                    <div class="active-promotion-main">
                        <div class="by-line">by <a href="<?php echo $activePromotion["business_website"]; ?>" target="_blank"><?php echo $activePromotion["business_name"]; ?></a></div>
                        <p><?php echo $activePromotion["description"]; ?></p><?php
    if ($activePromotion["notes"] != "") {
?>
                        <p><small>DISCLAIMER: <?php echo $activePromotion["notes"]; ?></small></p><?php
    }
?>
                    </div>
                    <div class="active-promotion-side">
                        <table>
                            <tbody>
                                <tr>
                                    <th>Start Time:</th>
                                    <td id="<?php echo $activePromotion["id"]; ?>_start_time"></td>
                                </tr>
                                <tr>
                                    <th>End Time:</th>
                                    <td id="<?php echo $activePromotion["id"]; ?>_end_time"></td>
                                </tr>
                                <tr>
                                    <th>Awarded To:</th>
                                    <td style="white-space: normal;">Up to <?php echo $activePromotion["max_recipients"]; ?> users who <?php echo ($activePromotion["checkin_location"] != null ? "checkin to <span style='white-space: nowrap;'>'" . $activePromotion["checkin_location"] . "'</span> using hashtag <span style='white-space: nowrap;'>'". $activePromotion["checkin_hashtag"] . "'</span>" : ($activePromotion["post_link"] != null ? "post a link to <span style='white-space: nowrap;'>'" . $activePromotion["post_link"] . "'</span>" : "like the URL <span style='white-space: nowrap;'>'" . $activePromotion["like_link"] . "'</span>")); ?> on <?php echo $activePromotion["service_name"]; ?></td>
                                </tr>
                                <tr>
                                    <th>Value:</th>
                                    <td><?php echo $activePromotion["value"] . " " . $activePromotion["currency_abbreviation"]; ?></td>
                                </tr>
                                <tr>
                                    <th>Eligibility:</th>
                                    <td><?php
    if ($activePromotion["eligibility_distance"] == "0") {
?>
                                        Global<?php
    } else {
?>
                                        Within <?php echo $activePromotion["eligibility_distance"] . " " . $activePromotion["eligibility_distance_unit_name"]; ?> of:<?php
        foreach($activePromotion["eligibility_addresses"] as $eligibilityAddress) {
?>
                                            <p><?php echo render_address($eligibilityAddress + array("country" => $eligibilityAddress["country_id"]), array("first_name", "last_name", "organization_name")); ?></p><?php
        }
    }
?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="promotion-footer">Added on <span id="<?php echo $activePromotion["id"]; ?>_date_signed"></span> by <?php echo $activePromotion["signatory"]; ?></div>
                <script type="text/javascript">
                    startDate = new Date('<?php echo $activePromotion["start_time"]; ?>');
                    endDate = new Date(startDate);
                    endDate.setSeconds(startDate.getSeconds() + <?php echo $activePromotion["duration"]; ?>);
                    $("#<?php echo $activePromotion["id"]; ?>_start_time")[0].innerText = startDate.toLocaleString();
                    $("#<?php echo $activePromotion["id"]; ?>_end_time")[0].innerText = endDate.toLocaleString();
                    $("#<?php echo $activePromotion["id"]; ?>_date_signed")[0].innerText = new Date('<?php echo $activePromotion["date_signed"]; ?>').toLocaleString();
                </script>
                <br /><?php
}
}
?>
            </div>
            <div id="active_promotions_navigation">
                <button onclick="window.history.back()">Back to Account</button>
            </div>
            <br />
            <br />
            <div id="social_media">
                <p>Follow Us On...</p>
                <p>
                    <a href="//www.facebook.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/facebook.png" alt="Facebook" title="Facebook" /></a>
                    <a href="//www.twitter.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/twitter.png" alt="Twitter" title="Twitter" /></a>
                    <a href="//metarank.tumblr.com" target="_blank"><img src="<?php echo BASE_URL; ?>/img/tumblr.png" alt="Tumblr" title="Tumblr" /></a>
                    <a href="//www.pinterest.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/pinterest.png" alt="Pinterest" title="Pinterest" /></a>
                    <a href="//www.google.com/+MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/gplus.png" alt="Google+" title="Google+" /></a>
                    <a href="//www.instagram.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/instagram.png" alt="Instagram" title="Instagram" /></a>
                </p>
            </div>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>