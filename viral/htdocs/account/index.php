<?php
require_once(dirname(dirname(__FILE__)) . "/../phpinc/session.php");

if (!isset($_SESSION['isLoggedIn']) || !$_SESSION['isLoggedIn']) {
	$_SESSION['redirectURL'] = BASE_URL . '/account/';
	header('Location: ' . BASE_URL . '/register/', true, 303);
	exit();
}

require_once(BASE_PATH . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);
$primaryBusiness = getAccountPrimaryBusiness($_SESSION["accountID"]);

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>

<section data-role="page" id="account_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <div class="green-header">
                <p>My Account</p>
            </div>
            <div class="grey-box">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2">
                                <p class="orange-text larger-message">Account Information</p>
                            </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th colspan="2">&nbsp;</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <th>Primary Account Holder:</th>
                            <td><?php echo $primaryAccountHolder["first_name"] . " " . $primaryAccountHolder["last_name"] . " &lt;" . $primaryAccountHolder["email"] . "&gt;"; ?></td>
                        </tr>
                        <tr>
                            <th>Primary Business:</th>
                            <td><?php echo $primaryBusiness["name"]; ?></td>
                        </tr>
                        <tr>
                            <th>Eligible Promotion Months:</th>
                            <td><?php $eligiblePromotionMonths = getEligiblePromotionMonths($_SESSION["accountID"]); echo '<span style="color: ' . ($eligiblePromotionMonths > 9 ? "green" : $eligiblePromotionMonths > 4 ? "yellow" : "red") . ';">' . $eligiblePromotionMonths . '</span>'; ?></td>
                        </tr>
                        <tr>
                            <th>Status:</th>
                            <td>Active</td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <thead>
                        <tr>
                            <th colspan="2">
                                <p class="orange-text larger-message">User Information</p>
                            </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th colspan="2">&nbsp;</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <th>Name:</th>
                            <td><?php echo $_SESSION["userFirstName"] . " " . $_SESSION["userLastName"] . " &lt;" . $_SESSION["userEmail"] . "&gt;"; ?></td>
                        </tr>
                        <tr>
                            <th>Phone:</th>
                            <td><?php echo $_SESSION["userPhone"]; ?></td>
                        </tr>
                        <tr>
                            <th>Role:</th>
                            <td><?php echo (($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) == PERMISSION_ADMINISTRATOR ? "Administrator" : (($_SESSION["userPermissions"] & PERMISSION_MANAGER) == PERMISSION_MANAGER ? "Manager" : (($_SESSION["userPermissions"] & PERMISSION_PROMOTER) == PERMISSION_PROMOTER ? "Promoter" : "N/A"))) ?></td>
                        </tr>
                        <tr>
                            <th>Status:</th>
                            <td>Active</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="account_navigation">
                <p class="orange-text larger-message bolded">Actions</p>
                <a href="<?php echo BASE_URL; ?>/account/perks/" data-ajax="true">
                    <button>Perks</button>
                </a><br />
                <span>Offer products and services to qualified people.</span>
                <br /><br />
                <a href="<?php echo BASE_URL; ?>/account/promotions/" data-ajax="true">
                    <button>Promotions</button>
                </a><br />
                <span>Offer products and services to your customers in return for spreading the word.</span>
                <br /><br />
                <a href="<?php echo BASE_URL; ?>/account/manage/">
                    <button class="grey-btn">Manage Account</button>
                </a>
                <br /><br />
                <a href="<?php echo BASE_URL; ?>/logout/" data-ajax="false">
                    <button>Logout</button>
                </a>
            </div>
            <br />
            <br />
            <div id="social_media">
                <p>Follow Us On...</p>
                <p>
                    <a href="//www.facebook.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/facebook.png" alt="Facebook" title="Facebook" /></a>
                    <a href="//www.twitter.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/twitter.png" alt="Twitter" title="Twitter" /></a>
                    <a href="//metarank.tumblr.com" target="_blank"><img src="<?php echo BASE_URL; ?>/img/tumblr.png" alt="Tumblr" title="Tumblr" /></a>
                    <a href="//www.pinterest.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/pinterest.png" alt="Pinterest" title="Pinterest" /></a>
                    <a href="//www.google.com/+MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/gplus.png" alt="Google+" title="Google+" /></a>
                    <a href="//www.instagram.com/MetaRank" target="_blank"><img src="<?php echo BASE_URL; ?>/img/instagram.png" alt="Instagram" title="Instagram" /></a>
                </p>
            </div>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>