<?php
require_once(dirname(__FILE__) . "/../phpinc/defines.php");
require_once(BASE_PATH . "/../phpinc/header.php");
?>

<section data-role="page" id="index_page">
	<div role="main" class="ui-content">
		<div class="content-wrapper">
			<p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
			<br class="hide-on-mobile" />
			<p class="black-text larger-message">Reward Who's Who, and they reward you!</p>
			<br class="hide-on-mobile" />
			<br />
			<div id="get_started" class="grey-box">
				<p class="black-text larger-message">Get started now and let your business go Viral.</p>
			</div>
			<br />
			<div id="begin_flow">
				<div>
					<a href="<?php echo BASE_URL; ?>/account/promotions/new/" data-ajax="false">
						<button>Create Promotion</button>
					</a>
					<br />
					<a href="<?php echo BASE_URL; ?>/login/" data-ajax="true">
						<button>Account Portal</button>
					</a>
					<br />
				</div>
			</div>
			<p class="smaller-message">
				Want to talk with a specialist?
				<br>
				Call 818.588.6836
			</p>
		</div>
	</div><!-- /content -->
</section><!-- /page -->

<?php
require_once(BASE_PATH . "/../phpinc/footer.php");
?>