<?php
require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
require_once(BASE_PATH . "/../phpinc/session.php");
require_once(BASE_PATH . "/../phpinc/db.php");

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>

<section data-role="page" id="login_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <p class="black-text larger-message">Reward Who's Who, and they reward you!</p>
            <br class="hide-on-mobile" />
            <br />
            <div id="status_message" style="display:none;"></div>
            <form action="<?php echo BASE_URL; ?>/ajax/login.php" method="POST" id="login_form">
                <input type="hidden" id="login_redirect_url" value="<?php echo BASE_URL . '/account/'; //echo $_SESSION['redirectURL']; ?>">
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" required="required" />
                <label for="password">Password:</label>
                <input type="password" id="password" name="password" required="required" />
                <div id="forgot_password">
                    <a href="<?php echo BASE_URL; ?>/password/">Forgot password?</a>
                </div>
                <div id="login_captcha">
					<?php if(getLoginFailureCountByRemoteIP() > MAX_LOGIN_FAILURES) { ?>
	                    <script type="text/javascript">
    	                showRecaptcha($("#login_captcha")[0]);
        	            </script>
	                <?php } ?>
                </div>
                <div id="navigation_box" class="grey-box">
                    <p>
                    	<button type="submit">Login</button>
                    	<button type="button" onclick="window.history.back()">Back</button>
                    </p>
                </div>
            </form>
            <script type="text/javascript">
            $("#login_form").on("submit", login);
            </script>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>