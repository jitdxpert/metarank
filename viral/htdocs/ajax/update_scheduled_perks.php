<?php

// Some basic input validation
if (!isset($_POST["business"]) || strlen($_POST["business"]) != 32 || !ctype_xdigit($_POST["business"])) {
	exit();
}

require_once(dirname(dirname(__FILE__)) . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");
require_once(BASE_PATH . "/../phpinc/session.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if ((($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR) || !checkBusinessInAccount($_POST["business"], $_SESSION["accountID"])) && (($_SESSION["userPermissions"] & PERMISSION_MANAGER) != PERMISSION_MANAGER || !checkBusinessInManagedBusiness($_POST["business"], $_SESSION["userID"]))) {
	exit();
}

$scheduledPerks = getScheduledPerks($_POST["business"]);
if (count($scheduledPerks) == 0) {
?>
						<div class="scheduled-perk"><p>There are no scheduled perks.</p><br/></div><?php
} else {
	foreach ($scheduledPerks as $scheduledPerk) {
?>
						<div class="perk-header"><?php echo $scheduledPerk["name"]; ?></div>
						<div class="scheduled-perk">
							<div class="scheduled-perk-main">
								<div class="by-line">by <a href="<?php echo $scheduledPerk["business_website"]; ?>" target="_blank"><?php echo $scheduledPerk["business_name"]; ?></a></div>
								<p><?php echo $scheduledPerk["description"]; ?></p><?php
		if ($scheduledPerk["notes"] != "") {
?>
								<p><small>DISCLAIMER: <?php echo $scheduledPerk["notes"]; ?></small></p><?php
		}
?>
							</div>
							<div class="scheduled-perk-side">
								<table>
									<tbody>
										<tr>
											<th>Start Time:</th>
											<td id="<?php echo $scheduledPerk["id"]; ?>_start_time"></td>
										</tr>
										<tr>
											<th>End Time:</th>
											<td id="<?php echo $scheduledPerk["id"]; ?>_end_time"></td>
										</tr>
										<tr>
											<th>Awarded To:</th>
											<td>Top <?php echo $scheduledPerk["max_recipients"]; ?> on <?php echo $scheduledPerk["service_name"]; ?></td>
										</tr>
										<tr>
											<th>Value:</th>
											<td><?php echo $scheduledPerk["value"] . " " . $scheduledPerk["currency_abbreviation"]; ?></td>
										</tr>
										<tr>
											<th>Eligibility:</th>
											<td><?php
		if ($scheduledPerk["eligibility_distance"] == "0") {
?>
												Global<?php
		} else {
?>
												Within <?php echo $scheduledPerk["eligibility_distance"] . " " . $scheduledPerk["eligibility_distance_unit_name"]; ?> of:<?php
			foreach($scheduledPerk["eligibility_addresses"] as $eligibilityAddress) {
?>
													<p><?php echo render_address($eligibilityAddress + array("country" => $eligibilityAddress["country_id"]), array("first_name", "last_name", "organization_name")); ?></p><?php
			}
		}
?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="perk-footer">Added on <span id="<?php echo $scheduledPerk["id"]; ?>_date_signed"></span> by <?php echo $scheduledPerk["signatory"]; ?></div>
						<script type="text/javascript">
							startDate = new Date('<?php echo $scheduledPerk["start_time"]; ?>');
							endDate = new Date(startDate);
							endDate.setSeconds(startDate.getSeconds() + <?php echo $scheduledPerk["duration"]; ?>);
							$("#<?php echo $scheduledPerk["id"]; ?>_start_time")[0].innerText = startDate.toLocaleString();
							$("#<?php echo $scheduledPerk["id"]; ?>_end_time")[0].innerText = endDate.toLocaleString();
							$("#<?php echo $scheduledPerk["id"]; ?>_date_signed")[0].innerText = new Date('<?php echo $scheduledPerk["date_signed"]; ?>').toLocaleString();
						</script>
						<br /><?php
	}
}
?>