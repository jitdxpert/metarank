<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["organization_name"]) || !isset($_POST["organization_name"][0]) || $_POST["organization_name"][0] == "" ||
		!isset($_POST["thoroughfare"]) || !isset($_POST["thoroughfare"][0]) || $_POST["thoroughfare"][0] == "" ||
		!isset($_POST["premise"]) || !isset($_POST["premise"][0]) ||
		!isset($_POST["locality"]) || !isset($_POST["locality"][0]) || $_POST["locality"][0] == "" ||
		!isset($_POST["dependent_locality"]) || !isset($_POST["dependent_locality"][0]) ||
		!isset($_POST["administrative_area"]) || !isset($_POST["administrative_area"][0]) || $_POST["administrative_area"][0] == "" ||
		!isset($_POST["postal_code"]) || !isset($_POST["postal_code"][0]) || $_POST["postal_code"][0] == "" ||
		!isset($_POST["country"]) || !isset($_POST["country"][0]) || strlen($_POST["country"][0]) != 32 || !ctype_xdigit($_POST["country"][0]) ||
		!isset($_POST["latitude"]) || !is_array($_POST["latitude"]) || !isset($_POST["latitude"][0]) || $_POST["latitude"][0] == "" ||
		!isset($_POST["longitude"]) || !is_array($_POST["longitude"]) || !isset($_POST["longitude"][0]) || $_POST["longitude"][0] == "" ||
		!isset($_POST["business_category"]) || strlen($_POST["business_category"]) != 32 || !ctype_xdigit($_POST["business_category"]) ||
		!isset($_POST["website"]) || $_POST["website"] == "" ||
		!isset($_POST["first_name"]) || $_POST["first_name"] == "" ||
		!isset($_POST["last_name"]) || $_POST["last_name"] == "" ||
		!isset($_POST["phone"]) || $_POST["phone"] == "" ||
		!isset($_POST["email"]) || $_POST["email"] == "" ||
		!isset($_POST["password"]) || $_POST["password"] == "" ||
		!isset($_POST["password_confirm"]) || $_POST["password_confirm"] == "" ||
		!isset($_POST["security_question"]) || strlen($_POST["security_question"]) != 32 || !ctype_xdigit($_POST["security_question"]) ||
		!isset($_POST["security_answer"]) || $_POST["security_answer"] == "" ||
		!isset($_POST["comments"])
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		if (!filter_var($_POST["website"], FILTER_VALIDATE_URL)) {
			$message = "Invalid website url.";
		} else if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
			$message = "Invalid email address.";
		} else if ($_POST["password"] != $_POST["password_confirm"]) {
			$message = "Password mismatch.";
		} else {
			// Begin processing
			require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
			require_once(BASE_PATH . "/../phpinc/session.php");
			require_once(BASE_PATH . "/../phpinc/db.php");
			
			$userID = createUser($_POST["first_name"], $_POST["last_name"], $_POST["phone"], $_POST["email"], $_POST["password"], $_POST["security_question"], $_POST["security_answer"]);

			if ($userID == null) {
				$message = "Email address already in use.";
			} else {
				if (!giveUserRole($userID, 'Administrator')) {
					rollbackChanges();
					
					$message = "Error elevating new user to Administrative level. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
				} else {
					$businessAddressID = createBusinessAddress($_POST["thoroughfare"][0],
					                                                                     $_POST["premise"][0],
					                                                                     $_POST["locality"][0],
				    	                                                                 $_POST["dependent_locality"][0],
				    	                                                                 $_POST["administrative_area"][0],
				    	                                                                 $_POST["postal_code"][0],
				    	                                                                 $_POST["country"][0],
					                                                                     $_POST["latitude"][0],
					                                                                     $_POST["longitude"][0]);
					if ($businessAddressID == null) {
						rollbackChanges();
					
						$message = "Error storing new business address information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
					} else {
						$businessID = createBusiness($_POST["organization_name"][0], $_POST["website"], $businessAddressID);
						if ($businessID == null) {
							rollbackChanges();
					
							$message = "Error storing new business information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
						} else {
							if (!linkBusinessToAddress($businessID, $businessAddressID)) {
								rollbackChanges();
					
								$message = "Error linking new business address information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
							} else {
								if (!linkCategoryToBusiness($_POST["business_category"], $businessID)) {
									rollbackChanges();
						
									$message = "Error assigning category to new business. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
								} else {
									$accountID = createAccount($_POST["comments"], $userID, $businessID);
									if ($accountID == null) {
										rollbackChanges();
						
										$message = "Error storing new account information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
									} else {
										if (!linkAccountToUser($accountID, $userID)) {
											rollbackChanges();
						
											$message = "Error storing new account user information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
										} else {
											if (!linkAccountToBusiness($accountID, $businessID)) {
												rollbackChanges();
												
												$message = "Error storing new account business information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
											} else {
												$success = true;
												$message = "Registration successful.";
												
												// Also log them in!
												logSuccessfulLogin($_POST["email"]);
						
												session_regenerate_id(true);
						
												loadSessionUserInformation($_POST["email"]);
												
												updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"));
												
												commitChanges();
												
												$_SESSION["isLoggedIn"] = true;
												
												// Send userID in email
												$headers =  "From: email_verification@metarank.com\r\n";
												$headers .= "Reply-To: do_not_reply@metarank.com\r\n";
												$headers .= "MIME-Version: 1.0\r\n";
												$headers .= 'Content-Type: multipart/related; boundary="m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y"; type="text/html"\r\n';
												
												$metaRankBottomPNG = base64_encode(fread(fopen(BASE_PATH . "/img/MetaRankBottom.png", "r"), filesize(BASE_PATH . "/img/MetaRankBottom.png")));
												$logoPNG = base64_encode(fread(fopen(BASE_PATH . "/img/logo.png", "r"), filesize(BASE_PATH . "/img/logo.png")));
												
												$emailContent = <<<EOD
--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y
Content-Type: text/html; charset="UTF-8"

<!DOCTYPE html>
<html>
	<head>
		<title>Viral by MetaRank</title>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style type="text/css">
			html, body {
				font-family: Verdana;
				margin: 0;
				padding: 0;
				height: 100%;
			}
			body {
				background: url(cid:MetaRankBottom.png) no-repeat bottom left;
			}
			.meta-logotext {
				color: #00B200;
				font-weight: bold;
			}
			.rank-logotext {
				color: #000000;
				font-weight: bold;
			}

			.black-text {
				color: #000000;
			}

			.orange-text {
				color: #FF7F00;
			}

			.larger-message {
				font-size: 1.1em;
			}

			.smaller-message {
				font-size: .9em;
			}

			.grey-box {
				background-color: rgba(238, 238, 238, .75);
				margin-left: -3em;
				margin-right: -3em;
				padding-left: 3em;
				padding-right: 3em;
			}

			.green-header {
				background-color: #00B200;
				margin-left: -3em;
				margin-right: -3em;
				padding-left: 3em;
				padding-right: 3em;
				color: #FFFFFF;
				text-shadow: none;
				padding: 1px;
				-webkit-border-radius: 10px;
				-moz-border-radius: 10px;
				border-radius: 10px;
			}

			#header {
				position: absolute;
				top: 0;
				left: 0;
				right: 0;
				text-align: center;
				height: 70px;
				z-index: 1;
				background-color: #F6F6F6;
				-webkit-box-shadow: 0px 5px 5px 0px rgba(50, 50, 50, 0.4);
				-moz-box-shadow:    0px 5px 5px 0px rgba(50, 50, 50, 0.4);
				box-shadow:         0px 5px 5px 0px rgba(50, 50, 50, 0.4);
			}
			#header img {
				width: 320px;
			}
			#main {
				bottom: 0;
				background-color: transparent;
			}

			#footer {
				position: absolute;
				left: 0;
				right: 0;
				bottom: 0;
				height: 18px;
				box-sizing: border-box;
				padding-left: 15%;
				padding-right: 15%;
				font-size: .9em;
				background-color: #00B200;
			}
			#footer span:last-child {
				display: inline-block;
				float: right;
			}
			#header, #footer {
				color: #FFFFFF;
				font-weight: normal;
				text-shadow: none;
			}
			
			#main_content {
				position: absolute;
				top: 70px;
				bottom: 17px;
				left: 0;
				right: 0;
				overflow-y: auto;
				padding:2em 0 2em 0;
			}

			.content-wrapper {
				margin: auto;
				max-width: 70%;
				color: #888888;
				text-align: center;
				box-sizing: border-box;
				background-color: rgba(246, 246, 246, .75);
				margin-top: 0;
				margin-bottom: 0;
				padding: 1em 3em 1em 3em;
				min-height: 100%;
			}

			a {
				color: #FFFFFF;
				text-decoration: none;
			}

			a:hover {
				color: #AAAAAA;
				text-decoration: none;
			}

			a:active {
				color: #CCCCCC;
				text-decoration: none;
			}

			a:visited {
				color: #888888;
				text-decoration: none;
			}
			
			#main a {
				text-decoration: none;
				color: #0080C0;
			}
			#main a:hover {
				text-decoration: none;
				color: #0080C0;
			}
			#main a:active {
				text-decoration: none;
				color: #0080C0;
			}
			#main a:visited {
				text-decoration: none;
				color: #0080C0;
			}
		</style>
	</head>
	<body>
		<div id="header">
			<img src="cid:logo.png" alt="MetaRank" />
		</div><!-- /header -->
		<div id="main">
			<div id="main_content" class="ui-content">
				<div class="content-wrapper">
					<p>Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
					<p class="black-text larger-message">Verify Email</p>
					<br />
					<div class="grey-box">
						<br />
						<p>Please verify your email by clicking or pasting into your browser address bar the following link:</p>
						<blockquote><a href="http://viral.metarank.com/verify/?uuid=$userID">http://viral.metarank.com/verify/?uuid=$userID</a></blockquote>
						<br />
					</div>
					<p>Thank you for using Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>!</p>
				</div>
			</div>
		</div>
		<div id="footer">
			<span>Use confirms acceptance of <a href="http://viral.metarank.com/terms/">Terms</a> and <a href="http://viral.metarank.com/privacy/">Privacy</a>.</span>
			<span>MetaRank &copy; 2014</span>
		</div><!-- /footer -->
	</body>
</html>

--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y
Content-Location: CID:ignored0000
Content-ID: <MetaRankBottom.png>
Content-Type: IMAGE/PNG
Content-Transfer-Encoding: BASE64

$metaRankBottomPNG

--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y
Content-Location: CID:ignored0001
Content-ID: <logo.png>
Content-Type: IMAGE/PNG
Content-Transfer-Encoding: BASE64

$logoPNG

--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y--
EOD;
												mail($_POST["email"], "Viral by metarank.com - Verify Email", $emailContent, $headers);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>
