<?php

// Some basic input validation
if (!isset($_POST["business"]) || strlen($_POST["business"]) != 32 || !ctype_xdigit($_POST["business"])) {
	exit();
}

require_once(dirname(dirname(__FILE__)) . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");
require_once(BASE_PATH . "/../phpinc/session.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if ((($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR) || !checkBusinessInAccount($_POST["business"], $_SESSION["accountID"])) && (($_SESSION["userPermissions"] & PERMISSION_MANAGER) != PERMISSION_MANAGER || !checkBusinessInManagedBusiness($_POST["business"], $_SESSION["userID"]))) {
	exit();
}

$activePerks = getActivePerks($_POST["business"]);
if (count($activePerks) == 0) {
?>
						<div class="active-perk"><p>There are no active perks.</p><br/></div><?php
} else {
	foreach ($activePerks as $activePerk) {
?>
						<div class="perk-header"><?php echo $activePerk["name"]; ?></div>
						<div class="active-perk">
							<div class="active-perk-main">
								<div class="by-line">by <a href="<?php echo $activePerk["business_website"]; ?>" target="_blank"><?php echo $activePerk["business_name"]; ?></a></div>
								<p><?php echo $activePerk["description"]; ?></p><?php
		if ($activePerk["notes"] != "") {
?>
								<p><small>DISCLAIMER: <?php echo $activePerk["notes"]; ?></small></p><?php
		}
?>
							</div>
							<div class="active-perk-side">
								<table>
									<tbody>
										<tr>
											<th>Start Time:</th>
											<td id="<?php echo $activePerk["id"]; ?>_start_time"></td>
										</tr>
										<tr>
											<th>End Time:</th>
											<td id="<?php echo $activePerk["id"]; ?>_end_time"></td>
										</tr>
										<tr>
											<th>Awarded To:</th>
											<td>Top <?php echo $activePerk["max_recipients"]; ?> on <?php echo $activePerk["service_name"]; ?></td>
										</tr>
										<tr>
											<th>Value:</th>
											<td><?php echo $activePerk["value"] . " " . $activePerk["currency_abbreviation"]; ?></td>
										</tr>
										<tr>
											<th>Eligibility:</th>
											<td><?php
		if ($activePerk["eligibility_distance"] == "0") {
?>
												Global<?php
		} else {
?>
												Within <?php echo $activePerk["eligibility_distance"] . " " . $activePerk["eligibility_distance_unit_name"]; ?> of:<?php
			foreach($activePerk["eligibility_addresses"] as $eligibilityAddress) {
?>
													<p><?php echo render_address($eligibilityAddress + array("country" => $eligibilityAddress["country_id"]), array("first_name", "last_name", "organization_name")); ?></p><?php
			}
		}
?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="perk-footer">Added on <span id="<?php echo $activePerk["id"]; ?>_date_signed"></span> by <?php echo $activePerk["signatory"]; ?></div>
						<script type="text/javascript">
							startDate = new Date('<?php echo $activePerk["start_time"]; ?>');
							endDate = new Date(startDate);
							endDate.setSeconds(startDate.getSeconds() + <?php echo $activePerk["duration"]; ?>);
							$("#<?php echo $activePerk["id"]; ?>_start_time")[0].innerText = startDate.toLocaleString();
							$("#<?php echo $activePerk["id"]; ?>_end_time")[0].innerText = endDate.toLocaleString();
							$("#<?php echo $activePerk["id"]; ?>_date_signed")[0].innerText = new Date('<?php echo $activePerk["date_signed"]; ?>').toLocaleString();
						</script>
						<br /><?php
	}
}
?>