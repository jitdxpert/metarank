<?php

// Some basic input validation
if (!isset($_POST["business"]) || strlen($_POST["business"]) != 32 || !ctype_xdigit($_POST["business"])) {
	exit();
}

require_once(dirname(dirname(__FILE__)) . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");
require_once(BASE_PATH . "/../phpinc/session.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if ((($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR) || !checkBusinessInAccount($_POST["business"], $_SESSION["accountID"])) && (($_SESSION["userPermissions"] & PERMISSION_MANAGER) != PERMISSION_MANAGER || !checkBusinessInManagedBusiness($_POST["business"], $_SESSION["userID"]))) {
	exit();
}

$completedPerks = getCompletedPerks($_POST["business"]);
if (count($completedPerks) == 0) {
?>
						<div class="completed-perk"><p>There are no completed perks.</p><br/></div><?php
} else {
	foreach ($completedPerks as $completedPerk) {
?>
						<div class="perk-header"><?php echo $completedPerk["name"]; ?></div>
						<div class="completed-perk">
							<div class="completed-perk-main">
								<div class="by-line">by <a href="<?php echo $completedPerk["business_website"]; ?>" target="_blank"><?php echo $completedPerk["business_name"]; ?></a></div>
								<p><?php echo $completedPerk["description"]; ?></p><?php
		if ($completedPerk["notes"] != "") {
?>
								<p><small>DISCLAIMER: <?php echo $completedPerk["notes"]; ?></small></p><?php
		}
?>
							</div>
							<div class="completed-perk-side">
								<table>
									<tbody>
										<tr>
											<th>Start Time:</th>
											<td id="<?php echo $completedPerk["id"]; ?>_start_time"></td>
										</tr>
										<tr>
											<th>End Time:</th>
											<td id="<?php echo $completedPerk["id"]; ?>_end_time"></td>
										</tr>
										<tr>
											<th>Awarded To:</th>
											<td>Top <?php echo $completedPerk["max_recipients"]; ?> on <?php echo $completedPerk["service_name"]; ?></td>
										</tr>
										<tr>
											<th>Value:</th>
											<td><?php echo $completedPerk["value"] . " " . $completedPerk["currency_abbreviation"]; ?></td>
										</tr>
										<tr>
											<th>Eligibility:</th>
											<td><?php
		if ($completedPerk["eligibility_distance"] == "0") {
?>
												Global<?php
		} else {
?>
												Within <?php echo $completedPerk["eligibility_distance"] . " " . $completedPerk["eligibility_distance_unit_name"]; ?> of:<?php
			foreach($completedPerk["eligibility_addresses"] as $eligibilityAddress) {
?>
													<p><?php echo render_address($eligibilityAddress + array("country" => $eligibilityAddress["country_id"]), array("first_name", "last_name", "organization_name")); ?></p><?php
			}
		}
?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="perk-footer">Added on <span id="<?php echo $completedPerk["id"]; ?>_date_signed"></span> by <?php echo $completedPerk["signatory"]; ?></div>
						<script type="text/javascript">
							startDate = new Date('<?php echo $completedPerk["start_time"]; ?>');
							endDate = new Date(startDate);
							endDate.setSeconds(startDate.getSeconds() + <?php echo $completedPerk["duration"]; ?>);
							$("#<?php echo $completedPerk["id"]; ?>_start_time")[0].innerText = startDate.toLocaleString();
							$("#<?php echo $completedPerk["id"]; ?>_end_time")[0].innerText = endDate.toLocaleString();
							$("#<?php echo $completedPerk["id"]; ?>_date_signed")[0].innerText = new Date('<?php echo $completedPerk["date_signed"]; ?>').toLocaleString();
						</script>
						<br /><?php
	}
}
?>