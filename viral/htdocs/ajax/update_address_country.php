<?php

// Some basic input validation
if (!isset($_POST["addressFieldNumber"]) || !is_numeric($_POST["addressFieldNumber"]) || !isset($_POST["removeFields"])) {
	exit;
}

require_once(dirname(dirname(__FILE__)) . "/../phpinc/addressfield.php");

$addressFieldNumber = $_POST["addressFieldNumber"];
$removeFields = explode(",", $_POST["removeFields"]);

$validAddressElements = array('name_line', 'first_name', 'last_name', 'organization_name', 'country', 'administrative_area', 'sub_administrative_area', 'locality', 'dependent_locality', 'postal_code', 'thoroughfare', 'premise', 'sub_premise');
foreach($_POST as $key => $value) {
	if (!in_array($key, $validAddressElements)) {
		unset($_POST[$key]);
	}
}

echo render_address_form($_POST, $removeFields, $addressFieldNumber);

?>