<?php

// Some basic input validation
if (!isset($_POST["business"]) || strlen($_POST["business"]) != 32 || !ctype_xdigit($_POST["business"])) {
	exit();
}

require_once(dirname(dirname(__FILE__)) . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");
require_once(BASE_PATH . "/../phpinc/session.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if ((($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR) || !checkBusinessInAccount($_POST["business"], $_SESSION["accountID"])) && (($_SESSION["userPermissions"] & PERMISSION_MANAGER) != PERMISSION_MANAGER || !checkBusinessInManagedBusiness($_POST["business"], $_SESSION["userID"]))) {
	exit();
}

$completedPromotions = getCompletedPromotions($primaryBusiness["id"]);
if (count($completedPromotions) == 0) {
?>
					<div class="completed-promotion">
						<p>There are no completed promotions.</p>
					</div><?php
} else {
	foreach ($completedPromotions as $completedPromotion) {
?>
						<div class="promotion-header"><?php echo $completedPromotion["name"]; ?></div>
						<div class="completed-promotion">
							<div class="completed-promotion-main">
								<div class="by-line">by <a href="<?php echo $completedPromotion["business_website"]; ?>" target="_blank"><?php echo $completedPromotion["business_name"]; ?></a></div>
								<p><?php echo $completedPromotion["description"]; ?></p><?php
		if ($completedPromotion["notes"] != "") {
?>
								<p><small>DISCLAIMER: <?php echo $completedPromotion["notes"]; ?></small></p><?php
		}
?>
							</div>
							<div class="completed-promotion-side">
								<table>
									<tbody>
										<tr>
											<th>Start Time:</th>
											<td id="<?php echo $completedPromotion["id"]; ?>_start_time"></td>
										</tr>
										<tr>
											<th>End Time:</th>
											<td id="<?php echo $completedPromotion["id"]; ?>_end_time"></td>
										</tr>
										<tr>
											<th>Awarded To:</th>
											<td style="white-space: normal;">Up to <?php echo $completedPromotion["max_recipients"]; ?> users who <?php echo ($completedPromotion["checkin_location"] != null ? "checkin to <span style='white-space: nowrap;'>'" . $completedPromotion["checkin_location"] . "'</span> using hashtag <span style='white-space: nowrap;'>'". $completedPromotion["checkin_hashtag"] . "'</span>" : ($completedPromotion["post_link"] != null ? "post a link to <span style='white-space: nowrap;'>'" . $completedPromotion["post_link"] . "'</span>" : "like the URL <span style='white-space: nowrap;'>'" . $completedPromotion["like_link"] . "'</span>")); ?> on <?php echo $completedPromotion["service_name"]; ?></td>
										</tr>
										<tr>
											<th>Value:</th>
											<td><?php echo $completedPromotion["value"] . " " . $completedPromotion["currency_abbreviation"]; ?></td>
										</tr>
										<tr>
											<th>Eligibility:</th>
											<td><?php
		if ($completedPromotion["eligibility_distance"] == "0") {
?>
												Global<?php
		} else {
?>
												Within <?php echo $completedPromotion["eligibility_distance"] . " " . $completedPromotion["eligibility_distance_unit_name"]; ?> of:<?php
			foreach($completedPromotion["eligibility_addresses"] as $eligibilityAddress) {
?>
													<p><?php echo render_address($eligibilityAddress + array("country" => $eligibilityAddress["country_id"]), array("first_name", "last_name", "organization_name")); ?></p><?php
			}
		}
?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="promotion-footer">Added on <span id="<?php echo $completedPromotion["id"]; ?>_date_signed"></span> by <?php echo $completedPromotion["signatory"]; ?></div>
						<script type="text/javascript">
							startDate = new Date('<?php echo $completedPromotion["start_time"]; ?>');
							endDate = new Date(startDate);
							endDate.setSeconds(startDate.getSeconds() + <?php echo $completedPromotion["duration"]; ?>);
							$("#<?php echo $completedPromotion["id"]; ?>_start_time")[0].innerText = startDate.toLocaleString();
							$("#<?php echo $completedPromotion["id"]; ?>_end_time")[0].innerText = endDate.toLocaleString();
							$("#<?php echo $completedPromotion["id"]; ?>_date_signed")[0].innerText = new Date('<?php echo $completedPromotion["date_signed"]; ?>').toLocaleString();
						</script>
						<br /><?php
	}
}
?>