<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["first_name"]) || $_POST["first_name"] == "" ||
		!isset($_POST["last_name"]) || $_POST["last_name"] == "" ||
		!isset($_POST["phone"]) || $_POST["phone"] == ""
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		// Begin processing
		require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
		require_once(BASE_PATH . "/../phpinc/session.php");
		require_once(BASE_PATH . "/../phpinc/db.php");
		
		if (!updateUserProfile($_SESSION["userID"], $_POST["first_name"], $_POST["last_name"], $_POST["phone"])) {
			rollbackChanges();
			$message = "Error updating user profile. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
		} else {
			commitChanges();
			$_SESSION["userFirstName"] = $_POST["first_name"];
			$_SESSION["userLastName"] = $_POST["last_name"];
			$_SESSION["userPhone"] = $_POST["phone"];
			
			$success = true;
			$message = "Profile successfully updated.";
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>