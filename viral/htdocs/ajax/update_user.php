<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["u"]) || strlen($_POST["u"]) != 32 || !ctype_xdigit($_POST["u"]) ||
		!isset($_POST["first_name"]) || $_POST["first_name"] == "" ||
		!isset($_POST["last_name"]) || $_POST["last_name"] == "" ||
		!isset($_POST["phone"]) || $_POST["phone"] == "" ||
		!isset($_POST["role"]) || strlen($_POST["role"]) != 32 || !ctype_xdigit($_POST["role"]) ||
		!isset($_POST["businesses"]) || !is_array($_POST["businesses"]) || count($_POST["businesses"]) == 0
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		if (isset($_POST["password"]) && $_POST["password"] != "" && isset($_POST["password_confirm"]) && $_POST["password_confirm"] != "" && $_POST["password"] != $_POST["password_confirm"]) {
			$message = "Password mismatch.";
		} else {
			$validBusinessIDs = true;
			foreach($_POST["businesses"] as $businessID) {
				if (!ctype_xdigit($businessID) || strlen($businessID) != 32) {
					$validBusinessIDs = false;
					break;
				}
			}
			
			if (!$validBusinessIDs) {
				$message = "Invalid business(es).";
			} else {
				// Begin processing
				require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
				require_once(BASE_PATH . "/../phpinc/session.php");
				require_once(BASE_PATH . "/../phpinc/db.php");
				
				$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);
				
				if ($_SESSION["userID"] == $_POST["u"]) {
					$message = "Invalid user.";
				} else {
					if ((($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR) || !checkUserInAccount($_POST["u"], $_SESSION["accountID"])) && (($_SESSION["userPermissions"] & PERMISSION_MANAGER) != PERMISSION_MANAGER || !checkUserInManagedBusiness($_POST["u"], $_SESSION["userID"]))) {
						$message = "Insufficient permissions.";
					} else {
						$managedBusinessIDs = true;
						if (($_SESSION["userPermissions"] & PERMISSION_MANAGER) == PERMISSION_MANAGER) {
							foreach($_POST["businesses"] as $businessID) {
								if (!checkBusinessInManagedBusinesses($businessID, $_SESSION["userID"])) {
									$managedBusinessIDs = false;
									break;
								}
							}
						}
						
						if (!$managedBusinessIDs) {
							$message = "Insufficient permissions.";
						} else {
							if (!updateUserProfile($_POST["u"], $_POST["first_name"], $_POST["last_name"], $_POST["phone"])) {
								rollbackChanges();
								$message = "Error updating user information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
							} else {
								if (isset($_POST["new_password"]) && $_POST["new_password"] != "" && isset($_POST["new_password_confirm"]) && $_POST["new_password_confirm"] != "") {
									if (!changeUserPassword($_POST["u"], $_POST["new_password"])) {
										rollbackChanges();
										
										$message = "Error storing new password. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
									} else {
										if (checkUserIsLocked($_POST["u"])["locked"]) {
											if (!isset($_POST["lock"])) {
												if (!unlockUser($_POST["u"], "Unlocked in User Management", $_SESSION["userID"])) {
													rollbackChanges();
													
													$message = "Unabled to unlock user. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
												} else {
													commitChanges();
													$success = true;
													$message = "User information, lock status and password updated successfully. Please communicate the new password to this user, as changes are effective immediately.";
												}
											} else {
												commitChanges();
												$success = true;
												$message = "User information and password updated successfully. Please communicate the new password to this user, as changes are effective immediately.";
											}
										} else {
											if (isset($_POST["lock"])) {
												if (!lockUser($_POST["u"], "Locked in User Management", $_SESSION["userID"])) {
													rollbackChanges();
													
													$message = "Unabled to lock user. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
												} else {
													commitChanges();
													$success = true;
													$message = "User information, lock status and password updated successfully. Please communicate the new password to this user, as changes are effective immediately. Note: They will not be able to login while locked.";
													
													exec('cd ' . BASE_PATH . "/../processes && php -f update_user_permissions.php " . escapeshellarg($_POST["u"]) . " false");
												}
											} else {
												commitChanges();
												$success = true;
												$message = "User information and password updated successfully. Please communicate the new password to this user, as changes are effective immediately.";
												
												exec('cd ' . BASE_PATH . "/../processes && php -f update_user_permissions.php " . escapeshellarg($_POST["u"]) . " true " . escapeshellarg($_POST["first_name"]) . " " . escapeshellarg($_POST["last_name"]) . " " . escapeshellarg($_POST["phone"]));
											}
										}
									}
								} else {
									if (checkUserIsLocked($_POST["u"])["locked"]) {
										if (!isset($_POST["lock"])) {
											if (!unlockUser($_POST["u"], "Unlocked in User Management", $_SESSION["userID"])) {
												rollbackChanges();
												
												$message = "Unabled to unlock user. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
											} else {
												commitChanges();
												$success = true;
												$message = "User information and lock status updated successfully.";
											}
										} else {
											commitChanges();
											$success = true;
											$message = "User information updated successfully.";
										}
									} else {
										if (isset($_POST["lock"])) {
											if (!lockUser($_POST["u"], "Locked in User Management", $_SESSION["userID"])) {
												rollbackChanges();
												
												$message = "Unabled to lock user. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
											} else {
												commitChanges();
												$success = true;
												$message = "User information and lock status updated successfully. Note: They will not be able to login while locked.";
												
												exec('cd ' . BASE_PATH . "/../processes && php -f update_user_permissions.php " . escapeshellarg($_POST["u"]) . " false");
											}
										} else {
											commitChanges();
											$success = true;
											$message = "User information updated successfully.";
											
											exec('cd ' . BASE_PATH . "/../processes && php -f update_user_permissions.php " . escapeshellarg($_POST["u"]) . " true " . escapeshellarg($_POST["first_name"]) . " " . escapeshellarg($_POST["last_name"]) . " " . escapeshellarg($_POST["phone"]));
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>
