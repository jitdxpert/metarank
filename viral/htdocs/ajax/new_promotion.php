<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["business"]) || strlen($_POST["business"]) != 32 || !ctype_xdigit($_POST["business"]) ||
		!isset($_POST["eligibility_distance"]) || $_POST["eligibility_distance"] == "" || $_POST["eligibility_distance"] != (string)(int)$_POST["eligibility_distance"] ||
		(
			$_POST["eligibility_distance"] != "0" &&
			(
				!isset($_POST["eligibility_distance_unit"]) || strlen($_POST["eligibility_distance_unit"]) != 32 || !ctype_xdigit($_POST["eligibility_distance_unit"]) ||
				!isset($_POST["thoroughfare"]) || !is_array($_POST["thoroughfare"]) || !isset($_POST["thoroughfare"][0]) || $_POST["thoroughfare"][0] == "" ||
				!isset($_POST["premise"]) || !is_array($_POST["premise"]) || !isset($_POST["premise"][0]) ||
				!isset($_POST["locality"]) || !is_array($_POST["locality"]) || !isset($_POST["locality"][0]) || $_POST["locality"][0] == "" ||
				!isset($_POST["dependent_locality"]) || !is_array($_POST["dependent_locality"]) || !isset($_POST["dependent_locality"][0]) ||
				!isset($_POST["administrative_area"]) || !is_array($_POST["administrative_area"]) || !isset($_POST["administrative_area"][0]) || $_POST["administrative_area"][0] == "" ||
				!isset($_POST["postal_code"]) || !is_array($_POST["postal_code"]) || !isset($_POST["postal_code"][0]) || $_POST["postal_code"][0] == "" ||
				!isset($_POST["country"]) || !is_array($_POST["country"]) || !isset($_POST["country"][0]) || strlen($_POST["country"][0]) != 32 || !ctype_xdigit($_POST["country"][0]) ||
				!isset($_POST["latitude"]) || !is_array($_POST["latitude"]) || !isset($_POST["latitude"][0]) || $_POST["latitude"][0] == "" ||
				!isset($_POST["longitude"]) || !is_array($_POST["longitude"]) || !isset($_POST["longitude"][0]) || $_POST["longitude"][0] == ""
			)
		) ||
		!isset($_POST["start_time"]) || $_POST["start_time"] == "" ||
		!isset($_POST["duration"]) || $_POST["duration"] == "" ||
		!isset($_POST["promotion_name"]) || $_POST["promotion_name"] == "" ||
		!isset($_POST["promotion_description"]) || $_POST["promotion_description"] == "" ||
		!isset($_POST["promotion_value"]) || $_POST["promotion_value"] == "" || $_POST["promotion_value"] != (string)(float)$_POST["promotion_value"] ||
		!isset($_POST["promotion_currency"]) || strlen($_POST["promotion_currency"]) != 32 || !ctype_xdigit($_POST["promotion_currency"]) ||
		!isset($_POST["promotion_max_recipients"]) || $_POST["promotion_max_recipients"] == "" || $_POST["promotion_max_recipients"] != (string)(int)$_POST["promotion_max_recipients"] ||
		(
			(
				isset($_POST["checkin_option"]) && strlen($_POST["checkin_option"]) == 32 && ctype_xdigit($_POST["checkin_option"]) &&
				(
					(
						(isset($_POST["post_option"]) && $_POST["post_option"] != "") || (isset($_POST["like_option"]) && $_POST["like_option"] != "")
					) ||
					(
						!isset($_POST["checkin_location"]) || $_POST["checkin_location"] == "" ||
						!isset($_POST["checkin_hashtag"]) || $_POST["checkin_hashtag"] == ""
					)
				)
			) ||
			(
				isset($_POST["post_option"]) && strlen($_POST["post_option"]) == 32 && ctype_xdigit($_POST["post_option"]) &&
				(
					(
						(isset($_POST["checkin_option"]) && $_POST["checkin_option"] != "") || (isset($_POST["like_option"]) && $_POST["like_option"] != "")
					) ||
					(
						!isset($_POST["post_link"]) || $_POST["post_link"] == ""
					)
				)
			) ||
			(
				isset($_POST["like_option"]) && strlen($_POST["like_option"]) == 32 && ctype_xdigit($_POST["like_option"]) &&
				(
					(
						(isset($_POST["checkin_option"]) && $_POST["checkin_option"] != "") || (isset($_POST["post_option"]) && $_POST["post_option"] != "")
					) ||
					(
						!isset($_POST["like_link"]) || $_POST["like_link"] == ""
					)
				)
			) ||
			(
				(!isset($_POST["checkin_option"]) || strlen($_POST["checkin_option"]) != 32 || !ctype_xdigit($_POST["checkin_option"])) &&
				(!isset($_POST["post_option"]) || strlen($_POST["post_option"]) != 32 || !ctype_xdigit($_POST["post_option"])) &&
				(!isset($_POST["like_option"]) || strlen($_POST["like_option"]) != 32 || !ctype_xdigit($_POST["like_option"]))
			)
		) ||
		!isset($_POST["date_signed"]) || $_POST["date_signed"] == "" ||
		!isset($_POST["agrees_to_terms"]) || $_POST["agrees_to_terms"] == "" ||
		!isset($_POST["signatory"]) || $_POST["signatory"] == ""
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		if ($_POST["agrees_to_terms"] != "yes") {
			$message = "You must indicate your acceptance of the terms of use.";
		} else {
			// Begin processing
			require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
			require_once(BASE_PATH . "/../phpinc/session.php");
			require_once(BASE_PATH . "/../phpinc/db.php");
			
			$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);
			
			$validBusiness = false;
			if ($_SESSION["userID"] == $primaryAccountHolder["id"] || ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) == PERMISSION_ADMINISTRATOR) {
				$validBusiness = checkBusinessInAccount($_POST["business"], $_SESSION["accountID"]);
			} else {
				$validBusiness = checkBusinessInManagedBusinesses($_POST["business"], $_SESSION["userID"]);
			}
			
			if (!$validBusiness) {
				$message = "Invalid business.";
			} else {
				if (!getLock(LOCK_NEW_PROMOTION . $_SESSION["accountID"], null)) {
					$message = "The system is experiencing an unusually high volume of traffic, and we are unable to complete your request at this time. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
				} else {
					if (getEligiblePromotionMonths($_SESSION["accountID"]) <= 0) {
						releaseLock(LOCK_NEW_PROMOTION . $_SESSION["accountID"]);
						
						$message = "There are insufficient promotion months available for this account. Please launch at least one additional perk to be eligible to run a new promotion.";
					} else {
						$promotionID = createPromotion($_POST["promotion_name"],
						                               $_POST["promotion_description"],
													   (isset($_POST["promotion_disclaimer"]) && $_POST["promotion_disclaimer"] != "" ? $_POST["promotion_disclaimer"] : null),
													   $_POST["promotion_value"],
													   $_POST["promotion_currency"],
													   $_POST["start_time"],
													   $_POST["duration"],
													   $_POST["eligibility_distance"],
													   ($_POST["eligibility_distance"] != "0" ? $_POST["eligibility_distance_unit"] : null),
(isset($_POST["checkin_option"]) && $_POST["checkin_option"] != "" ? $_POST["checkin_option"] : (isset($_POST["post_option"]) && $_POST["post_option"] != "" ? $_POST["post_option"] : $_POST["like_option"])),
													   $_POST["promotion_max_recipients"],
													   $_POST["signatory"],
													   $_SERVER["REMOTE_ADDR"],
													   $_POST["date_signed"]);
						
						if ($promotionID == null) {
							releaseLock(LOCK_NEW_PROMOTION . $_SESSION["accountID"]);
							
							$message = "Error creating promotion. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
						} else {
							if (!linkPromotionToBusiness($promotionID, $_POST["business"])) {
								rollbackChanges();
								
								releaseLock(LOCK_NEW_PROMOTION . $_SESSION["accountID"]);
								
								$message = "Error assigning promotion to business. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
							} else {
								if ((isset($_POST["checkin_option"]) && $_POST["checkin_option"] != "" && !createPromotionCheckinOption($promotionID, $_POST["checkin_location"], $_POST["checkin_hashtag"])) || (isset($_POST["post_option"]) && $_POST["post_option"] != "" && !createPromotionPostOption($promotionID, $_POST["post_link"])) || (isset($_POST["like_option"]) && $_POST["like_option"] != "" && !createPromotionLikeOption($promotionID, $_POST["like_link"]))) {
									rollbackChanges();
									
									releaseLock(LOCK_NEW_PROMOTION . $_SESSION["accountID"]);
									
									$message = "Error storing promotion fulfillment requirements. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
								} else {
									if ($_POST["eligibility_distance"] != "0") {
										$addressCreationSuccessful = true;
										for ($i = 0; $i < count($_POST["country"]); $i++) {
											if (createPromotionEligibilityAddress($promotionID,
																				  $_POST["thoroughfare"][$i],
																				  $_POST["premise"][$i],
																				  $_POST["locality"][$i],
																				  $_POST["dependent_locality"][$i],
																				  $_POST["administrative_area"][$i],
																				  $_POST["postal_code"][$i],
																				  $_POST["country"][$i],
																				  $_POST["latitude"][$i],
																				  $_POST["longitude"][$i]) == null) {
												$addressCreationSuccessful = false;
												break;
											}
										}
										
										if (!$addressCreationSuccessful) {
											rollbackChanges();
											
											releaseLock(LOCK_NEW_PROMOTION . $_SESSION["accountID"]);
											
											$message = "Error adding eligibility addresses. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
										} else {
											$success = true;
											$message = "Promotion created successfully.";
											
											commitChanges();
											
											releaseLock(LOCK_NEW_PROMOTION . $_SESSION["accountID"]);
										}
									} else {
										$success = true;
										$message = "Promotion created successfully.";
										
										commitChanges();
										
										releaseLock(LOCK_NEW_PROMOTION . $_SESSION["accountID"]);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>