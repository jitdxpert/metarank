<?php

// Some basic input validation
if (!isset($_POST["business"]) || strlen($_POST["business"]) != 32 || !ctype_xdigit($_POST["business"])) {
	exit();
}

require_once(dirname(dirname(__FILE__)) . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");
require_once(BASE_PATH . "/../phpinc/session.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if ((($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR) || !checkBusinessInAccount($_POST["business"], $_SESSION["accountID"])) && (($_SESSION["userPermissions"] & PERMISSION_MANAGER) != PERMISSION_MANAGER || !checkBusinessInManagedBusiness($_POST["business"], $_SESSION["userID"]))) {
	exit();
}

$activePromotions = getActivePromotions($_POST["business"]);
if (count($activePromotions) == 0) {
?>
					<div class="active-promotion">
						<p>There are no active promotions.</p>
					</div><?php
} else {
	foreach ($activePromotions as $activePromotion) {
?>
						<div class="promotion-header"><?php echo $activePromotion["name"]; ?></div>
						<div class="active-promotion">
							<div class="active-promotion-main">
								<div class="by-line">by <a href="<?php echo $activePromotion["business_website"]; ?>" target="_blank"><?php echo $activePromotion["business_name"]; ?></a></div>
								<p><?php echo $activePromotion["description"]; ?></p><?php
		if ($activePromotion["notes"] != "") {
?>
								<p><small>DISCLAIMER: <?php echo $activePromotion["notes"]; ?></small></p><?php
		}
?>
							</div>
							<div class="active-promotion-side">
								<table>
									<tbody>
										<tr>
											<th>Start Time:</th>
											<td id="<?php echo $activePromotion["id"]; ?>_start_time"></td>
										</tr>
										<tr>
											<th>End Time:</th>
											<td id="<?php echo $activePromotion["id"]; ?>_end_time"></td>
										</tr>
										<tr>
											<th>Awarded To:</th>
											<td style="white-space: normal;">Up to <?php echo $activePromotion["max_recipients"]; ?> users who <?php echo ($activePromotion["checkin_location"] != null ? "checkin to <span style='white-space: nowrap;'>'" . $activePromotion["checkin_location"] . "'</span> using hashtag <span style='white-space: nowrap;'>'". $activePromotion["checkin_hashtag"] . "'</span>" : ($activePromotion["post_link"] != null ? "post a link to <span style='white-space: nowrap;'>'" . $activePromotion["post_link"] . "'</span>" : "like the URL <span style='white-space: nowrap;'>'" . $activePromotion["like_link"] . "'</span>")); ?> on <?php echo $activePromotion["service_name"]; ?></td>
										</tr>
										<tr>
											<th>Value:</th>
											<td><?php echo $activePromotion["value"] . " " . $activePromotion["currency_abbreviation"]; ?></td>
										</tr>
										<tr>
											<th>Eligibility:</th>
											<td><?php
		if ($activePromotion["eligibility_distance"] == "0") {
?>
												Global<?php
		} else {
?>
												Within <?php echo $activePromotion["eligibility_distance"] . " " . $activePromotion["eligibility_distance_unit_name"]; ?> of:<?php
			foreach($activePromotion["eligibility_addresses"] as $eligibilityAddress) {
?>
													<p><?php echo render_address($eligibilityAddress + array("country" => $eligibilityAddress["country_id"]), array("first_name", "last_name", "organization_name")); ?></p><?php
			}
		}
?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="promotion-footer">Added on <span id="<?php echo $activePromotion["id"]; ?>_date_signed"></span> by <?php echo $activePromotion["signatory"]; ?></div>
						<script type="text/javascript">
							startDate = new Date('<?php echo $activePromotion["start_time"]; ?>');
							endDate = new Date(startDate);
							endDate.setSeconds(startDate.getSeconds() + <?php echo $activePromotion["duration"]; ?>);
							$("#<?php echo $activePromotion["id"]; ?>_start_time")[0].innerText = startDate.toLocaleString();
							$("#<?php echo $activePromotion["id"]; ?>_end_time")[0].innerText = endDate.toLocaleString();
							$("#<?php echo $activePromotion["id"]; ?>_date_signed")[0].innerText = new Date('<?php echo $activePromotion["date_signed"]; ?>').toLocaleString();
						</script>
						<br /><?php
	}
}
?>