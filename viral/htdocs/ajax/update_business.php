<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["b"]) || strlen($_POST["b"]) != 32 || !ctype_xdigit($_POST["b"]) ||
		!isset($_POST["address_id"]) || !is_array($_POST["address_id"]) || !isset($_POST["address_id"][0]) || strlen($_POST["address_id"][0]) != 32 || !ctype_xdigit($_POST["address_id"][0]) ||
		!isset($_POST["organization_name"]) || !is_array($_POST["organization_name"]) || !isset($_POST["organization_name"][0]) || $_POST["organization_name"][0] == "" ||
		!isset($_POST["thoroughfare"]) || !is_array($_POST["thoroughfare"]) || !isset($_POST["thoroughfare"][0]) || $_POST["thoroughfare"][0] == "" ||
		!isset($_POST["premise"]) || !is_array($_POST["premise"]) || !isset($_POST["premise"][0]) ||
		!isset($_POST["locality"]) || !is_array($_POST["locality"]) || !isset($_POST["locality"][0]) || $_POST["locality"][0] == "" ||
		!isset($_POST["dependent_locality"]) || !is_array($_POST["dependent_locality"]) || !isset($_POST["dependent_locality"][0]) ||
		!isset($_POST["administrative_area"]) || !is_array($_POST["administrative_area"]) || !isset($_POST["administrative_area"][0]) || $_POST["administrative_area"][0] == "" ||
		!isset($_POST["postal_code"]) || !is_array($_POST["postal_code"]) || !isset($_POST["postal_code"][0]) || $_POST["postal_code"][0] == "" ||
		!isset($_POST["country"]) || !is_array($_POST["country"]) || !isset($_POST["country"][0]) || strlen($_POST["country"][0]) != 32 || !ctype_xdigit($_POST["country"][0]) ||
		!isset($_POST["latitude"]) || !is_array($_POST["latitude"]) || !isset($_POST["latitude"][0]) || $_POST["latitude"][0] == "" ||
		!isset($_POST["longitude"]) || !is_array($_POST["longitude"]) || !isset($_POST["longitude"][0]) || $_POST["longitude"][0] == "" ||
		!isset($_POST["business_category"]) || strlen($_POST["business_category"]) != 32 || !ctype_xdigit($_POST["business_category"]) ||
		!isset($_POST["website"]) || $_POST["website"] == ""
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		if (!filter_var($_POST["website"], FILTER_VALIDATE_URL)) {
			$message = "Invalid website url.";
		} else {
			// Begin processing
			require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
			require_once(BASE_PATH . "/../phpinc/session.php");
			require_once(BASE_PATH . "/../phpinc/db.php");
			
			$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);
			
			if ($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR) {
				$message = "Insufficient permissions.";
			} else {
				$addressUpdateSuccess = true;
				for ($i = 0; $i < count($_POST["address_id"]); $i++) {
					if (
						!isset($_POST["address_id"][$i]) || strlen($_POST["address_id"][$i]) != 32 || !ctype_xdigit($_POST["address_id"][$i]) ||
						!isset($_POST["organization_name"][$i]) || $_POST["organization_name"][$i] == "" ||
						!isset($_POST["thoroughfare"][$i]) || $_POST["thoroughfare"][$i] == "" ||
						!isset($_POST["premise"][$i]) ||
						!isset($_POST["locality"][$i]) || $_POST["locality"][$i] == "" ||
						!isset($_POST["dependent_locality"][$i]) ||
						!isset($_POST["administrative_area"][$i]) || $_POST["administrative_area"][$i] == "" ||
						!isset($_POST["postal_code"][$i]) || $_POST["postal_code"][$i] == "" ||
						!isset($_POST["country"][$i]) || strlen($_POST["country"][$i]) != 32 || !ctype_xdigit($_POST["country"][$i]) ||
						!isset($_POST["latitude"][$i]) || $_POST["latitude"][$i] == "" ||
						!isset($_POST["longitude"][$i]) || $_POST["longitude"][$i] == ""
					) {
						$addressUpdateSuccess = false;
						break;
					} else {
						if (!updateBusinessAddress($_POST["address_id"][$i],
						                                          $_POST["thoroughfare"][$i],
						                                          $_POST["premise"][$i],
						                                          $_POST["locality"][$i],
						                                          $_POST["dependent_locality"][$i],
						                                          $_POST["administrative_area"][$i],
						                                          $_POST["postal_code"][$i],
						                                          $_POST["country"][$i],
						                                          $_POST["latitude"][$i],
						                                          $_POST["longitude"][$i])) {
							$addressUpdateSuccess = false;
							break;
						}
					}
				}
				
				if (!$addressUpdateSuccess) {
					rollbackChanges();
				
					$message = "Error updating business address information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
				} else {
					if (!updateBusiness($_POST["b"], $_POST["organization_name"][0], $_POST["website"])) {
						rollbackChanges();
				
						$message = "Error updating business information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
					} else {
						if (!unlinkBusinessCategories($_POST["b"])) {
							rollbackChanges();
							
							$message = "Error updating business category. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
						} else {
							if (!linkCategoryToBusiness($_POST["business_category"], $_POST["b"])) {
								rollbackChanges();
					
								$message = "Error assigning category to business. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
							} else {
								$success = true;
								$message = "Business updated successfully.";
									
								commitChanges();
							}
						}
					}
				}
			}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>