<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (!isset($_POST["email"]) || !isset($_POST["password"])) {
		$message = "Invalid request: Missing fields.";
	} else {
		// Begin processing
		require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
		require_once(BASE_PATH . "/../phpinc/session.php");
		require_once(BASE_PATH . "/../phpinc/db.php");
		
		$userLocked = checkUserIsLocked($_POST["email"]);
		if ($userLocked["locked"]) {
			if ($userLocked["modification_reason"] == "") {
				$message = "This username is locked. If you have used the Forgot Password link, complete the instructions in the email we sent to reset your password and unlock your account. Otherwise, lease contact the primary account holder to unlock the account.";
			} else {
				$message = "This username is locked. Reason: " . $userLocked["modification_reason"];
			}
		} else {
			$requireCaptcha = false;
			$loginIPFailures = getLoginFailureCountByRemoteIP();
			$loginUsernameFailures = getLoginFailureCountByUsername($_POST["email"]);
			
			if ($loginIPFailures > MAX_LOGIN_FAILURES || $loginUsernameFailures > MAX_LOGIN_FAILURES) {
				$requireCaptcha = true;
			}
			
			if ($requireCaptcha) {
				if (!isset($_POST["recaptcha_challenge_field"]) || !isset($_POST["recaptcha_challenge_field"])) {
					$message = "Invalid request: Missing fields.";
					$commands[] = '{"action":"loadCaptcha"}';
				} else {
					// Check captcha first
					require_once(BASE_PATH . "/../phpinc/recaptchalib.php");
					
					$resp = recaptcha_check_answer(RECAPTCHA_PRIVATE_KEY, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

					if (!$resp->is_valid) {
						$message = "The captcha wasn't entered correctly. Go back and try it again.";
						$commands[] = '{"action":"loadCaptcha"}';
						
						logFailedLogin($_POST["email"]);
					} else {
						if (checkUserPassword($_POST["email"], $_POST["password"])) {
							$success = true;
							$message = "Authenticated.";
							$commands[] = '{"action":"login"}';
							
							logSuccessfulLogin($_POST["email"]);
							
							session_regenerate_id(true);
							
							loadSessionUserInformation($_POST["email"]);
							
							updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"));
							
							$_SESSION["isLoggedIn"] = true;
						} else {
							$message = "Authentication failed.";
							$commands[] = '{"action":"loadCaptcha"}';
							
							logFailedLogin($_POST["email"]);
						}
					}
					
					commitChanges();
				}
			} else {
				if (checkUserPassword($_POST["email"], $_POST["password"])) {
					$success = true;
					$message = "Authenticated.";
					
					logSuccessfulLogin($_POST["email"]);
					
					session_regenerate_id(true);
					
					loadSessionUserInformation($_POST["email"]);
					
					updateSession($_SESSION["sessionTrackingID"], session_id(), $_SESSION["userID"], 0 + ini_get("session.gc_maxlifetime"));
							
					$_SESSION["isLoggedIn"] = true;
				} else {
					$message = "Authentication failed.";
					if ($loginIPFailures == MAX_LOGIN_FAILURES || $loginUsernameFailures == MAX_LOGIN_FAILURES) {
						$commands[] = '{"action":"loadCaptcha"}';
					}
					
					logFailedLogin($_POST["email"]);
				}
				
				commitChanges();
			}
		}
	}
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>