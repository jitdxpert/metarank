<?php

// Some basic input validation
if (!isset($_POST["business"]) || strlen($_POST["business"]) != 32 || !ctype_xdigit($_POST["business"])) {
	exit();
}

require_once(dirname(dirname(__FILE__)) . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");
require_once(BASE_PATH . "/../phpinc/session.php");

$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);

if ((($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR) || !checkBusinessInAccount($_POST["business"], $_SESSION["accountID"])) && (($_SESSION["userPermissions"] & PERMISSION_MANAGER) != PERMISSION_MANAGER || !checkBusinessInManagedBusiness($_POST["business"], $_SESSION["userID"]))) {
	exit();
}

$scheduledPromotions = getScheduledPromotions($_POST["business"]);
if (count($scheduledPromotions) == 0) {
?>
					<div class="scheduled-promotion">
						<p>There are no scheduled promotions.</p>
					</div><?php
} else {
	foreach ($scheduledPromotions as $scheduledPromotion) {
?>
						<div class="promotion-header"><?php echo $scheduledPromotion["name"]; ?></div>
						<div class="scheduled-promotion">
							<div class="scheduled-promotion-main">
								<div class="by-line">by <a href="<?php echo $scheduledPromotion["business_website"]; ?>" target="_blank"><?php echo $scheduledPromotion["business_name"]; ?></a></div>
								<p><?php echo $scheduledPromotion["description"]; ?></p><?php
		if ($scheduledPromotion["notes"] != "") {
?>
								<p><small>DISCLAIMER: <?php echo $scheduledPromotion["notes"]; ?></small></p><?php
		}
?>
							</div>
							<div class="scheduled-promotion-side">
								<table>
									<tbody>
										<tr>
											<th>Start Time:</th>
											<td id="<?php echo $scheduledPromotion["id"]; ?>_start_time"></td>
										</tr>
										<tr>
											<th>End Time:</th>
											<td id="<?php echo $scheduledPromotion["id"]; ?>_end_time"></td>
										</tr>
										<tr>
											<th>Awarded To:</th>
											<td style="white-space: normal;">Up to <?php echo $scheduledPromotion["max_recipients"]; ?> users who <?php echo ($scheduledPromotion["checkin_location"] != null ? "checkin to <span style='white-space: nowrap;'>'" . $scheduledPromotion["checkin_location"] . "'</span> using hashtag <span style='white-space: nowrap;'>'". $scheduledPromotion["checkin_hashtag"] . "'</span>" : ($scheduledPromotion["post_link"] != null ? "post a link to <span style='white-space: nowrap;'>'" . $scheduledPromotion["post_link"] . "'</span>" : "like the URL <span style='white-space: nowrap;'>'" . $scheduledPromotion["like_link"] . "'</span>")); ?> on <?php echo $scheduledPromotion["service_name"]; ?></td>
										</tr>
										<tr>
											<th>Value:</th>
											<td><?php echo $scheduledPromotion["value"] . " " . $scheduledPromotion["currency_abbreviation"]; ?></td>
										</tr>
										<tr>
											<th>Eligibility:</th>
											<td><?php
		if ($scheduledPromotion["eligibility_distance"] == "0") {
?>
												Global<?php
		} else {
?>
												Within <?php echo $scheduledPromotion["eligibility_distance"] . " " . $scheduledPromotion["eligibility_distance_unit_name"]; ?> of:<?php
			foreach($scheduledPromotion["eligibility_addresses"] as $eligibilityAddress) {
?>
													<p><?php echo render_address($eligibilityAddress + array("country" => $eligibilityAddress["country_id"]), array("first_name", "last_name", "organization_name")); ?></p><?php
			}
		}
?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="promotion-footer">Added on <span id="<?php echo $scheduledPromotion["id"]; ?>_date_signed"></span> by <?php echo $scheduledPromotion["signatory"]; ?></div>
						<script type="text/javascript">
							startDate = new Date('<?php echo $scheduledPromotion["start_time"]; ?>');
							endDate = new Date(startDate);
							endDate.setSeconds(startDate.getSeconds() + <?php echo $scheduledPromotion["duration"]; ?>);
							$("#<?php echo $scheduledPromotion["id"]; ?>_start_time")[0].innerText = startDate.toLocaleString();
							$("#<?php echo $scheduledPromotion["id"]; ?>_end_time")[0].innerText = endDate.toLocaleString();
							$("#<?php echo $scheduledPromotion["id"]; ?>_date_signed")[0].innerText = new Date('<?php echo $scheduledPromotion["date_signed"]; ?>').toLocaleString();
						</script>
						<br /><?php
	}
}
?>