<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if ((!isset($_POST["uuid"]) || $_POST["uuid"] == "") && ((!isset($_POST["password"]) || $_POST["password"] == "") || (!isset($_POST["password_confirm"]) || $_POST["password_confirm"] == ""))) {
		$message = "Invalid request: Missing fields.";
	} else {
		if (isset($_POST["uuid"]) && (strlen($_POST["uuid"]) != 32 || !ctype_xdigit($_POST["uuid"]))) {
			$message = "Invalid reset code.";
		} else {
			// Begin processing
			if (!isset($_POST["password"])) {
				require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
				require_once(BASE_PATH . "/../phpinc/session.php");
				require_once(BASE_PATH . "/../phpinc/db.php");
				// Validate resetPasswordKey
				
				$email = checkPasswordResetKeyValidity($_POST["uuid"]);
				if ($email == null) {
					$message = "Invalid information provided.";
				} else {
					$_SESSION["resetPasswordCompletion_email"] = $email;
					
					$success = true;
					$commands[] = '{"action":"showPhase2"}';
				}		
			} else {
				require_once(BASE_PATH . "/../phpinc/defines.php");
				require_once(BASE_PATH . "/../phpinc/session.php");
				require_once(BASE_PATH . "/../phpinc/db.php");
				
				if (!isset($_SESSION["resetPasswordCompletion_email"])) {
					$message = "Please provide required information first.";
				} else if ($_POST["password"] != $_POST["password_confirm"]) {
					$message = "Password mismatch.";
				} else {
					if (!unlockUser($_SESSION["resetPasswordCompletion_email"], "Reset Password Completion", null)) {
						rollbackChanges();
						
						$message = "Error unlocking user. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
					} else {
						if (!resetUserPassword($_SESSION["resetPasswordCompletion_email"], $_POST["password"])) {
							rollbackChanges();
							
							$message = "Error resetting user password. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
						} else {
							if (isset($_SESSION["sessionTrackingID"]) && $_SESSION["sessionTrackingID"] != null) {
								untrackSession($_SESSION["sessionTrackingID"]);
							}
							
							commitChanges();

							$_SESSION = array();
							if (ini_get("session.use_cookies")) {
								$params = session_get_cookie_params();
								setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
							}

							session_destroy();
							
							$success = true;
							$message = "Password reset successfully. You may now log in using your new password.";
							$commands[] = '{"action":"removeFields"}';
						}
					}
				}
			}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>