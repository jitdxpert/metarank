<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["business"]) || strlen($_POST["business"]) != 32 || !ctype_xdigit($_POST["business"]) ||
		!isset($_POST["eligibility_distance"]) || $_POST["eligibility_distance"] == "" || $_POST["eligibility_distance"] != (string)(int)$_POST["eligibility_distance"] ||
		(
			$_POST["eligibility_distance"] != "0" &&
			(
				!isset($_POST["eligibility_distance_unit"]) || strlen($_POST["eligibility_distance_unit"]) != 32 || !ctype_xdigit($_POST["eligibility_distance_unit"]) ||
				!isset($_POST["thoroughfare"]) || !is_array($_POST["thoroughfare"]) || !isset($_POST["thoroughfare"][0]) || $_POST["thoroughfare"][0] == "" ||
				!isset($_POST["premise"]) || !is_array($_POST["premise"]) || !isset($_POST["premise"][0]) ||
				!isset($_POST["locality"]) || !is_array($_POST["locality"]) || !isset($_POST["locality"][0]) || $_POST["locality"][0] == "" ||
				!isset($_POST["dependent_locality"]) || !is_array($_POST["dependent_locality"]) || !isset($_POST["dependent_locality"][0]) ||
				!isset($_POST["administrative_area"]) || !is_array($_POST["administrative_area"]) || !isset($_POST["administrative_area"][0]) || $_POST["administrative_area"][0] == "" ||
				!isset($_POST["postal_code"]) || !is_array($_POST["postal_code"]) || !isset($_POST["postal_code"][0]) || $_POST["postal_code"][0] == "" ||
				!isset($_POST["country"]) || !is_array($_POST["country"]) || !isset($_POST["country"][0]) || strlen($_POST["country"][0]) != 32 || !ctype_xdigit($_POST["country"][0]) ||
				!isset($_POST["latitude"]) || !is_array($_POST["latitude"]) || !isset($_POST["latitude"][0]) || $_POST["latitude"][0] == "" ||
				!isset($_POST["longitude"]) || !is_array($_POST["longitude"]) || !isset($_POST["longitude"][0]) || $_POST["longitude"][0] == ""
			)
		) ||
		!isset($_POST["start_time"]) || $_POST["start_time"] == "" ||
		!isset($_POST["duration"]) || $_POST["duration"] == "" ||
		!isset($_POST["perk_name"]) || $_POST["perk_name"] == "" ||
		!isset($_POST["perk_description"]) || $_POST["perk_description"] == "" ||
		!isset($_POST["perk_value"]) || $_POST["perk_value"] == "" || $_POST["perk_value"] != (string)(float)$_POST["perk_value"] ||
		!isset($_POST["perk_currency"]) || strlen($_POST["perk_currency"]) != 32 || !ctype_xdigit($_POST["perk_currency"]) ||
		!isset($_POST["perk_max_recipients"]) || ($_POST["perk_max_recipients"] != "5" && $_POST["perk_max_recipients"] != "10" && $_POST["perk_max_recipients"] != "20") ||
		!isset($_POST["perk_service"]) ||
		!isset($_POST["date_signed"]) || $_POST["date_signed"] == "" ||
		!isset($_POST["agrees_to_terms"]) || $_POST["agrees_to_terms"] == "" ||
		!isset($_POST["signatory"]) || $_POST["signatory"] == ""
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		if ($_POST["agrees_to_terms"] != "yes") {
			$message = "You must indicate your acceptance of the terms of use.";
		} else {
			// Begin processing
			require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
			require_once(BASE_PATH . "/../phpinc/session.php");
			require_once(BASE_PATH . "/../phpinc/db.php");
			
			$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);
			
			$validBusiness = false;
			if ($_SESSION["userID"] == $primaryAccountHolder["id"] || ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) == PERMISSION_ADMINISTRATOR) {
				$validBusiness = checkBusinessInAccount($_POST["business"], $_SESSION["accountID"]);
			} else {
				$validBusiness = checkBusinessInManagedBusinesses($_POST["business"], $_SESSION["userID"]);
			}
			
			if (!$validBusiness) {
				$message = "Invalid business.";
			} else {
				$perkID = createPerk($_POST["perk_name"],
									 $_POST["perk_description"],
									 (isset($_POST["perk_disclaimer"]) && $_POST["perk_disclaimer"] != "" ? $_POST["perk_disclaimer"] : null),
									 $_POST["perk_value"],
									 $_POST["perk_currency"],
									 $_POST["start_time"],
									 $_POST["duration"],
									 $_POST["eligibility_distance"],
									 ($_POST["eligibility_distance"] != "0" ? $_POST["eligibility_distance_unit"] : null),
									 ($_POST["perk_service"] != "" ? $_POST["perk_service"] : null),
									 $_POST["perk_max_recipients"],
									 $_POST["signatory"],
									 $_SERVER["REMOTE_ADDR"],
									 $_POST["date_signed"]);
				
				if ($perkID == null) {
					$message = "Error creating perk. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
				} else {
					if (!linkPerkToBusiness($perkID, $_POST["business"])) {
						rollbackChanges();
						
						$message = "Error assigning perk to business. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
					} else {
						if ($_POST["eligibility_distance"] != "0") {
							$addressCreationSuccessful = true;
							for ($i = 0; $i < count($_POST["country"]); $i++) {
								if (createPerkEligibilityAddress($perkID,
																 $_POST["thoroughfare"][$i],
																 $_POST["premise"][$i],
																 $_POST["locality"][$i],
																 $_POST["dependent_locality"][$i],
																 $_POST["administrative_area"][$i],
																 $_POST["postal_code"][$i],
																 $_POST["country"][$i],
																 $_POST["latitude"][$i],
																 $_POST["longitude"][$i]) == null) {
									$addressCreationSuccessful = false;
									break;
								}
							}
							
							if (!$addressCreationSuccessful) {
								rollbackChanges();
								
								$message = "Error adding eligibility addresses. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
							} else {
								$success = true;
								$message = "Perk created successfully.";
								
								commitChanges();
							}
						} else {
							$success = true;
							$message = "Perk created successfully.";
							
							commitChanges();
						}
					}
				}
			}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>