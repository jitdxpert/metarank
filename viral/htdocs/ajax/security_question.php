<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if ((isset($_POST["email"]) && (!isset($_POST["phone"]) || $_POST["phone"] == "")) || (isset($_POST["phone"]) && (!isset($_POST["email"]) || $_POST["email"] == "")) || (!isset($_POST["phone"]) && !isset($_POST["email"]) && (!isset($_POST["answer"]) || $_POST["answer"] == ""))) {
		$message = "Invalid request: Missing fields.";
	} else {
		if (isset($_POST["email"]) && !filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
			$message = "Invalid email address.";
		} else {
			// Begin processing
			if (!isset($_POST["answer"])) {
				require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
				require_once(BASE_PATH . "/../phpinc/session.php");
				require_once(BASE_PATH . "/../phpinc/db.php");
				
				// Get Question
				$securityQuestion = getSecurityQuestion($_POST["email"], $_POST["phone"]);
				
				if ($securityQuestion == null) {
					$message = "Invalid information provided, security question not set or email address unverified.";
				} else {
					$_SESSION["resetPassword_securityQuestionID"] = $securityQuestion["id"];
					$_SESSION["resetPassword_email"] = $_POST["email"];
					
					$success = true;
					$commands[] = '{"action":"showPhase2","params":["' . $securityQuestion["question"] . '"]}';
				}				
			} else {
				require_once(BASE_PATH . "/../phpinc/defines.php");
				require_once(BASE_PATH . "/../phpinc/session.php");
				require_once(BASE_PATH . "/../phpinc/db.php");
				
				if (!isset($_SESSION["resetPassword_email"]) || !isset($_SESSION["resetPassword_securityQuestionID"])) {
					$message = "Please provide required information first.";
				} else {
					// Check answer
					if (!checkSecurityQuestionAnswer($_SESSION["resetPassword_email"], $_SESSION["resetPassword_securityQuestionID"], $_POST["answer"])) {
						if (isset($_SESSION["sessionTrackingID"]) && $_SESSION["sessionTrackingID"] != null) {
							untrackSession($_SESSION["sessionTrackingID"]);
						}
						
						commitChanges();

						$_SESSION = array();
						if (ini_get("session.use_cookies")) {
							$params = session_get_cookie_params();
							setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
						}

						session_destroy();
						
						$message = "Wrong answer provided. Please try again.";
						$commands[] = '{"action":"showPhase1"}';
					} else {
						// Reset password
						if (!lockUser($_SESSION["resetPassword_email"], "Password Reset", null)) {
							rollbackChanges();
							$message = "Error locking user. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
						} else {
							$passwordResetKey = generateResetKey($_SESSION["resetPassword_email"]);
							if ($passwordResetKey == null) {
								rollbackChanges();
								
								$message = "Error generating password reset code. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
							} else {
								// Send key in email
								$headers =  "From: password_reset@metarank.com\r\n";
								$headers .= "Reply-To: do_not_reply@metarank.com\r\n";
								$headers .= "MIME-Version: 1.0\r\n";
								$headers .= 'Content-Type: multipart/related; boundary="m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y"; type="text/html"\r\n';
								
								$metaRankBottomPNG = base64_encode(fread(fopen(BASE_PATH . "/img/MetaRankBottom.png", "r"), filesize(BASE_PATH . "/img/MetaRankBottom.png")));
								$logoPNG = base64_encode(fread(fopen(BASE_PATH . "/img/logo.png", "r"), filesize(BASE_PATH . "/img/logo.png")));
								
								$emailContent = <<<EOD
--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y
Content-Type: text/html; charset="UTF-8"

<!DOCTYPE html>
<html>
	<head>
		<title>Viral by MetaRank</title>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style type="text/css">
			html, body {
				font-family: Verdana;
				margin: 0;
				padding: 0;
				height: 100%;
			}
			body {
				background: url(cid:MetaRankBottom.png) no-repeat bottom left;
			}
			.meta-logotext {
				color: #00B200;
				font-weight: bold;
			}
			.rank-logotext {
				color: #000000;
				font-weight: bold;
			}

			.black-text {
				color: #000000;
			}

			.orange-text {
				color: #FF7F00;
			}

			.larger-message {
				font-size: 1.1em;
			}

			.smaller-message {
				font-size: .9em;
			}

			.grey-box {
				background-color: rgba(238, 238, 238, .75);
				margin-left: -3em;
				margin-right: -3em;
				padding-left: 3em;
				padding-right: 3em;
			}

			.green-header {
				background-color: #00B200;
				margin-left: -3em;
				margin-right: -3em;
				padding-left: 3em;
				padding-right: 3em;
				color: #FFFFFF;
				text-shadow: none;
				padding: 1px;
				-webkit-border-radius: 10px;
				-moz-border-radius: 10px;
				border-radius: 10px;
			}

			#header {
				position: absolute;
				top: 0;
				left: 0;
				right: 0;
				text-align: center;
				height: 70px;
				z-index: 1;
				background-color: #F6F6F6;
				-webkit-box-shadow: 0px 5px 5px 0px rgba(50, 50, 50, 0.4);
				-moz-box-shadow:    0px 5px 5px 0px rgba(50, 50, 50, 0.4);
				box-shadow:         0px 5px 5px 0px rgba(50, 50, 50, 0.4);
			}
			#header img {
				width: 320px;
			}
			#main {
				bottom: 0;
				background-color: transparent;
			}

			#footer {
				position: absolute;
				left: 0;
				right: 0;
				bottom: 0;
				height: 18px;
				box-sizing: border-box;
				padding-left: 15%;
				padding-right: 15%;
				font-size: .9em;
				background-color: #00B200;
			}
			#footer span:last-child {
				display: inline-block;
				float: right;
			}
			#header, #footer {
				color: #FFFFFF;
				font-weight: normal;
				text-shadow: none;
			}
			
			#main_content {
				position: absolute;
				top: 70px;
				bottom: 17px;
				left: 0;
				right: 0;
				overflow-y: auto;
				padding:2em 0 2em 0;
			}

			.content-wrapper {
				margin: auto;
				max-width: 70%;
				color: #888888;
				text-align: center;
				box-sizing: border-box;
				background-color: rgba(246, 246, 246, .75);
				margin-top: 0;
				margin-bottom: 0;
				padding: 1em 3em 1em 3em;
				min-height: 100%;
			}

			a {
				color: #FFFFFF;
				text-decoration: none;
			}

			a:hover {
				color: #AAAAAA;
				text-decoration: none;
			}

			a:active {
				color: #CCCCCC;
				text-decoration: none;
			}

			a:visited {
				color: #888888;
				text-decoration: none;
			}
			
			#main a {
				text-decoration: none;
				color: #0080C0;
			}
			#main a:hover {
				text-decoration: none;
				color: #0080C0;
			}
			#main a:active {
				text-decoration: none;
				color: #0080C0;
			}
			#main a:visited {
				text-decoration: none;
				color: #0080C0;
			}
		</style>
	</head>
	<body>
		<div id="header">
			<img src="cid:logo.png" alt="MetaRank" />
		</div><!-- /header -->
		<div id="main">
			<div id="main_content" class="ui-content">
				<div class="content-wrapper">
					<p>Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
					<p class="black-text larger-message">Password Reset</p>
					<br />
					<div class="grey-box">
						<br />
						<p>To continue resetting your password, please click or paste into your browser address bar the following link and follow the instructions given there:</p>
						<blockquote><a href="http://viral.metarank.com:81/password/reset?uuid=$passwordResetKey">http://viral.metarank.com:81/password/reset?uuid=$passwordResetKey</a></blockquote>
						<br />
					</div>
					<p>Thank you for using Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>!</p>
				</div>
			</div>
		</div>
		<div id="footer">
			<span>Use confirms acceptance of <a href="https://viral.metarank.com/terms">Terms</a> and <a href="https://viral.metarank.com/privacy">Privacy</a>.</span>
			<span>MetaRank &copy; 2014</span>
		</div><!-- /footer -->
	</body>
</html>

--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y
Content-Location: CID:ignored0000
Content-ID: <MetaRankBottom.png>
Content-Type: IMAGE/PNG
Content-Transfer-Encoding: BASE64

$metaRankBottomPNG

--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y
Content-Location: CID:ignored0001
Content-ID: <logo.png>
Content-Type: IMAGE/PNG
Content-Transfer-Encoding: BASE64

$logoPNG

--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y--
EOD;
								mail($_SESSION["resetPassword_email"], "Viral by metarank.com - Password Reset", $emailContent, $headers);
								
								loadSessionUserInformation($_SESSION["resetPassword_email"]);
								
								exec("cd " . BASE_PATH . "/../processes && php -f logout_user.php " . $_SESSION["userID"]);
								
								if (isset($_SESSION["sessionTrackingID"]) && $_SESSION["sessionTrackingID"] != null) {
									untrackSession($_SESSION["sessionTrackingID"]);
								}
								
								commitChanges();

								$_SESSION = array();
								if (ini_get("session.use_cookies")) {
									$params = session_get_cookie_params();
									setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
								}

								session_destroy();
								
								$success = true;
								$message = "Password reset information has been sent to your email. Please follow the instructions there to continue.";
								$commands[] = '{"action":"removeFields"}';
							}
						}
					}
				}
			}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>
