<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["first_name"]) || $_POST["first_name"] == "" ||
		!isset($_POST["last_name"]) || $_POST["last_name"] == "" ||
		!isset($_POST["phone"]) || $_POST["phone"] == "" ||
		!isset($_POST["email"]) || $_POST["email"] == "" ||
		!isset($_POST["password"]) || $_POST["password"] == "" ||
		!isset($_POST["password_confirm"]) || $_POST["password_confirm"] == "" ||
		!isset($_POST["role"]) || strlen($_POST["role"]) != 32 || !ctype_xdigit($_POST["role"]) ||
		!isset($_POST["businesses"]) || !is_array($_POST["businesses"]) || count($_POST["businesses"]) == 0
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
			$message = "Invalid email address.";
		} else if ($_POST["password"] != $_POST["password_confirm"]) {
			$message = "Password mismatch.";
		} else {
			$validBusinessIDs = true;
			foreach($_POST["businesses"] as $businessID) {
				if (!ctype_xdigit($businessID) || strlen($businessID) != 32) {
					$validBusinessIDs = false;
					break;
				}
			}
			
			if (!$validBusinessIDs) {
				$message = "Invalid business(es).";
			} else {
				// Begin processing
				require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
				require_once(BASE_PATH . "/../phpinc/session.php");
				require_once(BASE_PATH . "/../phpinc/db.php");
				
				$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);
				
				if ($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR && ($_SESSION["userPermissions"] & PERMISSION_MANAGER) != PERMISSION_MANAGER) {
					$message = "Insufficient permissions.";
				} else {
					$managedBusinessIDs = true;
					if (($_SESSION["userPermissions"] & PERMISSION_MANAGER) == PERMISSION_MANAGER) {
						foreach($_POST["businesses"] as $businessID) {
							if (!checkBusinessInManagedBusinesses($businessID, $_SESSION["userID"])) {
								$managedBusinessIDs = false;
								break;
							}
						}
					}
					
					if (!$managedBusinessIDs) {
						$message = "Insufficient permissions.";
					} else {
						$userID = createUser($_POST["first_name"], $_POST["last_name"], $_POST["phone"], $_POST["email"], $_POST["password"], null, null);
						if ($userID == null) {
							$message = "Email address already in use.";
						} else {
							if (!giveUserRole($userID, $_POST["role"])) {
								rollbackChanges();
								
								$message = "Error assigning role to new user. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
							} else {
								if (!linkAccountToUser($_SESSION["accountID"], $userID)) {
									rollbackChanges();
				
									$message = "Error storing new user information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
								} else {
									$linkSuccessful = true;
									foreach ($_POST["businesses"] as $businessID) {
										if (!linkUserToBusiness($userID, $businessID)) {
											$linkSuccessful = false;
											break;
										}
									}
									if (!$linkSuccessful) {
										rollbackChanges();
										
										$message = "Error assigning new user to business(es). Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
									} else {
										$success = true;
										$message = "User added successfully.";
										
										commitChanges();
										
										// Send userID in email
										$headers =  "From: email_verification@metarank.com\r\n";
										$headers .= "Reply-To: do_not_reply@metarank.com\r\n";
										$headers .= "MIME-Version: 1.0\r\n";
										$headers .= 'Content-Type: multipart/related; boundary="m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y"; type="text/html"\r\n';
										
										$metaRankBottomPNG = base64_encode(fread(fopen(BASE_PATH . "/img/MetaRankBottom.png", "r"), filesize(BASE_PATH . "/img/MetaRankBottom.png")));
										$logoPNG = base64_encode(fread(fopen(BASE_PATH . "/img/logo.png", "r"), filesize(BASE_PATH . "/img/logo.png")));
										
										$emailContent = <<<EOD
--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y
Content-Type: text/html; charset="UTF-8"

<!DOCTYPE html>
<html>
	<head>
		<title>Viral by MetaRank</title>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style type="text/css">
			html, body {
				font-family: Verdana;
				margin: 0;
				padding: 0;
				height: 100%;
			}
			body {
				background: url(cid:MetaRankBottom.png) no-repeat bottom left;
			}
			.meta-logotext {
				color: #00B200;
				font-weight: bold;
			}
			.rank-logotext {
				color: #000000;
				font-weight: bold;
			}

			.black-text {
				color: #000000;
			}

			.orange-text {
				color: #FF7F00;
			}

			.larger-message {
				font-size: 1.1em;
			}

			.smaller-message {
				font-size: .9em;
			}

			.grey-box {
				background-color: rgba(238, 238, 238, .75);
				margin-left: -3em;
				margin-right: -3em;
				padding-left: 3em;
				padding-right: 3em;
			}

			.green-header {
				background-color: #00B200;
				margin-left: -3em;
				margin-right: -3em;
				padding-left: 3em;
				padding-right: 3em;
				color: #FFFFFF;
				text-shadow: none;
				padding: 1px;
				-webkit-border-radius: 10px;
				-moz-border-radius: 10px;
				border-radius: 10px;
			}

			#header {
				position: absolute;
				top: 0;
				left: 0;
				right: 0;
				text-align: center;
				height: 70px;
				z-index: 1;
				background-color: #F6F6F6;
				-webkit-box-shadow: 0px 5px 5px 0px rgba(50, 50, 50, 0.4);
				-moz-box-shadow:    0px 5px 5px 0px rgba(50, 50, 50, 0.4);
				box-shadow:         0px 5px 5px 0px rgba(50, 50, 50, 0.4);
			}
			#header img {
				width: 320px;
			}
			#main {
				bottom: 0;
				background-color: transparent;
			}

			#footer {
				position: absolute;
				left: 0;
				right: 0;
				bottom: 0;
				height: 18px;
				box-sizing: border-box;
				padding-left: 15%;
				padding-right: 15%;
				font-size: .9em;
				background-color: #00B200;
			}
			#footer span:last-child {
				display: inline-block;
				float: right;
			}
			#header, #footer {
				color: #FFFFFF;
				font-weight: normal;
				text-shadow: none;
			}
			
			#main_content {
				position: absolute;
				top: 70px;
				bottom: 17px;
				left: 0;
				right: 0;
				overflow-y: auto;
				padding:2em 0 2em 0;
			}

			.content-wrapper {
				margin: auto;
				max-width: 70%;
				color: #888888;
				text-align: center;
				box-sizing: border-box;
				background-color: rgba(246, 246, 246, .75);
				margin-top: 0;
				margin-bottom: 0;
				padding: 1em 3em 1em 3em;
				min-height: 100%;
			}

			a {
				color: #FFFFFF;
				text-decoration: none;
			}

			a:hover {
				color: #AAAAAA;
				text-decoration: none;
			}

			a:active {
				color: #CCCCCC;
				text-decoration: none;
			}

			a:visited {
				color: #888888;
				text-decoration: none;
			}
			
			#main a {
				text-decoration: none;
				color: #0080C0;
			}
			#main a:hover {
				text-decoration: none;
				color: #0080C0;
			}
			#main a:active {
				text-decoration: none;
				color: #0080C0;
			}
			#main a:visited {
				text-decoration: none;
				color: #0080C0;
			}
		</style>
	</head>
	<body>
		<div id="header">
			<img src="cid:logo.png" alt="MetaRank" />
		</div><!-- /header -->
		<div id="main">
			<div id="main_content" class="ui-content">
				<div class="content-wrapper">
					<p>Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
					<p class="black-text larger-message">Verify Email</p>
					<br />
					<div class="grey-box">
						<br />
						<p>Please verify your email by clicking or pasting into your browser address bar the following link:</p>
						<blockquote><a href="http://viral.metarank.com/verify/?uuid=$userID">http://viral.metarank.com/verify/?uuid=$userID</a></blockquote>
						<br />
					</div>
					<p>Thank you for using Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>!</p>
				</div>
			</div>
		</div>
		<div id="footer">
			<span>Use confirms acceptance of <a href="http://viral.metarank.com/terms/">Terms</a> and <a href="http://viral.metarank.com/privacy/">Privacy</a>.</span>
			<span>MetaRank &copy; 2014</span>
		</div><!-- /footer -->
	</body>
</html>

--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y
Content-Location: CID:ignored0000
Content-ID: <MetaRankBottom.png>
Content-Type: IMAGE/PNG
Content-Transfer-Encoding: BASE64

$metaRankBottomPNG

--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y
Content-Location: CID:ignored0001
Content-ID: <logo.png>
Content-Type: IMAGE/PNG
Content-Transfer-Encoding: BASE64

$logoPNG

--m-e-t-a-r-a-n-k-e-m-a-i-l-b-o-u-n-d-a-r-y--
EOD;
										mail($_POST["email"], "Viral by metarank.com - Verify Email", $emailContent, $headers);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>
