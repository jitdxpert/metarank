<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["password"]) || $_POST["password"] == "" ||
		(
			(
				!isset($_POST["new_password"]) || $_POST["new_password"] == "" || !isset($_POST["new_password_confirm"]) || $_POST["new_password_confirm"] == ""
			) && (
				!isset($_POST["security_question"]) || strlen($_POST["security_question"]) != 32 || !ctype_xdigit($_POST["security_question"]) || !isset($_POST["security_answer"]) || $_POST["security_answer"] == ""
			)
		)
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		if (isset($_POST["new_password"]) && $_POST["new_password"] != "" && isset($_POST["new_password_confirm"]) && $_POST["new_password_confirm"] != "" && $_POST["new_password"] != $_POST["new_password_confirm"]) {
			$message = "New password mismatch.";
		} else {
			// Begin processing
			require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
			require_once(BASE_PATH . "/../phpinc/session.php");
			require_once(BASE_PATH . "/../phpinc/db.php");
			
			$requireCaptcha = false;
			$elevationFailures = getElevationFailureCount($_SESSION["userID"]);
			
			if ($elevationFailures > MAX_ELEVATION_FAILURES) {
				$requireCaptcha = true;
			}
			
			/*if ($requireCaptcha) {
				if (!isset($_POST["recaptcha_challenge_field"]) || !isset($_POST["recaptcha_challenge_field"])) {
					$message = "Invalid request: Missing fields.";
					$commands[] = '{"action":"loadCaptcha"}';
				} else {
					// Check captcha first
					require_once(BASE_PATH . "/../phpinc/recaptchalib.php");
					
					$resp = recaptcha_check_answer (RECAPTCHA_PRIVATE_KEY, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

					if (!$resp->is_valid) {
						$message = "The captcha wasn't entered correctly. Go back and try it again.";
						$commands[] = '{"action":"loadCaptcha"}';
						
						logFailedElevation($_SESSION["userID"]);
					} else {
						if (!checkUserPassword($_SESSION["userEmail"], $_POST["password"])) {
							$message = "Invalid password.";
							$commands[] = '{"action":"loadCaptcha"}';
							
							logFailedElevation($_SESSION["userID"]);
						} else {
							logSuccessfulElevation($_SESSION["userID"]);
							$commands[] = '{"action":"removeCaptcha"}';
							
							$passwordChangeError = false;
							if (isset($_POST["new_password"]) && $_POST["new_password"] != "" && isset($_POST["new_password_confirm"]) && $_POST["new_password_confirm"] != "") {
								if (!changeUserPassword($_SESSION["userID"], $_POST["new_password"])) {
									$passwordChangeError = true;
									
									$message = "Error storing new password. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
								} else {
									$success = true;
									
									$message = "Password updated successfully.";
								}
							}
							
							if (!$passwordChangeError && isset($_POST["security_question"]) && strlen($_POST["security_question"]) == 32 && ctype_xdigit($_POST["security_question"]) && isset($_POST["security_answer"]) && $_POST["security_answer"] != "") {
								if (!changeUserSecurityQuestion($_SESSION["userID"], $_POST["security_question"], $_POST["security_answer"])) {
									$message .= ($success ? "\\n" : "") . "Error storing new security question/answer information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
									
									$success = false;
								} else {
									$message .= ($success ? "\\n" : "") . "Security Question/Answer updated successfully.";
									
									$success = true;
								}
							}
						}
					}
					
					commitChanges();
				}
			} else {*/
				if (!checkUserPassword($_SESSION["userEmail"], $_POST["password"])) {
					$message = "Invalid password.";
					
					if ($elevationFailures == MAX_ELEVATION_FAILURES) {
						$commands[] = '{"action":"loadCaptcha"}';
					}
					
					logFailedElevation($_SESSION["userID"]);
				} else {
					logSuccessfulElevation($_SESSION["userID"]);
					
					$passwordChangeError = false;
					if (isset($_POST["new_password"]) && $_POST["new_password"] != "" && isset($_POST["new_password_confirm"]) && $_POST["new_password_confirm"] != "") {
						if (!changeUserPassword($_SESSION["userID"], $_POST["new_password"])) {
							$passwordChangeError = true;
							
							$message = "Error storing new password. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
						} else {
							$success = true;
							
							$message = "Password updated successfully.";
						}
					}
					
					if (!$passwordChangeError && isset($_POST["security_question"]) && strlen($_POST["security_question"]) == 32 && ctype_xdigit($_POST["security_question"]) && isset($_POST["security_answer"]) && $_POST["security_answer"] != "") {
						if (!changeUserSecurityQuestion($_SESSION["userID"], $_POST["security_question"], $_POST["security_answer"])) {
							$message .= ($success ? "\\n" : "") . "Error storing new security question/answer information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
							
							$success = false;
						} else {
							$message .= ($success ? "\\n" : "") . "Security Question/Answer updated successfully.";
							
							$success = true;
						}
					}
				}
				
				commitChanges();
			//}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>