<?php
	$success = false;
	$message = "";
	$commands = array();

	// First check for required fields
	if (
		!isset($_POST["organization_name"]) || !isset($_POST["organization_name"][0]) || $_POST["organization_name"][0] == "" ||
		!isset($_POST["thoroughfare"]) || !isset($_POST["thoroughfare"][0]) || $_POST["thoroughfare"][0] == "" ||
		!isset($_POST["premise"]) || !isset($_POST["premise"][0]) ||
		!isset($_POST["locality"]) || !isset($_POST["locality"][0]) || $_POST["locality"][0] == "" ||
		!isset($_POST["dependent_locality"]) || !isset($_POST["dependent_locality"][0]) ||
		!isset($_POST["administrative_area"]) || !isset($_POST["administrative_area"][0]) || $_POST["administrative_area"][0] == "" ||
		!isset($_POST["postal_code"]) || !isset($_POST["postal_code"][0]) || $_POST["postal_code"][0] == "" ||
		!isset($_POST["country"]) || !isset($_POST["country"][0]) || strlen($_POST["country"][0]) != 32 || !ctype_xdigit($_POST["country"][0]) ||
		!isset($_POST["latitude"]) || !is_array($_POST["latitude"]) || !isset($_POST["latitude"][0]) || $_POST["latitude"][0] == "" ||
		!isset($_POST["longitude"]) || !is_array($_POST["longitude"]) || !isset($_POST["longitude"][0]) || $_POST["longitude"][0] == "" ||
		!isset($_POST["business_category"]) || strlen($_POST["business_category"]) != 32 || !ctype_xdigit($_POST["business_category"]) ||
		!isset($_POST["website"]) || $_POST["website"] == ""
	) {
		$message = "Invalid request: Missing fields.";
	} else {
		if (!filter_var($_POST["website"], FILTER_VALIDATE_URL)) {
			$message = "Invalid website url.";
		} else {
			// Begin processing
			require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
			require_once(BASE_PATH . "/../phpinc/session.php");
			require_once(BASE_PATH . "/../phpinc/db.php");
			
			$primaryAccountHolder= getAccountPrimaryUser($_SESSION["accountID"]);
			
			if ($_SESSION["userID"] != $primaryAccountHolder["id"] && ($_SESSION["userPermissions"] & PERMISSION_ADMINISTRATOR) != PERMISSION_ADMINISTRATOR) {
				$message = "Insufficient permissions.";
			} else {
				$businessAddressID = createBusinessAddress($_POST["thoroughfare"][0],
				                                                                     $_POST["premise"][0],
				                                                                     $_POST["locality"][0],
				   	                                                                 $_POST["dependent_locality"][0],
				   	                                                                 $_POST["administrative_area"][0],
				   	                                                                 $_POST["postal_code"][0],
				                                                                     $_POST["country"][0],
				                                                                     $_POST["latitude"][0],
				                                                                     $_POST["longitude"][0]);
				if ($businessAddressID == null) {
					rollbackChanges();
				
					$message = "Error storing new business address information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
				} else {
					$businessID = createBusiness($_POST["organization_name"][0], $_POST["website"], $businessAddressID);
					if ($businessID == null) {
						rollbackChanges();
				
						$message = "Error storing new business information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
					} else {
						if (!linkBusinessToAddress($businessID, $businessAddressID)) {
							rollbackChanges();
				
							$message = "Error linking new business address information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
						} else {
							if (!linkCategoryToBusiness($_POST["business_category"], $businessID)) {
								rollbackChanges();
					
								$message = "Error assigning category to new business. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
							} else {
								if (!linkAccountToBusiness($_SESSION["accountID"], $businessID)) {
									rollbackChanges();
									
									$message = "Error storing new business account information. Please try again later. If the problem still occurs, please contact technical support and provide them with this error message.";
								} else {
									$success = true;
									$message = "New business added successfully.";
									
									commitChanges();
								}
							}
						}
					}
				}
			}
		}
	}
	
	header('Content-Type: application/json');
	
	echo '{"success":' . ($success ? "true" : "false") . ',"message":"' . $message . '","commands":[' . implode(",", $commands) . ']}';
?>