<?php
require_once(dirname(dirname(__FILE__)) . "/../phpinc/defines.php");
require_once(BASE_PATH . "/../phpinc/session.php");
require_once(BASE_PATH . "/../phpinc/db.php");

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>

<section data-role="page" id="password_reset_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <p class="black-text larger-message">Password Reset</p>
            <br class="hide-on-mobile" />
            <br />
            <div id="status_message" style="display:none;"></div>
            <form action="<?php echo BASE_URL; ?>/ajax/security_question.php" method="POST">
                <div id="password_reset_phase1">
                    <label for="email">Email:</label>
                    <input type="email" id="email" name="email" required />
                    <label for="phone">Phone:</label>
                    <input type="tel" id="phone" name="phone" required />
                </div>
                <div id="password_reset_phase2" style="display:none;">
                    <p></p>
                    <input type="text" id="answer" name="answer" required disabled />
                </div>
                <div id="navigation_box" class="grey-box">
                    <p>
                        <button type="submit">Submit</button>
                        <button type="button" onclick="window.history.back()">Back</button>
                    </p>
                </div>
            </form>
            <script type="text/javascript">
			$("#password_reset_page form").on("submit", resetPassword);
			$(document).ready(function(evt) {
				$(document.body).on("pagecontainerchange", function(event, ui) {
					if (ui.prevPage.attr("id").toUpperCase() == "PASSWORD_RESET_PAGE") {
						$("#status_message").hide();
						$("#password_reset_phase2 p").text("");
						$("#password_reset_phase2 input").textinput("disable");
						$("#password_reset_phase1 input").textinput("enable");
						$("#password_reset_phase1 input").val("");
						$("#navigation_box input[type=submit]").parent().show();
						$("#password_reset_phase2").stop(true, true).fadeOut({queue: false}).slideUp();
						$("#password_reset_phase1").stop(true, true).fadeIn({queue: false}).css('display', 'none').slideDown();
					}
				});
			});
            </script>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>