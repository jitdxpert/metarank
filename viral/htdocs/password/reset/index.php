<?php
// Try to hide from interlopers...
if (!isset($_GET["uuid"]) || strlen($_GET["uuid"]) != 32 || !ctype_xdigit($_GET["uuid"])) {
	header("HTTP/1.1 404 Not Found");
	exit();
}

require_once(dirname(dirname(dirname(__FILE__))) . "/../phpinc/defines.php");
require_once(BASE_PATH . "/../phpinc/session.php");

require_once(BASE_PATH . "/../phpinc/header.php");
?>

<section data-role="page" id="password_reset_completion_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p>Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br />
            <p class="black-text larger-message">Password Reset Completion</p>
            <br />
            <br />
            <div id="status_message" style="display:none;"></div>
            <form action="<?php echo BASE_URL; ?>/ajax/reset_password.php" method="POST">
                <div id="password_reset_completion_phase1">
                    <label for="uuid">Password Reset Code:</label>
                    <input type="text" id="uuid" name="uuid" value="<?php echo $_GET["uuid"]; ?>" required />
                </div>
                <div id="password_reset_completion_phase2" style="display:none;">
                    <label for="password">New Password:</label>
                    <input type="password" id="password" name="password" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!#$%&()*+,-./:;<=>?@\\\^_{|}~]).{14,}" onchange="
                        this.setCustomValidity(this.validity.patternMismatch ? 'Password must be at least 14 characters long and contain at least one digit, one uppercase letter, one lowercase letter, and one of the following special characters: !#$%&()*+,-./:;<=>?@\\\^_{|}~' : '');
                        if(this.checkValidity()) document.getElementById('password_confirm').pattern = this.value.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
                    " disabled />
                    <label for="password_confirm">Confirm New Password:</label>
                    <input type="password" id="password_confirm" name="password_confirm" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!#$%&()*+,-./:;<=>?@\[\\\]\^_`{|}~]).{14,}" onchange="
                        this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above.' : '');
                    " disabled />
                </div>
                <div id="navigation_box" class="grey-box">
                    <p><button type="submit">Submit</button> <button type="button" onclick="window.history.back()">Back</button></p>
                    <?php /*?>$(document.body).pagecontainer('change', '<?php echo BASE_URL; ?>/login/');<?php */?>
                </div>
            </form>
            <script type="text/javascript">
            $("#password_reset_completion_page form").on("submit", resetPasswordCompletion);
            </script>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php include(BASE_PATH . "/../phpinc/footer.php"); ?>