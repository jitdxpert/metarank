<?php
require_once(dirname(dirname(__FILE__)) . "/../phpinc/db.php");
require_once(BASE_PATH . "/../phpinc/addressfield.php");

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/header.php");
}
?>

<section data-role="page" id="register_page">
    <div role="main" class="ui-content">
        <div class="content-wrapper">
            <p class="hide-on-mobile">Welcome to Viral by <span class="meta-logotext">meta</span><span class="rank-logotext">rank</span>.</p>
            <br class="hide-on-mobile" />
            <div id="step_header" class="green-box">
                <p>Account Registration</p>
            </div>
            <div id="notice" class="grey-box">
                <p class="black-text larger-message">Already have an account? &nbsp; <a href="<?php echo BASE_URL; ?>/login/"><button>Login</button></a></p>
            </div>
            <br />
            <p class="orange-text larger-message">Primary Business Information</p>
            <form action="<?php echo BASE_URL; ?>/ajax/register.php" method="POST">
                <input type="hidden" name="removeFields" value="first_name,last_name">
                <div id="fields">
                    <?php echo render_address_form(array(), array("first_name", "last_name"), "0000"); ?>
                    <label for="business_category">Business Category:</label>
                    <select id="business_category" name="business_category">
                        <?php
                        $businessCategories = getBusinessCategories(null);
                        foreach ($businessCategories as $row) { ?>
                            <option value="<?php echo $row["id"]; ?>"><?php echo $row["name"]; ?></option>
                        <?php
                        } ?>
                    </select>
                    <label for="website">Website:</label>
                    <input type="text" id="website" name="website" required="required" />
                    <br />
                    <div style="text-align: center;">
                        <p>Have more businesses? Don't worry, you'll be able to add them to your account later.</p>
                        <p class="orange-text larger-message">Primary User Information</p>
                    </div>
                    <label for="first_name">First Name:</label>
                    <input type="text" id="first_name" name="first_name" required="required" />
                    <label for="last_name">Last Name:</label>
                    <input type="text" id="last_name" name="last_name" required="required" />
                    <label for="phone">Phone:</label>
                    <input type="tel" id="phone" name="phone" required="required" />
                    <label for="email">Email:</label>
                    <input type="email" id="email" name="email" required="required" />
                    <label for="password">Password:</label>
                    <input type="password" id="password" name="password" required="required" pattern="<?php echo PASSWORD_HTML5_REGULAR_EXPRESSION; ?>" onchange="
                        this.setCustomValidity(this.validity.patternMismatch ? 'Password must be at least 14 characters long and contain at least one digit, one uppercase letter, one lowercase letter, and one of the following special characters: !#$%&()*+,-./:;<=>?@\\\^_{|}~' : '');
                        if(this.checkValidity()) document.getElementById('password_confirm').pattern = this.value.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
                    "/>
                    <label for="password_confirm">Confirm Password:</label>
                    <input type="password" id="password_confirm" name="password_confirm" required="required" pattern="<?php echo PASSWORD_HTML5_REGULAR_EXPRESSION; ?>" onchange="
                        this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above.' : '');
                    " />
                    <label for="security_question">Security Question:</label>
                    <select id="security_question" name="security_question">
                        <?php
                        $securityQuestions = getSecurityQuestions();
                        foreach ($securityQuestions as $row) { ?>
                            <option value="<?php echo $row["id"]; ?>"><?php echo $row["question"]; ?></option>
                        <?php
                        } ?>
                    </select>
                    <label for="security_answer">Answer:</label>
                    <input type="text" id="security_answer" name="security_answer" required="required" />
                    <br />
                    <br />
                    <label for="comments">What else would you like to tell us about your business(es)?</label>
                    <textarea id="comments" name="comments"></textarea>
                </div>
                <div id="status_message" style="display:none;"></div>
                <div id="navigation_box" class="grey-box">
                    <p>
                    	<button type="submit">Register</button>
                    	<button type="button" onclick="window.history.back()">Back</button>
                    </p>
                </div>
            </form>
            <script type="text/javascript">
			$("#register_page form").on("change", "fieldset[data-addressfield-number] select.country", updateAddressCountry);
			$("#register_page form").on("submit", register);
            </script>
            <p class="smaller-message">
                Want to talk with a specialist?
                <br>
                Call 818.588.6836
            </p>
        </div>
    </div><!-- /content -->
</section><!-- /page -->

<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	require_once(BASE_PATH . "/../phpinc/footer.php");
}
?>